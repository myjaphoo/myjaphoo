if (!System.properties['os.name'].toLowerCase().contains('windows')) {
    def buildNumber = properties.buildNumber;
    def fullVersionNo = "${properties.mjversion}-$buildNumber"

    def ant = new AntBuilder()
    ant.copy( todir:"${basedir}/target/debian" ) {
        fileset( dir:"${basedir}/src/deb" )           {
        }
    }
    ant.replace(file: "${basedir}/target/debian/DEBIAN/control", token: "@VERSION@", value: "${properties.mjversion}")

    ant.copy( tofile:"${basedir}/target/debian/usr/share/myjaphoo/4/myjaphoo.jar" ) {
        fileset( file:"${basedir}/target/myjaphoo-${properties.mjversion}-jar-with-dependencies.jar" )
    }

    println("chmod 755 ${basedir}/target/debian/usr/bin/myjaphoo".execute().text)
    println("dpkg -b ${basedir}/target/debian ${basedir}/target/myjaphoo.deb".execute().text)

    ant.copy( tofile:"${basedir}/target/installation/${fullVersionNo}/myjaphoo-${fullVersionNo}.deb" ) {
        fileset( file:"${basedir}/target/myjaphoo.deb" )
    }
}