package org.mlsoft.eventbus;

/**

 * @author lang
 */
public interface EventBus {

    void register(Object subscriber);

    void register(Object subscriber, SubscriberFilter subscriberFiler);

    void unregister(Object subscriber);

    void post(Object event);
}
