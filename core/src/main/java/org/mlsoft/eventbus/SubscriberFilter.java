package org.mlsoft.eventbus;

/**
 * A filter that a subscriber could use to filter the events to be delivered.
 * This is useful if a subscriber wants to react only on events based on a "context", e.g. only the events
 * of a certain controller instance.
 * @author mla
 * @version $Id$
 */
public interface SubscriberFilter {

    /**
     * should the event be accepted and delivered to the subscriber?
     * @param event
     * @return
     */
    boolean accept(Object event);
}
