/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mlsoft.structures;

import java.util.List;

/**
 * Basisinterface für Tree-Strukturen.
 *
 * @author mla
 */
public interface TreeStructure<T extends TreeStructure<T>> extends TreeParentRelation<T> {

    String getName();

    T getParent();

    List<T> getChildren();
}
