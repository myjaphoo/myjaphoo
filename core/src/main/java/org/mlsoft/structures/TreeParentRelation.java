/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mlsoft.structures;


/**
 * Basisinterface für Tree-Strukturen.
 * @author lang
 */
public interface TreeParentRelation<T extends TreeParentRelation<T>> {

    String getName();

    T getParent();
}
