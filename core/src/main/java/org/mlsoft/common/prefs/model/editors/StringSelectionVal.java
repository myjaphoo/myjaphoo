package org.mlsoft.common.prefs.model.editors;

import org.mlsoft.common.prefs.model.AbstractMetadata;
import org.mlsoft.common.prefs.model.edit.EditableStringSelectionVal;

import java.util.function.Supplier;

/**
 * <p>�berschrift: </p>
 * <p>Beschreibung: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Organisation: </p>
 *
 * @author unbekannt
 * @version 1.0
 */

public class StringSelectionVal extends StringVal implements EditableStringSelectionVal {

    private Supplier<String[]> auswahl;

    public StringSelectionVal(
        AbstractMetadata parent, String name, String guiName, String description,
        String defaultVal,
        Supplier<String[]> auswahl
    ) {
        super(parent, name, guiName, description, defaultVal);
        this.auswahl = auswahl;
    }

    public String[] getAuswahl() {
        return auswahl.get();
    }


    public int getIndex() {
        String[] vals = auswahl.get();
        for (int i = 0; i < vals.length; i++) {
            if (vals[i].equals(super.getVal()))
                return i;
        }
        return -1;
    }

    public void setIndex(int index) {
        super.setVal(auswahl.get()[index]);
    }

}
