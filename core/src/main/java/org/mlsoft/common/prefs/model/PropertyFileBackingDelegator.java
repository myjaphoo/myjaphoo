/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mlsoft.common.prefs.model;

import org.mlsoft.common.prefs.model.editors.AbstractPrefVal;
import org.mlsoft.common.prefs.model.editors.BackingDelegator;
import org.mlsoft.common.prefs.model.editors.BooleanVal;
import org.mlsoft.common.prefs.model.editors.ColorVal;
import org.mlsoft.common.prefs.model.editors.IntegerVal;
import org.mlsoft.common.prefs.model.editors.StringVal;

import java.awt.*;

/**
 * @author mla
 */
public class PropertyFileBackingDelegator extends BackingDelegator {

    PropertyFileConfiguration root;
    AbstractPrefVal abstractPrefVal;

    public PropertyFileBackingDelegator(
        PropertyFileConfiguration root, AbstractPrefVal abstractPrefVal, Object defaultVal
    ) {
        super(defaultVal);
        this.root = root;
        this.abstractPrefVal = abstractPrefVal;
    }

    public void setObjVal(Object newVal) {
        super.setObjVal(newVal);
        setPrefsObjVal(newVal);
    }


    public Object getObjVal() {
        String val = root.getProperties().getProperty(abstractPrefVal.getName());
        if (val == null) {
            return null;
        }
        if (abstractPrefVal instanceof BooleanVal) {
            return Boolean.valueOf(val);
        } else if (abstractPrefVal instanceof StringVal) {
            return val;
        } else if (abstractPrefVal instanceof IntegerVal) {
            return Integer.parseInt(val);
        } else if (abstractPrefVal instanceof ColorVal) {
            String sval = val;
            if (sval != null) {
                return Color.decode(sval);
            } else {
                return null;
            }
        } else {
            throw new RuntimeException("nicht unterstützter Typ!");
        }
    }

    public void commit() {
        super.commit();
    }

    public void rollback() {
        Object rollbackVal = getRollbackVal();
        if (rollbackVal != null) {
            setPrefsObjVal(rollbackVal);
        }
        super.rollback();

    }

    private void setPrefsObjVal(Object newVal) {
        if (newVal == null) {
            root.getProperties().setProperty(abstractPrefVal.getName(), null);
        } else if (newVal instanceof Boolean) {
            root.getProperties().setProperty(abstractPrefVal.getName(), newVal.toString());
        } else if (newVal instanceof String) {
            root.getProperties().setProperty(abstractPrefVal.getName(), (String) newVal);
        } else if (newVal instanceof Integer) {
            root.getProperties().setProperty(abstractPrefVal.getName(), newVal.toString());
        } else if (newVal instanceof Color) {
            root.getProperties().setProperty(abstractPrefVal.getName(), toString((Color) newVal));
        } else {
            throw new RuntimeException(
                "nicht unterstützter Typ bei PrefsBackingDelegator:" + newVal.getClass().getName());
        }
        root.save();
    }

    private String toString(Color newVal) {
        return "#" + Integer.toHexString(newVal.getRed()) + Integer.toHexString(newVal.getGreen())
            + Integer.toHexString(newVal.getBlue());
    }
}