/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.mlsoft.common.prefs.model;

import org.mlsoft.common.prefs.model.editors.AbstractPrefVal;
import org.mlsoft.common.prefs.model.editors.BackingDelegator;
import org.mlsoft.common.prefs.model.editors.EditorRoot;
import org.myjaphoo.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeSet;

/**
 * @author mla
 */
public class PropertyFileConfiguration extends EditorRoot {

    private final static Logger logger = LoggerFactory.getLogger(PropertyFileConfiguration.class);

    private String fileNameToSave;
    private Properties properties;

    public PropertyFileConfiguration() {
    }

    public PropertyFileConfiguration(String fileNameToSave, Properties properties) {
        init(fileNameToSave, properties);
    }

    public void init(String fileNameToSave, Properties properties) {
        this.fileNameToSave = fileNameToSave;
        this.properties = properties;
    }

    public void save() {
        try (FileWriter writer = new FileWriter(fileNameToSave)) {
            orderedProperties(properties).store(writer, "mj config file");
        } catch (IOException e) {
            throw new ApplicationException("could not save preferences!", e);
        }
    }

    /**
     * Produce a ordered properties object the silly way. This is used to store them ordered in a text file.
     * @param properties
     * @return
     */
    public static Properties orderedProperties(Properties properties) {
        Properties tmp = new Properties() {
            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<Object>(super.keySet()));
            }
        };
        tmp.putAll(properties);
        return tmp;
    }

    public String getDisplayClassName() {
        return "PrefsRoot";
    }

    public String info() {
        return "";
    }

    public BackingDelegator createBackingDelegator(
        AbstractPrefVal abstractPrefVal,
        Object defaultVal
    ) {
        return new PropertyFileBackingDelegator(this, abstractPrefVal, defaultVal);
    }

    public Properties getProperties() {
        return properties;
    }
}
