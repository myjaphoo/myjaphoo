package org.mlsoft.common.acitivity.events

import groovy.transform.Immutable
import org.mlsoft.common.acitivity.Channel

/**
 * MessageEvent 
 * @author mla
 * @version $Id$
 *
 */
@Immutable(knownImmutables = ["channel"])
class ProgressEvent {
    Channel channel
    int percentage
    int overallPercentage
}
