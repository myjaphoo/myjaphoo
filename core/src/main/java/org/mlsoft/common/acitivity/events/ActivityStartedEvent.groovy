package org.mlsoft.common.acitivity.events

import groovy.transform.Immutable
import org.mlsoft.common.acitivity.Channel

/**
 * ActivityStartedEvent
 * @author mla
 * @version $Id$
 */
@Immutable(knownImmutables = ["channel"])
class ActivityStartedEvent {
    Channel channel
    String activityName
    boolean nestedActivity
}
