/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mlsoft.common.acitivity

import groovy.transform.CompileStatic
import org.mlsoft.common.acitivity.events.*
import org.mlsoft.eventbus.GlobalBus

import java.util.concurrent.CopyOnWriteArrayList
import java.util.function.Consumer

/**
 * Channel Manager.
 * @author mla
 */
@CompileStatic
class ChannelManager {

    private static ChannelManager instance = null

    private List<Channel> listOfRunningChannels = new CopyOnWriteArrayList<Channel>()

    private ChannelManager() {
    }

    static Channel createChannel(Class clazz, String activityTitle) {
        return new DefaultChannel(clazz, activityTitle)
    }

    static ChannelManager getChannelManager() {
        if (instance == null) {
            instance = new ChannelManager()
        }
        return instance
    }

    void startActivity(Channel channel, String activityTitle) {
        listOfRunningChannels.add(channel)
        GlobalBus.bus.post(new ActivityStartedEvent(channel, activityTitle, listOfRunningChannels.size() > 1))
    }

    void message(Channel channel, String message) {
        GlobalBus.bus.post(new MessageEvent(channel, message, false))
    }

    void errormessage(Channel channel, String message) {
        GlobalBus.bus.post(new ErrorMessageEvent(channel, message, null))
    }

    void errormessage(Channel channel, String message, Throwable t) {
        GlobalBus.bus.post(new ErrorMessageEvent(channel, message, t))
    }

    void progress(Channel channel, int percentage) {
        if (percentage > 100) {
            percentage = 100
        }
        int overallPercentage = calcOveralPercentage()
        GlobalBus.bus.post(new ProgressEvent(channel, percentage, overallPercentage))
    }

    private int calcOveralPercentage() {
        int sum = 0
        if (listOfRunningChannels.size() > 0) {
            for (Channel channel : listOfRunningChannels) {
                sum += channel.getLastPercentage()
            }
            return (int) sum / listOfRunningChannels.size()
        } else {
            return 0
        }
    }

    void stopActivity(Channel channel, String activityTitle) {
        GlobalBus.bus.post(new ActivityFinishedEvent(channel, activityTitle, listOfRunningChannels.size() >= 2))
        listOfRunningChannels.remove(channel)
    }


    void emphasisedMessage(Channel channel, String message) {
        GlobalBus.bus.post(new MessageEvent(channel, message, true))
    }

    /**
     * executes a closure with a channel to display progress and messages.
     * The channel is an argument for the closure
     *
     * @param activityName the activity name for the new channel
     * @param steps the expected steps for the progress
     * @param closure the closure to execute
     */
    static void withProgress(String activityName, int steps, Consumer<Channel> closure) {
        Channel channel = ChannelManager.createChannel(ChannelManager.class, activityName);
        channel.startActivity();
        channel.setProgressSteps(steps);
        try {
            closure.accept(channel);
        } finally {
            channel.stopActivity();
        }
    }
}
