package org.myjaphoo.plugin;

import org.myjaphoo.project.Project;

/**
 * Plugin interface to be implemented by additional plugins.
 *
 * @author lang
 * @version $Id$
 */
public interface MyjaphooPlugin {

    /**
     * Plugin name.
     *
     * @return
     */
    String getName();


    /**
     * String representing the plugin version.
     */
    String getVersion();

    /**
     * initialize the plugin. Usually registers additional gui actions via the ComponentRegistry.
     *
     * @see org.myjaphoo.model.registry.ComponentRegistry
     */
    void init();

    /**
     * Do initialisations for a new project.
     *
     * @param project
     */
    void initProject(Project project);
}
