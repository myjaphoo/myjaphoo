package org.myjaphoo.plugin

import groovy.transform.TypeChecked
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType
import org.myjaphoo.model.cache.zipper.EntryZipper
import org.myjaphoo.model.cache.zipper.MetaTokenZipper
import org.myjaphoo.model.cache.zipper.TokenZipper
import org.myjaphoo.model.filterparser.expr.ExprType
import org.myjaphoo.model.filterparser.idents.EntryContextIdentifier
import org.myjaphoo.model.filterparser.idents.FixIdentifier
import org.myjaphoo.model.filterparser.idents.MetaTagContextIdentifier
import org.myjaphoo.model.filterparser.idents.TagContextIdentifier
import org.myjaphoo.model.filterparser.values.ObjectValue
import org.myjaphoo.model.filterparser.values.Value

/**
 * Utilities that plugins/scripts could use. Derived from common methods from current plugins.
 * This methods/signatures should therefore be stable as plugins will depend on this.
 *
 * The methods are mostly simply "shortcuts" for easy setup of things that could probably
 * many plugins could need.
 *
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class Utils {

    /**
     * Defines an additional identifier that depends on movie entry objects for the filter and grouping
     * language.
     *
     * @param name name of the identifier
     * @param descr description
     * @param exampleUsage example of usage
     * @param returnType the return type of the value for that identifier
     * @param extractValueClosure a closure which gets a movie entry as argument and returns the value
     * @return
     */
    def FixIdentifier defEntryIdent(String name, String descr, String exampleUsage, ExprType returnType,
                                    @ClosureParams(value = SimpleType.class, options = "org.myjaphoo.model.cache.zipper.EntryZipper") Closure extractValueClosure) {
        def FixIdentifier ident = new EntryContextIdentifier(
                name,
                descr,
                exampleUsage,
                returnType) {

            @Override
            public Value extractIdentValue(EntryZipper movieEntry) {
                def retVal = extractValueClosure.call(movieEntry);
                return new ObjectValue(retVal);
            }
        };
        return ident;
    }

    def FixIdentifier defTagIdent(String name, String descr, String exampleUsage, ExprType returnType,
                                  @ClosureParams(value = SimpleType.class, options = "org.myjaphoo.model.cache.zipper.TokenZipper") Closure extractValueClosure) {
        def FixIdentifier ident = new TagContextIdentifier(
                name,
                descr,
                exampleUsage,
                returnType) {

            @Override
            public Value extractIdentValue(TokenZipper token) {
                def retVal = extractValueClosure.call(token);
                return new ObjectValue(retVal);
            }
        };
        return ident;
    }


    def FixIdentifier defMetaTagIdent(String name, String descr, String exampleUsage, ExprType returnType,
                                      @ClosureParams(value = SimpleType.class, options = "org.myjaphoo.model.cache.zipper.MetaTokenZipper") Closure extractValueClosure) {
        def FixIdentifier ident = new MetaTagContextIdentifier(
                name,
                descr,
                exampleUsage,
                returnType) {

            @Override
            public Value extractIdentValue(MetaTokenZipper token) {
                def retVal = extractValueClosure.call(token);
                return new ObjectValue(retVal);
            }
        };
        return ident;
    }
}
