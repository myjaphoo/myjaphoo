package org.myjaphoo.plugin

import groovy.transform.TypeChecked
import org.myjaphoo.model.ThumbMode
import org.myjaphoo.model.db.BookMark
import org.myjaphoo.model.logic.MovieEntryJpaController
import org.myjaphoo.project.Project

/**
 * Utilities that plugins/scripts could use. Derived from common methods from current plugins.
 * This methods/signatures should therefore be stable as plugins will depend on this.
 *
 * The methods are mostly simply "shortcuts" for easy setup of things that could probably
 * many plugins could need.
 *
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class ProjectUtils {


    private Project project;

    ProjectUtils(Project project) {
        this.project = project
    }
/**
 * Creates/Updates a bookmark. Useful if a plugin wants to have "predefined" bookmarks
 * in place.
 *
 * @param path menu path of the bookmark or null
 * @param name name of the bookmark
 * @param expr the filter expression or null if no filter
 * @return
 */
    def ensureBookmark(String path, String name, String expr) {
        ensureBookmark(path, name, expr, "Directory");
    }

    /**
     * Creates/Updates a bookmark. Useful if a plugin wants to have "predefined" bookmarks
     * in place.
     *
     * @param path menu path of the bookmark or null
     * @param name name of the bookmark
     * @param expr the filter expression or null if no filter
     * @param grouping the grouping expression or null if no grouping required
     * @return
     */
    def ensureBookmark(String path, String name, String expr, String grouping) {
        MovieEntryJpaController jpa = new MovieEntryJpaController(project.connection);
        List<BookMark> bookmarks = jpa.findBookMarkEntities()
        BookMark bm = bookmarks.find { BookMark b -> b.name == name }
        if (bm == null) {
            bm = new BookMark();
            fillBookmark(path, name, bm, expr, grouping)
            jpa.create(bm);
        } else {
            fillBookmark(path, name, bm, expr, grouping);
            jpa.edit(bm);
        }
    }

    private void fillBookmark(String path, String name, BookMark bm, String expr, String grouping) {
        bm.name = name;
        bm.menuPath = path;
        bm.descr = ""
        bm.view.filterExpression = expr;
        bm.view.userDefinedStruct = grouping;
        bm.view.listChildMovies = true;
        bm.view.thumbmode = ThumbMode.ALTTHUMB;
    }

}
