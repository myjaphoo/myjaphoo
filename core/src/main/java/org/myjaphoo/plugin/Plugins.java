package org.myjaphoo.plugin;

import org.myjaphoo.project.Project;
import org.myjaphoo.util.AppProps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.function.Consumer;

/**
 * Plugins
 *
 * @author lang
 * @version $Id$
 */
public class Plugins {

    private static final Logger logger = LoggerFactory.getLogger(Plugins.class);


    /**
     * Loads all plugins and calls their init method.
     */
    public static void initAllPlugins() {
        eachPlugin("Check Compatibility", plugin -> {
            if (!isCompatible(plugin)) {
                String nameAndVersion = plugin.getName() + "[" + plugin.getVersion() + "]";
                logger.warn(nameAndVersion + " probably not compatible");
            }
        });

        eachPlugin("Init", plugin -> plugin.init());
    }

    public static boolean isCompatible(MyjaphooPlugin plugin) {
        return plugin.getVersion() != null && AppProps.versionNumber.startsWith(plugin.getVersion());
    }

    public static void initProject(Project project) {
        eachPlugin("Projectinitialisation", plugin -> plugin.initProject(project));
    }

    private static void eachPlugin(String what, Consumer<MyjaphooPlugin> consumer) {
        try {
            ServiceLoader<MyjaphooPlugin> loader = ServiceLoader.load(MyjaphooPlugin.class);
            Iterator<MyjaphooPlugin> iterator = loader.iterator();
            while (iterator.hasNext()) {
                MyjaphooPlugin plugin = iterator.next();
                String nameAndVersion = plugin.getName() + "[" + plugin.getVersion() + "]";
                logger.info(what + " for Plugin {} ...", nameAndVersion);
                try {
                    consumer.accept(plugin);
                    logger.info(what + " for Plugin {} finished", nameAndVersion);
                } catch (Exception e) {
                    logger.error(what + " Error in plugin " + nameAndVersion, e);
                }
            }
        } catch (Exception e) {
            // service loading at all could also fail, e.g. if listed service classes are missing, etc.
            logger.error("Error initializing plugins ", e);
        }
    }
}
