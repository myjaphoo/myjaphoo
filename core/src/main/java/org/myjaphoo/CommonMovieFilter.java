/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.myjaphoo;

import org.apache.commons.lang.time.StopWatch;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.db.DataView;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;
import org.myjaphoo.model.filterparser.expr.Expression;
import org.myjaphoo.model.filterparser.expr.Expressions;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.processing.DefaultProcessingRequirementInformation;
import org.myjaphoo.model.filterparser.processing.FilterAndGroupingProcessor;
import org.myjaphoo.model.filterparser.processing.ProcessingRequirementInformation;
import org.myjaphoo.model.filterparser.syntaxtree.StringLiteral;
import org.myjaphoo.model.util.Parsing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;


/**
 * experimentelle optimierte variante, die movies im speicher hält (zusammen mit
 * assignden tokens), u. damit weitere zugriffe verhindert. Ggf. müssen daraufhin
 * db- aktionen auf die gecachten objekte vorher mit einem merge eingeleitet
 * werden.
 *
 * @author lang
 */
public class CommonMovieFilter implements MovieFilterInterface {

    public static final Logger LOGGER = LoggerFactory.getLogger(CommonMovieFilter.class.getName());

    public static final Expressions.Predicate FIND_LITERALS = new Expressions.Predicate() {
        @Override
        public boolean eval(Expression expr) {
            // stop on first level of subst expr. to not scan into nested substitutions
            return !(expr instanceof StringLiteral);
        }
    };

    private final CommonFilterContext cfc;

    public CommonMovieFilter(CommonFilterContext cfc) {
        this.cfc = requireNonNull(cfc);
    }

    @Override
    public FilterResult loadFilteredEntries(
        DataView dataView, List<? extends ProcessingRequirementInformation> groupingAlgorithm
    )
        throws ParserException {

        // check prefilters and combine them if necessary with the main filter:
        // pre-parse them to get possible parser errors first:
        parseExpr(cfc.substitutionSupplier, dataView.getPreFilterExpression());

        // and now parse the main filter separate:
        parseExpr(cfc.substitutionSupplier, dataView.getFilterExpression());

        // if necessary combine them:
        String combinedExpr = dataView.getCombinedFilterExpression();

        return filter(
            cfc,
            combinedExpr,
            combinedProcessingInstructions(groupingAlgorithm)
        );
    }

    private ProcessingRequirementInformation combinedProcessingInstructions(
        List<? extends ProcessingRequirementInformation> groupingAlgorithm
    ) {
        DefaultProcessingRequirementInformation pri = new DefaultProcessingRequirementInformation();
        for (ProcessingRequirementInformation p : groupingAlgorithm) {
            pri.join(p);
        }
        return pri;
    }

    public static FilterResult filter(
        CommonFilterContext cfc,
        String filterpattern,
        ProcessingRequirementInformation reqForGroupings
    )
        throws ParserException {
        AbstractBoolExpression expr = parseExpr(cfc.substitutionSupplier, filterpattern);

        ArrayList<String> usedLiterals = new ArrayList<>();
        List<StringLiteral> literals = Expressions.findAllExprParts(expr, StringLiteral.class, FIND_LITERALS);
        for (StringLiteral literal : literals) {
            usedLiterals.add(literal.getLiteral());
        }

        ImmutableModel model = cfc.getModelSupplier().get();
        io.vavr.collection.List<ImmutableMovieEntry> allMovies = model.getEntryList();

        StopWatch watch = new StopWatch();
        watch.start();
        ArrayList<JoinedDataRow> resultList = FilterAndGroupingProcessor.filterEntries(
            cfc.fileTypes,
            model,
            allMovies,
            cfc.databaseComparison,
            expr,
            reqForGroupings
        );
        watch.stop();
        LOGGER.info("finished creating filtered entry list: duration:" + watch.toString()); //NOI18N

        return new FilterResult(model, resultList, usedLiterals);
    }


    public static AbstractBoolExpression parseExpr(
        Supplier<Map<String, Substitution>> substitutionSupplier, String strExpr
    ) {
        return Parsing.parseExpr(substitutionSupplier, strExpr);
    }


}
