package org.myjaphoo;

import org.apache.commons.lang.StringUtils;
import org.mlsoft.common.prefs.model.AbstractMetadata;
import org.mlsoft.common.prefs.model.PropertyFileConfiguration;
import org.mlsoft.common.prefs.model.editors.BooleanVal;
import org.mlsoft.common.prefs.model.editors.ColorVal;
import org.mlsoft.common.prefs.model.editors.EditorGroup;
import org.mlsoft.common.prefs.model.editors.EditorRoot;
import org.mlsoft.common.prefs.model.editors.EnumSelectionVal;
import org.mlsoft.common.prefs.model.editors.FileVal;
import org.mlsoft.common.prefs.model.editors.IntegerVal;
import org.mlsoft.common.prefs.model.editors.StringSelectionVal;
import org.mlsoft.common.prefs.model.editors.StringVal;
import org.mlsoft.structures.Trees;
import org.myjaphoo.model.config.DatabaseConfigLoadSave;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseConfigurations;
import org.myjaphoo.model.filterparser.FileTypes;
import org.myjaphoo.model.logic.imp.ImportTypes;
import org.myjaphoo.model.logic.imp.thumbprovider.ThumbnailProviders;
import org.myjaphoo.model.util.UserDirectory;
import org.myjaphoo.project.ProjectConfiguration;
import org.myjaphoo.project.ProjectFileType;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

/**
 * application preferences which are specific to the fat client version.
 * These preferences extend the AppConfig preferences.
 *
 * @author unbekannt
 * @version 1.0
 */
public class MyjaphooCorePrefs {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MyjaphooAppPrefs");
    public static final String VLC_LINUX_EXE = "vlc.linux.exe";
    public static final String VLC_WINDOWS_EXE = "vlc.windows.exe";
    public static final String MPLAYER_LINUX_EXE = "mplayer.linux.exe";
    public static final String MPLAYER_WINDOWS_EXE = "mplayer.windows.exe";
    public static final String KMPLAYER_LINUX_EXE = "kmplayer.linux.exe";
    public static final String FFMPEGTHUMBNAILER_LINUX_EXE = "ffmpegthumbnailer.linux.exe";
    public static final String FFMPEGTHUMBNAILER_WINDOWS_EXE = "ffmpegthumbnailer.windows.exe";
    public static final String FFMPEG_LINUX_EXE = "ffmpeg.linux.exe";
    public static final String FFMPEG_WINDOWS_EXE = "ffmpeg.windows.exe";
    public static final String PICTUREIMPORTFILTER = "pictureimportfilter";
    public static final String SPECIFICIMPORTFILTER = "specificimportfilter";
    public static final String MOVIEIMPORTFILTER = "movieimportfilter";
    public static final String COMPRESSEDFILESFILTER = "compressedfilesfilter";
    public static final String TEXTFILESFILTER = "textfilesfilter";

    
    public final PropertyFileConfiguration ROOT = new PropertyFileConfiguration(); //NOI18N
    public final EditorGroup COMMON = new EditorGroup(
        ROOT,
        localeBundle.getString("COMMON GUI"),
        localeBundle.getString("COMMON GUI"),
        localeBundle.getString("COMMON DESC")
    );
    public final EditorGroup IMPORT = new EditorGroup(
        ROOT,
        localeBundle.getString("IMPORT GUI"),
        localeBundle.getString("IMPORT GUI"),
        localeBundle.getString("IMPORT DESC")
    );
    private final EditorGroup PICIMPORT = new EditorGroup(
        IMPORT,
        localeBundle.getString("PICTURE IMPORT GUI"),
        localeBundle.getString("PICTURE IMPORT GUI"),
        localeBundle.getString("PICTURE IMPORT DESC")
    );
    public final EditorGroup MOVIMPORT = new EditorGroup(
        IMPORT,
        localeBundle.getString("MOVIE IMPORT GUI"),
        localeBundle.getString("MOVIE IMPORT GUI"),
        localeBundle.getString("MOVIE IMPORT DESC")
    );
    private final EditorGroup DB = new EditorGroup(
        ROOT,
        localeBundle.getString("DB GUI"),
        localeBundle.getString("DB GUI"),
        localeBundle.getString("DB DESC")
    );
    public final EditorGroup INTERNAL = new EditorGroup(
        ROOT,
        localeBundle.getString("INTERNAL GUI"),
        localeBundle.getString("INTERNAL GUI"),
        localeBundle.getString("INTERNAL DESC")
    );
    public final StringVal PRF_PICFILTER =
        new StringVal(PICIMPORT, PICTUREIMPORTFILTER,  //NOI18N
            localeBundle.getString("PICTURE IMPORT FILTER GUI"),
            localeBundle.getString("PICTURE IMPORT FILTER DESC"),
            ".jpg;.jpeg;.png"
        ); //NOI18N
    public final StringVal PRF_SPECIFICFILTER =
        new StringVal(IMPORT, SPECIFICIMPORTFILTER,  //NOI18N
            localeBundle.getString("SPECIFIC IMPORT FILTER GUI"),
            localeBundle.getString("SPECIFIC IMPORT FILTER DESC"),
            ""
        ); //NOI18N
    public final StringVal PRF_MOVIEFILTER =
        new StringVal(
            MOVIMPORT,
            MOVIEIMPORTFILTER,
            //NOI18N
            localeBundle.getString("MOVIE IMPORT FILTER GUI"),
            localeBundle.getString("MOVIE IMPORT FILTER DESC"),
            ".ts;.mpg;.mpeg;.MPG;.MPEG;.wmv;.avi;.AVI;.mp4;.WMV;.MP4;.iso;.divx;.DIVX;.mkv;.m4v;.f4v;.asf;.mov;.flv;.vob"
        ); //NOI18N
    public final StringVal PRF_COMPRESSEDFILESFILTER =
        new StringVal(IMPORT, COMPRESSEDFILESFILTER,  //NOI18N
            localeBundle.getString("FILE FILTER FOR COMPRESSED FILES GUI"),
            localeBundle.getString("FILE FILTER FOR COMPRESSED FILES DESC"),
            ".zip;.rar;.tar"
        ); //NOI18N
    public final StringVal PRF_TEXTFILTER =
        new StringVal(IMPORT, TEXTFILESFILTER,  //NOI18N
            localeBundle.getString("FILE FILTER FOR TEXT FILES GUI"),
            localeBundle.getString("FILE FILTER FOR TEXT FILES DESC"),
            ".txt;.log;.text"
        ); //NOI18N
    public final BooleanVal PRF_MOVIE_SCANFORCOVERPICS =
        new BooleanVal(IMPORT, "moviescanforcoverpics",  //NOI18N
            localeBundle.getString("SCANFORCOVERPICS GUI"),
            localeBundle.getString("SCANFORCOVERPICS DESC"),
            Boolean.FALSE
        ); //NOI18N
    public final StringVal PRF_MOVIE_SCANPATTERN =
        new StringVal(IMPORT, "moviescanpattern",  //NOI18N
            localeBundle.getString("SCANPATTERN GUI"),
            localeBundle.getString("SCANPATTERN DESC"),
            ".*$namewithoutsuffix.*"
        ); //NOI18N


    public final BooleanVal PRF_DB_USE_DERBY_EMBEDDEDMODE =
        new BooleanVal(DB, "db.useDerbyEmbeddedMode",  //NOI18N
            localeBundle.getString("PRF_DB_USE_DERBY_EMBEDDEDMODE GUI"),
            localeBundle.getString("PRF_DB_USE_DERBY_EMBEDDEDMODE DESC"),
            Boolean.TRUE
        );
    public final StringVal PRF_DB_DERBY_HOSTNAME =
        new StringVal(DB, "db.derbyHostName",  //NOI18N
            localeBundle.getString("PRF_DB_DERBY_HOSTNAME GUI"),
            localeBundle.getString("PRF_DB_DERBY_HOSTNAME DESC"),
            "localhost"
        ); //NOI18N
    public final StringVal PRF_DB_DERBY_PORT =
        new StringVal(DB, "db.derbyPort",  //NOI18N
            localeBundle.getString("PRF_DB_DERBY_PORT GUI"),
            localeBundle.getString("PRF_DB_DERBY_PORT DESC"),
            "1527"
        ); //NOI18N
    public final StringVal PRF_DB_FILE =
        new StringVal(DB, "db.name",  //NOI18N
            localeBundle.getString("PRF_DB_FILE GUI"),
            localeBundle.getString("PRF_DB_FILE DESC"),
            UserDirectory.getDirectory() + "myjaphoodb"
        ); //NOI18N

    public final BooleanVal PRF_DB_USE_H2 =
        new BooleanVal(DB, "db.useH2EmbeddedMode",  //NOI18N
            localeBundle.getString("PRF_DB_USE_H2 GUI"),
            localeBundle.getString("PRF_DB_USE_H2 DESC"),
            Boolean.FALSE);

    public final BooleanVal PRF_DB_USE_OTHERCONFIGURATION =
        new BooleanVal(DB, "db.useOtherConfiguration",  //NOI18N
            localeBundle.getString("PRF_DB_USE_OTHER_CONFIGURATION GUI"),
            localeBundle.getString("PRF_DB_USE_OTHER_CONFIGURATION DESC"),
            Boolean.FALSE
        );

    public final StringSelectionVal PRF_DB_CONFIGURATIONNAME =
        new StringSelectionVal(DB, "db.otherConfigname",  //NOI18N
            localeBundle.getString("PRF_DB_OTHER_CONFIGNAME GUI"),
            localeBundle.getString("PRF_DB_OTHER_CONFIGNAME DESC"),
            "", () -> getAllAvailableConfigurations()
        ); //NOI18N

    /***
     * prevents building checksum of media files. in some rare cases this might
     * be useful.
     */
    public final BooleanVal PRF_INTERNAL_DONOTBUILDCHECKSUM =
        new BooleanVal(INTERNAL, "internal.doNotBuildChecksum",  //NOI18N
            "do not calculate checksum of files, use filesize instead",
            "do not calculate checksum of files, use filesize instead",
            Boolean.FALSE
        );

    private String[] getAllAvailableConfigurations() {
        DatabaseConfigurations allConfigs = databaseConfigLoadSave.load();
        ArrayList<String> names = new ArrayList<String>();
        for (DatabaseConfiguration config : allConfigs.getDatabaseConfigurations()) {
            if (!StringUtils.isEmpty(config.getName())) {
                names.add(config.getName());
            }
        }
        Collections.sort(names);
        return names.toArray(new String[names.size()]);
    }

    private final EditorGroup VIEWER = new EditorGroup(
        ROOT,
        localeBundle.getString("VIEWER GUI"),
        localeBundle.getString("VIEWER GUI"),
        localeBundle.getString("VIEWER DESC")
    );

    private final EditorGroup COLORS = new EditorGroup(
        ROOT,
        localeBundle.getString("COLORS GUI"),
        localeBundle.getString("COLORS GUI"),
        localeBundle.getString("COLORS DESC")
    );
    private final EditorGroup SYNTAXHIGHLIGHTING = new EditorGroup(
        COLORS,
        localeBundle.getString("SYNTAXHIGHLIGHTING GUI"),
        localeBundle.getString("SYNTAXHIGHLIGHTING GUI"),
        localeBundle.getString("SYNTAXHIGHLIGHTING DESC")
    );

    private final EditorGroup CACHING = new EditorGroup(
        ROOT,
        localeBundle.getString("CACHING GUI"),
        localeBundle.getString("CACHING GUI"),
        localeBundle.getString("CACHING DESC")
    );

    public final BooleanVal PRF_USE_NATURALSORTING =
        new BooleanVal(COMMON, "common.useNaturalSorting",  //NOI18N
            localeBundle.getString("PRF_USE_NATURALSORTING GUI"),
            localeBundle.getString("PRF_USE_NATURALSORTING DESC"),
            Boolean.TRUE
        );
    public final IntegerVal PRF_THUMBCACHE_CACHESIZE =
        new IntegerVal(CACHING, "cache.thumbcache.cacheSize",  //NOI18N
            localeBundle.getString("PRF_THUMBCACHE_CACHESIZE GUI"),
            localeBundle.getString("PRF_THUMBCACHE_CACHESIZE DESC"),
            200
        );
    public final IntegerVal PRF_THUMBCACHE_MAXPREFETCH =
        new IntegerVal(CACHING, "cache.thumbcache.maxPrefetch",  //NOI18N
            localeBundle.getString("PRF_THUMBCACHE_MAXPREFETCH GUI"),
            localeBundle.getString("PRF_THUMBCACHE_MAXPREFETCH DESC"),
            30
        );
    public final IntegerVal PRF_THUMBCACHE_FETCHAROUNDSIZE =
        new IntegerVal(CACHING, "cache.thumbcache.fetcharoundSize",  //NOI18N
            localeBundle.getString("PRF_THUMBCACHE_FETCHAROUNDSIZE GUI"),
            localeBundle.getString("PRF_THUMBCACHE_FETCHAROUNDSIZE DESC"),
            10
        );
    public final BooleanVal PRF_THUMBCACHE_PREFETCHTHUMBS =
        new BooleanVal(CACHING, "cache.thumbcache.prefetchthumbs",  //NOI18N
            localeBundle.getString("PRF_THUMBCACHE_PREFETCHTHUMBS GUI"),
            localeBundle.getString("PRF_THUMBCACHE_PREFETCHTHUMBS DESC"),
            false
        );

    public final StringVal PRF_DATABASENAME =
        new StringVal(COMMON, "databasename",  //NOI18N
            localeBundle.getString("PRF_DATABASENAME GUI"),
            localeBundle.getString("PRF_DATABASENAME DESC"),
            "noname"
        ); //NOI18N

    public final BooleanVal PRF_PLAF_FRAME_DECORATED =
        new BooleanVal(COMMON, "app.plaf.frameDecorated",  //NOI18N
            localeBundle.getString("PRF_PLAF_FRAME_DECORATED_GUI"),
            localeBundle.getString("PRF_PLAF_FRAME_DECORATED_DESC"),
            false
        );

    public final EnumSelectionVal PRF_IMPORTMODE =
        new EnumSelectionVal(IMPORT, "importmode",  //NOI18N
            localeBundle.getString("PRF_IMPORTMODE GUI"),
            localeBundle.getString("PRF_IMPORTMODE DESC"),
            ImportTypes.Pictures,
            ImportTypes.class
        );

    public final IntegerVal PRF_THUMBSIZE =
        new IntegerVal(IMPORT, "thumbsize",  //NOI18N
            localeBundle.getString("PRF_THUMBSIZE GUI"),
            localeBundle.getString("PRF_THUMBSIZE DESC"),
            128
        );
    public final IntegerVal PRF_IMPORT_NUMWORKERTHREADS =
        new IntegerVal(IMPORT, "numWorkerThreads",  //NOI18N
            localeBundle.getString("PRF_NUMWORKERTHREADS GUI"),
            localeBundle.getString("PRF_NUMWORKERTHREADS DESC"),
            1
        );

    public final IntegerVal PRF_MAXCHRONIC =
        new IntegerVal(COMMON, "maxchronic",  //NOI18N
            localeBundle.getString("PRF_MAXCHRONIC GUI"),
            localeBundle.getString("PRF_MAXCHRONIC DESC"),
            20
        );

    public final EnumSelectionVal<ThumbnailProviders> PRF_PREFERED_THUMB_PROVIDER =
        new EnumSelectionVal(VIEWER, "app.thumbprovider.preferedProvider",  //NOI18N
            localeBundle.getString("PRF_PREFERED_THUMB_PROVIDER GUI"),
            localeBundle.getString("PRF_PREFERED_THUMB_PROVIDER DESC"),
            ThumbnailProviders.NONE,
            ThumbnailProviders.class
        );


    public final FileVal PRF_LINUX_VLCEXE =
        new FileVal(VIEWER, VLC_LINUX_EXE,  //NOI18N
            localeBundle.getString("PRF_LINUX_VLCEXE GUI"),
            localeBundle.getString("PRF_LINUX_VLCEXE DESC"),
            "vlc"
        ); //NOI18N
    public final FileVal PRF_WINDOWS_VLCEXE =
        new FileVal(VIEWER, VLC_WINDOWS_EXE,  //NOI18N
            localeBundle.getString("PRF_WINDOWS_VLCEXE GUI"),
            localeBundle.getString("PRF_WINDOWS_VLCEXE DESC"),
            "C:\\\\Program Files\\\\VideoLAN\\\\VLC\\\\vlc.exe"
        ); //NOI18N
    public final FileVal PRF_LINUX_MPLAYER_EXE =
        new FileVal(VIEWER, MPLAYER_LINUX_EXE,  //NOI18N
            localeBundle.getString("PRF_LINUX_MPLAYER_EXE GUI"),
            localeBundle.getString("PRF_LINUX_MPLAYER_EXE DESC"),
            "mplayer"
        ); //NOI18N

    public final FileVal PRF_WINDOWS_MPLAYER_EXE =
        new FileVal(VIEWER, MPLAYER_WINDOWS_EXE,  //NOI18N
            localeBundle.getString("PRF_WINDOWS_MPLAYER_EXE GUI"),
            localeBundle.getString("PRF_WINDOWS_MPLAYER_EXE DESC"),
            "c:/programme/mplayer/mplayer.exe"
        ); //NOI18N

    public final FileVal PRF_LINUX_KMPLAYER_EXE =
        new FileVal(VIEWER, KMPLAYER_LINUX_EXE,  //NOI18N
            localeBundle.getString("PRF_LINUX_KMPLAYER_EXE GUI"),
            localeBundle.getString("PRF_LINUX_KMPLAYER_EXE DESC"),
            "kmplayer"
        ); //NOI18N

    public final FileVal PRF_LINUX_FFMPEGTHUMBNAILER_EXE =
        new FileVal(VIEWER, FFMPEGTHUMBNAILER_LINUX_EXE,  //NOI18N
            localeBundle.getString("PRF_LINUX_FFMPEGTHUMBNAILER_EXE GUI"),
            localeBundle.getString("PRF_LINUX_FFMPEGTHUMBNAILER_EXE DESC"),
            "ffmpegthumbnailer"
        ); //NOI18N
    public final FileVal PRF_WINDOWS_FFMPEGTHUMBNAILER_EXE =
        new FileVal(VIEWER, FFMPEGTHUMBNAILER_WINDOWS_EXE,  //NOI18N
            localeBundle.getString("PRF_WINDOWS_FFMPEGTHUMBNAILER_EXE GUI"),
            localeBundle.getString("PRF_WINDOWS_FFMPEGTHUMBNAILER_EXE DESC"),
            "c:/programme/ffmpegthumbnailer/ffmpegthumbnailer.exe"
        ); //NOI18N

    public final FileVal PRF_LINUX_FFMPEG_EXE =
        new FileVal(VIEWER, FFMPEG_LINUX_EXE,  //NOI18N
            localeBundle.getString("PRF_LINUX_FFMPEG_EXE GUI"),
            localeBundle.getString("PRF_LINUX_FFMPEG_EXE DESC"),
            "ffmpeg"
        ); //NOI18N
    public final FileVal PRF_WINDOWS_FFMPEG_EXE =
        new FileVal(VIEWER, FFMPEG_WINDOWS_EXE,  //NOI18N
            localeBundle.getString("PRF_WINDOWS_FFMPEG_EXE GUI"),
            localeBundle.getString("PRF_WINDOWS_FFMPEG_EXE DESC"),
            "c:/programme/ffmpeg/bin/ffmpeg.exe"
        ); //NOI18N


    public final BooleanVal PRF_SHOWTIPOFDAY =
        new BooleanVal(COMMON, "gui.showTipOfDay",  //NOI18N
            localeBundle.getString("PRF_SHOWTIPOFDAY GUI"),
            localeBundle.getString("PRF_SHOWTIPOFDAY DESC"),
            Boolean.TRUE
        );

    public final BooleanVal PRF_SHOW_FILLOCALISATION_HINTS =
        new BooleanVal(INTERNAL,
            "intern.filelocalisation_hints.showHints",
            //NOI18N
            "show infos/hints, if a file could be localized on the file system",
            "deactivating may make the application more responsive, if most of the files are located on networks which are not available most of the time",
            Boolean.TRUE
        );


    public final ColorVal PRF_SYNTAXH_IDENTS = new ColorVal(SYNTAXHIGHLIGHTING, "syntaxhighlighting.idents",  //NOI18N
        localeBundle.getString("PRF_SYNTAXH_IDENTS GUI"),
        localeBundle.getString("PRF_SYNTAXH_IDENTS DESC"),
        new Color(0, 0, 255)
    );
    public final ColorVal PRF_SYNTAXH_OPERATORS = new ColorVal(SYNTAXHIGHLIGHTING,
        "syntaxhighlighting.operators",
        //NOI18N
        localeBundle.getString("PRF_SYNTAXH_OPERATORS GUI"),
        localeBundle.getString("PRF_SYNTAXH_OPERATORS DESC"),
        new Color(255, 20, 240).darker()
    );
    public final ColorVal PRF_SYNTAXH_UNITS = new ColorVal(SYNTAXHIGHLIGHTING, "syntaxhighlighting.units",  //NOI18N
        localeBundle.getString("PRF_SYNTAXH_UNITS GUI"),
        localeBundle.getString("PRF_SYNTAXH_UNITS DESC"),
        new Color(150, 150, 150)
    );
    public final ColorVal PRF_SYNTAXH_LITERALS = new ColorVal(SYNTAXHIGHLIGHTING,
        "syntaxhighlighting.literals",
        //NOI18N
        localeBundle.getString("PRF_SYNTAXH_LITERALS GUI"),
        localeBundle.getString("PRF_SYNTAXH_LITERALS DESC"),
        new Color(206, 123, 0)
    );
    public final ColorVal PRF_SYNTAXH_ERRORS = new ColorVal(SYNTAXHIGHLIGHTING, "syntaxhighlighting.errors",  //NOI18N
        localeBundle.getString("PRF_SYNTAXH_ERRORS GUI"),
        localeBundle.getString("PRF_SYNTAXH_ERRORS DESC"),
        new Color(255, 0, 0)
    );
    public final ColorVal PRF_SYNTAXH_BRACES = new ColorVal(SYNTAXHIGHLIGHTING, "syntaxhighlighting.braces",  //NOI18N
        localeBundle.getString("PRF_SYNTAXH_BRACES GUI"),
        localeBundle.getString("PRF_SYNTAXH_BRACES DESC"),
        new Color(255, 200, 100).darker()
    );


    private final EditorGroup SCRIPTING = new EditorGroup(
        ROOT,
        localeBundle.getString("SCRIPTING GUI"),
        localeBundle.getString("SCRIPTING GUI"),
        localeBundle.getString("SCRIPTING DESC")
    );

    public final BooleanVal PRF_LOADPLUGINS =
        new BooleanVal(SCRIPTING, "scripting.loadPlugins",  //NOI18N
            localeBundle.getString("PRF_SCRIPTING_LOADPLUGINS_GUI"),
            localeBundle.getString("PRF_SCRIPTING_LOADPLUGINS_DESC"),
            Boolean.TRUE
        );

    public final ProjectFileType Pictures;
    public final ProjectFileType Movies;
    public final ProjectFileType CompressedFiles;
    public final ProjectFileType Text;

    public final DatabaseConfigLoadSave databaseConfigLoadSave;

    public final String projectDir;

    public MyjaphooCorePrefs(ProjectConfiguration projectConfiguration) {
        this.projectDir = projectConfiguration.projectDir;
        ROOT.init(projectConfiguration.projectConfigFile, projectConfiguration.getProjectProperties());
        Pictures = new ProjectFileType(this, MyjaphooCorePrefs.PICTUREIMPORTFILTER);
        Movies = new ProjectFileType(this, MyjaphooCorePrefs.MOVIEIMPORTFILTER);
        CompressedFiles = new ProjectFileType(this, MyjaphooCorePrefs.COMPRESSEDFILESFILTER);
        Text = new ProjectFileType(this, MyjaphooCorePrefs.TEXTFILESFILTER);

        databaseConfigLoadSave = new DatabaseConfigLoadSave(projectConfiguration.projectDir);
    }

    public final EditorRoot getPrefStructure() {
        return ROOT;
    }


    public AbstractMetadata searchByName(String name) {
        AbstractMetadata result = Trees.searchDepthFirstSearch(getPrefStructure(), t -> name.equals(t.getName()));
        return result;
    }

    public FileTypes createFileTypes() {
        return new FileTypes(Movies, Pictures, Text);
    }
}
