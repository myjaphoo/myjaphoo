package org.myjaphoo.datasets;

import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;
import org.myjaphoo.model.db.Token;

/**
 * MiniTestSet
 *
 * @author mla
 * @version $Id$
 */
public class MiniTestSet extends TestSet {

    public MovieEntry e1;
    public MovieEntry e2;
    public MovieEntry e3;
    public MovieEntry e4;
    public MovieEntry e5;

    public Token t1;
    public Token t2;
    public Token t3;

    public MetaToken mt1;
    public MetaToken mt2;
    public MetaToken mt3;

    public MiniTestSet(String dbName) {
        super(dbName);
        project.connection.commit(em -> {

            e1 = createEntry("movie1", "aaa/bbb/ccc", 100, 100, Rating.BAD);
            e2 = createEntry("movie2", "ddd/eee/fff", 101, 101, Rating.MIDDLE);
            e3 = createEntry("movie3", "gggg/hhhh", 102, 103, Rating.MIDDLE);
            e4 = createEntry("movie4", "jjjj", 104, 104, Rating.VERY_BAD);
            e5 = createEntry("movie5", "ddd/eee/fff", 105, 105, Rating.VERY_GOOD);

            t1 = createToken("Token1");
            t2 = createToken("Token2");
            t3 = createToken("Token3");

            ass(e1, t1);
            ass(e2, t1);
            ass(e3, t2);
            ass(e3, t1);
            ass(e4, t2);

            mt1 = createMetaToken("meta1");
            mt2 = createMetaToken("meta2");
            mt3 = createMetaToken("meta3");
            ass(mt1, t1);
            ass(mt2, t1);
            ass(mt3, t2);

        });
        project.connection.getStatistics();
    }
}
