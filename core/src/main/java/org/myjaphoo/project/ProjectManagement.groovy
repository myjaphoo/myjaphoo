package org.myjaphoo.project

import groovy.transform.TypeChecked
import org.myjaphoo.ApplicationException
import org.myjaphoo.MyjaphooCorePrefs
import org.myjaphoo.model.util.UserDirectory

/**
 * Management functions for projects, like opening, creating, etc.
 *
 * @author mla
 * @version $Id$
 */
@TypeChecked
public class ProjectManagement {

    /**
     * Tries to open a project by its project path.
     * Throws an exception, if the path does not exist, or the path does not contain a proper project.
     *
     * @param dir
     * @return
     */
    static Project openProject(String dir) {
        checkProject(dir);
        return new Project(dir);
    }

    static def checkProject(String dir) {
        File fDir = new File(dir)
        if (!fDir.exists()) {
            throw new ApplicationException("Project dir $dir does not exist!")
        }
        if (!fDir.isDirectory()) {
            throw new ApplicationException("Project dir $dir is not a directory!")
        }
        File propFile = new File(dir, ProjectConfiguration.PREFERENCES_PROPERTIES)
        if (!propFile.exists()) {
            throw new ApplicationException("Project property file $propFile.canonicalPath does not exist!")
        }
    }


    static def createProjectFiles(String dir, ProjectDatabaseType databaseType, String dbName, String connection) {
        File fdir = new File(dir);
        if (!fdir.exists()) {
            fdir.mkdirs();
        }

        def projectConfiguration = new ProjectConfiguration(UserDirectory.getDirectory(), dir);
        def config = new MyjaphooCorePrefs(projectConfiguration);

        switch (databaseType) {
            case ProjectDatabaseType.H2:
                config.PRF_DB_USE_H2.setVal(true);

                break;
            case ProjectDatabaseType.Derby:
                config.PRF_DB_USE_H2.setVal(false);
                break;
        }
        // save a file name for the db, as this prop needs to override any settings in the user prop settings:
        config.PRF_DB_FILE.setVal(connection)
        config.PRF_DB_USE_OTHERCONFIGURATION.setVal(false)
        config.PRF_DB_USE_OTHERCONFIGURATION.commit();
        config.PRF_DATABASENAME.setVal(dbName)

    }
}
