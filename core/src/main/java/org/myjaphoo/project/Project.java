package org.myjaphoo.project;

import org.mlsoft.eventbus.GlobalBus;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.MyjaphooDBPreferences;
import org.myjaphoo.model.cache.EntityCacheActor;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.AttributedEntity;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.logic.dbhandling.WmDatabaseOpener;
import org.myjaphoo.model.util.UserDirectory;
import org.myjaphoo.plugin.Plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A Project bundles a database connection, project properties and configuration,
 * a name (based on the directory)
 *
 * @author mla
 * @version $Id$
 */
public class Project {

    public final EntityCacheActor cacheActor;

    public final DBConnection connection;

    public final String uniqueId;

    public final int uniqueHash;

    public final String projectDir;

    public final ProjectConfiguration projectConfiguration;

    public final MyjaphooCorePrefs prefs;
    public final MyjaphooDBPreferences dbPrefs;

    public final ProjectFileType Pictures;
    public final ProjectFileType Movies;
    public final ProjectFileType CompressedFiles;
    public final ProjectFileType Text;

    /**
     * a map where plugins could store any arbitrary values/objects.
     * The project object itself does not take care in any way of these values.
     * This is a common way to let a plugin store "state" relevant for a project during a projects live time.
     *
     */
    public final Map<String, Object> pluginProps = new HashMap<>();

    public Project(String projectDir) {
        this(projectDir, null);
    }

    public Project(String projectDir, DBConnection connection) {
        this.projectDir = projectDir;
        projectConfiguration = new ProjectConfiguration(UserDirectory.getDirectory(), projectDir);
        prefs = new MyjaphooCorePrefs(projectConfiguration);
        if (connection == null) {
            this.connection = WmDatabaseOpener.openDatabase(prefs);
        } else {
            this.connection = connection;
        }
        dbPrefs = new MyjaphooDBPreferences(this.connection);
        this.connection.open();
        uniqueId = this.connection.getConfiguration().getFilledConnectionUrl();
        uniqueHash = uniqueId.hashCode();

        cacheActor = new EntityCacheActorImpl(this.connection, GlobalBus.bus);
        Pictures = prefs.Pictures;
        Movies = prefs.Movies;
        CompressedFiles = prefs.CompressedFiles;
        Text = prefs.Text;
        Plugins.initProject(this);
    }

    public Token loadToken(Long id) {
        return connection.load(em -> em.find(Token.class, id));
    }

    public MetaToken loadMetaToken(Long id) {
        return connection.load(em -> em.find(MetaToken.class, id));
    }

    public MovieEntry loadMovieEntry(Long id) {
        return connection.load(em -> em.find(MovieEntry.class, id));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(uniqueId, project.uniqueId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uniqueId);
    }

    public AttributedEntity loadEntity(ImmutableAttributedEntity iae) {
        if (iae instanceof ImmutableMovieEntry) {
            return loadMovieEntry(((ImmutableMovieEntry) iae).getId());
        } else if (iae instanceof ImmutableToken) {
            return loadToken(((ImmutableToken) iae).getId());
        } else if (iae instanceof ImmutableMetaToken) {
            return loadMetaToken(((ImmutableMetaToken) iae).getId());
        }
        throw new IllegalArgumentException("internal error loading entity!");
    }
}
