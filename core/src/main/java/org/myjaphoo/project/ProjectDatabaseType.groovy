package org.myjaphoo.project

/**
 * DB types, which are supported out of the box.
 */
enum ProjectDatabaseType {

    H2,
    Derby
}