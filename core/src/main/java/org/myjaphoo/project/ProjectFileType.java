package org.myjaphoo.project;

import org.mlsoft.common.prefs.model.editors.StringVal;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.FileTypeInterface;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryWrapper;
import org.myjaphoo.model.db.MovieEntry;

/**
 * Interpreted file types depend on the project configuration.
 * This class handles the definition.
 *
 * @author mla
 * @version $Id$
 */
public class ProjectFileType implements FileTypeInterface {


    private StringVal pref;

    public ProjectFileType(Project project, String prefKey) {
        pref = (StringVal) project.prefs.searchByName(prefKey);
    }

    public ProjectFileType(MyjaphooCorePrefs prefs, String prefKey) {
        pref = (StringVal) prefs.searchByName(prefKey);
    }

    public String getFileFilter() {
        return pref.getVal();
    }

    public boolean is(EntryWrapper entry) {
        return is(entry.getName());
    }

    public boolean is(ImmutableMovieEntry entry) {
        return is(entry.getName());
    }


    public boolean is(MovieEntry entry) {
        return is(entry.getName());
    }

    public boolean is(String filename) {
        int dotIndex = filename.lastIndexOf('.');
        if (dotIndex == -1 | dotIndex >= filename.length() - 1) {
            return false;
        }
        String extension = filename.substring(dotIndex).toLowerCase();
        String picSuffixes = getFileFilter().toLowerCase();
        return picSuffixes.contains(extension);
    }
}
