package org.myjaphoo.project;

import org.myjaphoo.ApplicationException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * ProjectConfiguration
 *
 * @author mla
 * @version $Id$
 */
public class ProjectConfiguration {

    public static final String PREFERENCES_PROPERTIES = "preferences.properties";
    private ProjectProperties projectProperties;

    public final String projectConfigFile;

    public final String projectDir;

    public ProjectConfiguration(String appConfigDir, String projectDir) {
        this.projectDir = projectDir;
        Properties appProps = new Properties();

        loadProps(appProps, new File(appConfigDir, PREFERENCES_PROPERTIES));
        File projectConfFile = new File(projectDir, PREFERENCES_PROPERTIES);
        projectConfigFile = projectConfFile.getAbsolutePath();
        projectProperties = new ProjectProperties(appProps);
        loadProps(projectProperties, projectConfFile);
    }


    private void loadProps(Properties properties, File file) {
        if (file.exists()) {
            try (FileReader reader = new FileReader(file)) {
                properties.load(reader);
            } catch (IOException e) {
                throw new ApplicationException("error loading properties file!", e);
            }
        }
    }

    public ProjectProperties getProjectProperties() {
        return projectProperties;
    }
}
