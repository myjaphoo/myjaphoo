package org.myjaphoo.project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * ProjectConfiguration
 *
 * @author mla
 * @version $Id$
 */
public class ProjectProperties extends Properties {
    public ProjectProperties(Properties defaults) {
        super(defaults);
    }

    @Override
    public String getProperty(String key) {
        String fromSysProps = System.getProperties().getProperty(key);
        return fromSysProps != null ? fromSysProps : super.getProperty(key);
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        String fromSysProps = System.getProperties().getProperty(key);
        return fromSysProps != null ? fromSysProps : super.getProperty(key, defaultValue);
    }

    public List<String> getList(String baseName) {
        TreeMap<Integer, String> filtered = new TreeMap<>();
        for (Map.Entry entry : super.entrySet()) {
            if (entry.getKey().toString().startsWith(baseName)) {
                int index = Integer.parseInt(entry.getKey().toString().replace(baseName, "").replace(".", ""));
                filtered.put(index, entry.getValue() != null ? entry.getValue().toString() : null);
            }
        }
        return new ArrayList<>(filtered.values());
    }

    public void setList(String baseName, List<String> list) {
        deleteList(baseName);
        int index = 0;
        for (String val : list) {
            setProperty(baseName + "." + index, val);
            index++;
        }
    }

    private void deleteList(String baseName) {
        Iterator<Map.Entry<Object, Object>> iterator = super.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Object, Object> next = iterator.next();
            if (next.getKey().toString().startsWith(baseName)) {
                iterator.remove();
            }
        }
    }
}
