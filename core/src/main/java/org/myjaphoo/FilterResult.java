package org.myjaphoo;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * FilterResult
 *
 * @author mla
 * @version $Id$
 */
public class FilterResult {
    public final ImmutableModel model;
    public final List<JoinedDataRow> filteredRows;

    public final List<String> usedLiterals;

    public FilterResult(ImmutableModel model, List<JoinedDataRow> filteredRows, List<String> usedLiterals) {
        this.model = model;
        this.filteredRows = filteredRows;
        this.usedLiterals = usedLiterals;
    }

    public int calcNumOfDistinctMovies() {
        return getDistinctEntries(filteredRows).size();
    }

    public static Set<ImmutableMovieEntry> getDistinctEntries(List<JoinedDataRow> allEntries) {
        Set<ImmutableMovieEntry> distinctEntries = new HashSet<>(allEntries.size());
        for (JoinedDataRow cr : allEntries) {
            distinctEntries.add(cr.getEntry());
        }
        return distinctEntries;
    }
}
