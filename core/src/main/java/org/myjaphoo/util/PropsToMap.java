package org.myjaphoo.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * PropsToMap
 */
public class PropsToMap {

    private HashSet<String> excludedMethods = new HashSet<>();

    /**
     * Linked list of converters. They are searched in input order and checked if the target class
     * is assigneable from the registered converter.
     */
    private LinkedHashMap<Class, ObjectToMapConverter> converter = new LinkedHashMap<>();

    public PropsToMap() {
        addConverter(Map.class, new MapToMapConverter());
        excludeMethod("getClass");
        excludeMethod("getMetaClass");
    }

    /**
     * Adds a converter or overrides a converter for the class parameter. The order of added converters
     * is essential, because they are checked in input order if the target class is assigneable to the
     * registered class of the converter.
     *
     * @param aClass
     * @param objectToMapConverter
     */
    public void addConverter(Class aClass, ObjectToMapConverter objectToMapConverter) {
        converter.put(aClass, objectToMapConverter);
    }

    public void excludeMethod(String methodName) {
        excludedMethods.add(methodName);
    }

    public Map<String, Object> toMap(Object obj) {
        return convertObjectToMap(obj);
    }

    private Map<String, Object> convertObjectToMap(Object obj) {

        IdentityHashMap traversedObjs = new IdentityHashMap();
        Map<String, Object> map = new HashMap<>();
        try {
            convertObjectToMap(traversedObjs, "", map, obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    private void convertObjectToMap(
        IdentityHashMap traversedObjs, String prefix, Map<String, Object> map, Object obj
    ) throws
      IllegalAccessException,
      IllegalArgumentException,
      InvocationTargetException {
        if (obj == null) {
            return;
        }

        if (traversedObjs.put(obj, obj) == null) {
            Method[] methods = obj.getClass().getMethods();

            for (Method m : methods) {
                String name = m.getName();
                if (name.startsWith("get")
                    && name.length() > 3
                    && m.getParameterCount() == 0
                    && !excludedMethods.contains(name)) {

                    Object value = (Object) m.invoke(obj);
                    String propName = prefix + name.substring(3);
                    if (isBasic(value)) {
                        map.put(propName, (Object) value);
                    } else {
                        ObjectToMapConverter converter = findConverter(value.getClass());
                        if (converter != null) {
                            converter.convert(traversedObjs, propName + ".", map, value);
                        } else {
                            convertObjectToMap(traversedObjs, propName + ".", map, value);
                        }
                    }
                }
            }
        }
    }

    private ObjectToMapConverter findConverter(Class<?> aClass) {
        for (Map.Entry<Class, ObjectToMapConverter> entry : converter.entrySet()) {
            if (entry.getKey().isAssignableFrom(aClass)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private static Set<Class> basicClasses = new HashSet<>(Arrays.asList(
        String.class,
        Integer.class,
        Boolean.class,
        Character.class,
        Byte.class,
        Short.class,
        Integer.class,
        Long.class,
        Float.class,
        Double.class,
        java.util.Calendar.class,
        java.util.Date.class

    ));

    private boolean isBasic(Object value) {
        if (value == null) {
            return true;
        }
        if (value.getClass().isPrimitive()) {
            return true;
        }
        if (value.getClass().isEnum()) {
            return true;
        }
        return basicClasses.contains(value.getClass());
    }
}
