package org.myjaphoo.util;

import java.util.ResourceBundle;

/**
 * AppProps
 */
public class AppProps {
    private static ResourceBundle resourceMap = ResourceBundle.getBundle("MyjaphooApp");

    public final static String title = resourceMap.getString("Application.title");
    public final static String homepagelink = resourceMap.getString("Application.homepage");
    public final static String homepagelinkDocs = resourceMap.getString("Application.homepage.docs");

    public final static String versionNumber = resourceMap.getString("Application.versionNumber");
    public final static String buildNumber = resourceMap.getString("Application.buildNumber");
    public final static String buildDate = resourceMap.getString("Application.buildDate");
    public final static String mercurialRevision = resourceMap.getString("Application.revision");
    public final static String buildTag = resourceMap.getString("Application.tag");

}
