package org.myjaphoo.util;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * MapToMapConverter
 */
public class MapToMapConverter implements ObjectToMapConverter {
    @Override
    public void convert(
        IdentityHashMap traversedObjs, String prefix, Map<String, Object> result, Object value
    ) {
        Map<Object, Object> map = (Map<Object, Object>) value;
        for (Map.Entry entry : map.entrySet()) {
            String key = prefix + entry.getKey().toString();
            Object val = entry.getValue() != null ? entry.getValue() : null;
            result.put(key, val);
        }
    }
}
