package org.myjaphoo.util;

import org.apache.commons.lang.ObjectUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * ComparatorByProperties
 *
 * @author mla
 * @version $Id$
 */
public class ComparatorByProperties {

    private PropsToMap propsToMap = new PropsToMap();

    public boolean isEqualProps(Object o1, Object o2) {
        Map<String, Object> map1 = propsToMap.toMap(o1);
        Map<String, Object> map2 = propsToMap.toMap(o2);
        return ObjectUtils.equals(map1, map2);
    }


    public static class Diff {
        public final Object o1;
        public final Object o2;

        public Diff(Object o1, Object o2) {
            this.o1 = o1;
            this.o2 = o2;
        }

        @Override
        public String toString() {
            return Objects.toString(o1) + "<>" + Objects.toString(o2);
        }
    }

    public Map<String, Diff> getDiffProps(Object o1, Object o2) {
        Map<String, Diff> result = new HashMap<>();

        Map<String, Object> map1 = propsToMap.toMap(o1);
        Map<String, Object> map2 = propsToMap.toMap(o2);

        Set<String> keys = new HashSet<>(map1.keySet());
        keys.addAll(map2.keySet());
        for (String key : keys) {
            Object v1 = map1.get(key);
            Object v2 = map2.get(key);
            if (!Objects.equals(v1, v2)) {
                result.put(key, new Diff(v1, v2));
            }
        }
        return result;
    }
}
