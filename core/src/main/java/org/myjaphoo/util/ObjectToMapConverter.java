package org.myjaphoo.util;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * ObjectToMapConverter
 */
public interface ObjectToMapConverter {
    void convert(IdentityHashMap traversedObjs, String prefix, Map<String, Object> result, Object value);
}
