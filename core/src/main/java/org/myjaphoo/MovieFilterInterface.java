/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.myjaphoo.model.db.DataView;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.processing.ProcessingRequirementInformation;

import java.util.List;


/**
 * Loader interface, um auf gefilterte movie entries zuzugreigen.
 * Hier giebt es eine konventionelle implementierung, die alles jeweils aus der DB mittels hibernate holt,
 * und eine, die Daten cached u. diese verwendet.
 *
 * @author mla
 */
public interface MovieFilterInterface {

    FilterResult loadFilteredEntries(
        DataView dataView, List<? extends ProcessingRequirementInformation> groupingAlgorithm
    ) throws ParserException;
}
