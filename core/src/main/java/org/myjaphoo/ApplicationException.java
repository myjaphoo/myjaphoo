package org.myjaphoo;

/**
 * An exception thrown by the application for errors that might happen during execution
 * based on wrong user interaction or system faults.
 * This kind of exception is normally intended to continue with the application execution
 * and only stops a certain user action.
 */
public class ApplicationException extends RuntimeException {

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

}
