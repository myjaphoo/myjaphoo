package org.myjaphoo;

import org.myjaphoo.model.FileSubstitution;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.dbcompare.DatabaseComparison;
import org.myjaphoo.model.filterparser.FileTypes;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.logic.FileSubstitutionImpl;
import org.myjaphoo.project.Project;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Bundles information needed to execute a filter expression via the
 * CommonMovieFilter.
 *
 * @author mla
 * @version $Id$
 */
public class CommonFilterContext {


    public final DatabaseComparison databaseComparison;

    public final Supplier<Map<String, Substitution>> substitutionSupplier;

    private Supplier<ImmutableModel> modelSupplier;

    public final FileTypes fileTypes;

    public final FileSubstitution fileSubstitution;

    public CommonFilterContext(
        Project project,
        DatabaseComparison databaseComparison,
        Supplier<Map<String, Substitution>> substitutionSupplier
    ) {
        this(
            () -> project.cacheActor.getImmutableModel(),
            databaseComparison,
            substitutionSupplier,
            project.prefs,
            new FileSubstitutionImpl(project)
        );
    }

    public CommonFilterContext(
        Supplier<ImmutableModel> modelSupplier,
        DatabaseComparison databaseComparison,
        Supplier<Map<String, Substitution>> substitutionSupplier,
        MyjaphooCorePrefs prefs,
        FileSubstitution fileSubstitution
    ) {
        this(modelSupplier, databaseComparison, substitutionSupplier, prefs.createFileTypes(), fileSubstitution);
    }

    public CommonFilterContext(
        Supplier<ImmutableModel> modelSupplier,
        DatabaseComparison databaseComparison,
        Supplier<Map<String, Substitution>> substitutionSupplier,
        FileTypes fileTypes,
        FileSubstitution fileSubstitution
    ) {
        this.modelSupplier = modelSupplier;
        this.databaseComparison = databaseComparison;
        this.substitutionSupplier = substitutionSupplier;
        this.fileTypes = fileTypes;
        this.fileSubstitution = fileSubstitution;
    }

    public Supplier<ImmutableModel> getModelSupplier() {
        return modelSupplier;
    }
}
