/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.processing;

import org.myjaphoo.model.cache.EntityCacheActor;

/**
 * @author mla
 */
public class DelayedCacheActorEventsWrapper<T> implements ListProcessor<T> {

    private ListProcessor<T> delegator;

    private EntityCacheActor cacheActor;

    public DelayedCacheActorEventsWrapper(EntityCacheActor cacheActor, ListProcessor<T> delegator) {
        this.cacheActor = cacheActor;
        this.delegator = delegator;
    }

    @Override
    public void startProcess() {
        delegator.startProcess();
        cacheActor.accumulateEvents();
    }

    @Override
    public synchronized void process(final T entry) throws Exception {
        delegator.process(entry);
    }

    @Override
    public void stopProcess() {
        cacheActor.fireAllAccumulatedEvents();
        delegator.stopProcess();
    }

    @Override
    public String shortName(T t) {
        return delegator.shortName(t);
    }

    @Override
    public String longName(T t) {
        return delegator.shortName(t);
    }
};
