/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.processing;

import org.myjaphoo.project.Project;


/**
 * @author mla
 */
public class TransactionalProcessWrapper<T> implements ListProcessor<T> {

    private ListProcessor<T> delegator;
    private Project project;

    public TransactionalProcessWrapper(Project project, ListProcessor<T> delegator) {
        this.delegator = delegator;
        this.project = project;
    }

    @Override
    public void startProcess() {
        project.connection.commit(em -> delegator.startProcess());
    }

    @Override
    public void process(final T entry) throws Exception {
        project.connection.commit(em -> delegator.process(entry));
    }

    @Override
    public void stopProcess() {
        project.connection.commit(em -> delegator.stopProcess());
    }

    @Override
    public String shortName(T t) {
        return delegator.shortName(t);
    }

    @Override
    public String longName(T t) {
        return delegator.longName(t);
    }
};
