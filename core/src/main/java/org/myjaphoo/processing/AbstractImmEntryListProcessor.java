/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.processing;

import org.myjaphoo.model.cache.ImmutableMovieEntry;

/**
 * Abstract imlementation, welche keine implementierung für start- und ende hat.
 *
 * @author mla
 */
public abstract class AbstractImmEntryListProcessor implements ListProcessor<ImmutableMovieEntry> {

    @Override
    public void startProcess() {
    }


    @Override
    public void stopProcess() {
    }

    @Override
    public String shortName(ImmutableMovieEntry t) {
        return t.getName();
    }

    @Override
    public String longName(ImmutableMovieEntry t) {
        return t.getCanonicalPath();
    }


}
