/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.processing;

import org.myjaphoo.model.cache.zipper.EntryRef;

/**
 * Abstract imlementation, welche keine implementierung für start- und ende hat.
 *
 * @author mla
 */
public abstract class AbstractEntryRefListProcessor implements ListProcessor<EntryRef> {

    @Override
    public void startProcess() {
    }


    @Override
    public void stopProcess() {
    }

    @Override
    public String shortName(EntryRef t) {
        return t.ref.getName();
    }

    @Override
    public String longName(EntryRef t) {
        return t.ref.getCanonicalPath();
    }


}
