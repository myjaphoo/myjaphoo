/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when tags get unassigned.
 *
 * @author lang
 */
public class TagsUnassigendEvent extends AbstractTagsChangedEvent {
    public TagsUnassigendEvent(ImmutableModel model) {
        super(model);
    }
}
