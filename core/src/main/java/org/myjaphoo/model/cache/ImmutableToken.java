package org.myjaphoo.model.cache;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.db.TokenType;

/**
 * ImmutableToken
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableToken extends ImmutableUUIDEntity implements Comparable<ImmutableToken>, ImmutableAttributedEntity {

    private String name;

    private TokenType tokentype = TokenType.UNBESTIMMT;

    private String description;

    private Map<String, String> attributes = null;

    public ImmutableToken(Token token) {
        super(token);
        this.name = token.getName();
        this.tokentype = token.getTokentype();
        this.description = token.getDescription();
        this.attributes = HashMap.ofAll(token.getAttributes());
    }

    public Token createMutable() {
        Token token = new Token();
        copyToMutable(token);
        return token;
    }
    public void copyToMutable(Token token) {
        token.setUuid(getUuid());
        token.setCreatedMs(getCreatedMs());
        token.setName(name);
        token.setTokentype(tokentype);
        token.setDescription(description);
        token.setAttributes(attributes.toJavaMap());
    }

    public String getName() {
        return name;
    }

    public TokenType getTokentype() {
        return tokentype;
    }

    public String getDescription() {
        return description;
    }

    public String getComment() {
        return description;
    }

    @Override
    public Map<String, String> getAttributes() {
        return attributes;
    }

    public Set<ImmutableMovieEntry> getMovieEntries(ImmutableModel model) {
        return model.getEntryToken().getAssignedX(this).getOrElse(HashSet.empty());
    }

    public Set<ImmutableMetaToken> getAssignedMetaTokens(ImmutableModel model) {
        return model.getTokenMetaToken().getAssignedY(this).getOrElse(HashSet.empty());
    }

    @Override
    public int compareTo(ImmutableToken o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return name;
    }

}
