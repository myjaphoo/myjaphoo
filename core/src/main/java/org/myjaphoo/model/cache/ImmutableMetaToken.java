package org.myjaphoo.model.cache;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.myjaphoo.model.db.MetaToken;


/**
 * ImmutableMetaToken
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableMetaToken extends ImmutableUUIDEntity implements Comparable<ImmutableMetaToken>, ImmutableAttributedEntity {

    private String name;

    private String description;

    private Map<String, String> attributes;

    public ImmutableMetaToken(MetaToken metaToken) {
        super(metaToken);
        this.name = metaToken.getName();
        this.description = metaToken.getDescription();
        this.attributes = HashMap.ofAll(metaToken.getAttributes());
    }

    public MetaToken createMutable() {
        MetaToken mt = new MetaToken();
        copyToMutable(mt);
        return mt;
    }

    public void copyToMutable(MetaToken mt) {
        mt.setUuid(getUuid());
        mt.setCreatedMs(getCreatedMs());
        mt.setName(name);
        mt.setDescription(description);
        mt.setAttributes(attributes.toJavaMap());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getComment() {
        return description;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    @Override
    public int compareTo(ImmutableMetaToken o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return name;
    }

}
