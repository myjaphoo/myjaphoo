package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableUUIDEntity;

import java.util.Iterator;

/**
 * ZipperIterator
 *
 * @author mla
 * @version $Id$
 */
public class ZipperIterator<T extends ImmutableUUIDEntity, Z extends Zipper<T>> implements Iterator<Z> {

    private Iterator<T> iterator;

    private Z zipper;

    public ZipperIterator(Iterator<T> iterator, Z zipper) {
        this.iterator = iterator;
        this.zipper = zipper;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Z next() {
        zipper.setRef(iterator.next());
        return zipper;
    }
}
