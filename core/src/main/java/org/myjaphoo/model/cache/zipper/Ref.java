package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableUUIDEntity;
import org.myjaphoo.model.cache.zipper.Zipper;

/**
 * Ref
 *
 * @author mla
 * @version $Id$
 */
public interface Ref<T extends ImmutableUUIDEntity> {

    ImmutableModel getModel();

    T getRef();

    Zipper<T> toZipper();
}
