/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when movies get changed.
 *
 * @author lang
 */
public class MoviesChangedEvent extends AbstractMoviesChangedEvent {

    public MoviesChangedEvent(ImmutableModel model) {
        super(model);
    }
}
