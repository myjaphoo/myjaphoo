/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when tags get deleted.
 *
 * @author lang
 */
public class TagsDeletedEvent extends AbstractTagsChangedEvent {
    public TagsDeletedEvent(ImmutableModel model) {
        super(model);
    }
}
