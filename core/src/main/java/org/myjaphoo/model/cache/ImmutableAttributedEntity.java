package org.myjaphoo.model.cache;

import io.vavr.collection.Map;

import java.util.Date;

/**
 * AttributedEntity
 *
 * @author lang
 * @version $Id$
 */
public interface ImmutableAttributedEntity {
    String getName();

    String getComment();

    Map<String, String> getAttributes();

    Long getCreatedMs();

    Date getCreated();

    Date getEdited();

    String getUuid();

}
