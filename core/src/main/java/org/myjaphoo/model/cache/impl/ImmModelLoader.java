/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.impl;

import org.apache.commons.lang.time.StopWatch;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.logic.MetaTokenJpaController;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.TokenJpaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;

/**
 * Builds the immutable model from db entities.
 */
public class ImmModelLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImmModelLoader.class);
    private DBConnection dbConn;

    public ImmModelLoader(DBConnection dbConn) {
        this.dbConn = dbConn;
    }


    public ImmutableModel load() {
        EntityManager em = dbConn.getEmf().createEntityManager();
        try {
            return load(em);
        } finally {
            em.close();
        }
    }

    /**
     * Loads the entity model based on a entity manager. Doesn´t depend on our own database connection
     * and is therefore useful when dealing in managed environments.
     *
     * @param em
     *
     * @return
     */
    public static ImmutableModel load(EntityManager em) {
        StopWatch watch = new StopWatch();
        watch.start();
        LOGGER.info("**** creating MovieEntryCache!");
        LOGGER.info("* loading movies...");
        HashSet<MovieEntry> uniqueEntries = new HashSet<>(MovieEntryJpaController.fetchAll(em));

        LOGGER.info("* loading tokens...");
        List<Token> allTokens = TokenJpaController.fetchAll(em);
        LOGGER.info("* loading meta tokens...");
        List<MetaToken> allMetaTokens = MetaTokenJpaController.fetchAll(em);
        LOGGER.info("* building internal immutable model...");
        HashSet<Token> uniqueTokens = new HashSet<>(allTokens);
        HashSet<MetaToken> uniqueMetaTokens = new HashSet<>(allMetaTokens);
        ImmutableModel immutableModel = new ImmutableModel(uniqueEntries, uniqueTokens, uniqueMetaTokens);
        watch.stop();
        LOGGER.info("**** finished creating internal entity cache! duration " + watch.toString());
        return immutableModel;
    }
}
