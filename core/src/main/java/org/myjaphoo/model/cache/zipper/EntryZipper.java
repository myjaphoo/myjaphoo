package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.Immutables;

/**
 * EntryZipper
 *
 * @author mla
 * @version $Id$
 */
public class EntryZipper extends EntryWrapper implements Zipper<ImmutableMovieEntry>, ImmutableAttributedEntityZipper {

    private ImmutableMovieEntry ref;

    public EntryZipper(ImmutableModel model, ImmutableMovieEntry ref) {
        super(model);
        this.ref = ref;
    }

    @Override
    public Iterable<? extends ImmutableAttributedEntityZipper> getReferences() {
        return getTokenZippers();
    }

    @Override
    public ImmutableMovieEntry getRef() {
        return ref;
    }

    @Override
    public void setRef(ImmutableMovieEntry ref) {
        this.ref = ref;
    }

    @Override
    public EntryRef toRef() {
        return new EntryRef(model, ref);
    }


}
