/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event, when meta tags get deleted.
 *
 * @author lang
 */
public class MetaTagsDeletedEvent extends AbstractMetaTagsChangedEvent {


    public MetaTagsDeletedEvent(ImmutableModel model) {
        super(model);
    }
}
