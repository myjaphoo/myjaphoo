/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when tags get changed.
 *
 * @author lang
 */
public class TagsChangedEvent extends AbstractTagsChangedEvent {

    public TagsChangedEvent(ImmutableModel model) {
        super(model);
    }
}
