package org.myjaphoo.model.cache;

import io.vavr.collection.List;

/**
 * Changes
 *
 * @author mla
 * @version $Id$
 */
public class Changes {

    private List<ImmutableMovieEntry> changedEntries = List.empty();

    private List<ImmutableToken> changedTokens = List.empty();

    private List<ImmutableMetaToken> changedMetaTokens = List.empty();
    private ImmutableModel model;

    public ImmutableToken add(ImmutableToken immutableToken) {
        changedTokens = changedTokens.prepend(immutableToken);
        return immutableToken;
    }

    public ImmutableMetaToken add(ImmutableMetaToken immutableMetaToken) {
        changedMetaTokens = changedMetaTokens.prepend(immutableMetaToken);
        return immutableMetaToken;
    }

    public ImmutableMovieEntry add(ImmutableMovieEntry immutableMovieEntry) {
        changedEntries = changedEntries.prepend(immutableMovieEntry);
        return immutableMovieEntry;
    }

    public void setModel(ImmutableModel model) {
        this.model = model;
    }

    public ImmutableModel getModel() {
        return model;
    }

    public List<ImmutableMovieEntry> getChangedEntries() {
        return changedEntries;
    }

    public List<ImmutableToken> getChangedTokens() {
        return changedTokens;
    }

    public List<ImmutableMetaToken> getChangedMetaTokens() {
        return changedMetaTokens;
    }

}
