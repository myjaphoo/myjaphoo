package org.myjaphoo.model.cache;

import org.myjaphoo.model.db.MovieAttrs;

/**
 * ImmutableMovieAttrs
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableMovieAttrs {

    private int width;
    private int height;
    /**
     * länge in sekunden.
     */
    private int length;
    private int fps;
    private int bitrate;
    private String format;

    public ImmutableMovieAttrs(MovieAttrs movieAttrs) {
        this.width = movieAttrs.getWidth();
        this.height = movieAttrs.getHeight();
        this.length = movieAttrs.getLength();
        this.fps = movieAttrs.getFps();
        this.bitrate = movieAttrs.getBitrate();
        this.format = movieAttrs.getFormat();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public int getFps() {
        return fps;
    }

    public int getBitrate() {
        return bitrate;
    }

    public String getFormat() {
        return format;
    }

    public MovieAttrs createMutable() {
        MovieAttrs movieAttrs = new MovieAttrs();
        movieAttrs.setWidth(getWidth());
        movieAttrs.setHeight(getHeight());
        movieAttrs.setLength(getLength());
        movieAttrs.setFps(getFps());
        movieAttrs.setBitrate(getBitrate());
        movieAttrs.setFormat(getFormat());
        return movieAttrs;
    }
}
