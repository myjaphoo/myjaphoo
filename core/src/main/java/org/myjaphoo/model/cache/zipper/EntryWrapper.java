package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.ImmutableExifData;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieAttrs;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.Rating;

import java.util.Date;
import java.util.Objects;

import static org.myjaphoo.model.cache.Immutables.newTokenZipperIterable;

/**
 * EntryZipper
 *
 * @author mla
 * @version $Id$
 */
public abstract class EntryWrapper implements ImmutableAttributedEntity {

    public final ImmutableModel model;

    public EntryWrapper(ImmutableModel model) {
        this.model = Objects.requireNonNull(model);
    }

    abstract public ImmutableMovieEntry getRef();

    public Long getId() {
        return getRef().getId();
    }

    public Date getCreated() {
        return getRef().getCreated();
    }

    public String getUuid() {
        return getRef().getUuid();
    }

    public Long getCreatedMs() {
        return getRef().getCreatedMs();
    }

    public Date getEdited() {
        return getRef().getEdited();
    }

    public String getName() {
        return getRef().getName();
    }

    public String getCanonicalDir() {
        return getRef().getCanonicalDir();
    }

    public long getFileLength() {
        return getRef().getFileLength();
    }

    public Long getChecksumCRC32() {
        return getRef().getChecksumCRC32();
    }

    public String getSha1() {
        return getRef().getSha1();
    }

    public Rating getRating() {
        return getRef().getRating();
    }

    public ImmutableMovieAttrs getMovieAttrs() {
        return getRef().getMovieAttrs();
    }

    public String getTitle() {
        return getRef().getTitle();
    }

    public String getComment() {
        return getRef().getComment();
    }

    public ImmutableExifData getExifData() {
        return getRef().getExifData();
    }

    public Map<String, String> getAttributes() {
        return getRef().getAttributes();
    }

    /**
     * @return the canonicalPath
     */
    public String getCanonicalPath() {
        return getRef().getCanonicalPath();
    }

    public ImmutableModel getModel() {
        return model;
    }

    public Set<ImmutableToken> getTokenSet() {
        return getRef().getTokens(model);
    }

    public Iterable<TokenZipper> getTokenZippers() {
        return newTokenZipperIterable(model, getRef().getTokens(model));
    }
}
