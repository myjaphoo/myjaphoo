package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableAttributedEntity;

import java.util.Iterator;

/**
 * AttributedEntity
 *
 * @author lang
 * @version $Id$
 */
public interface ImmutableAttributedEntityZipper {

    Iterable<? extends ImmutableAttributedEntityZipper> getReferences();

    Ref<? extends ImmutableAttributedEntity> toRef();

    ImmutableAttributedEntity getRef();
}
