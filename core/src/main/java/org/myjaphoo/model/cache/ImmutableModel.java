package org.myjaphoo.model.cache;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.Tree;
import io.vavr.control.Option;
import org.myjaphoo.model.DuplicateHashMap;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.vavr.collection.HashSet.empty;
import static java.util.Collections.emptyList;

/**
 * ImmutableModel
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableModel {

    private Changes changes;

    private final TreeRel2<ImmutableToken, Token> tokenTree;

    private final TreeRel2<ImmutableMetaToken, MetaToken> metaTokenTree;

    private final ImmutableEntryList entryList;

    private Relation<ImmutableMovieEntry, ImmutableToken> entryToken;

    private Relation<ImmutableToken, ImmutableMetaToken> tokenMetaToken;

    public static ImmutableModel EMPTY_MODEL = new ImmutableModel(emptyList(), emptyList(), emptyList());

    public ImmutableModel(Collection<MovieEntry> entries, Collection<Token> tokens, Collection<MetaToken> metaTokens) {
        entryToken = new Relation<>();
        tokenMetaToken = new Relation<>();

        Map<MovieEntry, ImmutableMovieEntry> entryMap = new HashMap<>();
        Function<MovieEntry, ImmutableMovieEntry> entryMapper = m -> entryMap.computeIfAbsent(m, k -> k.toImmutable());

        Map<Token, ImmutableToken> tokMap = new HashMap<>();
        Function<Token, ImmutableToken> tokMapper = t -> tokMap.computeIfAbsent(t, k -> k.toImmutable());

        Map<MetaToken, ImmutableMetaToken> mtokMap = new HashMap<>();
        Function<MetaToken, ImmutableMetaToken> mtokMapper = t -> mtokMap.computeIfAbsent(t, k -> k.toImmutable());

        tokenTree = new TreeRel2<>(tokens, tokMapper);
        metaTokenTree = new TreeRel2<>(metaTokens, mtokMapper);

        entryList = new ImmutableEntryList(List.ofAll(entries.stream().map(entryMapper).collect(
            Collectors.toList())));
        for (Token token : tokens) {
            ImmutableToken it = tokMapper.apply(token);
            for (MovieEntry entry : token.getMovieEntries()) {
                ImmutableMovieEntry ie = entryMapper.apply(entry);
                entryToken = entryToken.relation(ie, it);
            }
        }
        for (MetaToken metaToken : metaTokens) {
            ImmutableMetaToken immutableMetaToken = mtokMapper.apply(metaToken);
            for (Token token : metaToken.getAssignedTokens()) {
                ImmutableToken immutableToken = tokMapper.apply(token);
                tokenMetaToken = tokenMetaToken.relation(immutableToken, immutableMetaToken);
            }
        }
    }

    public ImmutableModel(
        TreeRel2<ImmutableToken, Token> tokenTree,
        TreeRel2<ImmutableMetaToken, MetaToken> metaTokenTree,
        ImmutableEntryList entryList,
        Relation<ImmutableMovieEntry, ImmutableToken> entryToken,
        Relation<ImmutableToken, ImmutableMetaToken> tokenMetaToken,
        Changes changes
    ) {
        this.tokenTree = tokenTree;
        this.metaTokenTree = metaTokenTree;
        this.entryList = entryList;
        this.entryToken = entryToken;
        this.tokenMetaToken = tokenMetaToken;
        this.changes = changes;
        this.changes.setModel(this);
    }

    private ImmutableModelBuilder createBuilder() {
        return new ImmutableModelBuilder(this);
    }

    public ImmutableModel assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token) {
        return createBuilder().assignMetaTokenToToken(metaToken, token);
    }

    public ImmutableModel assignToken2MovieEntry(ImmutableToken token, java.util.List<ImmutableMovieEntry> movies) {
        return createBuilder().assignToken2MovieEntry(token, movies);
    }

    public ImmutableModel createMetaToken(MetaToken mt, ImmutableMetaToken parentToken) {
        return createBuilder().createMetaToken(mt, parentToken);
    }

    public ImmutableModel createToken(Token token, ImmutableToken parentToken) {
        return createBuilder().createToken(token, parentToken);
    }

    public ImmutableModel editMetaToken(MetaToken mt) {
        return createBuilder().editMetaToken(mt);
    }

    public ImmutableModel editMovie(MovieEntry entry) {
        return createBuilder().editMovie(entry);
    }


    public ImmutableModel editToken(Token token) {
        return createBuilder().editToken(token);
    }

    public ImmutableModel moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move) {
        return createBuilder().moveMetaTokens(tokenParent, token2Move);
    }


    public ImmutableModel moveToken(ImmutableToken newParent, ImmutableToken token2Move) {
        return createBuilder().moveToken(newParent, token2Move);
    }

    public ImmutableModel removeMetaToken(ImmutableMetaToken mt) {
        return createBuilder().removeMetaToken(mt);
    }


    public ImmutableModel removeToken(final ImmutableToken token) {
        return createBuilder().removeToken(token);
    }

    public ImmutableModel unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, java.util.List<ImmutableToken> toks2Remove) {
        return createBuilder().unAssignMetaTokenFromToken(currMetaToken, toks2Remove);
    }


    public ImmutableModel unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies) {
        return createBuilder().unassignTokenFromMovies(token, movies);
    }

    public ImmutableModel removeMovieEntry(ImmutableMovieEntry entry) {
        return createBuilder().removeMovieEntry(entry);
    }

    public ImmutableModel newMovie(MovieEntry entry) {
        return createBuilder().newMovie(entry);
    }

    public TreeRel2<ImmutableToken, Token> getTokenTree() {
        return tokenTree;
    }

    public TreeRel2<ImmutableMetaToken, MetaToken> getMetaTokenTree() {
        return metaTokenTree;
    }

    public List<ImmutableMovieEntry> getEntryList() {
        return entryList.getEntryList();
    }

    public DuplicateHashMap getDupHashMap() {
        return entryList.getDuplicateHashMap();
    }

    public Relation<ImmutableMovieEntry, ImmutableToken> getEntryToken() {
        return entryToken;
    }

    public Relation<ImmutableToken, ImmutableMetaToken> getTokenMetaToken() {
        return tokenMetaToken;
    }

    public Changes getChanges() {
        return changes;
    }

    ImmutableEntryList getImmutableEntryList() {
        return entryList;
    }

    public Option<ImmutableToken> getTokenByName(String tokenName) {
        return getTokenTree().getValues().find(t -> t.getName().equals(tokenName));
    }

    public Option<ImmutableMetaToken> getMetaTokenByName(String tokenName) {
        return getMetaTokenTree().getValues().find(t -> t.getName().equals(tokenName));
    }

    public List<Tree.Node<ImmutableToken>> getTagChildren(ImmutableToken token) {
        return getTokenTree().getChildren(token);
    }

    public Set<ImmutableMovieEntry> getAssignedEntries(ImmutableToken token) {
        return getEntryToken().getAssignedX(token).getOrElse(empty());
    }

    public long sizeOfMovies(ImmutableToken token) {
        long size = 0;
        for (ImmutableMovieEntry entry : getAssignedEntries(token)) {
            size += entry.getFileLength();
        }
        return size;
    }

    public Option<Set<ImmutableMetaToken>> getAssignedMetaTokens(ImmutableToken token) {
        return tokenMetaToken.getAssignedY(token);
    }

    public ImmutableToken getParentTag(ImmutableToken tag) {
        return tokenTree.getParent(tag);
    }

    public EntryRef entryRef(ImmutableMovieEntry entry) {
        return new EntryRef(this, entry);
    }
}
