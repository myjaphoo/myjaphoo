package org.myjaphoo.model.cache;

import org.apache.sanselan.formats.tiff.constants.TiffConstants;
import org.myjaphoo.model.db.exif.ExifData;
import org.myjaphoo.model.db.exif.ExifDataObject;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * ImmutableExifData
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableExifData implements ExifDataObject {

    private String exifMake;
    private String exifModel;

    private Date exifCreateDate;
    private String exifFnumber;
    private String exifIso;
    private String exifExposureTime;
    private Double longitude;
    private Double latitude;

    private Map<String, String> exifValues = null;

    public ImmutableExifData(ExifData exifData) {
        this.exifMake = exifData.getExifMake();
        this.exifModel = exifData.getExifModel();
        this.exifCreateDate = exifData.getExifCreateDate();
        this.exifFnumber = exifData.getExifFnumber();
        this.exifIso = exifData.getExifIso();
        this.exifExposureTime = exifData.getExifExposureTime();
        this.longitude = exifData.getLongitude();
        this.latitude = exifData.getLatitude();
        this.exifValues = Collections.unmodifiableMap(exifData.getExifValues());
    }

    public String getExifMake() {
        return exifMake;
    }

    public String getExifModel() {
        return exifModel;
    }

    public Date getExifCreateDate() {
        return exifCreateDate;
    }

    public String getExifFnumber() {
        return exifFnumber;
    }

    public String getExifIso() {
        return exifIso;
    }

    public String getExifExposureTime() {
        return exifExposureTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Map<String, String> getExifValues() {
        return exifValues;
    }

    @Override
    public Object getExifValue(String tagName) {
        if (tagName.equals(TiffConstants.EXIF_TAG_MAKE.name)) {
            return exifMake;
        } else if (tagName.equals(TiffConstants.EXIF_TAG_MODEL.name)) {
            return exifModel;
        } else if (tagName.equals(TiffConstants.EXIF_TAG_CREATE_DATE.name)) {
            return exifCreateDate;
        } else if (tagName.equals(TiffConstants.EXIF_TAG_FNUMBER.name)) {
            return exifFnumber;
        } else if (tagName.equals(TiffConstants.EXIF_TAG_ISO.name)) {
            return exifIso;
        } else if (tagName.equals(TiffConstants.EXIF_TAG_EXPOSURE_TIME.name)) {
            return exifExposureTime;
        } else {
            return getExifValues().get(tagName);
        }
    }

    public ExifData createMutable() {
        ExifData exifData = new ExifData(
            exifMake,
            exifModel,
            exifCreateDate,
            exifFnumber,
            exifIso,
            exifExposureTime,
            longitude,
            latitude,
            new HashMap<>(exifValues)
        );
        return exifData;
    }
}
