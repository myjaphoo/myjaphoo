/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache;

import groovyx.gpars.dataflow.DataflowVariable;
import org.myjaphoo.model.cache.events.TagsAddedEvent;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

import java.util.Collection;
import java.util.List;

/**
 * @author mla
 */
public interface EntityCacheActor {

    /**
     * verzögere events, bis zum aufruf von fireAllAccumulatedEvents.
     */
    public void accumulateEvents();

    /**
     * feuere alle aufgelaufenen events u. beende das anhäufen von events.
     */
    public void fireAllAccumulatedEvents();

    /**
     * Liefert eine immutables Modell der Entites.
     * Für dieses wird garantiert, dass es durch diese Caching-Klasse nicht
     * mehr geändert wird.
     * Gui-Komponenten können damit Threadsave darauf zugreifen/arbeiten.
     */
    public ImmutableModel getImmutableModel();

    void assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token);

    void assignToken2MovieEntry(ImmutableToken token, List<ImmutableMovieEntry> movies);

    void createMetaToken(MetaToken mt, ImmutableMetaToken parentToken);

    DataflowVariable<TagsAddedEvent> createToken(Token token, ImmutableToken parentToken);

    void editMetaToken(MetaToken mt);

    void editMovie(MovieEntry entry) throws Exception;

    void editToken(Token token);

    void moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move);

    void moveToken(ImmutableToken newParent, ImmutableToken token2Move);

    void removeMetaToken(ImmutableMetaToken mt);

    void removeToken(final ImmutableToken currentSelectedToken);

    void unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, List<ImmutableToken> toks2Remove);

    void unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies);

    void resetImmutableCopy();

    public void resetInternalCache();

    public void removeMovieEntry(ImmutableMovieEntry movieEntry);

    void newMovie(MovieEntry movieEntry);
}
