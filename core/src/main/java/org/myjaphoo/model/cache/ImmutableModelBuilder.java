package org.myjaphoo.model.cache;

import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

import java.util.Collection;

/**
 * ImmutableModelBuilder
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableModelBuilder {

    private Changes changes = new Changes();

    private TreeRel2<ImmutableToken, Token> tokenTree;

    private TreeRel2<ImmutableMetaToken, MetaToken> metaTokenTree;

    private ImmutableEntryList entryList;

    private Relation<ImmutableMovieEntry, ImmutableToken> entryToken;

    private Relation<ImmutableToken, ImmutableMetaToken> tokenMetaToken;

    public ImmutableModelBuilder(ImmutableModel model) {
        this.tokenTree = model.getTokenTree();
        this.metaTokenTree = model.getMetaTokenTree();
        this.entryList = model.getImmutableEntryList();
        this.entryToken = model.getEntryToken();
        this.tokenMetaToken = model.getTokenMetaToken();
    }


    public ImmutableModel build() {
        return new ImmutableModel(tokenTree, metaTokenTree, entryList, entryToken, tokenMetaToken, changes);
    }

    public ImmutableModel assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token) {
        tokenMetaToken = tokenMetaToken.relation(
            regChange(token),
            regChange(metaToken)
        );
        return build();
    }

    public ImmutableModel assignToken2MovieEntry(ImmutableToken token, java.util.List<ImmutableMovieEntry> movies) {
        ImmutableToken immutableToken = regChange(token);
        for (ImmutableMovieEntry entry : movies) {
            entryToken = entryToken.relation(regChange(entry), immutableToken);
        }
        return build();
    }

    public ImmutableModel createMetaToken(MetaToken mt, ImmutableMetaToken parentToken) {
        metaTokenTree = metaTokenTree.add(
            regChange(mt),
            parentToken
        );
        return build();
    }

    public ImmutableModel createToken(Token token, ImmutableToken parentToken) {
        tokenTree = tokenTree.add(
            regChange(token),
            parentToken
        );
        return build();
    }

    public ImmutableModel editMetaToken(MetaToken mt) {
        ImmutableMetaToken imt = regChange(mt);
        metaTokenTree = metaTokenTree.exchange(imt);
        tokenMetaToken = tokenMetaToken.updateY(imt);
        return build();
    }

    public ImmutableModel editMovie(MovieEntry entry) {
        ImmutableMovieEntry im = regChange(entry);
        entryList = new ImmutableEntryList(entryList.getEntryList().remove(im).prepend(im));
        entryToken = entryToken.updateX(im);

        return build();
    }


    public ImmutableModel editToken(Token token) {
        ImmutableToken it = regChange(token);
        tokenTree = tokenTree.exchange(it);
        entryToken = entryToken.updateY(it);
        tokenMetaToken = tokenMetaToken.updateX(it);
        return build();
    }

    public ImmutableModel moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move) {
        metaTokenTree = metaTokenTree.move(
            tokenParent,
            regChange(token2Move)
        );
        return build();
    }


    public ImmutableModel moveToken(ImmutableToken newParent, ImmutableToken token2Move) {
        tokenTree = tokenTree.move(
            newParent,
            regChange(token2Move)
        );
        return build();
    }

    public ImmutableModel removeMetaToken(ImmutableMetaToken mt) {
        ImmutableMetaToken immutableMetaToken = regChange(mt);
        metaTokenTree = metaTokenTree.remove(immutableMetaToken);

        tokenMetaToken = tokenMetaToken.removeY(immutableMetaToken);
        return build();
    }


    public ImmutableModel removeToken(final ImmutableToken token) {
        ImmutableToken it = regChange(token);
        tokenTree = tokenTree.remove(it);
        tokenMetaToken = tokenMetaToken.removeX(it);
        entryToken = entryToken.removeY(it);

        return build();
    }

    public ImmutableModel unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, java.util.List<ImmutableToken> toks2Remove) {
        ImmutableMetaToken mt = regChange(currMetaToken);
        for (ImmutableToken token : toks2Remove) {
            tokenMetaToken = tokenMetaToken.unassign(
                regChange(token),
                mt
            );
        }
        return build();
    }


    public ImmutableModel unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies) {
        for (ImmutableMovieEntry movieEntry : movies) {
            entryToken = entryToken.unassign(
                regChange(movieEntry),
                regChange(token)
            );
        }
        return build();
    }


    public ImmutableModel removeMovieEntry(ImmutableMovieEntry entry) {
        ImmutableMovieEntry im = regChange(entry);
        entryList = new ImmutableEntryList(entryList.getEntryList().remove(im));
        entryToken = entryToken.removeX(im);
        return build();
    }

    public ImmutableModel newMovie(MovieEntry entry) {
        entryList = new ImmutableEntryList(entryList.getEntryList().prepend(regChange(entry)));
        return build();
    }

    private ImmutableToken regChange(Token token) {
        return changes.add(token.toImmutable());
    }

    private ImmutableMetaToken regChange(MetaToken metaToken) {
        return changes.add(metaToken.toImmutable());
    }

    private ImmutableToken regChange(ImmutableToken token) {
        return changes.add(token);
    }

    private ImmutableMetaToken regChange(ImmutableMetaToken metaToken) {
        return changes.add(metaToken);
    }

    private ImmutableMovieEntry regChange(MovieEntry movieEntry) {
        return changes.add(movieEntry.toImmutable());
    }

    private ImmutableMovieEntry regChange(ImmutableMovieEntry movieEntry) {
        return changes.add(movieEntry);
    }
}
