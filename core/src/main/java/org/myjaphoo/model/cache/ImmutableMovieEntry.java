package org.myjaphoo.model.cache;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;

import static io.vavr.collection.HashSet.empty;

/**
 * ImmutableMovieEntry
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableMovieEntry extends ImmutableUUIDEntity implements ImmutableAttributedEntity {

    private String name;

    private String canonicalDir;

    private long fileLength;

    private Long checksumCRC32;

    private String sha1;

    private Rating rating;

    private ImmutableMovieAttrs movieAttrs;

    private String title;

    private String comment;

    private ImmutableExifData exifData;

    private Map<String, String> attributes;

    public ImmutableMovieEntry(MovieEntry entry) {
        super(entry);
        this.name = entry.getName();
        this.canonicalDir = entry.getCanonicalDir();
        this.fileLength = entry.getFileLength();
        this.checksumCRC32 = entry.getChecksumCRC32();
        this.sha1 = entry.getSha1();
        this.rating = entry.getRating();
        this.movieAttrs = new ImmutableMovieAttrs(entry.getMovieAttrs());
        this.title = entry.getTitle();
        this.comment = entry.getComment();
        this.exifData = new ImmutableExifData(entry.getExifData());
        this.attributes = HashMap.ofAll(entry.getAttributes());
    }

    public MovieEntry createMutable() {
        MovieEntry entry = new MovieEntry();
        copyToMutable(entry);
        return entry;
    }

    public void copyToMutable(MovieEntry entry) {
        entry.setUuid(getUuid());
        entry.setCreatedMs(getCreatedMs());
        entry.setName(getName());
        entry.setCanonicalDir(getCanonicalDir());
        entry.setFileLength(getFileLength());
        entry.setChecksumCRC32(getChecksumCRC32());
        entry.setSha1(getSha1());
        entry.setRating(getRating());
        entry.setMovieAttrs(getMovieAttrs().createMutable());
        entry.setTitle(getTitle());
        entry.setComment(getComment());
        entry.setExifData(getExifData().createMutable());
        entry.setAttributes(attributes.toJavaMap());
    }

    public String getName() {
        return name;
    }

    public String getCanonicalDir() {
        return canonicalDir;
    }

    public long getFileLength() {
        return fileLength;
    }

    public Long getChecksumCRC32() {
        return checksumCRC32;
    }

    public String getSha1() {
        return sha1;
    }

    public Rating getRating() {
        return rating;
    }

    public ImmutableMovieAttrs getMovieAttrs() {
        return movieAttrs;
    }

    public String getTitle() {
        return title;
    }

    public String getComment() {
        return comment;
    }

    public ImmutableExifData getExifData() {
        return exifData;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public Set<ImmutableToken> getTokens(ImmutableModel model) {
        return model.getEntryToken().getAssignedY(this).getOrElse(empty());
    }

    /**
     * @return the canonicalPath
     */
    public String getCanonicalPath() {
        return canonicalDir + "/" + name;
    }

}