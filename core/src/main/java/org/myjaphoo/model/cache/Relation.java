package org.myjaphoo.model.cache;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import io.vavr.control.Option;

/**
 * Relation
 *
 * @author mla
 * @version $Id$
 */
public class Relation<X, Y> {

    private Map<X, Set<Y>> xToYMap;

    private Map<Y, Set<X>> yToXMap;

    public Relation() {
        xToYMap = HashMap.empty();
        yToXMap = HashMap.empty();
    }

    public Relation(Map<X, Set<Y>> xToYMap, Map<Y, Set<X>> yToXMap) {
        this.xToYMap = xToYMap;
        this.yToXMap = yToXMap;
    }

    public Relation<X, Y> relation(X x, Y y) {
        return new Relation<X, Y>(
            xToYMap.put(x, xToYMap.get(x).getOrElse(HashSet.empty()).add(y)),
            yToXMap.put(y, yToXMap.get(y).getOrElse(HashSet.empty()).add(x))
        );
    }

    public Option<Set<X>> getAssignedX(Y y) {
        return yToXMap.get(y);
    }

    public Option<Set<Y>> getAssignedY(X x) {
        return xToYMap.get(x);
    }

    public Relation<X, Y> updateX(X x) {
        Relation<X, Y> r = this;
        Option<Set<Y>> assignedY = xToYMap.get(x);
        if (assignedY.isDefined()) {
            for (Y y : assignedY.get()) {
                // remove "old" object
                r = r.unassign(x, y);
                // add new object
                r = r.relation(x, y);
            }
        }
        return r;
    }

    public Relation<X, Y> updateY(Y y) {
        Relation<X, Y> r = this;
        Option<Set<X>> assignedX = yToXMap.get(y);
        if (assignedX.isDefined()) {
            for (X x : assignedX.get()) {
                // remove "old" object
                r = r.unassign(x, y);
                // add new object
                r = r.relation(x, y);
            }
        }
        return r;
    }


    public Relation<X, Y> removeX(X x) {
        Relation<X, Y> r = this;
        Option<Set<Y>> assignedY = xToYMap.get(x);
        if (assignedY.isDefined()) {
            for (Y y : assignedY.get()) {
                // remove "old" object
                r = r.unassign(x, y);
            }
        }
        return r;
    }


    public Relation<X, Y> removeY(Y y) {
        Relation<X, Y> r = this;
        Option<Set<X>> assignedX = yToXMap.get(y);
        if (assignedX.isDefined()) {
            for (X x : assignedX.get()) {
                // remove "old" object
                r = r.unassign(x, y);
            }
        }
        return r;
    }

    public Relation<X, Y> unassign(X x, Y y) {
        return new Relation<X, Y>(
            xToYMap.put(x, xToYMap.get(x).getOrElse(HashSet.empty()).remove(y)),
            yToXMap.put(y, yToXMap.get(y).getOrElse(HashSet.empty()).remove(x))
        );
    }
}
