/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ChangeSet;
import org.myjaphoo.model.cache.ImmutableModel;

/**
 * base event class for changes on movies.
 *
 * @author lang
 */
public class AbstractMoviesChangedEvent extends ChangeSet {
    public AbstractMoviesChangedEvent(ImmutableModel model) {
        super(model);
    }
}
