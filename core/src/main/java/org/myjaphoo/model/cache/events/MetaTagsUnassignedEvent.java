/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when meta tags get unassigned from tags.
 *
 * @author lang
 */
public class MetaTagsUnassignedEvent extends AbstractMetaTagsChangedEvent {

    public MetaTagsUnassignedEvent(ImmutableModel model) {
        super(model);
    }
}
