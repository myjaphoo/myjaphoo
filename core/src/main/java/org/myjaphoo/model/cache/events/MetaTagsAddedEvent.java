/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event fired when adding meta tags
 *
 * @author lang
 */
public class MetaTagsAddedEvent extends AbstractMetaTagsChangedEvent {

    public MetaTagsAddedEvent(ImmutableModel model) {
        super(model);
    }
}
