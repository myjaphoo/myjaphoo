package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableUUIDEntity;

/**
 * Base class to implement zipper classes for the immutable classes.
 *
 * @author mla
 * @version $Id$
 */
public interface Zipper<T extends ImmutableUUIDEntity> {

    ImmutableModel getModel();

    void setRef(T ref);

    Ref<T> toRef();

}
