package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.List;
import io.vavr.collection.Tree;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;

import static java.util.Collections.emptyIterator;
import static org.myjaphoo.model.cache.Immutables.newMetaTokenZipperIterable;
import static org.myjaphoo.model.cache.Immutables.newTokenZipperIterable;

/**
 * MetaTokenZipper
 *
 * @author mla
 * @version $Id$
 */
public class MetaTokenZipper extends MetaTokenWrapper implements Zipper<ImmutableMetaToken>, ImmutableAttributedEntityZipper, ZipperTree {

    private ImmutableMetaToken ref;

    public MetaTokenZipper(ImmutableModel model, ImmutableMetaToken ref) {
        super(model);
        this.ref = ref;
    }

    public Iterable<TokenZipper> getTokens() {
        return newTokenZipperIterable(model, getAssignedTokens());
    }

    public MetaTokenZipper getParentZipper() {
        ImmutableMetaToken parent = model.getMetaTokenTree().getParent(ref);
        if (parent == null) {
            return null;
        } else {
            return new MetaTokenZipper(model, parent);
        }
    }

    public Iterable<MetaTokenZipper> getChildren() {
        List<Tree.Node<ImmutableMetaToken>> children = model.getMetaTokenTree().getChildren(ref);
        return newMetaTokenZipperIterable(model, children.map(n -> n.getValue()));
    }

    @Override
    public Iterable<? extends ImmutableAttributedEntityZipper> getReferences() {
        return (Iterable<ImmutableAttributedEntityZipper>) () -> emptyIterator();
    }

    @Override
    public ImmutableMetaToken getRef() {
        return ref;
    }

    @Override
    public void setRef(ImmutableMetaToken ref) {
        this.ref = ref;
    }

    @Override
    public MetaTokenRef toRef() {
        return new MetaTokenRef(model, ref);
    }
}
