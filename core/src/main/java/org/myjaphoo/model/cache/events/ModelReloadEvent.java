/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ChangeSet;
import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when the immutable model gets reloaded.
 *
 */
public class ModelReloadEvent extends ChangeSet {

    public ModelReloadEvent(ImmutableModel model) {
        super(model);
    }
}
