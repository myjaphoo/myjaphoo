/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when tags get added.
 *
 * @author lang
 */
public class TagsAddedEvent extends AbstractTagsChangedEvent {

    public TagsAddedEvent(ImmutableModel model) {
        super(model);
    }
}
