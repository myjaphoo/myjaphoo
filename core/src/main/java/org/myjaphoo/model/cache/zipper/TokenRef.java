package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;

import java.util.Objects;

/**
 * TokenRef
 *
 * @author mla
 * @version $Id$
 */
public class TokenRef extends TokenWrapper implements Ref<ImmutableToken>, Comparable<TokenRef> {

    public final ImmutableToken ref;

    public TokenRef(ImmutableModel model, ImmutableToken ref) {
        super(model);
        this.ref = Objects.requireNonNull(ref);
    }

    @Override
    public ImmutableToken getRef() {
        return ref;
    }

    @Override
    public TokenZipper toZipper() {
        return new TokenZipper(model, ref);
    }

    @Override
    public int compareTo(TokenRef o) {
        return this.getRef().compareTo(o.getRef());
    }
}
