package org.myjaphoo.model.cache;

import org.myjaphoo.model.db.EntityObject;
import org.myjaphoo.model.db.UUIDEntity;

import java.util.Date;
import java.util.Objects;

/**
 * UUIDEntity
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableUUIDEntity implements EntityObject {

    private String uuid;

    private Long id;

    private Long createdMs;

    private Date edited;

    ImmutableUUIDEntity(UUIDEntity entity) {
        this.id = Objects.requireNonNull(entity.getId());
        this.uuid = entity.getUuid();
        this.createdMs = entity.getCreatedMs();
        this.edited = entity.getEdited();
    }

    protected void fill(UUIDEntity entity) {
        entity.setId(id);
        entity.setUuid(uuid);
        entity.setCreatedMs(createdMs);
        entity.setEdited(edited);
    }

    public Long getId() {
        return id;
    }


    public Date getCreated() {
        return createdMs != null ? new Date(createdMs) : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImmutableUUIDEntity that = (ImmutableUUIDEntity) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public String getUuid() {
        return uuid;
    }

    public Long getCreatedMs() {
        return createdMs;
    }

    public Date getEdited() {
        return edited;
    }
}
