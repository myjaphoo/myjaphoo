package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;

import java.util.Date;
import java.util.Objects;

/**
 * MetaTokenZipper
 *
 * @author mla
 * @version $Id$
 */
public abstract class MetaTokenWrapper implements ImmutableAttributedEntity {

    public final ImmutableModel model;

    public MetaTokenWrapper(ImmutableModel model) {
        this.model = Objects.requireNonNull(model);
    }

    abstract public ImmutableMetaToken getRef();

    public Long getId() {
        return getRef().getId();
    }

    public Date getCreated() {
        return getRef().getCreated();
    }

    public String getName() {
        return getRef().getName();
    }

    public String getDescription() {
        return getRef().getDescription();
    }

    public String getComment() {
        return getRef().getComment();
    }

    public Map<String, String> getAttributes() {
        return getRef().getAttributes();
    }

    public String getUuid() {
        return getRef().getUuid();
    }

    public Long getCreatedMs() {
        return getRef().getCreatedMs();
    }

    public Date getEdited() {
        return getRef().getEdited();
    }

    public ImmutableModel getModel() {
        return model;
    }

    public ImmutableMetaToken getParentMetaTag() {
        return model.getMetaTokenTree().getParent(getRef());
    }

    public Set<ImmutableToken> getAssignedTokens() {
        return model.getTokenMetaToken().getAssignedX(getRef()).getOrElse(HashSet.empty());
    }

    @Override
    public String toString() {
        return getName();
    }
}
