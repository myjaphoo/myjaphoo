/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Events when metatags get changed.
 *
 * @author lang
 */
public class MetaTagsChangedEvent extends AbstractMetaTagsChangedEvent {

    public MetaTagsChangedEvent(ImmutableModel model) {
        super(model);
    }
}
