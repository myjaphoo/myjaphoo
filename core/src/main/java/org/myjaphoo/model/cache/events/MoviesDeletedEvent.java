/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when movies get deleted.
 *
 * @author lang
 */
public class MoviesDeletedEvent extends AbstractMoviesChangedEvent {

    public MoviesDeletedEvent(ImmutableModel model) {
        super(model);
    }
}
