package org.myjaphoo.model.cache;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Tree;
import io.vavr.collection.Tree.Node;
import org.mlsoft.structures.TreeParentRelation;
import org.myjaphoo.model.db.EntityObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * Tree
 *
 * @author mla
 * @version $Id$
 */
public class TreeRel2<T extends EntityObject, O extends TreeParentRelation<O>> {

    private Node<T> root;

    public TreeRel2(Collection<O> all, Function<O, T> mapper) {
        // order by depth
        ArrayList<O> lst = new ArrayList<O>(all);
        Collections.sort(
            lst,
            new Comparator<O>() {

                @Override
                public int compare(O o1, O o2) {
                    int d1 = 0;
                    while (o1.getParent() != null) {
                        d1++;
                        o1 = o1.getParent();
                    }
                    int d2 = 0;
                    while (o2.getParent() != null) {
                        d2++;
                        o2 = o2.getParent();
                    }
                    return d1 - d2;
                }
            }
        );

        for (O o : lst) {
            T t = mapper.apply(o);
            if (o.getParent() == null) {
                this.root = Tree.of(t);
            } else {
                T tParent = mapper.apply(o.getParent());
                root = addChild(root, tParent, t);
            }
        }
    }

    private static <T> Node<T> addChild(Node<T> node, T currentElement, T newElement) {
        return addChild(node, currentElement, Tree.of(newElement));
    }

    private static <T> Node<T> addChild(Node<T> node, T currentElement, Node<T> newElement) {
        if (Objects.equals(node.getValue(), currentElement)) {
            return new Node<>(currentElement, node.getChildren().prepend(newElement));
        } else {
            for (Node<T> child : node.getChildren()) {
                final Node<T> newChild = addChild(child, currentElement, newElement);
                final boolean found = newChild != child;
                if (found) {
                    final List<Node<T>> newChildren = node.getChildren().replace(child, newChild);
                    return new Node<>(node.getValue(), newChildren);
                }
            }
            return node;
        }
    }

    private static <T> Node<T> remove(Node<T> node, Node<T> currentElement) {
        if (node.getChildren().contains(currentElement)) {
            return new Node<>(node.getValue(), node.getChildren().remove(currentElement));
        } else {
            for (Node<T> child : node.getChildren()) {
                final Node<T> newChild = remove(child, currentElement);
                final boolean found = newChild != child;
                if (found) {
                    final List<Node<T>> newChildren = node.getChildren().replace(child, newChild);
                    return new Node<>(node.getValue(), newChildren);
                }
            }
            return node;
        }
    }


    private static <T> Optional<Node<T>> find(Node<T> node, T t) {
        if (Objects.equals(node.getValue(), t)) {
            return Optional.of(node);
        } else {
            for (Node<T> child : node.getChildren()) {
                final Optional<Node<T>> foundNode = find(child, t);
                if (foundNode.isPresent()) {
                    return foundNode;
                }
            }
            return Optional.empty();
        }
    }

    private static <T> Optional<Node<T>> findParent(Node<T> node, Node<T> currentParent, T t) {
        if (Objects.equals(node.getValue(), t)) {
            return Optional.ofNullable(currentParent);
        } else {
            for (Node<T> child : node.getChildren()) {
                final Optional<Node<T>> foundNode = findParent(child, node, t);
                if (foundNode.isPresent()) {
                    return foundNode;
                }
            }
            return Optional.empty();
        }
    }

    public TreeRel2(Node<T> root) {
        this.root = root;
    }

    public T getParent(T t) {
        Optional<Node<T>> parent = findParent(root, null, t);
        if (parent.isPresent()) {
            return parent.get().getValue();
        } else {
            return null;
        }
    }

    public List<Node<T>> getChildren(T t) {
        Optional<Node<T>> node = find(root, t);
        if (node.isPresent()) {
            return node.get().getChildren();
        } else {
            return List.empty();
        }
    }

    public TreeRel2<T, O> add(T t, T parent) {
        return new TreeRel2<T, O>(
            addChild(root, parent, t)
        );
    }

    public TreeRel2<T, O> exchange(T t) {
        return new TreeRel2<>(
            (Node<T>) root.replace(t, t)
        );
    }

    public TreeRel2<T, O> remove(T t) {
        Optional<Node<T>> node = find(root, t);
        if (node.isPresent()) {
            return new TreeRel2<>(
                remove(root, node.get())
            );
        } else {
            return this;
        }
    }

    public TreeRel2<T, O> move(T newParent, T t) {
        Node<T> changedRoot = root;
        Optional<Node<T>> foundNode = find(changedRoot, t);
        if (!foundNode.isPresent()) {
            throw new IllegalArgumentException("node not found!");
        }
        changedRoot = remove(changedRoot, foundNode.get());
        changedRoot = addChild(changedRoot, newParent, foundNode.get());
        return new TreeRel2<>(changedRoot);
    }

    public Node<T> getRoot() {
        return root;
    }

    public Seq<T> getValues() {
        if (root == null) {
            return List.empty();
        } else {
            return root.values();
        }
    }

    public Optional<T> find(T t) {
        Optional<Node<T>> node = find(root, t);
        if (node.isPresent()) {
            return Optional.of(node.get().getValue());
        } else {
            return Optional.empty();
        }
    }
}
