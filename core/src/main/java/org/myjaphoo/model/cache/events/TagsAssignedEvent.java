/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when tags get assigned to movies.
 *
 * @author lang
 */
public class TagsAssignedEvent extends AbstractTagsChangedEvent {

    public TagsAssignedEvent(ImmutableModel model) {
        super(model);
    }
}
