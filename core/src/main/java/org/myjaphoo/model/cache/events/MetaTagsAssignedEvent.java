/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when metatags are assigned to tags.
 *
 * @author lang
 */
public class MetaTagsAssignedEvent extends AbstractMetaTagsChangedEvent {

    public MetaTagsAssignedEvent(ImmutableModel model) {
        super(model);
    }
}
