package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;

import java.util.Objects;

/**
 * MetaTokenRef
 *
 * @author mla
 * @version $Id$
 */
public class MetaTokenRef extends MetaTokenWrapper implements Ref<ImmutableMetaToken>, Comparable<MetaTokenRef> {

    public final ImmutableMetaToken ref;

    public MetaTokenRef(ImmutableModel model, ImmutableMetaToken ref) {
        super(model);
        this.ref = Objects.requireNonNull(ref);
    }

    public ImmutableMetaToken getRef() {
        return ref;
    }

    @Override
    public MetaTokenZipper toZipper() {
        return new MetaTokenZipper(model, ref);
    }

    @Override
    public int compareTo(MetaTokenRef o) {
        return this.getRef().compareTo(o.getRef());
    }
}
