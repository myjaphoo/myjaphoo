package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.List;
import io.vavr.collection.Tree;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.Immutables;

import static org.myjaphoo.model.cache.Immutables.newEntrryZipperIterable;

/**
 * TokenZipper
 *
 * @author mla
 * @version $Id$
 */
public class TokenZipper extends TokenWrapper implements Zipper<ImmutableToken>, ImmutableAttributedEntityZipper, ZipperTree {

    private ImmutableToken ref;

    public TokenZipper(ImmutableModel model, ImmutableToken ref) {
        super(model);
        this.ref = ref;
    }

    public Iterable<EntryZipper> getMovieEntries() {
        return newEntrryZipperIterable(model, ref.getMovieEntries(model));
    }

    public Iterable<TokenZipper> getChildren() {
        List<Tree.Node<ImmutableToken>> children = model.getTokenTree().getChildren(ref);
        return Immutables.newTokenZipperIterable(model, children.map(n -> n.getValue()));
    }

    @Override
    public Iterable<MetaTokenZipper> getReferences() {
        return getMetaTokensZipper();
    }

    @Override
    public ImmutableToken getRef() {
        return ref;
    }

    @Override
    public void setRef(ImmutableToken ref) {
        this.ref = ref;
    }

    @Override
    public TokenRef toRef() {
        return new TokenRef(model, ref);
    }
}
