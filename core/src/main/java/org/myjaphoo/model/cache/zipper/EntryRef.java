package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;

import java.util.Objects;

/**
 * EntryRef: tuple for model and entry.
 *
 * @author mla
 * @version $Id$
 */
public class EntryRef extends EntryWrapper implements Ref<ImmutableMovieEntry> {

    public final ImmutableMovieEntry ref;

    public EntryRef(ImmutableModel model, ImmutableMovieEntry ref) {
        super(model);
        this.ref = Objects.requireNonNull(ref);
    }

    public Set<ImmutableToken> getTokens() {
        return ref.getTokens(model);
    }

    public Set<TokenRef> getTokenRefs() {
        return ref.getTokens(model).map(token -> new TokenRef(model, token));
    }

    @Override
    public ImmutableMovieEntry getRef() {
        return ref;
    }

    @Override
    public EntryZipper toZipper() {
        return new EntryZipper(model, ref);
    }
}
