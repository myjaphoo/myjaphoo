package org.myjaphoo.model.cache.zipper;

/**
 * ZipperTree
 *
 * @author mla
 * @version $Id$
 */
public interface ZipperTree<T extends ZipperTree<T>> {

    T getParentZipper();

    String getName();
}
