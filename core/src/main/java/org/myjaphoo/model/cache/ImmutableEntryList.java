package org.myjaphoo.model.cache;

import io.vavr.collection.List;
import org.myjaphoo.model.DuplicateHashMap;

/**
 * ImmutableEntryList
 *
 * @author mla
 * @version $Id$
 */
public class ImmutableEntryList {

    private List<ImmutableMovieEntry> entryList;

    private DuplicateHashMap duplicateHashMap;

    public ImmutableEntryList(List<ImmutableMovieEntry> entryList) {
        this.entryList = entryList;
        this.duplicateHashMap = new DuplicateHashMap(entryList);
    }

    public List<ImmutableMovieEntry> getEntryList() {
        return entryList;
    }

    public DuplicateHashMap getDuplicateHashMap() {
        return duplicateHashMap;
    }
}
