/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import groovyx.gpars.activeobject.ActiveMethod
import groovyx.gpars.activeobject.ActiveObject
import groovyx.gpars.dataflow.DataflowVariable
import org.mlsoft.eventbus.EventBus
import org.myjaphoo.model.cache.events.TagsAddedEvent
import org.myjaphoo.model.db.MetaToken
import org.myjaphoo.model.db.MovieEntry
import org.myjaphoo.model.db.Token
import org.myjaphoo.model.dbconfig.DBConnection

/**
 * Diese Klasse ist für Änderungen an den Daten zuständig.
 * Sie macht generell zwei Dinge:
 *  - Die Ändeurngen persistent über die entsprechenden DAO Klassen ansteuern
 *  - Die Änderungen im entsprechenden Cache nachziehen.
 *  - nach jeder internen Cache Änderung wird der kopierte (immutable) 
 *
 * Dadurch sollte nach jeder Änderung die Datenbank u. das Cache-Modell
 * synchron sein.
 * Alle Manipulationszugriffe aus der GUI sollten also immer über diese Klasse erfolgen,
 * damit gewährleistet ist, dass der Cache aktuell ist; zugriff direkt über DAO
 * Klassen sollte vermieden werden.
 *
 * Diese Zugriffsklasse wird als Actor zur Verfügung gestellt.
 * Alle Aktionen die über das Interface implementiert werden,
 * werden in einem Actor asynchron abgearbeitet.
 *
 * @author lang
 */
@ActiveObject
@TypeChecked
public class EntityCacheActorImpl implements EntityCacheActor {

    private EntityCacheInternal cache;

    public EntityCacheActorImpl(DBConnection dbConn, EventBus eventBus) {
        cache = new EntityCacheInternal(dbConn, eventBus);
    }

    @Override
    @ActiveMethod
    public void editMovie(MovieEntry entry) throws Exception {
        cache.editMovie(entry);
    }

    @Override
    @ActiveMethod
    public void removeToken(final ImmutableToken currentSelectedToken) {
        cache.removeToken(currentSelectedToken);
    }

    @Override
    @ActiveMethod
    public void removeMovieEntry(ImmutableMovieEntry movieEntry) {
        cache.removeMovieEntry(movieEntry);
    }

    @Override
    @ActiveMethod
    public void newMovie(MovieEntry movieEntry) {
        cache.newMovie(movieEntry);
    }

    @Override
    @ActiveMethod
    public void unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies) {
        cache.unassignTokenFromMovies(token, movies);
    }

    @Override
    @ActiveMethod
    public void assignToken2MovieEntry(ImmutableToken token, List<ImmutableMovieEntry> movies) {
        cache.assignToken2MovieEntry(token, movies);
    }

    @Override
    @ActiveMethod
    @TypeChecked(value = TypeCheckingMode.SKIP)
    def DataflowVariable<TagsAddedEvent> createToken(Token token, ImmutableToken parentToken) {
        return cache.createToken(token, parentToken);
    }

    @Override
    @ActiveMethod
    public void editToken(Token token) {
        cache.editToken(token)
    }

    @Override
    @ActiveMethod
    public void moveToken(ImmutableToken newParent, ImmutableToken token2Move) {
        cache.moveToken(newParent, token2Move)
    }

    @Override
    @ActiveMethod
    public void removeMetaToken(ImmutableMetaToken mt) {
        cache.removeMetaToken(mt)
    }

    @Override
    @ActiveMethod
    public void assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token) {
        cache.assignMetaTokenToToken(metaToken, token)
    }

    @Override
    @ActiveMethod
    public void createMetaToken(MetaToken mt, ImmutableMetaToken parentToken) {
        cache.createMetaToken(mt, parentToken);
    }

    @Override
    @ActiveMethod
    public void editMetaToken(MetaToken mt) {
        cache.editMetaToken(mt)
    }

    @Override
    @ActiveMethod
    public void moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move) {
        cache.moveMetaTokens(tokenParent, token2Move)
    }

    @Override
    @ActiveMethod
    public void unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, List<ImmutableToken> toks2Remove) {
        cache.unAssignMetaTokenFromToken(currMetaToken, toks2Remove)
    }

    @Override
    @ActiveMethod(blocking = true)
    public ImmutableModel getImmutableModel() {
        return cache.getImmutableModel()
    }

    @Override
    @ActiveMethod
    public void resetImmutableCopy() {
        cache.resetImmutableCopy();
    }

    @Override
    @ActiveMethod
    public void resetInternalCache() {
        cache.resetInternalCache();
    }

    @Override
    @ActiveMethod
    public void accumulateEvents() {
        cache.accumulateEvents();
    }

    @Override
    @ActiveMethod
    public void fireAllAccumulatedEvents() {
        cache.fireAllAccumulatedEvents();
    }

}
