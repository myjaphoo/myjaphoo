package org.myjaphoo.model.cache.zipper;

import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.TokenType;

import java.util.Date;
import java.util.Objects;

import static io.vavr.collection.HashSet.empty;
import static org.myjaphoo.model.cache.Immutables.newMetaTokenZipperIterable;

/**
 * TokenZipper
 *
 * @author mla
 * @version $Id$
 */
public abstract class TokenWrapper implements ImmutableAttributedEntity {

    public final ImmutableModel model;

    public TokenWrapper(ImmutableModel model) {
        this.model = Objects.requireNonNull(model);
    }

    abstract public ImmutableToken getRef();

    public Long getId() {
        return getRef().getId();
    }

    public Date getCreated() {
        return getRef().getCreated();
    }

    public String getUuid() {
        return getRef().getUuid();
    }

    public Long getCreatedMs() {
        return getRef().getCreatedMs();
    }

    public String getName() {
        return getRef().getName();
    }

    public Date getEdited() {
        return getRef().getEdited();
    }

    public TokenType getTokentype() {
        return getRef().getTokentype();
    }

    public String getDescription() {
        return getRef().getDescription();
    }

    public String getComment() {
        return getRef().getComment();
    }

    public Map<String, String> getAttributes() {
        return getRef().getAttributes();
    }

    public ImmutableModel getModel() {
        return model;
    }

    public Set<ImmutableMetaToken> getMetaTokens() {
        return model.getTokenMetaToken().getAssignedY(getRef()).getOrElse(empty());
    }

    public ImmutableToken getParentTag() {
        return model.getParentTag(getRef());
    }

    public TokenZipper getParentZipper() {
        ImmutableToken parent = model.getTokenTree().getParent(getRef());
        if (parent == null) {
            return null;
        } else {
            return new TokenZipper(model, parent);
        }
    }

    public Set<ImmutableMovieEntry> getAssignedMovieEntries() {
        return getRef().getMovieEntries(model);
    }

    public Iterable<MetaTokenZipper> getMetaTokensZipper() {
        return newMetaTokenZipperIterable(model, getRef().getAssignedMetaTokens(model));
    }

    @Override
    public String toString() {
        return getName();
    }
}
