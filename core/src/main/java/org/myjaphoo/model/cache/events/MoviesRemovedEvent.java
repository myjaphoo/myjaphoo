/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when movies get removed
 *
 * @author lang
 */
public class MoviesRemovedEvent extends AbstractMoviesChangedEvent {

    public MoviesRemovedEvent(ImmutableModel model) {
        super(model);
    }
}
