package org.myjaphoo.model.cache;

import org.myjaphoo.model.cache.zipper.EntryZipper;
import org.myjaphoo.model.cache.zipper.MetaTokenZipper;
import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.cache.zipper.ZipperIterable;
import org.myjaphoo.model.db.Token;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Immutables
 *
 * @author mla
 * @version $Id$
 */
public class Immutables {

    public static Iterable<TokenZipper> newTokenZipperIterable(
        ImmutableModel model, Iterable<ImmutableToken> tokens
    ) {
        return new ZipperIterable<>(tokens, () -> new TokenZipper(model, null));
    }

    public static Iterable<MetaTokenZipper> newMetaTokenZipperIterable(
        ImmutableModel model, Iterable<ImmutableMetaToken> tokens
    ) {
        return new ZipperIterable<>(tokens, () -> new MetaTokenZipper(model, null));
    }

    public static Iterable<EntryZipper> newEntrryZipperIterable(
        ImmutableModel model, Iterable<ImmutableMovieEntry> entries
    ) {
        return new ZipperIterable<>(entries, () -> new EntryZipper(model, null));
    }
}
