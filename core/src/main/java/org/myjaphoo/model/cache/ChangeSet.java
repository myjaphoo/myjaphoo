/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache;

import io.vavr.Tuple2;
import io.vavr.collection.List;

import java.util.Map;

/**
 * base class for all change events.
 *
 * @author lang
 */
public class ChangeSet {

    private final ImmutableModel model;

    private final Changes changes;

    public ChangeSet(ImmutableModel model) {
        this.model = model;
        this.changes = model.getChanges();
    }


    /**
     * @return the movieEntrySet
     */
    public List<ImmutableMovieEntry> getMovieEntrySet() {
        return changes.getChangedEntries();
    }

    /**
     * @return the tokenSet
     */
    public List<ImmutableToken> getTokenSet() {
        return changes.getChangedTokens();
    }

    /**
     * @return the metaTokenSet
     */
    public List<ImmutableMetaToken> getMetaTokenSet() {
        return changes.getChangedMetaTokens();
    }


    public boolean contains(ImmutableMovieEntry entry) {
        return getMovieEntrySet().contains(entry);
    }


    public Map<Long, ImmutableMovieEntry> createEntryIdMap() {
        Map<Long, ImmutableMovieEntry> idMap = getMovieEntrySet().toJavaMap(ie -> new Tuple2<Long, ImmutableMovieEntry>(
            ie.getId(),
            ie
        ));
        return idMap;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + getMovieEntrySet()
            + getTokenSet()
            + getMetaTokenSet();
    }

    public ImmutableModel getModel() {
        return model;
    }

    public Changes getChanges() {
        return changes;
    }
}
