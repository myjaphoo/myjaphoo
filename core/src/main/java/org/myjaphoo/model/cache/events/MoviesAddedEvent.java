/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache.events;

import org.myjaphoo.model.cache.ImmutableModel;

/**
 * Event when movies get added.
 *
 * @author lang
 */
public class MoviesAddedEvent extends AbstractMoviesChangedEvent {

    public MoviesAddedEvent(ImmutableModel model) {
        super(model);
    }
}
