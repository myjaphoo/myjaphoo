package org.myjaphoo.model.cache.zipper;

import org.myjaphoo.model.cache.ImmutableUUIDEntity;

import java.util.Iterator;
import java.util.function.Supplier;

/**
 * ZipperIterator
 *
 * @author mla
 * @version $Id$
 */
public class ZipperIterable<T extends ImmutableUUIDEntity, Z extends Zipper<T>> implements Iterable<Z> {

    private Iterable<T> iterable;

    private Supplier<Z> zipperFactory;

    public ZipperIterable(Iterable<T> iterable, Supplier<Z> zipperFactory) {
        this.iterable = iterable;
        this.zipperFactory = zipperFactory;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Z> iterator() {
        return new ZipperIterator<T, Z>(iterable.iterator(), zipperFactory.get());
    }
}
