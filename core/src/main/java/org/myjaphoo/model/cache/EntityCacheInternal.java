/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.cache;

import org.mlsoft.eventbus.DelayableEventBus;
import org.mlsoft.eventbus.EventBus;
import org.myjaphoo.model.cache.events.MetaTagsAddedEvent;
import org.myjaphoo.model.cache.events.MetaTagsAssignedEvent;
import org.myjaphoo.model.cache.events.MetaTagsChangedEvent;
import org.myjaphoo.model.cache.events.MetaTagsDeletedEvent;
import org.myjaphoo.model.cache.events.MetaTagsUnassignedEvent;
import org.myjaphoo.model.cache.events.ModelReloadEvent;
import org.myjaphoo.model.cache.events.MoviesChangedEvent;
import org.myjaphoo.model.cache.events.MoviesRemovedEvent;
import org.myjaphoo.model.cache.events.TagsAddedEvent;
import org.myjaphoo.model.cache.events.TagsAssignedEvent;
import org.myjaphoo.model.cache.events.TagsChangedEvent;
import org.myjaphoo.model.cache.events.TagsDeletedEvent;
import org.myjaphoo.model.cache.events.TagsUnassigendEvent;
import org.myjaphoo.model.cache.impl.ImmModelLoader;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.logic.MetaTokenJpaController;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.TokenJpaController;

import java.util.Collection;
import java.util.List;

/**
 * Diese Klasse ist für Änderungen an den Daten zuständig.
 * Sie macht generell zwei Dinge:
 * - Die Ändeurngen persistent über die entsprechenden DAO Klassen ansteuern
 * - Die Änderungen im entsprechenden Cache nachziehen.
 * - nach jeder internen Cache Änderung wird der kopierte (immutable)
 *
 * Dadurch sollte nach jeder Änderung die Datenbank u. das Cache-Modell
 * synchron sein.
 * Alle Manipulationszugriffe aus der GUI sollten also immer über diese Klasse erfolgen,
 * damit gewährleistet ist, dass der Cache aktuell ist; zugriff direkt über DAO
 * Klassen sollte vermieden werden.
 *
 * Diese Zugriffsklasse wird als Actor zur Verfügung gestellt.
 * Alle Aktionen die über das Interface implementiert werden,
 * werden in einem Actor asynchron abgearbeitet.
 *
 * @author lang
 */
public class EntityCacheInternal {

    private DBConnection dbConn;
    private TokenJpaController tjpa;
    private MovieEntryJpaController jpa;
    private MetaTokenJpaController mtjpa;

    private ImmutableModel model;

    public EntityCacheInternal(DBConnection dbConn, EventBus eventBus) {
        this.dbConn = dbConn;
        tjpa = new TokenJpaController(dbConn);
        jpa = new MovieEntryJpaController(dbConn);
        mtjpa = new MetaTokenJpaController(dbConn);
        this.eventBus = new DelayableEventBus(eventBus);
    }

    private DelayableEventBus eventBus;


    public void editMovie(MovieEntry entry) throws Exception {
        ensureModelIsLoaded();
        jpa.edit(entry);
        model = getOrCreateModel().editMovie(entry);
        eventBus.post(new MoviesChangedEvent(model));
    }

    public void removeToken(final ImmutableToken currentSelectedToken) {
        ensureModelIsLoaded();
        tjpa.removeToken(currentSelectedToken);
        // cache aktualiseren:
        model = getOrCreateModel().removeToken(currentSelectedToken);
        eventBus.post(new TagsDeletedEvent(model));
    }

    public void removeMovieEntry(ImmutableMovieEntry movieEntry) {
        ensureModelIsLoaded();
        jpa.removeMovieEntry(movieEntry);
        model = getOrCreateModel().removeMovieEntry(movieEntry);
        eventBus.post(new MoviesRemovedEvent(model));
    }

    public void newMovie(MovieEntry movieEntry) {
        ensureModelIsLoaded();
        jpa.create(movieEntry);
        model = getOrCreateModel().newMovie(movieEntry);
        eventBus.post(new MoviesChangedEvent(model));
    }

    public void unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies) {
        ensureModelIsLoaded();
        tjpa.unassignTokenFromMovies(token, movies);
        model = getOrCreateModel().unassignTokenFromMovies(token, movies);
        eventBus.post(new TagsUnassigendEvent(model));
    }

    public void assignToken2MovieEntry(ImmutableToken token, List<ImmutableMovieEntry> movies) {
        ensureModelIsLoaded();
        tjpa.assignToken2MovieEntry(token, movies);
        model = getOrCreateModel().assignToken2MovieEntry(token, movies);
        eventBus.post(new TagsAssignedEvent(model));
    }


    public TagsAddedEvent createToken(Token token, ImmutableToken parentToken) {
        ensureModelIsLoaded();
        if (parentToken == null) {
            parentToken = tjpa.findRootToken().toImmutable();
        }
        tjpa.create(token, parentToken);
        model = getOrCreateModel().createToken(token, parentToken);
        TagsAddedEvent event = new TagsAddedEvent(model);
        eventBus.post(event);
        return event;
    }

    public void editToken(Token token) {
        ensureModelIsLoaded();
        tjpa.edit(token);
        model = getOrCreateModel().editToken(token);
        eventBus.post(new TagsChangedEvent(model));
    }

    public void moveToken(ImmutableToken newParent, ImmutableToken token2Move) {
        ensureModelIsLoaded();
        tjpa.moveToken(newParent, token2Move);
        model = getOrCreateModel().moveToken(newParent, token2Move);
        // reload the moved token to update the "edit" field which is updated by jpa:
        dbConn.commit((em) -> {
            Token updatedMT = em.find(Token.class, token2Move.getId());
            model = model.editToken(updatedMT);
        });
        eventBus.post(new TagsChangedEvent(model));
    }

    public void removeMetaToken(ImmutableMetaToken mt) {
        ensureModelIsLoaded();
        mtjpa.removeToken(mt);
        model = getOrCreateModel().removeMetaToken(mt);
        eventBus.post(new MetaTagsDeletedEvent(model));
    }

    public void assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token) {
        ensureModelIsLoaded();
        mtjpa.assignTok(token, metaToken);
        model = getOrCreateModel().assignMetaTokenToToken(metaToken, token);
        eventBus.post(new MetaTagsAssignedEvent(model));
    }

    public void createMetaToken(MetaToken mt, ImmutableMetaToken parentToken) {
        ensureModelIsLoaded();
        if (parentToken == null) {
            parentToken = mtjpa.findRootToken().toImmutable();
        }
        mtjpa.create(mt, parentToken);
        model = getOrCreateModel().createMetaToken(mt, parentToken);
        eventBus.post(new MetaTagsAddedEvent(model));
    }

    public void editMetaToken(MetaToken mt) {
        ensureModelIsLoaded();
        mtjpa.edit(mt);
        model = getOrCreateModel().editMetaToken(mt);
        eventBus.post(new MetaTagsChangedEvent(model));
    }

    public void moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move) {
        ensureModelIsLoaded();
        mtjpa.moveToken(tokenParent, token2Move);
        model = getOrCreateModel().moveMetaTokens(tokenParent, token2Move);
        // reload the moved meta token to update the "edit" field which is updated by jpa:
        dbConn.commit((em) -> {
            MetaToken updatedMT = em.find(MetaToken.class, token2Move.getId());
            model = model.editMetaToken(updatedMT);
        });
        eventBus.post(new MetaTagsChangedEvent(model));
    }

    public void unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, List<ImmutableToken> toks2Remove) {
        ensureModelIsLoaded();
        mtjpa.unassignMetaToken(currMetaToken, toks2Remove);
        model = getOrCreateModel().unAssignMetaTokenFromToken(currMetaToken, toks2Remove);
        eventBus.post(new MetaTagsUnassignedEvent(model));
    }

    public ImmutableModel getImmutableModel() {
        return getOrCreateModel();
    }

    private ImmutableModel getOrCreateModel() {
        if (model == null) {
            model = new ImmModelLoader(dbConn).load();
            eventBus.post(new ModelReloadEvent(model));
        }
        return model;
    }

    public void resetImmutableCopy() {
        model = null;
    }

    public void resetInternalCache() {
        model = null;
    }

    public void accumulateEvents() {
        eventBus.accumulateEvents();
    }

    public void fireAllAccumulatedEvents() {
        eventBus.fireAllAccumulatedEvents();
    }

    /**
     * ensures that the lazy holded model is loaded.
     * The change methods work all the same way:
     * 1. change in the db via jpa methods
     * 2. update the immutable model which is holded in ram (and is a immutable copy of the db entities)
     *
     * It is therefore essential, that the model exists (is already properly loaded from the db) before
     * such changes happen.
     * Otherwise it would be in the worst case loaded after the jpa change, and then inconsistent.
     */
    private void ensureModelIsLoaded() {
        getOrCreateModel();
    }
}
