/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.impactors;


import groovyx.gpars.actor.DynamicDispatchActor;


/**
 * @author lang
 */

public abstract class ImportingWorkerActor extends DynamicDispatchActor {

    public abstract void onMessage(StopMessage msg);

    public abstract void onMessage(WorkerMsg msg);

}
