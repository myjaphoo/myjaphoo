/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.dbhandling;

import org.myjaphoo.model.dbconfig.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

/**
 * A transaction handler which holds an entity manager per thread and started transaction.
 * It uses a nested transaction for nested calls to doInNewTransaction.
 * The outest call then commits the transaction and closes the entity manager and discards it afterwards.
 *
 * @author mla
 */
public class NestableTransaction {

    private static final Logger LOGGER = LoggerFactory.getLogger(NestableTransaction.class.getName());

    private final Long connectionId;

    private final DBConnection connection;

    private final static ThreadLocal<Map<Long, EntityManager>> threadLocalEntityManagerMap = ThreadLocal.withInitial(() -> new HashMap<>());

    public NestableTransaction(final DBConnection connection) {
        this.connection = connection;
        connectionId = connection.getConfiguration().getId();
    }

    private EntityManager getEntityManager() {
        Map<Long, EntityManager> map = threadLocalEntityManagerMap.get();
        EntityManager em = map.get(connectionId);
        if (em == null) {
            // create lazy:
            em = connection.getEmf().createEntityManager();
            map.put(connectionId, em);
        }
        return em;
    }


    public void doInTransaction(TransactionBoundaryDelegator.CommitBlock runnable) {
        EntityManager em = null;
        boolean hasStartedTransaction = false;
        try {
            em = getEntityManager();

            hasStartedTransaction = beginOrParticipateOnTransaction(em);
            runnable.runCommitBlock(em);
            LOGGER.trace("********* commit transaction");
            if (hasStartedTransaction) {
                em.getTransaction().commit();
                if (em.getTransaction().isActive()) {
                    throw new IllegalArgumentException("internal error! transaction still active!");
                }
            }

        } catch (Throwable t) {

            if (hasStartedTransaction) {
                LOGGER.error("exception in transaction; trying rollback!", t);
                if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
                LOGGER.trace("********* rollbacked transaction");
            } else {
                LOGGER.trace("********* nested participated transaction: let outer transaction boundary do a rollback");
            }

            throw new RuntimeException("Exception in transaction!", t);
        } finally {
            if (hasStartedTransaction) {
                disposeEntityMananger(em);
            }
        }
    }

    private void disposeEntityMananger(EntityManager em) {
        em.close();
        threadLocalEntityManagerMap.get().remove(connectionId);
    }

    private boolean beginOrParticipateOnTransaction(EntityManager em) {
        if (!em.getTransaction().isActive()) {
            LOGGER.trace("********* begin new transaction");
            em.getTransaction().begin();
            return true;
        }
        // participate on an outer nested transaction:
        // do nothing return marker false:
        LOGGER.trace("********* participate on outer nested transaction");
        return false;
    }

}
