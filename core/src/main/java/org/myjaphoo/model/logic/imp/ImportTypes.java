/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.imp;

import org.myjaphoo.project.Project;

/**
 * @author mla
 */
public enum ImportTypes {

    Movies() {
        public ImportDelegator createImportDelegator(Project project) {
            return new MovieDelegator(project);
        }
    },
    Pictures() {
        public ImportDelegator createImportDelegator(Project project) {
            return new PicDelegator(project);
        }
    },
    Text() {
        public ImportDelegator createImportDelegator(Project project) {
            return new TextDelegator(project);
        }
    },

    AllMedia() {
        public ImportDelegator createImportDelegator(Project project) {
            return new AllMediaDelegator(project);
        }
    },

    AllFiles() {
        public ImportDelegator createImportDelegator(Project project) {
            return new AllFilesDelegator(project);
        }
    },

    SpecificFiles() {
        public ImportDelegator createImportDelegator(Project project) {
            return new SpecificFilesDelegator(project);
        }
    };

    public abstract ImportDelegator createImportDelegator(Project project);
}
