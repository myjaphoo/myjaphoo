/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.myjaphoo.model.db.SavedGroovyScript;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;


/**
 * dao class for scripts.
 *
 * @author lang
 */
public class ScriptJpaController extends AbstractDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptJpaController.class);


    public ScriptJpaController(DBConnection dbConn) {
        super(dbConn);
    }

    public void create(final SavedGroovyScript script) {
        getDbConn().commit(em -> em.persist(script));
    }


    public void edit(final SavedGroovyScript script) {
        getDbConn().commit(em -> em.merge(script));
    }


    public List<SavedGroovyScript> findScriptEntities() {
        return getDbConn().load(em -> {
                Query q = em.createQuery("select object(o) from SavedGroovyScript as o order by name");
                return q.getResultList();
            }
        );
    }


    public void removeScript(final SavedGroovyScript script) {
        getDbConn().commit(em -> {
                SavedGroovyScript mergedScript = em.merge(script);
                em.remove(mergedScript);
            }
        );
    }

    public SavedGroovyScript findScriptByName(final String name) {
        return getDbConn().load(em -> {
                TypedQuery<SavedGroovyScript> q = em.createQuery(
                    "select s from SavedGroovyScript s where s.name = :name",
                    SavedGroovyScript.class
                );
                q.setParameter("name", name);
                List<SavedGroovyScript> list = q.getResultList();
                if (list.size() == 1) {
                    return list.get(0);
                } else {
                    return null;
                }
            }
        );
    }
}
