/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.hibernate.jpa.QueryHints;
import org.mlsoft.structures.Trees;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;


/**
 * @author lang
 */
public class MetaTokenJpaController extends AbstractDao {

    private static Logger logger = LoggerFactory.getLogger(MetaTokenJpaController.class.getName());
    private MovieEntryJpaController jpa;


    public MetaTokenJpaController(DBConnection dbConn) {
        super(dbConn);
        jpa = new MovieEntryJpaController(dbConn);
    }

    public void removeToken(final ImmutableMetaToken currentSelectedToken) {
        getDbConn().commit(em -> {
                MetaToken tokDel = em.find(MetaToken.class, currentSelectedToken.getId());
                if (tokDel.getParent() == null) {
                    throw new RuntimeException("Root can not be removed!");
                }
                Query q = em.createQuery("from MetaToken t where t.parent = :toDelete");
                q.setParameter("toDelete", tokDel);
                if (q.getResultList().size() > 0) {
                    throw new RuntimeException("meta token '" + tokDel.getName() + "' has children: can not remove it!");
                }
                EntityRelations.unlinkMetaToken(tokDel);
                em.remove(tokDel);
            }
        );
    }

    public void assignTok(final ImmutableToken token, final ImmutableMetaToken metatoken) {
        logger.info("assign token/metatkoken: " + token.getName() + "<->" + metatoken.getName());
        getDbConn().commit(em -> {
                Token mergedTok = em.find(Token.class, token.getId());
                MetaToken mergedMetaToken = em.find(MetaToken.class, metatoken.getId());
                EntityRelations.linkTokenToMetatoken(mergedTok, mergedMetaToken);
            }
        );
    }

    public void create(final MetaToken metatoken, final ImmutableMetaToken parentMetaToken) {
        create(metatoken, parentMetaToken != null ? parentMetaToken.getId() : null);
    }

    public void create(final MetaToken metatoken, final Long parentMetaTokenId) {
        getDbConn().commit(em -> {
                if (parentMetaTokenId != null) {
                    MetaToken mergedParentMetaToken = em.find(MetaToken.class, parentMetaTokenId);
                    metatoken.setParent(mergedParentMetaToken);
                }
                em.persist(metatoken);
            }
        );
    }

    public void edit(final MetaToken metatoken) {
        getDbConn().commit(em -> {
                if (metatoken.getParent().equals(metatoken)) {
                    // selbstzuweisung: nicht möglich:
                    throw new RuntimeException("Metatoken has itself as parent!!");
                }
                em.merge(metatoken);
            }
        );
    }

    public void moveToken(final ImmutableMetaToken newParent, final ImmutableMetaToken token2Move) {
        logger.info("move " + token2Move.getName() + " under " + newParent.getName());
        if (token2Move.equals(newParent)) {
            throw new RuntimeException("Metatoken has itself as parent!!");
        }
        getDbConn().commit(em -> {
                MetaToken tok2Move = em.find(MetaToken.class, token2Move.getId());
                MetaToken newPar = em.find(MetaToken.class, newParent.getId());
                Trees.checkCircle(newPar, tok2Move);
                tok2Move.setParent(newPar);
            }
        );
    }

    public List<MetaToken> fetchAll() {
        return getDbConn().load(em -> fetchAll(em));
    }

    public static List<MetaToken> fetchAll(EntityManager em) {
        Query q = em.createQuery(
            "select o from MetaToken as o  left join fetch o.attributes left join fetch o.assignedTokens");
        q.setHint(QueryHints.HINT_READONLY, true);
        return q.getResultList();
    }

    public MetaToken findRootToken() {
        return getDbConn().load(em -> {
                Query q = em.createQuery("select object(o) from MetaToken as o where o.parent is null");
                return (MetaToken) q.getSingleResult();
            }
        );
    }

    public int getMetaTokenCount() {
        return getDbConn().load(em -> {
                return ((Long) em.createQuery("select count(o) from MetaToken as o").getSingleResult()).intValue();
            }
        );
    }

    public void unassignMetaToken(final ImmutableMetaToken metatoken, final Collection<ImmutableToken> tokens) {
        getDbConn().commit(em -> {
                MetaToken mergedMetaTok = em.find(MetaToken.class, metatoken.getId());
                for (ImmutableToken token : tokens) {
                    logger.info("unassign token/metatoken: " + mergedMetaTok.getName() + "<->" + token.getName());
                    Token mergedTok = em.find(Token.class, token.getId());
                    EntityRelations.unlinkTokenFromMetatoken(mergedTok, mergedMetaTok);
                }
            }
        );
    }

    public MetaToken findTokenByName(final String tokenName) {
        return getDbConn().load(em -> {
                try {
                    Query q = em.createQuery("select object(o) from MetaToken as o where o.name=:nam");
                    q.setParameter("nam", tokenName);
                    return (MetaToken) q.getSingleResult();
                } catch (NoResultException nre) {
                    return null;
                }
            }
        );
    }
}
