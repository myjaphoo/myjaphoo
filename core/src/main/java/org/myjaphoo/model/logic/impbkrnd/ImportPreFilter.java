package org.myjaphoo.model.logic.impbkrnd;

import java.io.File;
import java.util.List;

/**
 * ImportPreFilter
 */
public interface ImportPreFilter {

    List<File> filterScannedFiles(List<File> scannedFiles) throws Exception;

}
