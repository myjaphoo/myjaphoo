/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.dbhandling;

import javax.persistence.EntityManager;

/**
 *
 * @author mla
 */
public interface TransactionBoundaryDelegator {

    void doInNewTransaction(CommitBlock runnable);

    void clear();

    interface CommitBlock {

        void runCommitBlock(EntityManager em) throws Exception;
    }

    <T> T doLoading(LoaderBlock<T> loader);

    interface LoaderBlock<T> {

        T runLoadBlock(EntityManager em);
    }

    void close();
}
