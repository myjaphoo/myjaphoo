/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.hibernate.jpa.QueryHints;
import org.mlsoft.structures.Trees;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.processing.AbstractImmEntryListProcessor;
import org.myjaphoo.processing.Processing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;


/**
 * @author lang
 */
public class TokenJpaController extends AbstractDao {

    private static Logger logger = LoggerFactory.getLogger(TokenJpaController.class.getName());

    public TokenJpaController(DBConnection dbConn) {
        super(dbConn);
    }

    public void removeToken(final ImmutableToken currentSelectedToken) {
        getDbConn().commit(em -> {
            Token tokDel = em.find(Token.class, currentSelectedToken.getId());
            if (tokDel.getParent() == null) {
                throw new RuntimeException("Root can not be removed!");
            }
            Query q = em.createQuery("from Token t where t.parent = :toDelete");
            q.setParameter("toDelete", tokDel);
            if (q.getResultList().size() > 0) {
                throw new RuntimeException("token '" + tokDel.getName() + "' has children: can not remove it!");
            }
            TypedQuery<MetaToken> tq = em.createQuery("from MetaToken m where :toDelete member of m.assignedTokens",
                MetaToken.class);
            tq.setParameter("toDelete", tokDel);
            for (MetaToken mt : tq.getResultList()) {
                mt.getAssignedTokens().remove(tokDel);
            }
            EntityRelations.unlinkToken(tokDel);
            em.remove(tokDel);
        });
    }

    public void assignTok(ImmutableMovieEntry movieEntry, ImmutableToken token) {
        getDbConn().commit(em -> {
            MovieEntry attachedMovieEntry = em.find(MovieEntry.class, movieEntry.getId());
            Token attachedToken = em.find(Token.class, token.getId());

            if (!attachedToken.getMovieEntries().contains(attachedMovieEntry)) {
                attachedToken.getMovieEntries().add(attachedMovieEntry);
            }
        });
    }

    public void assignToken2MovieEntry(ImmutableToken token, List<ImmutableMovieEntry> movies) {
        AbstractImmEntryListProcessor processor = new AbstractImmEntryListProcessor() {

            @Override
            public void process(ImmutableMovieEntry entry) throws Exception {
                assignTok(entry, token);
            }
        };
        Processing.processtInOneTransaction(
            getDbConn(),
            movies,
            processor,
            "assign '" + token.getName() + "' to movies"
        );
    }

    public void create(final Token token, final ImmutableToken parentToken) {
        create(token, parentToken != null ? parentToken.getId() : null);
    }

    public void create(final Token token, final Long parentTokenId) {
        getDbConn().commit(em -> {
                if (parentTokenId != null) {
                    Token mergedParentToken = em.find(Token.class, parentTokenId);
                    token.setParent(mergedParentToken);
                }
                em.persist(token);
            }
        );
    }

    public void edit(final Token token) {
        getDbConn().commit(em -> {
                if (token.getParent().equals(token)) {
                    // selbstzuweisung: nicht möglich:
                    throw new RuntimeException("Token has itself as parent!!");
                }
                em.merge(token);
            }
        );
    }

    public Token findTokenByName(final String tokenname) {
        return getDbConn().load(em -> findTokenByName(em, tokenname));
    }

    public static Token findTokenByName(EntityManager em, final String tokenname) {
        try {
            Query q = em.createQuery("select object(o) from Token as o where o.name=:nam");
            q.setParameter("nam", tokenname);
            return (Token) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public void moveToken(ImmutableToken newParent, ImmutableToken token2Move) {
        logger.info("move " + token2Move.getName() + " under " + newParent.getName());
        if (token2Move.equals(newParent)) {
            throw new RuntimeException("Token has itself as parent!!");
        }
        getDbConn().commit(em -> {
                Token tok2Move = em.find(Token.class, token2Move.getId());
                Token newPar = em.find(Token.class, newParent.getId());
                Trees.checkCircle(newPar, tok2Move);
                Token oldParent = tok2Move.getParent();
                tok2Move.setParent(newPar);
            }
        );
    }

    public List<Token> fetchAll() {
        return getDbConn().load(em -> fetchAll(em));
    }

    public static List<Token> fetchAll(EntityManager em) {
        Query q = em.createQuery(
            "select o from Token as o left join fetch o.attributes left join fetch o.movieEntries");
        q.setHint(QueryHints.HINT_READONLY, true);
        return q.getResultList();
    }

    public Token findRootToken() {
        return getDbConn().load(em -> findRootToken(em));
    }

    public static Token findRootToken(EntityManager em) {
        Query q = em.createQuery("select object(o) from Token as o where o.parent is null");
        return (Token) q.getSingleResult();
    }

    public void unassignTokenFromMovies(ImmutableToken token, final Collection<ImmutableMovieEntry> movies) {
        getDbConn().commit(em -> {
                Token mergedToken = em.find(Token.class, token.getId());
                for (ImmutableMovieEntry movieEntry : movies) {
                    MovieEntry mergedMovie = em.find(MovieEntry.class, movieEntry.getId());
                    EntityRelations.unlinkTokenFromMovie(mergedMovie, mergedToken);
                }
            }
        );
    }
}
