package org.myjaphoo.model.logic.impbkrnd;

import groovyx.gpars.actor.DynamicDispatchActor;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.myjaphoo.model.logic.impactors.ImportMsg;
import org.myjaphoo.model.logic.impactors.StopMessage;
import org.myjaphoo.model.util.FRessourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CallBackActor
 *
 * @author mla
 * @version $Id$
 */
public class CallBackActor extends DynamicDispatchActor {
    public static Logger IMPLOGGER = LoggerFactory.getLogger(BackgroundImport.WMIMPORT);
    private int currIndex = 0;
    private int allFiles;
    private BackgroundImportListener listener;
    private long start;
    private BackgroundImport importer;
    private FRessourceBundle bundle = new FRessourceBundle(
        "org/myjaphoo/model/logic/impbkrnd/resources/BackgroundImport");

    public CallBackActor(BackgroundImportListener listener, BackgroundImport importer, int allFiles) {
        this.listener = listener;
        this.allFiles = allFiles;
        start = System.currentTimeMillis();
        this.importer = importer;
    }

    public void onMessage(StopMessage msg) {
        stop();
    }

    public void onMessage(ImportMsg.FinishedMsg msg) {
        currIndex++;
        String filePath = msg.getMsg().getNext().getAbsolutePath();
        if (currIndex < 100) {
            // fÃ¼r kleine mengen von zu importierenden files, geben wir mehr infos aus:
            IMPLOGGER.info(bundle.getString("IMPORTING_DIR", filePath)); //NOI18N
        }
        // ab 100 nur noch wenig infos ausgeben:
        if (currIndex % 100 == 0) {
            IMPLOGGER.info(bundle.getString("IMPORT_N_OF_M_FILENAME", currIndex, allFiles, filePath)); //NOI18N
        }

//        try {
//            importer.publishIt(filePath);
//        } catch (Exception e) {
//            IMPLOGGER.error("could not import file", e); //NOI18N
//        }

        long currtime = System.currentTimeMillis();
        long timeSpend = currtime - start;
        long estimatedTimeToFinish = timeSpend / currIndex * (allFiles - currIndex);
        listener.updatefileAndRemainingTime(
            filePath,
            DurationFormatUtils.formatDurationWords(estimatedTimeToFinish, true, true)
        );
    }
}