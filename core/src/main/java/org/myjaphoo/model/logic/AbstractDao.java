package org.myjaphoo.model.logic;

import groovy.lang.Closure;
import org.myjaphoo.model.dbconfig.DBConnection;

import java.util.Objects;

/**
 * Base class for all dao classes.
 * Handles the database connection.
 *
 * @author lang
 * @version $Id$
 */
public abstract class AbstractDao<T> {
    /**
     * holds a special database connection. If null, then the default- db connection gets used.
     */
    private DBConnection dbConn;

    public AbstractDao(DBConnection dbConn) {
        super();
        this.dbConn = Objects.requireNonNull(dbConn);
    }

    protected DBConnection getDbConn() {
        return dbConn;
    }

    public T findEntity(final Class<T> clazz, final Long id) {
        return getDbConn().load(em -> em.find(clazz, id));
    }

    public void withTransaction(final Closure closure) {
        getDbConn().commit(em -> closure.call(em));
    }
}
