package org.myjaphoo.model.logic.imp;

import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.project.Project;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * AllFilesDelegator
 *
 * @author mla
 * @version $Id$
 */
public class AllFilesDelegator implements ImportDelegator {

    private MovieDelegator movieDelegator;

    private PicDelegator picDelegator;

    private Project project;

    public AllFilesDelegator(Project project) {
        this.project = project;
        movieDelegator = new MovieDelegator(project);
        picDelegator = new PicDelegator(project);
    }

    @Override
    public String getScanFilter() {
        return null;
    }

    @Override
    public List<Thumbnail> createAllThumbNails(MovieEntry movieEntry, File file) {
        if (project.Pictures.is(file.getName())) {
            return picDelegator.createAllThumbNails(movieEntry, file);
        } else if (project.Movies.is(file.getName())) {
            return movieDelegator.createAllThumbNails(movieEntry, file);
        } else return Collections.emptyList();
    }

    @Override
    public void getMediaInfos(MovieEntry movieEntry) {
        if (project.Pictures.is(movieEntry.getName())) {
            picDelegator.getMediaInfos(movieEntry);
        } else if (project.Movies.is(movieEntry.getName())) {
            movieDelegator.getMediaInfos(movieEntry);
        }
    }
}
