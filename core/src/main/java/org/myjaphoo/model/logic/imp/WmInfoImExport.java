/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.model.logic.imp;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.project.Project;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Export to wminfo files.
 */
public class WmInfoImExport {

    public static String WMINFO_FILETYPE = "wminfo";
    public static String WMINFO_POSTFIX = "." + WMINFO_FILETYPE;

    private static JAXBContext jaxb;

    static {
        try {
            jaxb = JAXBContext.newInstance(WmInfo.class);
        } catch (JAXBException e) {
            throw new IllegalStateException("error initializing jaxb!", e);
        }
    }

    private Project project;

    public WmInfoImExport(Project project) {
        this.project = project;
    }

    public void export(File file, EntryRef entryRef) {
        project.connection.load(em -> {
            MovieEntry entry = em.find(MovieEntry.class, entryRef.getId());
            export(file, entry, entryRef.getTokens());
            return null;
        });
    }

    public static void export(File file, MovieEntry entry) {
        export(file, entry, HashSet.empty());
    }

    public static void export(File file, MovieEntry entry, Set<ImmutableToken> optionalTokens) {

        try (FileOutputStream fos = new FileOutputStream(file)) {
            Marshaller m = jaxb.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            WmInfo info = new WmInfo();

            info.checksum = entry.getChecksumCRC32();
            info.sha1 = entry.getSha1();
            info.rating = entry.getRating();

            if (optionalTokens.size() > 0) {
                String[] arr = new String[optionalTokens.size()];
                int i = 0;
                for (ImmutableToken token : optionalTokens) {
                    arr[i] = token.getName();
                    i++;
                }
                info.tokens = arr;
            }

            info.thumb1 = entry.getThumbnail1();
            if (entry.getThumbnails().size() > 1) {
                info.thumb2 = entry.getThumbnails().get(1).getThumbnail();
            }
            if (entry.getThumbnails().size() > 2) {
                info.thumb3 = entry.getThumbnails().get(2).getThumbnail();
            }
            if (entry.getThumbnails().size() > 3) {
                info.thumb4 = entry.getThumbnails().get(3).getThumbnail();
            }
            if (entry.getThumbnails().size() > 4) {
                info.thumb5 = entry.getThumbnails().get(4).getThumbnail();
            }

            info.width = entry.getMovieAttrs().getWidth();
            info.height = entry.getMovieAttrs().getHeight();
            info.length = entry.getMovieAttrs().getLength();
            info.fps = entry.getMovieAttrs().getFps();
            info.bitrate = entry.getMovieAttrs().getBitrate();
            info.format = entry.getMovieAttrs().getFormat();

            m.marshal(info, fos);
        } catch (IOException | JAXBException e) {
            throw new RuntimeException("error writing wminfo file!", e);
        }
    }


    public static boolean existsWmInfo(File file) {
        return constructWmInfoFileName(file).exists();
    }

    public static File constructWmInfoFileName(File file) {
        // construct filename:
        String filename = file.getAbsolutePath() + WmInfoImExport.WMINFO_POSTFIX;
        File filetoread = new File(filename);
        return filetoread;
    }
}
