package org.myjaphoo.model.logic.impbkrnd;

/**
 * Listener for background import updates.
 *
 * @author mla
 * @version $Id$
 */
public interface BackgroundImportListener {
    void message(String message);

    void stopped();

    void progressUpdate(int perc);

    void updatefileAndRemainingTime(String filePath, String formatDurationWords);
}
