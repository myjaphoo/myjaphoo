package org.myjaphoo.model.logic.imp;

import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * AllFilesDelegator
 *
 * @author mla
 * @version $Id$
 */
public class SpecificFilesDelegator implements ImportDelegator {

    public static final Logger LOGGER = LoggerFactory.getLogger(SpecificFilesDelegator.class.getName());

    private MovieDelegator movieDelegator;

    private PicDelegator picDelegator;

    private Project project;

    public SpecificFilesDelegator(Project project) {
        this.project = project;
        movieDelegator = new MovieDelegator(project);
        picDelegator = new PicDelegator(project);
    }

    @Override
    public String getScanFilter() {
        return project.prefs.PRF_SPECIFICFILTER.getVal();
    }

    @Override
    public List<Thumbnail> createAllThumbNails(MovieEntry movieEntry, File file) {
        if (project.Pictures.is(file.getName())) {
            return picDelegator.createAllThumbNails(movieEntry, file);
        } else if (project.Movies.is(file.getName())) {
            return movieDelegator.createAllThumbNails(movieEntry, file);
        } else {
            List<Thumbnail> result = new ArrayList<>();
            try {
                MovieAdditionalFilesImporter addImporter = new MovieAdditionalFilesImporter();
                List<Thumbnail> addThumbs = addImporter.scanAndCreateAdditionalFiles(project.prefs, movieEntry, file);
                result.addAll(addThumbs);
            } catch (Exception e) {
                LOGGER.error("error creating thumb nails!", e);
            }
            return result;
        }
    }

    @Override
    public void getMediaInfos(MovieEntry movieEntry) {
        if (project.Pictures.is(movieEntry)) {
            picDelegator.getMediaInfos(movieEntry);
        } else if (project.Movies.is(movieEntry)) {
            movieDelegator.getMediaInfos(movieEntry);
        }
    }
}
