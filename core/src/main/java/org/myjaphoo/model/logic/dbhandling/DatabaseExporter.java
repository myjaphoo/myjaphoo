/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.dbhandling;

import io.vavr.collection.Seq;
import io.vavr.collection.Tree;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.mlsoft.common.acitivity.Channel;
import org.mlsoft.common.acitivity.ChannelManager;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.MetaTokenZipper;
import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.db.BookMark;
import org.myjaphoo.model.db.ChangeLog;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.SavedGroovyScript;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.ScriptJpaController;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.vavr.collection.Tree.Order.LEVEL_ORDER;
import static org.myjaphoo.model.cache.Immutables.newMetaTokenZipperIterable;
import static org.myjaphoo.model.cache.Immutables.newTokenZipperIterable;

/**
 * Experimental class to export the database information to another database.
 * This code copies the complete information from one database  to another.
 * This could be used e.g. to migrate from one database vendor to another
 * or to create backup databases on another system.
 *
 * @author lang
 */
public class DatabaseExporter {

    private static Logger logger = LoggerFactory.getLogger(DatabaseExporter.class);
    private MovieEntryJpaController moviejpa;

    public void exportToDatabase(Project sourceProject, DatabaseConfiguration targetDBConfig) {
        DBConnection connection = new DBConnection(targetDBConfig);
        try {
            exportToDatabase(sourceProject, connection);
        } finally {
            connection.close();
        }
    }

    public void exportToDatabase(Project sourceProject, DBConnection targetConn) {

        moviejpa = new MovieEntryJpaController(sourceProject.connection);

        Channel channel = ChannelManager.createChannel(
            DatabaseExporter.class,
            "exporting to " + targetConn.getConfiguration().getName()
        );
        channel.startActivity();
        channel.setProgressSteps(8);
        targetConn.open();
        EntityManager em = targetConn.createEntityManager();
        try {
            em.getTransaction().begin();
            Map<Long, Long> sourceKeyToTargetKeyMapping = exportEntries(sourceProject, channel, em);
            commit(em);
            channel.nextProgress();

            channel.nextProgress();
            exportTags(sourceProject, channel, em, sourceKeyToTargetKeyMapping);
            commit(em);
            channel.nextProgress();
            exportMetaTags(sourceProject, channel, em);
            commit(em);
            channel.nextProgress();
            em.clear();
            commit(em);
            exportBookMarks(channel, em);
            channel.nextProgress();
            em.clear();
            commit(em);
            exportChronics(channel, em);
            channel.nextProgress();
            em.clear();
            commit(em);
            exportChangelogs(channel, em);
            channel.nextProgress();
            commit(em);
            exportScripts(sourceProject, channel, em);

            em.clear();
            commit(em);
            exportThumbs(sourceProject, channel, em, sourceKeyToTargetKeyMapping);
            commit(em);
            channel.nextProgress();
        } finally {
            em.close();
            channel.emphasisedMessage("export finished!");
            channel.stopActivity();
        }
    }

    private void exportTags(
        Project sourceProject,
        final Channel channel, EntityManager em,
        final Map<Long, Long> sourceKeyToTargetKeyMapping
    ) {

        // get all tokens in level order (starting with the root):
        ImmutableModel model = sourceProject.cacheActor.getImmutableModel();
        final Seq<ImmutableToken> allTokens = model.getTokenTree().getRoot().traverse(
            LEVEL_ORDER).map(Tree.Node::getValue);

        // export all except the internal root token which is already existing
        // in a initialized database:
        boolean firstOne = true;
        for (TokenZipper token : newTokenZipperIterable(model, allTokens)) {
            if (!firstOne) {
                channel.message("export tag " + token.getName());
                Token clone = token.getRef().createMutable();

                clone.setId(null);

                // set the parent one:
                if (token.getParentZipper() != null) {
                    Token parentToken = getTagByName(em, token.getParentZipper().getName());
                    clone.setParent(parentToken);
                }

                em.persist(clone);

                // recreate the relation to movies:
                for (ImmutableMovieEntry originEntry : token.getAssignedMovieEntries()) {
                    Long idInTargetDB = sourceKeyToTargetKeyMapping.get(originEntry.getId());
                    MovieEntry targetEntry = em.find(MovieEntry.class, idInTargetDB);
                    clone.getMovieEntries().add(targetEntry);
                }
            }
            firstOne = false;
        }
    }

    public Token getTagByName(EntityManager em, String name) {
        Query query = em.createQuery("from Token where name = :name");
        query.setParameter("name", name);
        Token tok = (Token) query.getSingleResult();
        return tok;
    }

    public MetaToken getMetaTagByName(EntityManager em, String name) {
        Query query = em.createQuery("from MetaToken where name = :name");
        query.setParameter("name", name);
        MetaToken metaTag = (MetaToken) query.getSingleResult();
        return metaTag;
    }

    private void exportMetaTags(Project sourceProject, final Channel channel, EntityManager em) {
        ImmutableModel model = sourceProject.cacheActor.getImmutableModel();
        final Seq<ImmutableMetaToken> allTokens = model.getMetaTokenTree().getRoot().traverse(
            LEVEL_ORDER).map(Tree.Node::getValue);

        // export all except the internal root token which is already existing
        // in a initialized database:
        boolean firstOne = true;
        for (MetaTokenZipper metatag : newMetaTokenZipperIterable(model, allTokens)) {
            if (!firstOne) {
                channel.message("export metatag " + metatag.getName());
                MetaToken clone = metatag.getRef().createMutable();
                clone.setId(null);

                // set the parent one:
                if (metatag.getParentZipper() != null) {

                    MetaToken parentMetaTag = getMetaTagByName(em, metatag.getParentZipper().getName());
                    clone.setParent(parentMetaTag);
                }
                // assign relations to tags:
                for (ImmutableToken token : metatag.getAssignedTokens()) {
                    Token tokenInDB = getTagByName(em, token.getName());
                    clone.getAssignedTokens().add(tokenInDB);
                }
                em.persist(clone);
            }
            firstOne = false;
        }
    }

    private void exportBookMarks(final Channel channel, EntityManager em) {
        final List<BookMark> bms = moviejpa.findBookMarkEntities();
        for (BookMark bm : bms) {
            channel.message("export bookmark " + bm.getName());
            BookMark copy = new BookMark();
            copy.setMenuPath(bm.getMenuPath());
            copy.setName(bm.getName());
            copy.setDescr(bm.getDescr());
            copy.setView(bm.getView());
            em.persist(copy);
        }
    }

    private void exportChronics(Channel channel, EntityManager em) {
        final List<ChronicEntry> chronics = moviejpa.findChronicEntryEntities();
        channel.message("export all chronics ");
        for (ChronicEntry chronic : chronics) {
            ChronicEntry copy = new ChronicEntry();
            copy.setView(chronic.getView());
            em.persist(copy);
        }
    }

    private void exportChangelogs(Channel channel, EntityManager em) {
        final List<ChangeLog> chronics = moviejpa.findChangeLogEntities();
        channel.message("export all changelogs ");
        for (ChangeLog changelog : chronics) {
            ChangeLog copy = new ChangeLog();

            copy.setCltype(changelog.getCltype());
            copy.setCreatedMs(changelog.getCreatedMs());
            copy.setMsg(changelog.getMsg());
            copy.setObjDescription(changelog.getObjDescription());
            em.persist(copy);
        }
    }

    private void exportScripts(Project sourceProject, Channel channel, EntityManager em) {
        final List<SavedGroovyScript> scripts = new ScriptJpaController(sourceProject.connection).findScriptEntities();
        channel.message("export all scripts ");
        for (SavedGroovyScript script : scripts) {
            SavedGroovyScript copy = new SavedGroovyScript();
            copy.setDescr(script.getDescr());
            copy.setMenuPath(script.getMenuPath());
            copy.setName(script.getName());
            copy.setScriptText(script.getScriptText());
            copy.setScriptType(script.getScriptType());
            em.persist(copy);
        }
    }


    private Map<Long, Long> exportEntries(Project sourceProject, Channel channel, EntityManager em) {
        channel.message("export all media entries ");
        final io.vavr.collection.List<ImmutableMovieEntry> entries = sourceProject.cacheActor.getImmutableModel().getEntryList();
        final Map<Long, Long> sourceKeyToTargetKeyMapping = new HashMap<>(entries.size() * 2);
        BatchCommit batchCommit = new BatchCommit();

        for (ImmutableMovieEntry entry : entries) {
            MovieEntry clone = entry.createMutable();
            clone.setId(null);
            batchCommit.persist(em, clone);
            sourceKeyToTargetKeyMapping.put(entry.getId(), clone.getId());
        }
        return sourceKeyToTargetKeyMapping;
    }

    /**
     * exports the thumbnails. this is the most critical one.
     * The thumbnail table contains the most of the data of a database.
     * Therefore for very big databases, the attemt is to
     * - use scrollable results from hibernate to read the thumbnail data
     * - not to map the read result in Thumbnail-Objects (which would lead to
     * load the relation to movie entry by hinbernate), but instead to use
     * a jpa query to select all relevant columns directly.
     * - commit often on the target db during insert, to minimize temporary storage for
     * rollback
     *
     * @param channel
     * @param em
     * @param sourceKeyToTargetKeyMapping
     */
    private void exportThumbs(
        Project sourceProject,
        Channel channel, EntityManager em,
        final Map<Long, Long> sourceKeyToTargetKeyMapping
    ) {
        EntityManager sourceEm = sourceProject.connection.createEntityManager();
        channel.message("export all thumb nails ");
        final Session SourceSession = (Session) sourceEm.getDelegate();
        try {
            // load thumbnails as scrollable result and NOT as entity object 
            // to minimize the dramatic overhead when accessing the movie entry relation

            final ScrollableResults cursor = SourceSession.createQuery(
                "select t.movieEntry.id, t.thumbnail from Thumbnail t order by t.movieEntry.id").scroll();

            BatchCommit batchCommit = new BatchCommit();
            int count = 0;
            while (cursor.next()) {
                Long movie_id = cursor.getLong(0);
                byte[] byteData = cursor.getBinary(1);

                Long targetId = sourceKeyToTargetKeyMapping.get(movie_id);
                MovieEntry entryInTargetDB = em.find(MovieEntry.class, targetId);
                Thumbnail tn = new Thumbnail();
                tn.setMovieEntry(entryInTargetDB);
                tn.setThumbnail(byteData);
                batchCommit.persist(em, tn);
                count++;

                if (count % 100 == 0) {
                    SourceSession.flush();
                    SourceSession.clear();
                }
            }
        } finally {
            sourceEm.close();
        }

    }

    static class BatchCommit {

        private int batchCounter = 0;
        private static int batchMax = 100;

        public void persist(EntityManager em, Object o) {
            batchCounter++;
            em.persist(o);
            if (batchCounter % batchMax == 0) {
                em.flush();
                em.clear();
                em.getTransaction().commit();
                em.getTransaction().begin();
            }
        }
    }

    private static void commit(EntityManager em) {
        em.getTransaction().commit();
        em.getTransaction().begin();
    }

}
