/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.impactors;


import groovyx.gpars.actor.DynamicDispatchActor;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.logic.TokenJpaController;
import org.myjaphoo.model.logic.dbhandling.ThreadLocalTransactionHandler;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

/**
 * Dieser Actor übernimmt das anlegen u. verlinken von Tokens
 * für Entries innerhalb des Import-Prozesses.
 * Damit werden alle Tokenimporte sequenziell durchgeführt, anstatt
 * parallel, was ansonsten zusätzlichen Synchronisierungsaufwand bedeuted hätte.
 *
 * @author mla
 */

public class ImportTokenActor extends DynamicDispatchActor {

    private static Logger logger = LoggerFactory.getLogger(ImportTokenActor.class.getName());
    /**
     * Eigenen default transaction handler einrichten, der einen eigenen
     * EntitManager hat.
     */
    private ThreadLocalTransactionHandler tr;

    public ImportTokenActor(Project project) {
        tr = new ThreadLocalTransactionHandler(project.connection);
    }

    public void onMessage(StopMessage msg) {
        stop();
    }

    public void onMessage(AssignTokenMsg msg) {
        try {
            createAndAssignTokens(msg);

        } catch (Exception e) {
            logger.error("error creating/assigning tokens!", e);
        }
    }

    private void createAndAssignTokens(final AssignTokenMsg msg) {
        tr.doInNewTransaction(em -> {
            MovieEntry entry = em.find(MovieEntry.class, msg.getMovieId());
            for (String tokenname : msg.getTokenNames()) {
                assignToken(em, entry, tokenname);
            }
        });
    }

    private void assignToken(EntityManager em, MovieEntry entry, String tokenname) {

        Token token = TokenJpaController.findTokenByName(em, tokenname);
        if (token == null) {
            // need to create the token:
            Token newToken = new Token();
            newToken.setName(tokenname);
            Token rootToken = TokenJpaController.findRootToken(em);
            newToken.setParent(rootToken);
            em.persist(newToken);
            token = newToken;
        }
        if (!token.getMovieEntries().contains(entry)) {
            token.getMovieEntries().add(entry);
        }
    }
}
