package org.myjaphoo.model.logic.impbkrnd;

import org.myjaphoo.model.logic.imp.WmInfoImExport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * FilterOutFilesWithWmInfoFiles
 *
 * @author mla
 * @version $Id$
 */
public class FilterOutFilesWithWmInfoFiles implements ImportPreFilter {


    public FilterOutFilesWithWmInfoFiles() {
    }

    @Override
    public List<File> filterScannedFiles(List<File> scannedFiles) throws Exception {

        // prefilter files:
        ArrayList<File> filteredFiles = new ArrayList<File>();
        for (File file : scannedFiles) {
            if (!WmInfoImExport.existsWmInfo(file)) {
                filteredFiles.add(file);
            }
        }
        return filteredFiles;
    }
}
