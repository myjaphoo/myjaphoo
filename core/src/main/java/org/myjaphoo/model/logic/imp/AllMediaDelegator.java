package org.myjaphoo.model.logic.imp;

import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.project.Project;

import java.io.File;
import java.util.List;

/**
 * AllMediaDelegator
 *
 * @author mla
 * @version $Id$
 */
public class AllMediaDelegator implements ImportDelegator {

    private MovieDelegator movieDelegator;

    private PicDelegator picDelegator;

    private MyjaphooCorePrefs prefs;

    public AllMediaDelegator(Project project) {
        movieDelegator = new MovieDelegator(project);
        picDelegator = new PicDelegator(project);
        this.prefs = project.prefs;
    }

    @Override
    public String getScanFilter() {
        return movieDelegator.getScanFilter() + ";" + picDelegator.getScanFilter();
    }

    @Override
    public List<Thumbnail> createAllThumbNails(MovieEntry movieEntry, File file) {
        if (prefs.Pictures.is(file.getName())) {
            return picDelegator.createAllThumbNails(movieEntry, file);
        } else if (prefs.Movies.is(file.getName())) {
            return movieDelegator.createAllThumbNails(movieEntry, file);
        } else {
            throw new IllegalArgumentException("illegal file!");
        }
    }

    @Override
    public void getMediaInfos(MovieEntry movieEntry) {
        if (prefs.Pictures.is(movieEntry)) {
            picDelegator.getMediaInfos(movieEntry);
        } else if (prefs.Movies.is(movieEntry)) {
            movieDelegator.getMediaInfos(movieEntry);
        } else {
            throw new IllegalArgumentException("illegal file!");
        }
    }
}
