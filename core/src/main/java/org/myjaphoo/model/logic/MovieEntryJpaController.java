/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.hibernate.jpa.QueryHints;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.db.BookMark;
import org.myjaphoo.model.db.ChangeLog;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.filterparser.Substitution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * dao class for movie entries.
 *
 * @author lang
 */
public class MovieEntryJpaController extends AbstractDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieEntryJpaController.class);


    public MovieEntryJpaController(DBConnection dbConn) {
        super(dbConn);
    }

    public void create(final ChangeLog changeLog) {
        getDbConn().commit(em -> em.persist(changeLog));
    }

    public void create(final ChronicEntry movieEntry) {
        getDbConn().commit(em -> em.persist(movieEntry));
    }

    public void create(final BookMark bookmark) {
        getDbConn().commit(em -> em.persist(bookmark));
    }

    public void create(final MovieEntry movieEntry) {
        getDbConn().commit(em -> em.persist(movieEntry));
    }

    public void edit(final MovieEntry movieEntry) throws Exception {
        getDbConn().commit(em -> em.merge(movieEntry));
    }

    public void edit(final BookMark bookmark) {
        getDbConn().commit(em -> em.merge(bookmark));
    }

    public void removeMovieEntry(final ImmutableMovieEntry entry) {
        getDbConn().commit(em -> {
                MovieEntry mentry = em.find(MovieEntry.class, entry.getId());
                for (Thumbnail tn : mentry.getThumbnails()) {
                    //tn.setMovieEntry(null);
                    em.remove(tn);
                }
                mentry.getThumbnails().clear();
                Query query = em.createNativeQuery("delete from Token_MovieEntry e where movieentries_id = :mid");
                query.setParameter("mid", mentry.getId());
                query.executeUpdate();

                em.remove(mentry);
            }
        );
    }

    public List<MovieEntry> fetchAll() {
        return getDbConn().load(em -> fetchAll(em));
    }

    public static List<MovieEntry> fetchAll(EntityManager em) {
        LOGGER.debug("find/load all movie entries");
        Query q = em.createQuery("select o from MovieEntry o left join fetch o.attributes");
        q.setHint(QueryHints.HINT_READONLY, true);
        return q.getResultList();
    }

    public List<ChronicEntry> findChronicEntryEntities() {
        return findChronicEntryEntities(true, -1, -1);
    }

    public List<ChronicEntry> findChronicEntryEntities(int maxResults, int firstResult) {
        return findChronicEntryEntities(false, maxResults, firstResult);
    }

    private List<ChronicEntry> findChronicEntryEntities(
        final boolean all, final int maxResults, final int firstResult
    ) {
        return getDbConn().load(em -> {
                Query q = em.createQuery("select object(o) from ChronicEntry as o order by id desc");
                if (!all) {
                    q.setMaxResults(maxResults);
                    q.setFirstResult(firstResult);
                }
                return q.getResultList();
            }
        );
    }

    public List<ChangeLog> findChangeLogEntities() {
        return findChangeLogEntities(true, -1, -1);
    }

    private List<ChangeLog> findChangeLogEntities(final boolean all, final int maxResults, final int firstResult) {

        return getDbConn().load(em -> {
                Query q = em.createQuery("select object(o) from ChangeLog as o order by id desc");
                if (!all) {
                    q.setMaxResults(maxResults);
                    q.setFirstResult(firstResult);
                }
                return q.getResultList();
            }
        );
    }

    public static Map<String, Substitution> createSubst(List<BookMark> bookmarks) {
        HashMap<String, Substitution> result = new HashMap<String, Substitution>();
        for (BookMark bm : bookmarks) {
            Substitution subst = new Substitution(bm.getName(), bm.getView().getCombinedFilterExpression());
            result.put(bm.getName(), subst);
        }
        return result;
    }

    public List<BookMark> findBookMarkEntities() {
        return findBookMarkEntities(true, -1, -1);
    }

    private List<BookMark> findBookMarkEntities(final boolean all, final int maxResults, final int firstResult) {
        return getDbConn().load(em -> {
                Query q = em.createQuery("select object(o) from BookMark as o order by name");
                if (!all) {
                    q.setMaxResults(maxResults);
                    q.setFirstResult(firstResult);
                }
                return q.getResultList();
            }
        );
    }

    public void removeBookMark(final BookMark bm) {
        getDbConn().commit(em -> {
                BookMark bmmerged = em.merge(bm);
                em.remove(bmmerged);
            }
        );
    }
}
