package org.myjaphoo.model.logic.impactors;

import org.myjaphoo.model.logic.imp.ImportDelegator;
import org.myjaphoo.project.Project;

/**
 * ImportWorkerActorFactory
 *
 * @author mla
 * @version $Id$
 */
public interface ImportWorkerActorFactory {

    ImportingWorkerActor createWorker(
        Project project, ImportTokenActor importTokenActor, ImportDelegator importDelegator
    );
}
