/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.dbhandling;

import org.myjaphoo.model.dbconfig.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A transaction handler which holds an entity manager per thread.
 * It uses a nested transaction for nested calls to doInNewTransaction.
 * The outest call then commits the transaction and closes the entity manager afterwards.
 *
 * @author mla
 */
public class ThreadLocalTransactionHandler implements TransactionBoundaryDelegator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadLocalTransactionHandler.class.getName());

    private final DBConnection connection;

    public ThreadLocalTransactionHandler(final DBConnection connection) {
        this.connection = connection;
    }

    @Override
    public void doInNewTransaction(CommitBlock runnable) {
        new NestableTransaction(connection).doInTransaction(runnable);
    }

    @Override
    public <T> T doLoading(LoaderBlock<T> loader) {
        Object[] tarr = new Object[1];
        doInNewTransaction(em -> tarr[0] = loader.runLoadBlock(em));
        return (T) tarr[0];
    }

    @Override
    public void clear() {
        // should not be necessary anymore. em lives only for the time of the transaction.
        // if needed one could inside that transaction clear it.
        //LOGGER.info("********* clearing entity manager");
        //getEntityManager().clear();
    }

    public void close() {
        // should not be necessary anymore. entity manager gets closed after each (outest) transaction.
        // explicit closing makes no sense
    }
}
