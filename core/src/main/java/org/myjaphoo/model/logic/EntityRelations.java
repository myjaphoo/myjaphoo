/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

/**
 * Hilfs-Funktionen, um relationen zwischen den Entities zu ändern.
 * @author mla
 */
public class EntityRelations {

    /**
     * Entfernt ein Metatoken von allen möglichen Verlinkungen zu anderen Entities.
     */
    public static void unlinkMetaToken(MetaToken tokDel) {
        tokDel.getAssignedTokens().clear();
        MetaToken parent = tokDel.getParent();
    }

    public static void unlinkToken(Token tok) {
        // alle relationen auf movies entfernen:
        tok.getMovieEntries().clear();
        // parent relation:
        if (tok.getParent() != null) {
            tok.setParent(null);
        }
    }
    
    public static void linkTokenToMetatoken(Token token, MetaToken metaToken) {
        if (!metaToken.getAssignedTokens().contains(token)) {
            metaToken.getAssignedTokens().add(token);
        }
    }

    public static void unlinkTokenFromMetatoken(Token token, MetaToken metatoken) {
        if (metatoken.getAssignedTokens().contains(token)) {
            metatoken.getAssignedTokens().remove(token);
        }
    }

    public static void unlinkTokenFromMovie(MovieEntry movieEntry, Token token) {
        if (token.getMovieEntries().contains(movieEntry)) {
            token.getMovieEntries().remove(movieEntry);
        }
    }
    
}
