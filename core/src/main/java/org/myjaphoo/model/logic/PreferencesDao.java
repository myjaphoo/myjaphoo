/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic;

import org.myjaphoo.model.dbconfig.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;


/**
 * dao class for preferences saved in the database
 *
 * @author lang
 */
public class PreferencesDao extends AbstractDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesDao.class);
    
    public PreferencesDao(DBConnection dbConn) {
        super(dbConn);
    }

    public <T> void create(final T entity) {
        getDbConn().commit(em -> em.persist(entity));
    }

    public <T> void edit(final T entity) throws Exception {
        getDbConn().commit(em -> em.merge(entity));
    }

    public <T> T find(final String key, final Class<T> clazz) {
        return getDbConn().load(em -> em.find(clazz, key));
    }

    public void remove(final String key) {
        getDbConn().commit(em -> {
                Query query = em.createQuery("delete from Prefs where id = :id");
                query.setParameter("id", key);
                query.executeUpdate();
            }
        );
    }
}
