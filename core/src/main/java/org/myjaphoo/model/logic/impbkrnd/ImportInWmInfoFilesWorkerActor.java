/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.logic.impbkrnd;


import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.model.logic.CheckSumCalculator;
import org.myjaphoo.model.logic.dbhandling.ThreadLocalTransactionHandler;
import org.myjaphoo.model.logic.dbhandling.TransactionBoundaryDelegator;
import org.myjaphoo.model.logic.imp.ImportDelegator;
import org.myjaphoo.model.logic.imp.WmInfoImExport;
import org.myjaphoo.model.logic.impactors.ImportMsg;
import org.myjaphoo.model.logic.impactors.ImportTokenActor;
import org.myjaphoo.model.logic.impactors.ImportingWorkerActor;
import org.myjaphoo.model.logic.impactors.StopMessage;
import org.myjaphoo.model.logic.impactors.WorkerMsg;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.myjaphoo.model.logic.imp.WmInfoImExport.constructWmInfoFileName;


/**
 * @author lang
 */

public class ImportInWmInfoFilesWorkerActor extends ImportingWorkerActor {

    private final Logger logger = LoggerFactory.getLogger(ImportInWmInfoFilesWorkerActor.class);
    private final boolean doNotBuildChecksum;
    /**
     * Eigenen default transaction handler einrichten, der einen eigenen
     * EntitManager hat.
     */
    private TransactionBoundaryDelegator tr;
    private ImportDelegator importDelegator;
    private ImportTokenActor importTokenActor;

    public ImportInWmInfoFilesWorkerActor(
        Project project, ImportTokenActor importTokenActor, ImportDelegator importDelegator
    ) {
        tr = new ThreadLocalTransactionHandler(project.connection);
        this.importDelegator = importDelegator;
        this.importTokenActor = importTokenActor;
        doNotBuildChecksum = project.prefs.PRF_INTERNAL_DONOTBUILDCHECKSUM.getVal();
    }

    public void onMessage(StopMessage msg) {
        stop();
    }

    public void onMessage(WorkerMsg msg) {
        try {
            importMovie(msg);
        } catch (Exception ex) {
            // alle Fehler catchen, damit auf jeden Fall eine messgage zurückgeschickt wird.
            // ansonsten würde evtl. der Aufrufer ewig warten...
            logger.error("import failed", ex);
        }
        // work on this: import it usw...
        // send back to caller that it is finished.
        msg.getCaller().send(new ImportMsg.FinishedMsg(msg, this));
    }

    private void importMovie(WorkerMsg msg) throws IOException {
        File file = msg.getNext();
        File dir = file.getParentFile();
        String path = file.getCanonicalPath();
        logger.debug(path + "\t" + file.length());
        final MovieEntry movieEntry = new MovieEntry();
        movieEntry.setName(file.getName());
        movieEntry.setCanonicalDir(dir.getCanonicalPath());
        movieEntry.setFileLength(file.length());

        if (!doNotBuildChecksum) {
            logger.debug("calculating checksum for " + path);
            CheckSumCalculator.Checksums checkSums = CheckSumCalculator.build(file);
            checkSums.applyChecksums(movieEntry);
        }
        logger.debug("creating thumbs");
        List<Thumbnail> thumbs = importDelegator.createAllThumbNails(movieEntry, file);
        movieEntry.getThumbnails().addAll(thumbs);

        logger.debug("extracting media information");
        importDelegator.getMediaInfos(movieEntry);

        WmInfoImExport.export(constructWmInfoFileName(file), movieEntry);
    }

}
