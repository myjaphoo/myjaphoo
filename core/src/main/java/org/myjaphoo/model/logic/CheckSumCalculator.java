package org.myjaphoo.model.logic;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.myjaphoo.model.db.MovieEntry;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/**
 * CheckSumCalculator
 * @author mla
 * @version $Id$
 */
public class CheckSumCalculator {
    private static final int STREAM_BUFFER_LENGTH = 1024 * 1024;

    public static String buildSha1(File f) throws IOException {
        try (FileInputStream fis = new FileInputStream(f); BufferedInputStream bis = new BufferedInputStream(fis, 10000000)) {
            String sha1 = org.apache.commons.codec.digest.DigestUtils.sha1Hex(bis);
            return sha1;
        }
    }

    public static class Checksums {
        public String sha1;
        public long crc32;

        public boolean applyChecksums(MovieEntry movieEntry) {
            boolean changed = false;
            if (movieEntry.getChecksumCRC32() == null) {
                movieEntry.setChecksumCRC32(crc32);
                changed = true;
            } else if (movieEntry.getChecksumCRC32().longValue() != crc32) {
                throw new RuntimeException("error, overwriting different crc32 checksum! existing is " + movieEntry.getChecksumCRC32() + "; new calculated is " + crc32);
            }
            if (movieEntry.getSha1() == null) {
                movieEntry.setSha1(sha1);
                changed = true;
            } else if (!movieEntry.getSha1().equals(sha1)) {
                throw new RuntimeException("error, overwriting different sha1 checksum! existing is " + movieEntry.getSha1() + "; new calculated is " + sha1);
            }
            return changed;
        }
    }

    public static Checksums build(File f) throws IOException {
        MessageDigest sha1Digest = DigestUtils.getSha1Digest();
        CRC32 crc = new CRC32();
        try (FileInputStream fis = new FileInputStream(f);
             CheckedInputStream in = new CheckedInputStream(fis, crc);) {
            final byte[] buffer = new byte[STREAM_BUFFER_LENGTH];
            int read = in.read(buffer, 0, STREAM_BUFFER_LENGTH);

            while (read > -1) {
                sha1Digest.update(buffer, 0, read);
                read = in.read(buffer, 0, STREAM_BUFFER_LENGTH);
            }
        }
        Checksums result = new Checksums();
        result.sha1 = Hex.encodeHexString(sha1Digest.digest());
        result.crc32 = crc.getValue();
        //long checksum = org.apache.commons.io.FileUtils.checksumCRC32(file);
        return result;
    }
}
