package org.myjaphoo.model.logic.impbkrnd;

import org.myjaphoo.model.logic.MovieImport;
import org.myjaphoo.model.logic.impbkrnd.ImportPreFilter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * FilterOutExistingEntries
 *
 * @author mla
 * @version $Id$
 */
public class FilterOutExistingEntries implements ImportPreFilter {

    private MovieImport importer;

    public FilterOutExistingEntries(MovieImport importer) {
        this.importer = importer;
    }

    @Override
    public List<File> filterScannedFiles(List<File> scannedFiles) throws Exception {
        Set<String> allCanonicalPaths = importer.getCanonicalUnifiedPathsOfAllEntries();

        // prefilter files:
        ArrayList<File> filteredFiles = new ArrayList<File>();
        for (File file : scannedFiles) {
            if (!allCanonicalPaths.contains(MovieImport.unifyPath(file.getCanonicalPath()))) {
                filteredFiles.add(file);
            } else {
                //logger.info("skipping " + file.getAbsolutePath() + "; already exists in db...");
            }
        }
        return filteredFiles;
    }
}
