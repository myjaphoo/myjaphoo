/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.model.logic.imp;

import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.project.Project;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mla
 */
public class TextDelegator implements ImportDelegator {

    private Project project;

    public TextDelegator(Project project) {
        this.project = project;
    }

    @Override
    public String getScanFilter() {
        return project.prefs.PRF_TEXTFILTER.getVal();
    }

    @Override
    public List<Thumbnail> createAllThumbNails(MovieEntry movieEntry, File file) {
        return new ArrayList<>();
    }

    @Override
    public void getMediaInfos(MovieEntry movieEntry) {

    }

}
