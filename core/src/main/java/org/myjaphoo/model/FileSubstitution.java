/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model;

/**
 * @author mla
 */
public interface FileSubstitution {

    /**
     * quasi dasselbe wie substitute: suche mittels substitution, ob u. wo das file existiert.
     * Falls es nicht auffindbar ist, dann returniere null
     *
     * @param canonicalPath
     *
     * @return
     */
    String locateFileOnDrive(String canonicalPath);

    String substitude(String canonicalPath);

}
