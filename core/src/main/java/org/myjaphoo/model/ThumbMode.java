/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model;

/**
 * possible selectable thumb view modes.
 *
 * @author lang
 */
public enum ThumbMode {

    EXTENDED_TABLEVIEW(ThumbDisplayFilterResultMode.PlAINLIST),
    ALTTHUMB(ThumbDisplayFilterResultMode.PlAINLIST),
    STRIPES(ThumbDisplayFilterResultMode.SLIDESGROUPED);
    private ThumbDisplayFilterResultMode mode;

    ThumbMode(ThumbDisplayFilterResultMode mode) {
        this.mode = mode;
    }

    /**
     * @return the mode
     */
    public ThumbDisplayFilterResultMode getMode() {
        return mode;
    }
}
