package org.myjaphoo.model.externalPrograms;

import static org.myjaphoo.MyjaphooCorePrefs.FFMPEGTHUMBNAILER_LINUX_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.FFMPEGTHUMBNAILER_WINDOWS_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.FFMPEG_LINUX_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.FFMPEG_WINDOWS_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.KMPLAYER_LINUX_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.MPLAYER_LINUX_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.MPLAYER_WINDOWS_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.VLC_LINUX_EXE;
import static org.myjaphoo.MyjaphooCorePrefs.VLC_WINDOWS_EXE;

/**
 * All external programs that get used in this application.
 *
 * @author mla
 * @version $Id$
 */
public class ExternalPrograms {

    /**
     * external program vlc.
     */
    public static final ExternalProgram VLC = new ExternalProgram(
        "VLC",
        "VLC is needed for playback of videos, import of videos (thumb nails)",
        VLC_WINDOWS_EXE,
        VLC_LINUX_EXE,
        "c:/programme/videoLAN/vlc/vlc.exe",
        "vlc",
        "/usr/bin/vlc"
    );

    /**
     * external program mplayer.
     */
    public static final ExternalProgram MPLAYER = new ExternalProgram(
        "Mplayer",
        "Mplayer is used to import properties of videos. It could be alternatively used for playback of videos",
        MPLAYER_WINDOWS_EXE,
        MPLAYER_LINUX_EXE,
        "mplayer",
        "c:/programme/mplayer/mplayer.exe",
        "/usr/bin/mplayer"
    );

    /**
     * external program kmplayer.
     */
    public static final ExternalProgram KMPLAYER = new ExternalProgram(
        "KMplayer",
        "KMplayer could be alternatively used for playback of videos",
        KMPLAYER_LINUX_EXE,
        KMPLAYER_LINUX_EXE,
        "/usr/bin/kmplayer"
    );

    /**
     * ffmpegthumbnailer.
     */
    public static final ExternalProgram FFMPEGTHUMBNAILER = new ExternalProgram(
        "ffmpegthumbnailer",
        "ffmpegthumbnailer could be alternatively used to create thumbnails for videos during import (instead of VLC)",
        FFMPEGTHUMBNAILER_WINDOWS_EXE,
        FFMPEGTHUMBNAILER_LINUX_EXE,
        "ffmpegthumbnailer",
        "c:/programme/ffmpegthumbnailer/ffmpegthumbnailer.exe",
        "/usr/bin/ffmpegthumbnailer"
    );

    /**
     * ffmpeg.
     */
    public static final ExternalProgram FFMPEG = new ExternalProgram(
        "ffmpeg",
        "ffmpeg could be alternatively used to create thumbnails for videos during import (instead of VLC)",
        FFMPEG_WINDOWS_EXE,
        FFMPEG_LINUX_EXE,
        "ffmpeg",
        "c:/programme/ffmpeg/bin/ffmpeg.exe",
        "/usr/bin/ffmpeg"
    );

    public static final ExternalProgram[] ALL_PRGS = {VLC, MPLAYER, KMPLAYER, FFMPEGTHUMBNAILER, FFMPEG};

}
