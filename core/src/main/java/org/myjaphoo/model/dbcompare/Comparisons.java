package org.myjaphoo.model.dbcompare;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.ResourceBundle;

/**
 * Comparisons
 */
public class Comparisons {
    public static final ResourceBundle BUNDLE = java.util.ResourceBundle.getBundle(
        "org/myjaphoo/model/dbcompare/resources/DatabaseComparison");

    public static String getCategoryName(JoinedDataRow row) {
        if (!(row instanceof DBDiffCombinationResult)) {
            return BUNDLE.getString("NO COMPARISON AVAILABLE");
        }
        ImmutableModel otherModel = ((DBDiffCombinationResult) row).getT2().getModel();
        if (otherModel.getDupHashMap().containsEntry(row.getEntry().getChecksumCRC32())) {
            return BUNDLE.getString("EXISTS_IN_COMPARISON_DB");
        } else {
            return BUNDLE.getString("EXISTS_NOT_IN_COMPARISON_DB");
        }
    }
}
