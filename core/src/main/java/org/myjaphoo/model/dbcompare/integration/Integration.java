package org.myjaphoo.model.dbcompare.integration;

import io.vavr.control.Option;
import org.myjaphoo.gui.picmode.Picture;
import org.myjaphoo.gui.util.ThumbLoader;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.ThumbType;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbcompare.ComparisonContext;
import org.myjaphoo.model.dbcompare.DBDiffCombinationResult;
import org.myjaphoo.model.dbcompare.keys.AbstractEntryKey;
import org.myjaphoo.model.logic.ThumbnailJpaController;
import org.myjaphoo.project.Project;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Integrates differences from the compare db into this db based on DBDiffCombinationResults.
 *
 *
 * todo needs to be implemented after migration to immutable model
 * for now this is leaved out
 */
public class Integration {

    ComparisonContext context;
    Project project;
    ThumbnailJpaController thumbnailJpaController;

    Project otherProject;
    ImmutableModel otherModel;

    ThumbLoader otherThumbLoader;

    /**
     * map with all created entries by this integration. We need to take care, that we do not create
     * entries multiple times, when mulitple diff nodes for e.g. different tags exists.
     */
    HashMap<AbstractEntryKey, ImmutableMovieEntry> newlyCreatedEntries = new HashMap<>();


    public Integration(Project project, ComparisonContext context) {
        this.project = project;
        this.context = context;
        otherProject = context.getDatabaseComparison().getComparisonProject();
        otherModel = otherProject.cacheActor.getImmutableModel();
        otherThumbLoader = new ThumbLoader(otherProject);
        thumbnailJpaController = new ThumbnailJpaController(project.connection);
    }


    /**
     * integreate all differences described in the db diff into the current database.
     *
     * @param dbdiff
     */
    public void integrate(DBDiffCombinationResult dbdiff) throws Exception {
        ImmutableMovieEntry entry = integrateEntry(dbdiff);
        ImmutableToken tag = integrateTag(dbdiff, entry);
        integrateMetaTag(dbdiff, tag);
    }

    private ImmutableToken integrateTag(DBDiffCombinationResult dbdiff, ImmutableMovieEntry entry) {
        switch (dbdiff.getDiffTag()) {
            case EQUAL: // nothing to do
                return dbdiff.getToken();
            case CHANGED:
                Token attached = project.loadToken(dbdiff.getToken().getId());
                dbdiff.getCDBToken().copyToMutable(attached);
                project.cacheActor.editToken(attached);
                return attached.toImmutable();

            case NEW:
                ImmutableToken tag = prepare(dbdiff.getCDBToken());
                // now assign the tag to the movie:
                project.cacheActor.assignToken2MovieEntry(tag, Arrays.asList(entry));
                return tag;
            case REMOVED:
                // todo remove could probably mean two different things:
                // 1. remove the association between entry and tag
                // 2. the tag got completely removed (this also includes 1.)

                // 1. in any case remove the association (if the tag still exists in our db):
                if (project.cacheActor.getImmutableModel().getTokenByName(dbdiff.getToken().getName()).isDefined()) {
                    project.cacheActor.unassignTokenFromMovies(dbdiff.getToken(), Arrays.asList(entry));

                    // 2. if the comparison db doesnt have the tag at all, we delete it completely:
                    if (!otherModel.getTokenByName(dbdiff.getToken().getName()).isDefined()) {
                        project.cacheActor.removeToken(dbdiff.getToken());
                    }
                }

                return null;
        }
        throw new RuntimeException("illegal state!");
    }

    private void integrateMetaTag(DBDiffCombinationResult dbdiff, ImmutableToken token) {
        switch (dbdiff.getDiffTag()) {
            case EQUAL: // nothing to do
                return;
            case CHANGED:
                MetaToken attached = project.loadMetaToken(dbdiff.getMetaToken().getId());
                dbdiff.getCDBMetaToken().copyToMutable(attached);
                project.cacheActor.editMetaToken(attached);
                return;

            case NEW:
                ImmutableMetaToken tag = prepare(dbdiff.getCDBMetaToken());
                // now assign the tag to the movie:
                project.cacheActor.assignMetaTokenToToken(tag, token);
                return;
            case REMOVED:
                // todo remove could probably mean two different things:
                // 1. remove the association between entry and tag
                // 2. the tag got completely removed (this also includes 1.)

                // 1. in any case remove the association (if the tag still exists in our db):
                if (project.cacheActor.getImmutableModel().getMetaTokenByName(dbdiff.getMetaToken().getName()).isDefined()) {
                    project.cacheActor.unAssignMetaTokenFromToken(dbdiff.getMetaToken(), Arrays.asList(token));

                    // 2. if the comparison db doesnt have the tag at all, we delete it completely:
                    if (!otherModel.getMetaTokenByName(dbdiff.getMetaToken().getName()).isDefined()) {
                        project.cacheActor.removeMetaToken(dbdiff.getMetaToken());
                    }
                }
        }
        throw new RuntimeException("illegal state!");
    }


    private ImmutableMovieEntry integrateEntry(DBDiffCombinationResult dbdiff) throws Exception {
        switch (dbdiff.getDiffEntry()) {
            case EQUAL: // nothing to do;
                return dbdiff.getEntry();
            case CHANGED:
                MovieEntry attached = project.loadMovieEntry(dbdiff.getEntry().getId());
                dbdiff.getCDBEntry().copyToMutable(attached);

                project.cacheActor.editMovie(attached);
                // todo return the created immutable object, not a new one...
                return attached.toImmutable();
            case NEW:
                // the other db has the new entry:
                AbstractEntryKey key = dbdiff.getContext().createEntryKey(dbdiff.getCDBEntry());
                ImmutableMovieEntry alreadyCreatedEntry = newlyCreatedEntries.get(key);
                if (alreadyCreatedEntry != null) {
                    // it has been created by another diff node before;
                    // which means, this is a diffnode that describes e.g. another tag difference for this entry;
                    // so we just return the previously created entry:
                    return alreadyCreatedEntry;
                }

                MovieEntry copy = new MovieEntry();
                dbdiff.getCDBEntry().copyToMutable(copy);
                project.cacheActor.newMovie(copy);
                copyThumbnails(dbdiff.getCDBEntry(), copy);
                newlyCreatedEntries.put(key, copy.toImmutable());
                // todo return the created immutable object, not a new one...
                return copy.toImmutable();
            case REMOVED:
                // delete the movie in our database:
                project.cacheActor.removeMovieEntry(dbdiff.getEntry());
                return null;
        }
        throw new RuntimeException("illegal state!");
    }

    private void copyThumbnails(final ImmutableMovieEntry source, MovieEntry dest) {
        // load thumbs from the comparison db:
        for (int i = 0; i < 5; i++) {
            BufferedImage bi = otherThumbLoader.load(source.getId(), i);
            if (bi != null) {
                Thumbnail destTn = new Thumbnail();
                destTn.setH(bi.getHeight());
                destTn.setW(bi.getWidth());

                destTn.setMovieEntry(dest);
                destTn.setType(ThumbType.NORMAL);
                try {
                    destTn.setThumbnail(Picture.toByte(bi));
                } catch (IOException e) {
                    throw new RuntimeException("error merging thumbnail", e);
                }
                thumbnailJpaController.create(destTn);
            }
        }

    }

    private ImmutableToken prepare(ImmutableToken tagInOtherDb) {
        if (tagInOtherDb == null) {
            return null;
        }
        // could be either a fully new tag, or could also mean, that
        // the tag is existing, but the assignment to this movie is new:
        Option<ImmutableToken> tag = project.cacheActor.getImmutableModel().getTokenByName(tagInOtherDb.getName());
        if (tag.isDefined()) {
            Token token = project.loadToken(tag.get().getId());
            tagInOtherDb.copyToMutable(token);
            project.cacheActor.editToken(token);
            return token.toImmutable();
        } else {
            Token token = new Token();
            tagInOtherDb.copyToMutable(token);
            ImmutableToken parent = prepare(otherModel.getParentTag(tagInOtherDb));
            project.cacheActor.createToken(token, parent);
            return token.toImmutable();
        }
    }

    private ImmutableMetaToken prepare(ImmutableMetaToken tagInOtherDb) {
        if (tagInOtherDb == null) {
            return null;
        }
        // could be either a fully new tag, or could also mean, that
        // the tag is existing, but the assignment to this movie is new:
        Option<ImmutableMetaToken> tag = project.cacheActor.getImmutableModel().getMetaTokenByName(tagInOtherDb.getName());
        if (tag.isDefined()) {
            MetaToken token = project.loadMetaToken(tag.get().getId());
            tagInOtherDb.copyToMutable(token);
            project.cacheActor.editMetaToken(token);
            return token.toImmutable();
        } else {
            MetaToken token = new MetaToken();
            tagInOtherDb.copyToMutable(token);
            ImmutableMetaToken parent = prepare(otherModel.getMetaTokenTree().getParent(tagInOtherDb));
            project.cacheActor.createMetaToken(token, parent);
            return token.toImmutable();
        }
    }

}
