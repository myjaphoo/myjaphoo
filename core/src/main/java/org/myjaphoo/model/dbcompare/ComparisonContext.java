package org.myjaphoo.model.dbcompare;

import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.dbcompare.keys.AbstractDataRowCompareKey;
import org.myjaphoo.model.dbcompare.keys.AbstractEntryKey;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.processing.CombinationResultGenerator;
import org.myjaphoo.util.ComparatorByProperties;
import org.myjaphoo.util.PropsToMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Context holder object.
 */
public class ComparisonContext {

    private ComparisonMethod comparisonMethod;

    private DatabaseComparison databaseComparison;

    private PropsToMap propsToMap = new PropsToMap();

    private ComparatorByProperties comparatorByProperties = new ComparatorByProperties();

    public ComparisonContext(DatabaseComparison databaseComparison) {
        this.databaseComparison = databaseComparison;
        comparisonMethod = databaseComparison.getComparisonMethod();
    }

    private <T, M> Map<String, ComparatorByProperties.Diff> determineCompareDiffs(T o1, T o2, Object nullMarker) {
        if (determineCompareResult(o1, o2, nullMarker) == CompareResult.CHANGED) {
            return comparatorByProperties.getDiffProps(o1, o2);
        }
        return new HashMap<>();
    }

    private <T, M> String determineCompareDiffInfo(T o1, T o2, Object nullMarker) {
        Map<String, ComparatorByProperties.Diff> diffs = determineCompareDiffs(o1, o2, nullMarker);
        StringBuilder b = new StringBuilder();
        for (Map.Entry<String, ComparatorByProperties.Diff> entry : diffs.entrySet()) {
            b.append(entry.getKey() + " changed:" + entry.getValue().o1 + " <-> " + entry.getValue().o2);
            b.append("<br>");
        }
        return b.toString();
    }

    private <T, M> CompareResult determineCompareResult(T o1, T o2, Object nullMarker) {
        if (o1 == o2) {
            return CompareResult.EQUAL;
        } else if (o1 == nullMarker) {
            return CompareResult.NEW;
        } else if (o2 == nullMarker) {
            return CompareResult.REMOVED;
        } else {
            return compare(o1, o2);
        }
    }

    private <T, M> CompareResult compare(T o1, T o2) {
        Map<String, Object> map1 = propsToMap.toMap(o1);
        Map<String, Object> map2 = propsToMap.toMap(o2);
        if (!comparatorByProperties.isEqualProps(o1, o2)) {
            return CompareResult.CHANGED;
        } else {
            return CompareResult.EQUAL;
        }
    }


    public CompareResult determineCompareResult(ImmutableMovieEntry entry, ImmutableMovieEntry cdbentry) {
        if (entry.getId() < 0) {
            return CompareResult.NEW;
        } else if (cdbentry.getId() < 0) {
            return CompareResult.REMOVED;
        }

        return determineCompareResult(
            entry,
            cdbentry,
            ComparisonSetGenerator.NULL_ENTRY
        );
    }

    public CompareResult determineCompareResult(ImmutableToken token, ImmutableToken cdbtoken) {
        return determineCompareResult(
            token,
            cdbtoken,
            CombinationResultGenerator.NULL_TOKEN
        );
    }

    public CompareResult determineCompareResult(ImmutableMetaToken metaToken, ImmutableMetaToken cdbmetatoken) {
        return determineCompareResult(
            metaToken,
            cdbmetatoken,
            CombinationResultGenerator.NULL_META_TOKEN
        );
    }

    public Map<String, ComparatorByProperties.Diff> determineCompareDiffs(
        ImmutableMetaToken metaToken, ImmutableMetaToken cdbmetatoken
    ) {
        return determineCompareDiffs(
            metaToken,
            cdbmetatoken,
            CombinationResultGenerator.NULL_META_TOKEN
        );
    }

    public String determineCompareDiffInfo(ImmutableMetaToken metaToken, ImmutableMetaToken cdbmetatoken) {
        return determineCompareDiffInfo(
            metaToken,
            cdbmetatoken,
            CombinationResultGenerator.NULL_META_TOKEN
        );
    }

    public Map<String, ComparatorByProperties.Diff> determineCompareDiffs(
        ImmutableToken token, ImmutableToken cdbtoken
    ) {
        return determineCompareDiffs(
            token,
            cdbtoken,
            CombinationResultGenerator.NULL_TOKEN
        );
    }

    public String determineCompareDiffInfo(ImmutableToken token, ImmutableToken cdbtoken) {
        return determineCompareDiffInfo(
            token,
            cdbtoken,
            CombinationResultGenerator.NULL_TOKEN
        );
    }

    public Map<String, ComparatorByProperties.Diff> determineCompareDiffs(
        ImmutableMovieEntry entry, ImmutableMovieEntry cdbentry
    ) {
        return determineCompareDiffs(
            entry,
            cdbentry,
            ComparisonSetGenerator.NULL_ENTRY
        );
    }

    public String determineCompareDiffInfo(ImmutableMovieEntry entry, ImmutableMovieEntry cdbentry) {
        return determineCompareDiffInfo(
            entry,
            cdbentry,
            ComparisonSetGenerator.NULL_ENTRY
        );
    }

    public AbstractDataRowCompareKey createKey(JoinedDataRow cr) {
        return comparisonMethod.createKey(cr);
    }

    public AbstractEntryKey createEntryKey(ImmutableMovieEntry entry) {
        return comparisonMethod.createEntryKey(entry);
    }

    public DatabaseComparison getDatabaseComparison() {
        return databaseComparison;
    }
}
