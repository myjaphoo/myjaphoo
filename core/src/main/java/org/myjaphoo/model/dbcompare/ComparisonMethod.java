package org.myjaphoo.model.dbcompare;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.dbcompare.keys.AbstractDataRowCompareKey;
import org.myjaphoo.model.dbcompare.keys.AbstractEntryKey;
import org.myjaphoo.model.dbcompare.keys.DataRowCompareKeyChecksumBased;
import org.myjaphoo.model.dbcompare.keys.DataRowCompareKeyPathAndChecksumBased;
import org.myjaphoo.model.dbcompare.keys.DataRowCompareKeyPathBased;
import org.myjaphoo.model.dbcompare.keys.EntryKeyChecksumBased;
import org.myjaphoo.model.dbcompare.keys.EntryKeyPathAndChecksumBased;
import org.myjaphoo.model.dbcompare.keys.EntryKeyPathBased;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

/**
 * Defines the method for comparison.
 */
public enum ComparisonMethod {

    COMPARE_PATH{
        @Override
        public AbstractDataRowCompareKey createKey(JoinedDataRow cr) {
            return new DataRowCompareKeyPathBased(cr);
        }

        @Override
        public AbstractEntryKey createEntryKey(ImmutableMovieEntry entry) {
            return new EntryKeyPathBased(entry);
        }
    },
    COMPARE_CHECKSUM{
        @Override
        public AbstractDataRowCompareKey createKey(JoinedDataRow cr) {
            return new DataRowCompareKeyChecksumBased(cr);
        }

        @Override
        public AbstractEntryKey createEntryKey(ImmutableMovieEntry entry) {
            return new EntryKeyChecksumBased(entry);
        }
    },
    COMPARE_PATH_AND_CHECKSUM{
        @Override
        public AbstractDataRowCompareKey createKey(JoinedDataRow cr) {
            return new DataRowCompareKeyPathAndChecksumBased(cr);
        }

        @Override
        public AbstractEntryKey createEntryKey(ImmutableMovieEntry entry) {
            return new EntryKeyPathAndChecksumBased(entry);
        }
    };

    public abstract  AbstractEntryKey createEntryKey(ImmutableMovieEntry entry);

    public abstract AbstractDataRowCompareKey createKey(JoinedDataRow cr);

}
