package org.myjaphoo.model.dbcompare;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.ArrayList;

/**
 * A combination set, which contains all combination results and additional information.
 */
public class CombinationSet {
    private ImmutableModel model;
    private ArrayList<JoinedDataRow> result;
    private io.vavr.collection.List<ImmutableMovieEntry> movies;

    private boolean needsTagRelation;
    private boolean needsMetaTagRelation;

    public CombinationSet(  ImmutableModel model,
        ArrayList<JoinedDataRow> result, io.vavr.collection.List<ImmutableMovieEntry> movies,
        boolean needsTagRelation,
        boolean needsMetaTagRelation
    ) {
        this.model = model;
        this.result = result;
        this.movies = movies;
        this.needsTagRelation = needsTagRelation;
        this.needsMetaTagRelation = needsMetaTagRelation;
    }

    public ArrayList<JoinedDataRow> getResult() {
        return result;
    }

    public io.vavr.collection.List<ImmutableMovieEntry> getMovies() {
        return movies;
    }

    public boolean isNeedsTagRelation() {
        return needsTagRelation;
    }

    public boolean isNeedsMetaTagRelation() {
        return needsMetaTagRelation;
    }

    public ImmutableModel getModel() {
        return model;
    }
}
