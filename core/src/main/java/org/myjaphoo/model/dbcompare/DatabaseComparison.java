/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.dbcompare;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.project.Project;

import java.util.ArrayList;
import java.util.ResourceBundle;


/**
 * Alle Vergleichsfunktionen mit einer weiteren Datenbank werden über diese Klasse geregelt.
 * Diese Klasse hält alle notwendigen Informationen, um vergleiche anzustellen, bzw. vergleichsinfos zu liefern.
 *
 * @author
 * @todo needs rework after project support
 */
public class DatabaseComparison {
//    public static final ResourceBundle BUNDLE = java.util.ResourceBundle.getBundle(
//        "org/myjaphoo/model/dbcompare/resources/DatabaseComparison");

    private ComparisonMethod comparisonMethod = ComparisonMethod.COMPARE_CHECKSUM;

    private Project comparisonProject;


    public DatabaseComparison() {
    }

    public Project getComparisonProject() {
        return comparisonProject;
    }

    public void setComparisonProject(Project comparisonProject) {
        this.comparisonProject = comparisonProject;
    }


    public void closeComparisonDatabase() {
        comparisonProject = null;
    }

    public boolean isActive() {
        return comparisonProject != null;
    }

    public boolean hasSameEntry(ImmutableMovieEntry entry) {
        if (comparisonProject == null) {
            return false;
        }
        return comparisonProject.cacheActor.getImmutableModel().getDupHashMap().containsEntry(entry.getChecksumCRC32());
    }

    public ArrayList<ImmutableMovieEntry> getDups(MovieEntry entry) {
        if (comparisonProject == null) {
            return null;
        }
        return comparisonProject.cacheActor.getImmutableModel().getDupHashMap().getDuplicatesForCheckSum(
            entry.getChecksumCRC32());
    }

    public int getComparisonColor(MovieEntry entry) {
        if (comparisonProject == null) {
            return 0;
        }
        if (comparisonProject.cacheActor.getImmutableModel().getDupHashMap().containsEntry(entry.getChecksumCRC32())) {
            return 1;
        } else {
            return 2;
        }
    }

    public ComparisonMethod getComparisonMethod() {
        return comparisonMethod;
    }

    public void setComparisonMethod(ComparisonMethod comparisonMethod) {
        this.comparisonMethod = comparisonMethod;
    }

    public static class Info {

        public boolean isComparisonDBOpened;
        public String comparisonDBName = ""; //NOI18N
        public int numOfMovies;
        public long sizeOfMovies;
        public int numOfDuplicates;
        public long wastedMem;
    }

    public Info getInfo() {
        Info info = new Info();
        info.isComparisonDBOpened = comparisonProject != null;
        if (info.isComparisonDBOpened) {
            info.comparisonDBName = comparisonProject.uniqueId;
            info.numOfMovies = comparisonProject.cacheActor.getImmutableModel().getEntryList().size();
            info.numOfDuplicates = comparisonProject.cacheActor.getImmutableModel().getDupHashMap().calcDuplicationCount();
            info.wastedMem = comparisonProject.cacheActor.getImmutableModel().getDupHashMap().calcWastedMem();
        }
        return info;
    }
}
