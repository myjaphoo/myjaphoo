/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model;

import io.vavr.collection.Seq;
import org.myjaphoo.model.cache.ImmutableMovieEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Hashmap to build a map of duplicates of the movie entries.
 * It does this by the checksum.
 * The determination of duplicates could be suppressed by an internal option.
 * This is the case, if the calculation of checksums is disabled for the
 * databse.
 *
 * @author lang
 */
public class DuplicateHashMap {

    // todo use immutable objects...
    private Map<Long, ArrayList<ImmutableMovieEntry>> chsumMap = null;
    private boolean suppressMode = false;

    private int numOfDups;
    private long wastedMem;

    private DuplicateHashMap(int initialSize) {
        chsumMap = new HashMap<>(initialSize);
    }

    public DuplicateHashMap(Seq<ImmutableMovieEntry> entries) {
        this(entries.size() * 2);
        for (ImmutableMovieEntry entry : entries) {
            add(entry.getChecksumCRC32(), entry);
        }
    }

    public DuplicateHashMap(Collection<ImmutableMovieEntry> entries) {
        this(entries.size() * 2);
        for (ImmutableMovieEntry entry : entries) {
            add(entry.getChecksumCRC32(), entry);
        }
    }

    private void add(Long chSum, ImmutableMovieEntry elem) {
        ArrayList<ImmutableMovieEntry> listOfIdentical = chsumMap.get(chSum);
        if (listOfIdentical == null) {
            listOfIdentical = new ArrayList<ImmutableMovieEntry>(3);
            chsumMap.put(chSum, listOfIdentical);
        } else {
            // a entry is already existing: count duplicate infos:
            numOfDups++;
            wastedMem += elem.getFileLength();
        }
        // add the movie node as child:
        listOfIdentical.add(elem);
    }

    public boolean hasDuplicates(Long checksum) {
        if (suppressMode) {
            return false;
        }
        ArrayList<ImmutableMovieEntry> listOfIdentical = chsumMap.get(checksum);
        if (listOfIdentical == null) {
            return false;
        }
        return listOfIdentical.size() > 1; // because it contains itself
    }

    public boolean containsEntry(Long checksumCRC32) {
        return chsumMap.containsKey(checksumCRC32);
    }

    public ArrayList<ImmutableMovieEntry> getDuplicatesForCheckSum(Long checksumCRC32) {
        return chsumMap.get(checksumCRC32);
    }

    public Collection<ImmutableMovieEntry> getDuplicatesForMovie(ImmutableMovieEntry entry) {
        ArrayList<ImmutableMovieEntry> allDups = getDuplicatesForCheckSum(entry.getChecksumCRC32());
        if (allDups == null || allDups.size() <= 1) {
            return null;
        }
        ArrayList<ImmutableMovieEntry> allDupsExceptMe = new ArrayList<ImmutableMovieEntry>();
        for (ImmutableMovieEntry dup : allDups) {
            if (!dup.equals(entry)) {
                allDupsExceptMe.add(dup);
            }
        }
        return allDupsExceptMe;
    }

    public long calcWastedMem() {
        return wastedMem;
    }

    public int calcDuplicationCount() {
        return numOfDups;
    }

    public Map<Long, ArrayList<ImmutableMovieEntry>> getChsumMap() {
        return chsumMap;
    }
}
