package org.myjaphoo.model;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryWrapper;
import org.myjaphoo.model.db.MovieEntry;

/**
 * FileTypeInterface
 *
 * @author mla
 * @version $Id$
 */
public interface FileTypeInterface {
    boolean is(EntryWrapper entry);

    boolean is(ImmutableMovieEntry entry);

    boolean is(MovieEntry entry);

    boolean is(String filename);
}
