package org.myjaphoo.model;

/**
 * ThumbDisplayFilterResultMode
 *
 * @author mla
 * @version $Id$
 */
public enum ThumbDisplayFilterResultMode {
    PlAINLIST,
    SLIDESGROUPED
}
