package org.myjaphoo.model;

import org.myjaphoo.model.logic.ConfigurableFileSubstitution;
import org.myjaphoo.model.registry.ComponentRegistry;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;

/**
 * AbstractFileSubstitution
 *
 * @author mla
 * @version $Id$
 */
public abstract class AbstractFileSubstitution implements FileSubstitution {

    private final static Logger logger = LoggerFactory.getLogger(AbstractFileSubstitution.class.getName());

    private Project project;

    public AbstractFileSubstitution(Project project) {
        this.project = project;
    }

    @Override
    public String substitude(String canonicalPath) {
        String located = locateFileOnDrive(canonicalPath);
        if (located == null) {
            logger.trace("not substituted, use " + canonicalPath);
            return canonicalPath;
        } else {
            return located;
        }
    }

    protected String tryLocateByDynamicConfiguredSubstitutors(String canoncialPath) {
        Collection<ConfigurableFileSubstitution> configuredFs = ComponentRegistry.registry.getEntryCollection(
            ConfigurableFileSubstitution.class);
        for (ConfigurableFileSubstitution cfs : configuredFs) {
            String result = cfs.locateFileOnDrive(project, canoncialPath);
            if (result != null) {
                return result;
            }
        }
        return null;
    }


    protected boolean fileExists(String filename) {
        File tester = new File(filename);
        return tester.exists();
    }

    public Project getProject() {
        return project;
    }
}
