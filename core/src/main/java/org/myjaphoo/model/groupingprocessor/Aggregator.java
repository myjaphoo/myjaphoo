package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.filterparser.ExecutionContext;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.filterparser.values.Value;
import org.myjaphoo.model.groupbyparser.expr.AggregatedExpression;
import org.myjaphoo.model.groupbyparser.expr.Aggregation;

import java.util.Iterator;
import java.util.List;

/**
 * Aggregator
 */
public class Aggregator {

    private MyjaphooCorePrefs prefs;

    private List<Aggregation> aggregations;
    private AggregatedExpression havingClause;

    public Aggregator(
        MyjaphooCorePrefs prefs, List<Aggregation> aggregations,
        AggregatedExpression havingClause
    ) {
        this.prefs = prefs;
        this.aggregations = aggregations;
        this.havingClause = havingClause;
    }

    public boolean hasAggregations() {
        return aggregations != null;
    }

    public boolean hasHavingClause() {
        return havingClause != null;
    }

    public void aggregateOnAllStructureNodes(ImmutableModel model, Branch node) {
        if (hasAggregations()) {
            aggregateOnAllStructureNodes(new JoinedDataRowZipper(model), node);
        }
    }

    private void aggregateOnAllStructureNodes(JoinedDataRowZipper zipper, Branch node) {
        // recursively aggregate on all levels of structure nodes, bottom to top:
        for (Branch child : node.getChildBranches()) {
            if (child instanceof Branch) {
                aggregateOnAllStructureNodes(zipper, (Branch) child);
            }
        }

        // calc aggregated values for this level and node:
        for (Aggregation aggregation : aggregations) {
            Value result = calcAggregation(zipper, node, aggregation.getExpr());
            node.putAggregatedValue(aggregation.getExpr().getDisplayExprTxt(), new Double(result.asLong()));
        }
    }

    private Value calcAggregation(JoinedDataRowZipper zipper, Branch branch, AggregatedExpression expression) {
        // new context for this aggregation (agg-functions hold their status there).
        ExecutionContext context = new ExecutionContext(
            zipper.getEntry().model,
            prefs.createFileTypes()
        );
        // fill the aggregations:
        for (Leaf leaf : branch.getLeafs()) {
            zipper.updateZipper(leaf.getRow());
            expression.populateAggregations(context, zipper);
        }
        // "close" the aggregation mode now:
        context.closeAggregations();
        // and evaluate the clause with the previously aggregated values:
        return expression.evaluate(context, null);
    }

    public void pruneByHavingClause(ImmutableModel model, Branch node) {
        if (hasHavingClause()) {
            pruneByHavingClause(new JoinedDataRowZipper(model), node);
        }
    }

    private void pruneByHavingClause(JoinedDataRowZipper zipper, Branch node) {
        Iterator<Branch> afterDeleting = node.getChildBranches().iterator();
        while (afterDeleting.hasNext()) {
            Branch child = afterDeleting.next();
            pruneByHavingClause(zipper, child);
            // if the child has after pruning no more children, then we can remove it:
            if (child.getLeafs().size() == 0 && child.getChildBranches().size() == 0) {
                afterDeleting.remove();
                child.setParent(null);
            }
        }

        // calc aggregated values and evaluate having condition:
        if (isFalse(calcAggregation(zipper, node, havingClause))) {
            // prune all this child nodes:
            Iterator<Leaf> iterator = node.getLeafs().iterator();
            while (iterator.hasNext()) {
                Leaf child = iterator.next();
                iterator.remove();
                child.setParent(null);
            }
        }
    }

    private boolean isFalse(Value value) {
        return !value.asBool();
    }

}
