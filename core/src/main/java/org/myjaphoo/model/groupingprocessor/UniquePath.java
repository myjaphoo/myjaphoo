package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.model.grouping.Path;

/**
 * used to condense path+entry combinations which where created by
 * the cartesion product of entry+tag+metatag, but which are not
 * identified by the grouping definition.
 */
public class UniquePath {

    private Path path;
    private long movieId;

    public UniquePath(Path path, long movieId) {
        this.path = path;
        this.movieId = movieId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UniquePath that = (UniquePath) o;

        if (movieId != that.movieId) return false;
        return path.equals(that.path);
    }

    @Override
    public int hashCode() {
        int result = path.hashCode();
        result = 31 * result + (int) (movieId ^ (movieId >>> 32));
        return result;
    }
}