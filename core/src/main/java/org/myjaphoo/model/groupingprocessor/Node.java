package org.myjaphoo.model.groupingprocessor;

/**
 * Node
 */

/**
 * Property node
 */
public abstract class Node {

    private Node parent;

    private String name;

    public Node(String name) {
        this.name = name;
    }


    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathName() {
        StringBuilder b = new StringBuilder();
        Node node = this;
        while (node != null) {
            if (node.getParent() != null) {
                if (b.length() > 0) {
                    b.insert(0, "/"); //NOI18N
                }
                b.insert(0, node.getName());
            } else {
                // für root setzen wir nicht den namen:
                //b.insert(0, "Structure/");
            }
            node = node.getParent();
        }
        return b.toString();
    }
}