package org.myjaphoo.model.groupingprocessor;

/**
 * Pruning
 *
 * @author mla
 * @version $Id$
 */
public class Pruning {

    public static void pruneEmptyDirs(Branch node) {
        // mittels tiefensuche traversieren:
        for (Branch child : node.getChildBranches()) {
            pruneEmptyDirs((Branch) child);
        }

        if (node.getChildBranches().size() == 1 && node.getLeafs().size() == 0) {

            // nur ein kind, welches selbst ein dir ist; wir kÃ¶nnen die nodes also
            // zusammenfassen:
            Branch child = node.getChildBranches().iterator().next();
            String newname = node.getName() + "/" + child.getName();

            node.setName(newname);
            node.removeChild(child);
            child.setParent(null);
            for (Branch childOfChild : child.getChildBranches()) {
                node.addChild(childOfChild);
                childOfChild.setParent(node);
            }
            for (Leaf childOfChild : child.getLeafs()) {
                node.addLeaf(childOfChild);
                childOfChild.setParent(node);
            }
        }
    }
}
