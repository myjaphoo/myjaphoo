package org.myjaphoo.model.groupingprocessor;

import org.mlsoft.common.acitivity.Channel;
import org.myjaphoo.FilterResult;
import org.myjaphoo.model.db.DataView;

/**
 * WorkContext
 *
 * @author mla
 * @version $Id$
 */
public class WorkContext {
    public final Channel channel;
    public final FilterResult filterResult;
    public final DuplicationCondensator duplicationCondensator;
    public final NodeCreator nodeCreator;

    public final String selectedDir;

    public Branch retainedSelectedDir = null;

    public WorkContext(
        Channel channel, FilterResult filterResult,
        DuplicationCondensator duplicationCondensator,
        NodeCreator nodeCreator,
        DataView dataView
    ) {
        this.channel = channel;
        this.filterResult = filterResult;
        this.duplicationCondensator = duplicationCondensator;
        this.nodeCreator = nodeCreator;
        this.selectedDir = dataView.getCurrentSelectedDir();
    }
}