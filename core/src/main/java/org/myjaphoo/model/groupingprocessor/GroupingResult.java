package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.FilterResult;

/**
 * CreatedTreeModelResult
 */
public class GroupingResult {
    /**
     * the root node.
     */
    public final Branch root;

    public final boolean condenseDuplicates;
    /**
     * the filter result and immutable model, this is based on.
     */
    public final FilterResult filterResult;

    /**
     * if a dir was selected before, this attribute holds a retained dir of the new model. maybe null if it is not
     * possible
     * to retain the dir, or if no dir was selected before.
     */
    public final Branch retainedSelectedDir;

    /**
     * abhängig von den gewählten gruppierungen (und den daten) können movies
     * mehrfach unter verschiedenen gruppierungen erscheinen. Dann ist dieses
     * flag auf false gesetzt. (Wäre z.b. der Fall, wenn zwei verschiedene Tokens
     * einem Movie zugeordnet werden, und Gruppierung nach Token gewählt ist).
     */
    public final boolean treeShowsMoviesUnique;

    public GroupingResult(Branch root, WorkContext workContext, boolean condenseDuplicates) {
        this.root = root;
        this.filterResult = workContext.filterResult;
        this.condenseDuplicates = condenseDuplicates;
        this.retainedSelectedDir = workContext.retainedSelectedDir;
        this.treeShowsMoviesUnique = workContext.nodeCreator.isTreeShowsMoviesUnique();
    }
}
