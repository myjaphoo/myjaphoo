/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.filterparser.processing.ProcessingRequirementInformation;
import org.myjaphoo.model.grouping.GroupingExecutionContext;

import java.util.List;


/**
 * Grouping algorithm to structure the movie nodes into a tree.
 *
 * @author mla
 */
public interface GroupAlgorithm extends ProcessingRequirementInformation {

    /**
     * Groups a node into the tree.
     *
     * @param root the root of the tree
     * @param node the node to add to the tree
     */
    public List<Branch> findParents(Branch root, JoinedDataRowZipper row);

    public Branch getOrCreateRoot();

    public Branch findAccordingNode(Branch root, String path);

    public void postProcess(Branch root);

    public void preProcess(GroupingExecutionContext context);

    /**
     * a description of this grouper; probably the group expression representing this grouping.
     */
    public String getText();

    void pruneByHavingClause(ImmutableModel model, Branch root);

    void aggregate(ImmutableModel model, Branch root);

    boolean hasAggregations();
}
