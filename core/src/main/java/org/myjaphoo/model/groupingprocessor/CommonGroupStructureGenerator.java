/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.myjaphoo.model.groupingprocessor;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.mlsoft.common.acitivity.Channel;
import org.mlsoft.common.acitivity.ChannelManager;
import org.myjaphoo.CommonFilterContext;
import org.myjaphoo.CommonMovieFilter;
import org.myjaphoo.FilterResult;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.db.DataView;
import org.myjaphoo.model.filterparser.ExecutionContext;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.grouping.GroupingExecutionContext;
import org.myjaphoo.model.util.GroupingExecutionContextImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;


/**
 * Grouping and Aggregation processing
 */
public class CommonGroupStructureGenerator {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MovieDataBaseFilter");
    public static final Logger LOGGER = LoggerFactory.getLogger(CommonGroupStructureGenerator.class.getName());

    private final CommonFilterContext cfc;

    /**
     * Default constructor.
     */
    public CommonGroupStructureGenerator(CommonFilterContext cfc) {
        this.cfc = requireNonNull(cfc);
    }

    public GroupingResult createStructuredTreeModel(
        List<? extends GroupAlgorithm> grouper, DataView dataView, boolean condenseDuplicates
    ) {
        return withChannel(channel -> {
            FilterResult filterResult = new CommonMovieFilter(cfc).loadFilteredEntries(dataView, grouper);
            return tryCreateStructuredTreeModel(channel, filterResult, grouper, dataView, condenseDuplicates);
        });
    }

    public GroupingResult createStructuredTreeModel(
        FilterResult filterResult,
        List<? extends GroupAlgorithm> grouper, DataView dataView, boolean condenseDuplicates
    ) {
        return withChannel(channel -> {
            return tryCreateStructuredTreeModel(channel, filterResult, grouper, dataView, condenseDuplicates);
        });

    }

    private GroupingResult withChannel(Function<Channel, GroupingResult> closure) {
        Channel channel = ChannelManager.createChannel(
            CommonGroupStructureGenerator.class,
            "Creating tree model"
        );//NOI18N
        channel.startActivity();
        try {
            return closure.apply(channel);
        } catch (ParserException ex) {
            LOGGER.error("error", ex); //NOI18N
            channel.errormessage(ex.getMessage());
            throw ex;
        } finally {
            channel.stopActivity();
        }
    }

    public GroupingResult tryCreateStructuredTreeModel(
        Channel channel,
        FilterResult filterResult,
        List<? extends GroupAlgorithm> grouper, DataView dataView, boolean condenseDuplicates
    ) {
        List<JoinedDataRow> allEntries = filterResult.filteredRows;

        StopWatch watch = new StopWatch();
        watch.start();

        channel.setProgressSteps(allEntries.size() * grouper.size());

        DuplicationCondensator duplicationCondensator = new DuplicationCondensator(
            condenseDuplicates,
            allEntries.size()
        );
        NodeCreator nodeCreator = new NodeCreator();

        WorkContext workContext = new WorkContext(channel, filterResult, duplicationCondensator, nodeCreator, dataView);

        List<Branch> rootNodes = new ArrayList<>();
        for (GroupAlgorithm groupAlgorithm : grouper) {
            rootNodes.add(group(workContext, groupAlgorithm));
        }

        // prune, if necessary:
        if (dataView.isPruneTree()) {
            for (Branch rootNode : rootNodes) {
                Pruning.pruneEmptyDirs(rootNode);
            }
        }

        watch.stop();
        LOGGER.info("Grouping of movie tree finished, took " + watch.toString()); //NOI18N
        duplicationCondensator.clear();
        int numOfMovies = filterResult.calcNumOfDistinctMovies();
        // build model:
        if (rootNodes.size() == 1) {
            Branch root = rootNodes.get(0);
            setRootNodeName(dataView, root, numOfMovies);
            return new GroupingResult(root, workContext, condenseDuplicates);
        } else {
            Branch root = new Branch("root");
            setRootNodeName(dataView, root, numOfMovies);
            for (int i = 0; i < rootNodes.size(); i++) {
                Branch node = rootNodes.get(i);
                GroupAlgorithm groupAlg = grouper.get(i);
                node.setName(groupAlg.getText());
                root.addChild(node);
            }
            return new GroupingResult(root, workContext, condenseDuplicates);
        }
    }

    private Branch group(WorkContext workContext, GroupAlgorithm grouper) {
        Branch root = grouper.getOrCreateRoot();
        List<JoinedDataRow> allEntries = workContext.filterResult.filteredRows;
        ImmutableModel model = workContext.filterResult.model;

        grouper.preProcess(createGroupingExecutionContext(model, allEntries));

        boolean hasAggregations = grouper.hasAggregations();

        JoinedDataRowZipper joinedDataRowZipper = new JoinedDataRowZipper(model);
        DuplicationCondensator condensator = workContext.duplicationCondensator;
        NodeCreator creator = workContext.nodeCreator;
        for (JoinedDataRow cr : allEntries) {
            workContext.channel.nextProgress();
            joinedDataRowZipper.updateZipper(cr);
            if (!condensator.appendDuplicateIfIsDuplicate(cr)) {

                Leaf oneNode = creator.addNodes(
                    grouper.findParents(root, joinedDataRowZipper),
                    cr,
                    hasAggregations
                );
                condensator.updateCondensatorCache(oneNode);
            }
        }

        grouper.postProcess(root);

        grouper.pruneByHavingClause(model, root);

        grouper.aggregate(model, root);

        retainSelectedDir(workContext, grouper, root);
        return root;
    }

    private GroupingExecutionContext createGroupingExecutionContext(
        ImmutableModel model, List<JoinedDataRow> allEntries
    ) {
        GroupingExecutionContextImpl groupingExecutionContext = new GroupingExecutionContextImpl(
            cfc.substitutionSupplier,
            allEntries,
            new ExecutionContext(model, cfc.fileTypes),
            cfc.fileSubstitution
        );
        return groupingExecutionContext;
    }

    private void setRootNodeName(DataView dataView, Branch root, int numOfMovies) {
        if (dataView.isFilter()) {
            root.setName(MessageFormat.format(localeBundle.getString("FILTERED:"), numOfMovies));
        } else {
            root.setName(MessageFormat.format(localeBundle.getString("ALL"), numOfMovies));
        }
    }

    private void retainSelectedDir(WorkContext workContext, GroupAlgorithm grouper, Branch root) {
        // reset the current dir, if possible:
        if (!StringUtils.isEmpty(workContext.selectedDir)) {
            LOGGER.debug("retain selected dir " + workContext.selectedDir); //NOI18N
            workContext.retainedSelectedDir = grouper.findAccordingNode(
                root,
                workContext.selectedDir
            );
        }
        if (workContext.retainedSelectedDir == null) {
            // setze es einfach auf das erste kind der wurzel:
            if (root.getChildBranches().size() > 0) {
                workContext.retainedSelectedDir = root.getChildBranches().iterator().next();
                LOGGER.debug(
                    "no better way: select first child " + workContext.retainedSelectedDir.getPathName()); //NOI18N
            } else {
                workContext.retainedSelectedDir = null;
            }
        }
    }
}
