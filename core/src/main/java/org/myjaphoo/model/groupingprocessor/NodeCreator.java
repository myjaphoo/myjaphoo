package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.ArrayList;
import java.util.List;

public class NodeCreator {

    /**
     * abhängig von den gewählten gruppierungen (und den daten) können movies
     * mehrfach unter verschiedenen gruppierungen erscheinen. Dann ist dieses
     * flag auf false gesetzt. (Wäre z.b. der Fall, wenn zwei verschiedene Tokens
     * einem Movie zugeordnet werden, und Gruppierung nach Token gewählt ist).
     */
    private boolean treeShowsMoviesUnique = true;

    List<Leaf> createdLeafs = new ArrayList<>(50000);

    Leaf createNode(EntryRef entryRef, JoinedDataRow cr, boolean unique) {
        return new Leaf(entryRef, cr, unique);
    }

    public NodeCreator() {
    }

    /**
     * Adds a new node to each parent to build up the tree structure.
     *
     * @param parents         all parents to add a node
     * @param cr              the combination result to build leaf nodes from.
     * @param hasAggregations
     *
     * @return
     */
    public Leaf addNodes(List<Branch> parents, JoinedDataRow cr, boolean hasAggregations) {
        Leaf node = null;
        if (parents != null) {
            boolean unique = parents.size() <= 1;
            if (parents.size() > 1) {
                // grouping is not unique
                treeShowsMoviesUnique = false;
            }
            EntryRef entryRef = cr.toEntryRef();
            for (Branch parent : parents) {
                node = createNode(entryRef, cr, unique);
                parent.addLeaf(node);
                createdLeafs.add(node);
            }
        }
        return node;
    }

    /**
     * @return the treeShowsMoviesUnique
     */
    public boolean isTreeShowsMoviesUnique() {
        return treeShowsMoviesUnique;
    }
}