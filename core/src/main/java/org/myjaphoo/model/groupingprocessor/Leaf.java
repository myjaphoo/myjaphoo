package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.ArrayList;

/**
 * Leaf
 */
public class Leaf extends Node {
    /** the associated entry ref. what if this is a diff entry based leaf?? */
    private EntryRef entryRef;
    private boolean unique;
    private ArrayList<EntryRef> condensedDuplicates = null;

    /**
     * row where this leaf is based on.
     */
    private JoinedDataRow row;

    public Leaf(EntryRef entryRef, JoinedDataRow row, boolean unique) {
        // todo maybe the name attribute does not make sense for leafs or nodes in general...
        super(entryRef.getName());
        this.entryRef = entryRef;
        this.row = row;
        this.unique = unique;
    }

    public void addCondensedDuplicate(EntryRef duplicate) {
        getCondensedDuplicates().add(duplicate);
    }

    public ArrayList<EntryRef> getCondensedDuplicates() {
        if (condensedDuplicates == null) {
            condensedDuplicates = new ArrayList<>(1);
        }
        return condensedDuplicates;
    }

    public int getCondensedDuplicatesSize() {
        if (condensedDuplicates == null) {
            return 0;
        } else {
            return condensedDuplicates.size();
        }
    }

    public boolean isUnique() {
        return unique;
    }


    public EntryRef getEntryRef() {
        return entryRef;
    }

    public JoinedDataRow getRow() {
        return row;
    }
}
