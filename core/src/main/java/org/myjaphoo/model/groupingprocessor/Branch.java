package org.myjaphoo.model.groupingprocessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

/**
 * Branch
 */
public class Branch extends Node {
    private String groupingExpr;
    private String canonicalDir;
    private String marker;

    private ArrayList<Branch> childBranches = new ArrayList<>();

    private ArrayList<Leaf> leafs = new ArrayList<>();

    private HashMap<String, Double> aggregatedValues = null;

    public Branch(String name) {
        super(name);
    }

    public void addChild(Branch node) {
        childBranches.add(node);
        node.setParent(this);
    }

    public void setGroupingExpr(String groupingExpr) {
        this.groupingExpr = groupingExpr;
    }

    public String getGroupingExpr() {
        return groupingExpr;
    }

    public void setCanonicalDir(String canonicalDir) {
        this.canonicalDir = canonicalDir;
    }

    public String getCanonicalDir() {
        return canonicalDir;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getMarker() {
        return marker;
    }


    public Set<String> getAllAggregatedKeys() {
        if (aggregatedValues == null) {
            return Collections.emptySet();
        } else {
            return aggregatedValues.keySet();
        }
    }

    public Double getAggregatedValue(String key) {
        if (aggregatedValues == null) {
            return null;
        } else {
            return aggregatedValues.get(key);
        }
    }

    public void putAggregatedValue(String key, Double value) {
        if (aggregatedValues == null) {
            aggregatedValues = new HashMap<>();
        }
        aggregatedValues.put(key, value);
    }

    public HashMap<String, Double> getAggregatedValues() {
        return aggregatedValues;
    }

    public void removeChild(Branch child) {
        childBranches.remove(child);
    }

    public void sort(Comparator<Node> comparator) {
        sortList(comparator);
        for (Branch branch : childBranches) {
            branch.sort(comparator);
        }
    }

    public void sortList(Comparator comparator) {
        Collections.sort(leafs, comparator);
    }

    public ArrayList<Branch> getChildBranches() {
        return childBranches;
    }

    public ArrayList<Leaf> getLeafs() {
        return leafs;
    }

    public void addLeaf(Leaf leaf) {
        leafs.add(leaf);
    }
}
