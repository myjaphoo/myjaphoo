/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.groupbyparser.expr.AggregatedExpression;
import org.myjaphoo.model.groupbyparser.expr.Aggregation;
import org.myjaphoo.model.grouping.GroupingExecutionContext;
import org.myjaphoo.model.grouping.PartialPathBuilder;
import org.myjaphoo.model.grouping.Path;

import java.util.List;


/**
 * Ein abstrakter grouper, der die Gruppierung durch angegebene Pfade durchführt.
 * Der Pfad wird durch ein String-array angegeben. Jeder Pfad muss für sich
 * natürlich eindeutig sein.
 *
 * @author mla
 */
public class PartialGrouper extends AbstractByPathGrouper {

    private PartialPathBuilder[] groupies;

    private String text;

    private Aggregator aggregator;

    public PartialGrouper(
        MyjaphooCorePrefs prefs,
        List<PartialPathBuilder> groupies, AggregatedExpression havingClause, List<Aggregation> aggregations
    ) {
        this(prefs, havingClause, aggregations, groupies.toArray(new PartialPathBuilder[groupies.size()]));
    }

    public PartialGrouper(
        MyjaphooCorePrefs prefs,
        AggregatedExpression havingClause, List<Aggregation> aggregations, PartialPathBuilder... groupies
    ) {
        super(prefs);
        this.groupies = groupies;
        this.aggregator = new Aggregator(prefs, aggregations, havingClause);
    }

    @Override
    public void preProcess(GroupingExecutionContext context) {
        super.preProcess(context);
        for (PartialPathBuilder groupie : getGroupies()) {
            groupie.preProcess(context);
        }
    }


    @Override
    public Path[] getPaths(JoinedDataRowZipper row) {

        Path[] createdPaths = new Path[0];
        for (PartialPathBuilder groupie : getGroupies()) {
            Path[] paths = groupie.getPaths(row);
            createdPaths = multiply(createdPaths, paths);
        }
        return createdPaths;
    }

    private Path[] multiply(Path[] paths, Path[] createdPaths) {
        if (paths == null || createdPaths == null) {
            return null;
        }
        if (paths.length == 0) {
            // this is the first multiply, no other pathes exists, so just return the created paths:
            return createdPaths;
        }
        return cartesianMultiply(paths, createdPaths);

    }

    private Path[] cartesianMultiply(Path[] paths, Path[] createdPaths) {
        Path[] result = new Path[paths.length * createdPaths.length];
        int i = 0;
        for (Path p1 : paths) {
            for (Path p2 : createdPaths) {
                result[i] = new Path(p1, p2);
                i++;
            }
        }
        return result;
    }

    /**
     * @return the groupies
     */
    public PartialPathBuilder[] getGroupies() {
        return groupies;
    }

    @Override
    public boolean needsTagRelation() {
        for (PartialPathBuilder builder : groupies) {
            if (builder.needsTagRelation()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean needsMetaTagRelation() {
        for (PartialPathBuilder builder : groupies) {
            if (builder.needsMetaTagRelation()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void aggregate(ImmutableModel model, Branch root) {
        aggregator.aggregateOnAllStructureNodes(model, root);
    }

    @Override
    public boolean hasAggregations() {
        return aggregator.hasAggregations();
    }

    @Override
    public void pruneByHavingClause(ImmutableModel model, Branch root) {
        aggregator.pruneByHavingClause(model, root);
    }

    @Override
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
