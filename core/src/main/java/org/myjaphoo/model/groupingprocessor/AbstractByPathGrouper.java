/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.groupingprocessor;

import org.apache.commons.lang.StringUtils;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.grouping.GroupingDim;
import org.myjaphoo.model.grouping.GroupingExecutionContext;
import org.myjaphoo.model.grouping.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Ein abstrakter grouper, der die Gruppierung durch angegebene Pfade durchfÃ¼hrt.
 * Der Pfad wird durch ein String-array angegeben. Jeder Pfad muss fÃ¼r sich
 * natÃ¼rlich eindeutig sein.
 *
 * @author mla
 */
public abstract class AbstractByPathGrouper implements GroupAlgorithm {

    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractByPathGrouper.class.getName());
    private Map<Path, Branch> dirMap = new HashMap<>(50000);

    private NodeComparator comparator;

    public AbstractByPathGrouper(MyjaphooCorePrefs prefs) {
        comparator = new NodeComparator(prefs);
    }

    private Set<UniquePath> uniqueSet = new HashSet<UniquePath>(50000);

    @Override
    public List<Branch> findParents(Branch root, JoinedDataRowZipper row) {
        Path paths[] = getPaths(row);
        return buildDirNodesFromPaths(root, paths, row);
    }

    private ArrayList<Branch> buildDirNodesFromPaths(
        Branch root, Path paths[], JoinedDataRowZipper row
    ) {
        if (paths != null) {
            ArrayList<Branch> result = new ArrayList<>(paths.length);
            for (Path path : paths) {
                if (row.isDbDiffRow()) {
                    // we add all diff nodes, since each node contains a diff information.
                    // we do not take care for unique paths, as this would be not so easy anyway:
                    // we would need to take both left and right part of the node into account.
                    Branch foundDirNode = getOrCreateDirNode(root, path);
                    result.add(foundDirNode);

                } else {
                    // take care that the nodes are unique, this could happen due to grouping/filter issues
                    // but normally we do not want to show identical nodes twice
                    long id = row.getEntry().getId();

                    UniquePath uniquePath = new UniquePath(path, id);
                    if (!uniqueSet.contains(uniquePath)) {
                        uniqueSet.add(uniquePath);
                        Branch foundDirNode = getOrCreateDirNode(root, path);
                        result.add(foundDirNode);
                    }
                }
            }
            return result;
        }
        return null;
    }

    private Branch createDirNode(Branch root, Path path) {
        if (path.isRoot()) {
            // we are recursively iterated to the root:
            return root;
        }
        Branch node = createStructureNode(path);

        // find the parent:
        Path parentPath = path.getParentPath();
        Branch parent = getOrCreateDirNode(root, parentPath);

        parent.addChild(node);
        dirMap.put(path, node);
        return node;
    }

    protected Branch createStructureNode(Path path) {
        Branch node = new Branch(path.getLastPathName());
        node.setGroupingExpr(path.getLastPathBuilder());
        node.setCanonicalDir(path.getCanonicalDir());
        node.setMarker(path.getPathMarker());
        return node;
    }

    private Branch getOrCreateDirNode(Branch root, Path path) {
        Branch node = dirMap.get(path);
        if (node == null) {
            node = createDirNode(root, path);
        }
        return node;
    }

    @Override
    public Branch findAccordingNode(Branch root, String path) {
        // first check, if its a structure path expression:
        String[] parts = StringUtils.split(path, "/\\:");
        Path ppath = new Path(GroupingDim.Directory, parts);
        Branch node = dirMap.get(ppath);
        if (node != null) {
            return node;
        }
        // or is it a directory path expression:
        // then do a stupid tree search by the path components:
        return findPath(root, parts, 0);
    }

    private Branch findPath(Branch root, String[] parts, int i) {
        if (i >= parts.length) {
            // path has full matched:
            return root;
        }
        String currSearchName = parts[i];

        for (Branch child : root.getChildBranches()) {
            if (currSearchName.equals(child.getName())) {
                // found name on this level: go deeper:
                return findPath(child, parts, i + 1);
            }
        }
        // no match on this level;
        return null;
    }


    @Override
    public Branch getOrCreateRoot() {
        return new Branch("Structure");
    }

    @Override
    public void postProcess(Branch root) {
        root.sort(comparator);
        dirMap.clear();
    }

    @Override
    public void preProcess(GroupingExecutionContext context) {
    }

    public abstract Path[] getPaths(JoinedDataRowZipper row);

}
