package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles condensation of grouping duplicates during aggregation.
 */
public class DuplicationCondensator {
    /**
     * Holds all already processed leafs to lookup duplicates.
     */
    Map<Long, Leaf> mapChksumMovieNodes;

    /**
     * is condensation activated?
     */
    private boolean condenseDuplicates;

    public DuplicationCondensator(boolean condenseDuplicates, int initSize) {
        mapChksumMovieNodes = new HashMap<>(initSize);
        this.condenseDuplicates = condenseDuplicates;
    }

    public Leaf findDuplicate(JoinedDataRow cr) {
        if (!condenseDuplicates) {
            return null;
        }
        // suchen, obs schon einen identischen movie gibt:
        ImmutableMovieEntry currEntry = cr.getEntry();
        Leaf alreadyExistingNode = mapChksumMovieNodes.get(currEntry.getChecksumCRC32());
        if (alreadyExistingNode != null && !alreadyExistingNode.getEntryRef().ref.equals(currEntry)) {
            return alreadyExistingNode;
        } else {
            return null;
        }
    }

    public boolean appendDuplicateIfIsDuplicate(JoinedDataRow cr) {
        Leaf alreadyExistingNode = findDuplicate(cr);
        if (alreadyExistingNode != null) {
            alreadyExistingNode.addCondensedDuplicate(cr.toEntryRef());
            return true;
        } else {
            return false;
        }
    }

    public void updateCondensatorCache(Leaf oneNode) {
        if (condenseDuplicates) {
            mapChksumMovieNodes.put(oneNode.getEntryRef().getChecksumCRC32(), oneNode);
        }
    }

    public void clear() {
        mapChksumMovieNodes.clear();
    }
}
