package org.myjaphoo.model.groupingprocessor;

import com.eekboom.utils.Strings;
import org.myjaphoo.MyjaphooCorePrefs;

import java.util.Comparator;

/**
 * NodeComparator
 */
public class NodeComparator implements Comparator<Node> {

    private static Comparator<String> NATURAL_COMPARATOR = (o1, o2) -> Strings.compareNatural(o1, o2, true, null);

    private static Comparator<String> NORMAL_COMPARATOR = (o1, o2) -> o1.compareTo(o2);

    private Comparator<String> comparator = NATURAL_COMPARATOR;

    public NodeComparator(MyjaphooCorePrefs prefs) {
        if (!prefs.PRF_USE_NATURALSORTING.getVal()) {
            comparator = NORMAL_COMPARATOR;
        }
    }

    @Override
    public int compare(Node o1, Node o2) {
        // sortierung:
        // 1. nach dir, 2. nach name:
//        boolean o1haschildren = o1.getChildCount() > 0;
//        boolean o2haschildren = o2.getChildCount() > 0;
//        if (o1haschildren && !o2haschildren) {
//            return 1;
//        }
//        if (!o1haschildren && o2haschildren) {
//            return -1;
//        }
        return comparator.compare(o1.getName(), o2.getName());
    }
}
