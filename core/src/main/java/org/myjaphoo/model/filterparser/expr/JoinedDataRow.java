/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser.expr;

import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.EntryRef;


/**
 * This represents a "row" during filtering and grouping with all relevant data.
 * This contains "joined" data from other entities if necessary.
 *
 * @author lang
 */
public class JoinedDataRow {

    private ImmutableModel model;

    /**
     * the movie entry for filtering.
     */
    private ImmutableMovieEntry entry;
    /**
     * a joined tag, if tags get joined for this row.
     */
    private ImmutableToken token;
    /**
     * a joined meta-tag, if tags get joined for this row.
     */
    private ImmutableMetaToken metaToken;

    public JoinedDataRow(
        ImmutableModel model, ImmutableMovieEntry entry, ImmutableToken token, ImmutableMetaToken metaToken
    ) {
        this.model = model;
        this.entry = entry;
        this.token = token;
        this.metaToken = metaToken;
    }

    /**
     * @return the entry
     */
    public ImmutableMovieEntry getEntry() {
        return entry;
    }

    /**
     * @return the token
     */
    public ImmutableToken getToken() {
        return token;
    }

    /**
     * @return the metaToken
     */
    public ImmutableMetaToken getMetaToken() {
        return metaToken;
    }

    public ImmutableModel getModel() {
        return model;
    }

    public EntryRef toEntryRef() {
        return new EntryRef(model, entry);
    }
}
