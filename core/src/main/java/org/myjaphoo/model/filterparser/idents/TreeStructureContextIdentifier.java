package org.myjaphoo.model.filterparser.idents;

import org.mlsoft.structures.TreeStructure;
import org.myjaphoo.model.cache.zipper.ZipperTree;
import org.myjaphoo.model.filterparser.expr.ExprType;

/**
 * Base class for identifiers that return values from movie entries.
 * All those identifiers act also as "qualifier" that deliver a movie entry as context.
 */
public abstract class TreeStructureContextIdentifier extends FixIdentifier<ZipperTree, ZipperTree> {

    public TreeStructureContextIdentifier(String name, String descr, String exampleUsage, ExprType exprType) {
        super(name, descr, exampleUsage, exprType, ZipperTree.class, ZipperTree.class, false, false);

    }

    @Override
    public ZipperTree extractQualifierContext(ZipperTree row) {
        return row;
    }
}