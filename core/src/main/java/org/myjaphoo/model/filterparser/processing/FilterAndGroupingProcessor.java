/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser.processing;

import org.apache.commons.lang.time.StopWatch;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.dbcompare.CombinationSet;
import org.myjaphoo.model.dbcompare.ComparisonSetGenerator;
import org.myjaphoo.model.dbcompare.DBDiffCombinationResult;
import org.myjaphoo.model.dbcompare.DatabaseComparison;
import org.myjaphoo.model.filterparser.ExecutionContext;
import org.myjaphoo.model.filterparser.FileTypes;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;
import org.myjaphoo.model.filterparser.expr.Expression;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.filterparser.values.BoolValue;
import org.myjaphoo.model.filterparser.values.Value;
import org.myjaphoo.model.filterparser.visitors.ExpressionVisitor;
import org.myjaphoo.model.groupbyparser.GroupByParser;
import org.myjaphoo.model.groupbyparser.expr.GroupingExpression;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;
import org.myjaphoo.model.groupingprocessor.PartialGrouper;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Handles the filtering and grouping by expressions.
 *
 * @author lang
 */
public class FilterAndGroupingProcessor {

    public static final Logger LOGGER = LoggerFactory.getLogger(FilterAndGroupingProcessor.class);

    private static final AbstractBoolExpression TRUE_EXPRESSION = new AbstractBoolExpression() {

        @Override
        public String getDisplayExprTxt() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Value evaluate(ExecutionContext context, JoinedDataRowZipper row) {
            return BoolValue.TRUE;
        }

        @Override
        public List<Expression> getChildren() {
            return Collections.EMPTY_LIST;
        }

        @Override
        public boolean needsTagRelation() {
            return false;
        }

        @Override
        public boolean needsMetaTagRelation() {
            return false;
        }

        @Override
        public <T> T accept(ExpressionVisitor<T> expressionVisitor) {
            return expressionVisitor.visit(this);
        }
    };

    public static ArrayList<JoinedDataRow> filterEntries(
        MyjaphooCorePrefs prefs,
        ImmutableModel model,
        io.vavr.collection.List<ImmutableMovieEntry> allMovies,
        DatabaseComparison databaseComparison,
        AbstractBoolExpression expr, ProcessingRequirementInformation pri
    ) {
        return filterEntries(prefs.createFileTypes(), model, allMovies, databaseComparison, expr, pri);
    }
    /**
     * Filters the entries. It therefore creates a cross product of entries * token * metatoken if necessary
     * and then checks via the expression evaluate function, if that particular combination
     * gets filtered.
     */
    public static ArrayList<JoinedDataRow> filterEntries(
        FileTypes fileTypes,
        ImmutableModel model,
        io.vavr.collection.List<ImmutableMovieEntry> allMovies,
        DatabaseComparison databaseComparison,
        AbstractBoolExpression expr, ProcessingRequirementInformation pri
    ) {
        StopWatch watch = new StopWatch();
        watch.start();
        ArrayList<JoinedDataRow> result = new ArrayList<>(allMovies.size() * 10);
        if (expr == null) {
            expr = TRUE_EXPRESSION;
        }

        ExecutionContext context = new ExecutionContext(model, fileTypes);
        CombinationSet combSet = CombinationGeneratorFactory.createCombinations(model, allMovies, expr, pri);

        if (databaseComparison != null && databaseComparison.getComparisonProject() != null) {
            ImmutableModel otherModel = databaseComparison.getComparisonProject().cacheActor.getImmutableModel();
            io.vavr.collection.List<ImmutableMovieEntry> comparisonMovies = otherModel.getEntryList();
            // prepare comparison combinations of the two databases:
            CombinationSet combOtherDBSet = CombinationGeneratorFactory.createCombinations(
                otherModel,
                comparisonMovies,
                expr,
                pri
            );
            ComparisonSetGenerator generator = new ComparisonSetGenerator(databaseComparison);
            Iterable<DBDiffCombinationResult> iterator = generator.createComparisonSetIterator(combSet, combOtherDBSet);

            JoinedDataRowZipper zipperRow = new JoinedDataRowZipper(model);
            for (DBDiffCombinationResult comb : iterator) {
                zipperRow.updateZipper(comb);
                if (expr.evaluate(context, zipperRow).asBool()) {
                    result.add(comb);
                }
            }
        } else {
            JoinedDataRowZipper zipperRow = new JoinedDataRowZipper(model);
            for (JoinedDataRow comb : combSet.getResult()) {
                zipperRow.updateZipper(comb);
                if (expr.evaluate(context, zipperRow).asBool()) {
                    result.add(comb);
                }
            }
        }

        watch.stop();
        LOGGER.info("finished filter processing result: duration:" + watch.toString()); //NOI18N
        return result;
    }


    public static List<? extends GroupAlgorithm> createGroupingAlgorithm(
        MyjaphooCorePrefs prefs,
        Supplier<Map<String, Substitution>> substitutionsSupplier,
        String groupExpr
    ) throws ParserException {
        GroupByParser parser = new GroupByParser(substitutionsSupplier.get());
        List<GroupingExpression> expressions = parser.parseGroupByExpression(groupExpr);
        ArrayList<GroupAlgorithm> result = new ArrayList<>();
        for (GroupingExpression expression : expressions) {
            PartialGrouper algorithm = new PartialGrouper(
                prefs,
                expression.createGrouper(),
                expression.getHavingClause(),
                expression.getAggregations()
            );
            algorithm.setText(expression.getText());
            result.add(algorithm);
        }
        return result;

    }

}
