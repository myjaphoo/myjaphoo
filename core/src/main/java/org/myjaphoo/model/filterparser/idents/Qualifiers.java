package org.myjaphoo.model.filterparser.idents;

import org.myjaphoo.model.filterparser.expr.ExprType;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;

/**
 * All defined qualifiers.
 */
public class Qualifiers {


    public static final Qualifier LEFT = new Qualifier<JoinedDataRowZipper, JoinedDataRowZipper>(
        "left",
        "left side of comparison",
        "left.path like blubb",
        ExprType.NULL,
        JoinedDataRowZipper.class,
        JoinedDataRowZipper.class,
        false,
        false
    ) {

        @Override
        public JoinedDataRowZipper extractQualifierContext(JoinedDataRowZipper row) {
            return row;
        }
    };

    public static final Qualifier RIGHT = new Qualifier<JoinedDataRowZipper, JoinedDataRowZipper>(
        "right",
        "right side of comparison",
        "right.path like blubb",
        ExprType.NULL,
        JoinedDataRowZipper.class,
        JoinedDataRowZipper.class,
        false,
        false
    ) {
        @Override
        public JoinedDataRowZipper extractQualifierContext(JoinedDataRowZipper row) {
            // this does only work for diff results;
            // what we do here is, we just return the right side of the comparison result:
            if (row.isDbDiffRow()) {
                row.switchRight();
            }
            return row;
        }
    };
}
