package org.myjaphoo.model.filterparser.idents;

import org.myjaphoo.model.cache.zipper.EntryZipper;
import org.myjaphoo.model.filterparser.expr.ExprType;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;

/**
 * Base class for identifiers that return values from movie entries.
 * All those identifiers act also as "qualifier" that deliver a movie entry as context.
 */
public abstract class EntryContextIdentifier extends FixIdentifier<JoinedDataRowZipper, EntryZipper> {

    public EntryContextIdentifier(String name, String descr, String exampleUsage, ExprType exprType) {
        super(name, descr, exampleUsage, exprType, JoinedDataRowZipper.class, EntryZipper.class, false, false);

    }

    @Override
    public EntryZipper extractQualifierContext(JoinedDataRowZipper row) {
        return row.getEntry();
    }
}