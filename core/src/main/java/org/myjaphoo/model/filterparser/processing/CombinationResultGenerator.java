/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser.processing;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbcompare.CombinationSet;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;

import java.util.ArrayList;

/**
 * Generates a set of combination results for a database.
 * <p/>
 * it delivers (db1.movie x db1.tag x db1.metatag) tuples
 * as result.
 * <p/>
 * This is the base for filtering & grouping and also database comparison.
 * The crossproduct of movie * tag * metatag is only generated, if
 * its necessary (this depends, if tag or metatag is part of the expressions
 * used for filtering or grouping).
 *
 * @author lang
 */
public class CombinationResultGenerator {

    private static final Token NULL_TOKEN_MUTABLE = new Token();
    static {
        NULL_TOKEN_MUTABLE.setId(-1L);
    }
    public static final ImmutableToken NULL_TOKEN = new ImmutableToken(NULL_TOKEN_MUTABLE);
    private static final MetaToken NULL_META_TOKEN_MUTABLE = new MetaToken();
    static {
        NULL_META_TOKEN_MUTABLE.setId(-1L);
    }
    public static final ImmutableMetaToken NULL_META_TOKEN = new ImmutableMetaToken(NULL_META_TOKEN_MUTABLE);

    public static final Set<ImmutableToken> EMPTY_TOKEN_LIST = HashSet.of(NULL_TOKEN);
    public static final Set<ImmutableMetaToken> EMPTY_META_TOKEN_LIST = HashSet.of(NULL_META_TOKEN);

    /**
     * same as getIterator, but only in a simple version, which just produces
     * simply all combinations in a result list and then creates an iterator
     * from this.
     * The code is much clearer, but a possible drawback would be that
     * all combination objects would be created and holded in a list.
     */
    public CombinationSet getSillyIterator(
        ImmutableModel model, io.vavr.collection.List<ImmutableMovieEntry> allMovies,
        ProcessingRequirementInformation pri1, ProcessingRequirementInformation pri2
    ) {

        ArrayList<JoinedDataRow> result = new ArrayList<JoinedDataRow>(allMovies.size() * 10);

        boolean needsTagRelation = determineNeedOfTagRelation(pri1, pri2);
        boolean needsMetaTagRelation = determineNeedOfMetaTagRelation(pri1, pri2);

        for (ImmutableMovieEntry entry : allMovies) {

            Set<ImmutableToken> assignedTokens = EMPTY_TOKEN_LIST;
            if (needsTagRelation && !entry.getTokens(model).isEmpty()) {
                assignedTokens = entry.getTokens(model);
            }

            for (ImmutableToken token : assignedTokens) {

                Set<ImmutableMetaToken> assignedMetaTokens = EMPTY_META_TOKEN_LIST;
                if (needsMetaTagRelation && !token.getAssignedMetaTokens(model).isEmpty()) {
                    assignedMetaTokens = token.getAssignedMetaTokens(model);
                }
                for (ImmutableMetaToken metaToken : assignedMetaTokens) {

                    result.add(new JoinedDataRow(model, entry, token, metaToken));
                }
            }
        }
        CombinationSet set = new CombinationSet(model, result, allMovies, needsTagRelation, needsMetaTagRelation);
        return set;
    }

    private static boolean determineNeedOfTagRelation(
        ProcessingRequirementInformation pri1, ProcessingRequirementInformation pri2
    ) {
        // we need the tag relation if a expression needs it, or (indirectly) if metatags are needed.
        return pri1.needsTagRelation() || pri2.needsTagRelation() || determineNeedOfMetaTagRelation(pri1, pri2);
    }

    private static boolean determineNeedOfMetaTagRelation(
        ProcessingRequirementInformation pri1, ProcessingRequirementInformation pri2
    ) {
        return pri1.needsMetaTagRelation() || pri2.needsMetaTagRelation();
    }

}
