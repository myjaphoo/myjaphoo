package org.myjaphoo.model.filterparser.idents;

import org.myjaphoo.model.cache.zipper.ImmutableAttributedEntityZipper;
import org.myjaphoo.model.filterparser.expr.ExprType;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;

/**
 * Base class for identifiers that return values from attribute entities.
 */
public abstract class AttributedEntityContextIdentifier extends FixIdentifier<Object, ImmutableAttributedEntityZipper> {

    public AttributedEntityContextIdentifier(String name, String descr, String exampleUsage, ExprType exprType) {
        super(name, descr, exampleUsage, exprType, Object.class, ImmutableAttributedEntityZipper.class, false, false);

    }

    @Override
    public ImmutableAttributedEntityZipper extractQualifierContext(Object row) {
        if (row instanceof JoinedDataRowZipper) {
            return ((JoinedDataRowZipper) row).getEntry();
        } else if (row instanceof ImmutableAttributedEntityZipper) {
            // we are in the context of a tag, metatag probably:
            return (ImmutableAttributedEntityZipper) row;
        } else {
            throw new IllegalArgumentException("qualifier context error on object " + row);
        }
    }
}