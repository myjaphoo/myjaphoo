package org.myjaphoo.model.filterparser.idents;

import org.myjaphoo.model.cache.zipper.MetaTokenZipper;
import org.myjaphoo.model.filterparser.expr.ExprType;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;

/**
 * Base class for identifiers that return values from metatags.
 * All those identifiers act also as "qualifier" that deliver a metatag as context.
 */
public abstract class MetaTagContextIdentifier extends FixIdentifier<JoinedDataRowZipper, MetaTokenZipper> {

    public MetaTagContextIdentifier(String name, String descr, String exampleUsage, ExprType exprType) {
        super(name, descr, exampleUsage, exprType, JoinedDataRowZipper.class, MetaTokenZipper.class, false, true);

    }

    @Override
    public MetaTokenZipper extractQualifierContext(JoinedDataRowZipper row) {
        return row.getMetaToken();
    }
}