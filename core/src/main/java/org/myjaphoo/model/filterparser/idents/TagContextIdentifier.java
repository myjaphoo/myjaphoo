package org.myjaphoo.model.filterparser.idents;

import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.filterparser.expr.ExprType;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;

/**
 * Base class for identifiers that return values from tags.
 * All those identifiers act also as "qualifier" that deliver a tag as context.
 */
public abstract class TagContextIdentifier extends FixIdentifier<JoinedDataRowZipper, TokenZipper> {

    public TagContextIdentifier(String name, String descr, String exampleUsage, ExprType exprType) {
        super(name, descr, exampleUsage, exprType, JoinedDataRowZipper.class, TokenZipper.class, true, false);

    }

    @Override
    public TokenZipper extractQualifierContext(JoinedDataRowZipper row) {
        return row.getToken();
    }
}