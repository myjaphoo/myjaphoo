/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser.processing;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.dbcompare.CombinationSet;

/**
 * Factory for creating combinations.
 * Hides the concrete implementation of combation creation algorithm.
 *
 * @author lang
 */
class CombinationGeneratorFactory {

    public static CombinationSet createCombinations(
        ImmutableModel model, final io.vavr.collection.List<ImmutableMovieEntry> allMovies,
        final ProcessingRequirementInformation pri1, final ProcessingRequirementInformation pri2
    ) {
        // for now, use the silly method
        return new CombinationResultGenerator().getSillyIterator(model, allMovies, pri1, pri2);
    }
}
