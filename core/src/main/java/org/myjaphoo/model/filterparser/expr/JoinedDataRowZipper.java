/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser.expr;

import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.zipper.EntryZipper;
import org.myjaphoo.model.cache.zipper.MetaTokenZipper;
import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.dbcompare.Comparisons;
import org.myjaphoo.model.dbcompare.DBDiffCombinationResult;


/**
 * This represents a "row" during filtering and grouping with all relevant data.
 * This contains "joined" data from other entities if necessary.
 *
 * @author lang
 */
public class JoinedDataRowZipper {

    /**
     * the movie entry for filtering.
     */
    private EntryZipper entry;
    /**
     * a joined tag, if tags get joined for this row.
     */
    private TokenZipper token;
    /**
     * a joined meta-tag, if tags get joined for this row.
     */
    private MetaTokenZipper metaToken;

    private JoinedDataRow row;

    public JoinedDataRowZipper(ImmutableModel model) {
        entry = new EntryZipper(model, null);
        token = new TokenZipper(model, null);
        metaToken = new MetaTokenZipper(model, null);
    }

    /**
     * @return the entry
     */
    public EntryZipper getEntry() {
        return entry;
    }

    /**
     * @return the token
     */
    public TokenZipper getToken() {
        return token;
    }

    /**
     * @return the metaToken
     */
    public MetaTokenZipper getMetaToken() {
        return metaToken;
    }

    public void updateZipper(JoinedDataRow row) {
        this.row = row;
        entry.setRef(row.getEntry());
        token.setRef(row.getToken());
        metaToken.setRef(row.getMetaToken());
    }

    public boolean isDbDiffRow() {
        return row instanceof DBDiffCombinationResult;
    }


    public String getCategoryName() {
        return Comparisons.getCategoryName(row);
    }

    public void switchRight() {
        row = ((DBDiffCombinationResult)row).getT2();
        updateZipper(row);
    }
}
