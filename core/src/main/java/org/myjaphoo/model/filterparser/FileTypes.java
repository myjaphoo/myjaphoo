package org.myjaphoo.model.filterparser;

import org.myjaphoo.model.FileTypeInterface;

/**
 * Bundles supported file type defintions for usage in a filter or grouping context.
 *
 * @author mla
 * @version $Id$
 */
public class FileTypes {
    private FileTypeInterface Movie;
    private FileTypeInterface Picture;
    private FileTypeInterface Text;

    public FileTypes(FileTypeInterface movie, FileTypeInterface picture, FileTypeInterface text) {
        Movie = movie;
        Picture = picture;
        Text = text;
    }

    public FileTypeInterface getMovie() {
        return Movie;
    }

    public FileTypeInterface getPicture() {
        return Picture;
    }

    public FileTypeInterface getText() {
        return Text;
    }
}
