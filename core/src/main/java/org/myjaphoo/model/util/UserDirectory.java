/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.util;

import ch.qos.logback.core.PropertyDefinerBase;

import java.io.File;

/**
 * <p>Supply the directory name where user's preferences, log files and
 * database are located. </p>
 * <p/>
 * <p>By default, a directory name of <code>'${user.home}/.myjaphoo'</code> is
 * supplied. The user can define another directory name by setting the
 * parameter <code>'wankman.model.util.UserDirectory'</code>
 * to the application command line. For example: <code>
 * -Dwankman.model.util.UserDirectory=path/.myjaphoo_copy1</code></p>
 * <p/>
 * <p>The resulting directory name is formated to make it
 * coherent with system specific directory separator '<code>\</code>',
 * or '<code>/</code>'. A directory separator is added at the end of the
 * name.</p>
 * @author gsd
 */
public class UserDirectory extends PropertyDefinerBase {
    // The name of the application command line parameter passed as 
    // system property
    private static String userDirPropNameOld = "wankman.model.util.UserDirectory";

    private static String userDirPropNameNew = "myjaphoo.userDir";
    /**
     * <p>Get the directory name where user's preferences, log files and
     * database should be located.</p>
     * @return the directory name supplied on the application command line
     * parameter or the expanded directory name
     * <code>${user.home}/.myjaphoo/</code>. In either cases, the directory
     * name supplied end with a '<code>\</code>', or '<code>/</code>'.
     */
    public static String getDirectory() {
        // Fetch the command line parameter from system properties
        String userDirPropValue = System.getProperty(userDirPropNameOld);

        // Fallback content if the property does not exist
        if (userDirPropValue == null) {
            userDirPropValue = System.getProperty(userDirPropNameNew);
            if (userDirPropValue == null) {
                String userHome= System.getProperty("user.home");
                // Supply an hidden directory in the user space, named after the
                // application.
                userDirPropValue = userHome + "/.myjaphoo";
            }
        }

        // Format the name according to the operating system rules
        // This is a best effort to get well formated directory name
        try {
            File file = new File(userDirPropValue);
            userDirPropValue = file.getCanonicalPath();
        } catch (Exception ex) {
            // Nothing to do since this method might be used to set
            // the logger.
        }
        return userDirPropValue + File.separator;
    }

    /**
     * Get the property value, defined by this property definer
     * @return defined property value
     */
    @Override
    public String getPropertyValue() {
        return getDirectory();
    }
}
