package org.myjaphoo.model.util;

import org.apache.commons.lang.StringUtils;
import org.myjaphoo.model.filterparser.FilterParser;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Parsing
 *
 * @author mla
 * @version $Id$
 */
public class Parsing {
    
    public static AbstractBoolExpression parseExpr(
        Supplier<Map<String, Substitution>> substitutionSupplier, String strExpr
    ) {
        AbstractBoolExpression expr = null;
        if (!StringUtils.isEmpty(strExpr)) {
            FilterParser parser = new FilterParser(substitutionSupplier.get());
            expr = parser.parse(strExpr);
        }
        return expr;
    }
}
