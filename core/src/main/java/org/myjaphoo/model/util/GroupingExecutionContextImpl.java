/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.util;

import io.vavr.collection.Seq;
import org.myjaphoo.model.FileSubstitution;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.filterparser.ExecutionContext;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.grouping.GroupingExecutionContext;
import org.myjaphoo.model.logic.FileSubstitutionImpl;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.project.Project;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;


/**
 * Hält Kontext Informationen, die die einzelnen Gruppierer benötigen.
 * Notwendig, um den allgemeinen Grouping Code von Details, wie etwa
 * spezielle DAO Klassen abzukapseln.
 *
 * @author mla
 */
public class GroupingExecutionContextImpl implements GroupingExecutionContext {

    private List<JoinedDataRow> allEntries;
    private FileSubstitution fileSubstitution;
    private Supplier<Map<String, Substitution>> substitutionSupplier;
    private ExecutionContext context;

    public GroupingExecutionContextImpl(Project project,
                                        List<JoinedDataRow> allEntries,
                                        ExecutionContext context) {
        this.allEntries = allEntries;
        this.context = context;
        MovieEntryJpaController jpa = new MovieEntryJpaController(project.connection);
        substitutionSupplier = () -> MovieEntryJpaController.createSubst(jpa.findBookMarkEntities());

        fileSubstitution = new FileSubstitutionImpl(project);
    }

    /**
     * used for tests... @todo make this smarter...
     *
     */
    public GroupingExecutionContextImpl(
        Supplier<Map<String, Substitution>> substitutionSupplier,
        List<JoinedDataRow> allEntries,
        ExecutionContext context,
        FileSubstitution fileSubstitution
    ) {
        this.allEntries = allEntries;
        this.context = context;
        this.substitutionSupplier = substitutionSupplier;
        this.fileSubstitution = fileSubstitution;
    }

    @Override
    public Set<ImmutableMovieEntry> getAllEntriesToGroup() {
        return getDistinctEntries(allEntries);
    }

    public static Set<ImmutableMovieEntry> getDistinctEntries(List<JoinedDataRow> allEntries) {
        Set<ImmutableMovieEntry> distinctEntries = new HashSet<>(allEntries.size());
        for (JoinedDataRow cr : allEntries) {
            distinctEntries.add(cr.getEntry());
        }
        return distinctEntries;
    }


    @Override
    public List<JoinedDataRow> getAllCombinationsToGroup() {
        return allEntries;
    }

    @Override
    public Seq<ImmutableToken> getTagList() {
        return context.getModel().getTokenTree().getValues();
    }

    @Override
    public FileSubstitution getFileSubstitution() {
        return fileSubstitution;
    }

    @Override
    public Map<String, Substitution> createSubst() {
        return substitutionSupplier.get();
    }

    @Override
    public ExecutionContext getFilterContext() {
        return context;
    }
}
