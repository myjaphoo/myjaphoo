/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.grouping;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryZipper;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;


/**
 * Gruppiert nach der Size und dann nach directories.
 *
 * @author mla
 */
public class SizePartialGrouper extends CachingPartialPathBuilder<String> {

    @Override
    public final Path[] getPaths(JoinedDataRowZipper row) {
        String size = decideSizeCategory(row.getEntry());
        return new Path[]{getPath(size)};
    }

    private String decideSizeCategory(EntryZipper movieEntry) {
        SizeCategory cat = SizeCategory.searchNearesCatBySize(movieEntry.getFileLength());
        return cat.getName();
    }

    @Override
    protected Path createPath(String size) {
        return new Path(GroupingDim.Size, size);
    }

    @Override
    public boolean needsTagRelation() {
        return false;
    }

    @Override
    public boolean needsMetaTagRelation() {
        return false;
    }
}
