/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.grouping;

import org.myjaphoo.model.dbcompare.Comparisons;
import org.myjaphoo.model.dbcompare.DatabaseComparison;
import org.myjaphoo.model.filterparser.expr.JoinedDataRow;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;


/**
 * Gruppiert nach der Tokenhierarchie und dann nach directories.
 * @author mla
 */
public class DBComparePartialGrouper extends CachingPartialPathBuilder<String> {

    @Override
    public final Path[] getPaths(JoinedDataRowZipper row) {
        String cat = row.getCategoryName();
        return new Path[]{getPath(cat)};
    }

    @Override
    protected Path createPath(String cat) {
        return new Path(GroupingDim.DB_Comparison, cat);
    }

    @Override
    public boolean needsTagRelation() {
        return false;
    }

    @Override
    public boolean needsMetaTagRelation() {
        return false;
    }
}
