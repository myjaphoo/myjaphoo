/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.myjaphoo.model.grouping;

import org.apache.commons.lang.StringUtils;
import org.myjaphoo.model.filterparser.FilterParser;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.Substitution;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Gruppiert nach Bookmark Zugehörigkeit.
 *
 * @author mla
 */
public class BookmarkPartialGrouper extends AbstractPartialPathBuilder {

    private static final String NOASSGNMENT = "no bookmark";
    private static final Path[] NOASS = new Path[]{new Path(GroupingDim.Bookmark, NOASSGNMENT)};
    private Map<String, Substitution> substitutions = null;
    private AbstractBoolExpression[] bmExpr = null;
    private String[] names = null;

    @Override
    public void preProcess(GroupingExecutionContext context) {
        super.preProcess(context);
        substitutions = context.createSubst();
        bmExpr = new AbstractBoolExpression[substitutions.size()];
        names = new String[substitutions.size()];
        Iterator<Substitution> iterator = substitutions.values().iterator();
        for (int i = 0; i < substitutions.size(); i++) {
            Substitution substitution = iterator.next();
            if (!StringUtils.isEmpty(substitution.getExpression())) {
                FilterParser parser = new FilterParser(substitutions);
                try {
                    bmExpr[i] = parser.parse(substitution.getExpression());
                    names[i] = substitution.getName();
                } catch (ParserException ex) {
                    LoggerFactory.getLogger(BookmarkPartialGrouper.class.getName()).error("parser error:", ex);
                }
            }
        }

    }

    @Override
    public final Path[] getPaths(JoinedDataRowZipper row) {
        Set<String> filteredbm = new HashSet<String>();
        for (int i = 0; i < bmExpr.length; i++) {
            if (bmExpr[i] == null) {
                filteredbm.add(NOASSGNMENT);
            } else if (bmExpr[i].evaluate(getContext().getFilterContext(), row).asBool()) {
                filteredbm.add(names[i]);
            }
        }
        Path[] result = new Path[filteredbm.size()];
        int i = 0;
        for (String name : filteredbm) {
            result[i] = new Path(GroupingDim.Bookmark, name);
            i++;
        }

        return result;
    }

    @Override
    public boolean needsTagRelation() {
        return true;
    }

    @Override
    public boolean needsMetaTagRelation() {
        return true;
    }
}
