/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.config;

import org.myjaphoo.ApplicationException;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseConfigurations;
import org.myjaphoo.model.dbconfig.DatabaseDriver;
import org.myjaphoo.model.dbconfig.DriverParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * @author lang
 */
public class DatabaseConfigLoadSave {

    private static Logger logger = LoggerFactory.getLogger(DatabaseConfigLoadSave.class);

    private String dir;

    public DatabaseConfigLoadSave(String dir) {
        this.dir = dir;
    }

    public void save(DatabaseConfigurations configurations) {
        try {
            JAXBContext jaxb = createJaxbContext();
            File file = new File(dir, "databases.xml");

            Marshaller m = jaxb.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(configurations, file);
        } catch (Exception e) {
            logger.error("error saving database configurations!", e);
            throw new ApplicationException("error saving database configurations!", e);
        }
    }

    private JAXBContext createJaxbContext() throws JAXBException {
        return JAXBContext.newInstance(
            DatabaseConfigurations.class,
            DatabaseConfiguration.class,
            DriverParameter.class,
            DatabaseDriver.class
        );
    }

    public DatabaseConfigurations load() {
        try {
            JAXBContext jaxb = createJaxbContext();
            Unmarshaller um = jaxb.createUnmarshaller();

            File file = new File(dir, "databases.xml");
            DatabaseConfigurations dbconfigs = new DatabaseConfigurations();
            if (file.exists()) {
                dbconfigs = (DatabaseConfigurations) um.unmarshal(file);
            }
            return dbconfigs;
        } catch (Exception e) {
            logger.error("error loading database configurations!", e);
            throw new ApplicationException("error loading database configurations!", e);
        }
    }
}
