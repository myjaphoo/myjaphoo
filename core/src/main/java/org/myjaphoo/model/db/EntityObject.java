package org.myjaphoo.model.db;

/**
 * EntityObject
 *
 * @author mla
 * @version $Id$
 */
public interface EntityObject {

    public Long getId();

}
