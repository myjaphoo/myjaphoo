/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.model.db;

import javax.persistence.*;

/**
 * @author mla
 */
@Entity
public class Thumbnail extends BaseEntity {

    @Lob
    @Column(length = 1000000000)
    private byte[] thumbnail;

    private int w;

    private int h;

    private ThumbType type;

    @ManyToOne(fetch = FetchType.LAZY)
    private MovieEntry movieEntry;


    @Override
    public String toString() {
        return "org.myjaphoo.model.db.Thumbnail[id=" + getId() + "]";
    }

    /**
     * @return the thumbnail
     */
    public byte[] getThumbnail() {
        return thumbnail;
    }

    /**
     * @param thumbnail the thumbnail to set
     */
    public void setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * @return the w
     */
    public int getW() {
        return w;
    }

    /**
     * @param w the w to set
     */
    public void setW(int w) {
        this.w = w;
    }

    /**
     * @return the h
     */
    public int getH() {
        return h;
    }

    /**
     * @param h the h to set
     */
    public void setH(int h) {
        this.h = h;
    }

    /**
     * @return the movieEntry
     */
    public MovieEntry getMovieEntry() {
        return movieEntry;
    }

    /**
     * @param movieEntry the movieEntry to set
     */
    public void setMovieEntry(MovieEntry movieEntry) {
        this.movieEntry = movieEntry;
    }

    /**
     * @return the type
     */
    public ThumbType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ThumbType type) {
        this.type = type;
    }

}
