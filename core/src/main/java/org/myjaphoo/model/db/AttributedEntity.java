package org.myjaphoo.model.db;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * AttributedEntity
 *
 * @author lang
 * @version $Id$
 */
public interface AttributedEntity {
    String getName();

    String getComment();

    void setComment(String str);

    Map<String, String> getAttributes();

    void setAttributes(Map<String, String> attributes);

    Long getCreatedMs();

    Date getCreated();

    Date getEdited();

    String getUuid();
}
