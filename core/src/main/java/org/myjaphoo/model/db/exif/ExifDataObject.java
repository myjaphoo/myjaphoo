package org.myjaphoo.model.db.exif;

import java.util.Date;
import java.util.Map;

/**
 * ExifDataObject
 *
 * @author mla
 * @version $Id$
 */
public interface ExifDataObject {
    Object getExifValue(String tagName);

    String getExifMake();

    String getExifModel();

    Date getExifCreateDate();

    String getExifFnumber();

    String getExifIso();

    String getExifExposureTime();

    Map<String, String> getExifValues();

    Double getLongitude();

    Double getLatitude();
}
