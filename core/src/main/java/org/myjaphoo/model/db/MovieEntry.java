/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.db;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.db.exif.ExifData;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


/**
 * Main DB class for the movies.
 *
 * @author lang
 */
@Entity
public class MovieEntry extends UUIDEntity implements AttributedEntity {

    private String name;
    @Column(length = 1024)
    private String canonicalDir;
    @OneToMany(mappedBy = "movieEntry", fetch = FetchType.LAZY)
    private List<Thumbnail> thumbnails = new ArrayList<Thumbnail>();

    @Index(name = "filIndex")
    private long fileLength;
    private Long checksumCRC32;
    @Enumerated(EnumType.ORDINAL)
    private Rating rating;
    @Embedded
    private MovieAttrs movieAttrs = new MovieAttrs();
    private String title;
    @Column(length = 4000)
    private String comment;

    @Embedded
    private ExifData exifData = new ExifData();

    @ElementCollection(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SELECT)
    @Column(name = "attributes", length = 4000)
    private Map<String, String> attributes = new HashMap<>();

    /**
     * sha1 checksum of that entry.
     */
    private String sha1;

    /**
     * Liefert den ersten  thumbnail oder null, falls keiner existiert:
     *
     * @return der erste thumbnail
     */
    public byte[] getThumbnail1() {
        if (thumbnails != null && thumbnails.size() > 0) {
            return thumbnails.get(0).getThumbnail();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "MovieEntry[id=" + getId() + "]";
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = StringPool.pooledString(name);
    }

    /**
     * @return the canonicalPath
     */
    public String getCanonicalPath() {
        return canonicalDir + "/" + name;
    }

    /**
     * @return the canonicalPath
     */
    public String getCanonicalDir() {
        return canonicalDir;
    }

    /**
     * @param canonicalDir the canonicalPath to set
     */
    public void setCanonicalDir(String canonicalDir) {
        this.canonicalDir = StringPool.pooledString(canonicalDir);
    }

    /**
     * @return the fileLength
     */
    public long getFileLength() {
        return fileLength;
    }

    /**
     * @param fileLength the fileLength to set
     */
    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    /**
     * @return the thumbnails
     */
    public List<Thumbnail> getThumbnails() {
        return thumbnails;
    }

    /**
     * @return the checksumCRC32
     */
    public Long getChecksumCRC32() {
        return checksumCRC32;
    }

    /**
     * @param checksumCRC32 the checksumCRC32 to set
     */
    public void setChecksumCRC32(Long checksumCRC32) {
        this.checksumCRC32 = checksumCRC32;
    }

    /**
     * @return the rating
     */
    public Rating getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(Rating rating) {
        this.rating = rating;
    }

    /**
     * Sets all string values to pooled string values.
     */
    @PostLoad
    public void poolStringValues() {
        canonicalDir = StringPool.pooledString(canonicalDir);
        name = StringPool.pooledString(name);
    }

    /**
     * @return the movieAttrs
     */
    public MovieAttrs getMovieAttrs() {
        if (movieAttrs == null) {
            setMovieAttrs(new MovieAttrs());
        }
        return movieAttrs;
    }

    /**
     * @param movieAttrs the movieAttrs to set
     */
    public void setMovieAttrs(MovieAttrs movieAttrs) {
        this.movieAttrs = movieAttrs;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * makes a shallow copy of this object.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ImmutableMovieEntry toImmutable() {
        return new ImmutableMovieEntry(this);
    }

    /**
     * @return the exifData
     */
    public ExifData getExifData() {
        if (exifData == null) {
            exifData = new ExifData();
        }
        return exifData;
    }

    /**
     * @param exifData the exifData to set
     */
    public void setExifData(ExifData exifData) {
        this.exifData = exifData;
    }

    /**
     * @param thumbnails the thumbnails to set
     */
    public void setThumbnails(List<Thumbnail> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }
}
