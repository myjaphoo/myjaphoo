package org.myjaphoo.model.db;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Date;
import java.util.UUID;

/**
 * UUIDEntity
 * @author mla
 * @version $Id$
 */
@MappedSuperclass
public class UUIDEntity extends BaseEntity {

    /** uuid of entry. This could be used to synchronize different db states. Its not used internally as id key,
     * there we have simple long id values. */
    private String uuid;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @PrePersist
    public void initUUID() {
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }
    }
}
