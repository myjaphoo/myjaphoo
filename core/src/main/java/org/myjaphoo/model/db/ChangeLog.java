/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author mla
 */
@Entity
public class ChangeLog extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private ChangeLogType cltype;

    private String msg;

    @Column(length = 4048)
    private String objDescription;

    /**
     * @return the cltype
     */
    public ChangeLogType getCltype() {
        return cltype;
    }

    /**
     * @param cltype the cltype to set
     */
    public void setCltype(ChangeLogType cltype) {
        this.cltype = cltype;
    }

    /**
     * @return the objDescription
     */
    public String getObjDescription() {
        return objDescription;
    }

    /**
     * @param objDescription the objDescription to set
     */
    public void setObjDescription(String objDescription) {
        this.objDescription = objDescription;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

}
