/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.db;

import javax.persistence.*;

/**
 * saved script
 * @author lang
 */
@Entity
public class SavedGroovyScript extends BaseEntity {


    @Column(unique = true)
    private String name;

    private String descr;

    @Lob
    @Column(length = 1000000000)
    private String scriptText;

    private String menuPath;

    @Enumerated(EnumType.STRING)
    private ScriptType scriptType = ScriptType.SCRIPT;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScriptText() {
        return scriptText;
    }

    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public ScriptType getScriptType() {
        return scriptType;
    }

    public void setScriptType(ScriptType scriptType) {
        this.scriptType = scriptType;
    }

    public String getMenuPath() {
        return menuPath;
    }

    public void setMenuPath(String menuPath) {
        this.menuPath = menuPath;
    }
}
