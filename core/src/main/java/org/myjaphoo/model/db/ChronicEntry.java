/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.db;

import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 * Chroniken eintrag.
 * @author mla
 */
@Entity
public class ChronicEntry extends BaseEntity implements Cloneable {

    @Embedded
    private DataView view;

    public ChronicEntry() {
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        ChronicEntry cloned = (ChronicEntry) super.clone();
        if (getView() != null) {
            cloned.setView((DataView) getView().clone());
        }
        return cloned;
    }

    public boolean isContentequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        ChronicEntry other = (ChronicEntry) obj;
        return getView().isContentequals(other.getView());
    }

    /**
     * @return the view
     */
    public DataView getView() {
        if (view == null) {
            setView(new DataView());
        }
        return view;
    }

    /**
     * @param view the view to set
     */
    public void setView(DataView view) {
        this.view = view;
    }

}
