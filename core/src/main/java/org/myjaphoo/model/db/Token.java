/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.db;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mlsoft.structures.TreeParentRelation;
import org.mlsoft.structures.TreeStructure;
import org.myjaphoo.model.cache.ImmutableToken;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author mla
 */
@Entity
public class Token extends UUIDEntity implements Comparable<Token>, TreeParentRelation<Token>, AttributedEntity {

    @Column(unique = true)
    private String name;
    @Enumerated(EnumType.ORDINAL)
    private TokenType tokentype = TokenType.UNBESTIMMT;
    @Column(length = 2024)
    private String description;

    @org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SELECT)
    @ManyToOne(fetch = FetchType.LAZY)
    private Token parent;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "TOKEN_MOVIEENTRY",
        joinColumns = {
            @JoinColumn(name = "TOKENS_ID", referencedColumnName = "ID")},
        inverseJoinColumns = {
            @JoinColumn(name = "MOVIEENTRIES_ID", referencedColumnName = "ID")})
    @org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
    private Set<MovieEntry> movieEntries = new HashSet<MovieEntry>();

    @ElementCollection(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SELECT)
    @Column(name = "attributes", length = 4000)
    private Map<String, String> attributes = new HashMap<>();

    @Override
    public String toString() {
        return name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public String getComment() {
        return description;
    }

    @Override
    public void setComment(String str) {
        this.description = str;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the movieEntries
     */
    public Set<MovieEntry> getMovieEntries() {
        return movieEntries;
    }

    /**
     * @return the parent
     */
    public Token getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Token parent) {
        this.parent = parent;
    }

    @Override
    public int compareTo(Token o) {
        return this.getName().compareTo(o.getName());
    }

    /**
     * @return the tokentype
     */
    public TokenType getTokentype() {
        return tokentype;
    }

    /**
     * @param tokentype the tokentype to set
     */
    public void setTokentype(TokenType tokentype) {
        this.tokentype = tokentype;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * makes a shallow copy of this object.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ImmutableToken toImmutable() {
        return new ImmutableToken(this);
    }

    /**
     * @param movieEntries the movieEntries to set
     */
    public void setMovieEntries(Set<MovieEntry> movieEntries) {
        this.movieEntries = movieEntries;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

}
