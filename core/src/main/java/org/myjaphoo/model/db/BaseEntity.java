package org.myjaphoo.model.db;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Base class for all entites.
 * @author mla
 * @version $Id$
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "table-hilo-generator")
    private Long id;

    /** time when the entry has been created. This value is saved as long in current time millis. It will be
     * generated as a strict unique value (on one system), to make entries be strict ordered or identified by
     * the time creation. This is not directly used anywhere in the system, but could be relevant for extensions
     * to have unique ids over several databases without using more cost expensive uuids. */
    private Long createdMs;

    /** time when the entry has been edited the last time. */
    @Temporal(TemporalType.TIMESTAMP)
    private Date edited;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return createdMs != null ? new Date(createdMs) : null;
    }

    public Long getCreatedMs() {
        return createdMs;
    }

    public void setCreatedMs(Long createdMs) {
        this.createdMs = createdMs;
    }

    public Date getEdited() {
        return edited;
    }

    public void setEdited(Date edited) {
        this.edited = edited;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @PrePersist
    public void initCreateDate() {
        if (updateCreateDats && createdMs == null) {
            createdMs = createOrderedUniqueTimeMillis();
        }
    }

    private static long lastTm = System.currentTimeMillis();

    /**
     * Produces a system wide unique ordered current time millis value.
     * @return
     */
    public static synchronized long createOrderedUniqueTimeMillis() {
        long t = System.currentTimeMillis();
        if (t <= lastTm) {
            t = lastTm + 1;
        }
        lastTm = t;
        return t;
    }

    /**
     * flag to deactivate the update of edit date during jpa updates. Necessary, if external dumps get imported into
     * the system where the edit date should be preserved.
     */
    public static transient boolean updateEditDats = true;

    /**
     * flag to deactivate the update of edit date during jpa updates. Necessary, if external dumps get imported into
     * the system where the edit date should be preserved.
     */
    public static transient boolean updateCreateDats = true;

    @PreUpdate
    public void initEditDate() {
        if (updateEditDats) {
            edited = new Date();
        }
    }
}
