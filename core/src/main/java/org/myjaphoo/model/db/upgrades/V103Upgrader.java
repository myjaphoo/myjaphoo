package org.myjaphoo.model.db.upgrades;

import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.Token;

import javax.persistence.EntityManager;

/**
 * hotfix for 3.5: a root tag has never been created.
 */
public class V103Upgrader implements Upgrader {
    @Override
    public void upgrade(EntityManager em) {
        int count = ((Long) em.createQuery("select count(o) from MetaToken as o").getSingleResult()).intValue();
        if (count == 0) {
            MetaToken roottoken = new MetaToken();
            roottoken.setName("InternRootMetaToken");
            em.persist(roottoken);
        }
    }
}
