/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.groupingtests;

import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

/**
 *
 * @author mla
 */
public class DataTestSets {

    static int dbcounter= 0;

    public static TestSet createTestSet1(String dbname) {
        return createTestSet1(dbname, 1000, 10, 10);
    }

    public static MiniTestSet createMiniTestSet(String dbname) {
        return new MiniTestSet(dbname);
    }

    public static MiniTestSet createMiniTestSet() {
        dbcounter++;
        return new MiniTestSet("miniDbTestSet" + dbcounter);
    }

    public static TestSet createTestSet1(String dbname, int noMovies, int noTokens, int noMetaTokens) {
        TestSet set = new TestSet(dbname);
        for (int i = 0; i < noMovies; i++) {
            MovieEntry entry = set.createEntry(i);

        }

        for (int i = 0; i < noTokens; i++) {
            Token token = set.createToken(i);
        }

        for (int i = 0; i < noMetaTokens; i++) {
            MetaToken mt = set.createMetaToken(i);
        }
        return set;
    }
}
