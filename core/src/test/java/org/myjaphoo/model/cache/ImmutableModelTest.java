package org.myjaphoo.model.cache;

import org.junit.Test;
import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * ImmutableModelTest
 */
public class ImmutableModelTest {

    MovieEntry entry = new MovieEntry();

    Token a = new Token();
    Token b = new Token();

    {
        entry.setId(7L);
        a.setName("a");
        a.setId(0L);
        b.setName("b");
        b.setId(1L);
        b.setParent(a);
    }

    @Test
    public void checkGetParent() {
        ImmutableModel model = new ImmutableModel(Arrays.asList(entry), Arrays.asList(a, b), Collections.emptyList());
        assertNull(model.getTokenTree().getParent(a.toImmutable()));

        assertEquals(a.toImmutable(), model.getTokenTree().getParent(b.toImmutable()));

        System.out.println(model.getTokenTree().getRoot().draw());
        ImmutableToken b = model.getTokenByName("b").get();
        ImmutableToken a = model.getTokenByName("a").get();
        TokenZipper tokenZipper = new TokenZipper(model, b);
        assertEquals(a, tokenZipper.getParentTag());
    }
}