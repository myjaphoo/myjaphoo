/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser;

import junit.framework.TestCase;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbcompare.DatabaseComparison;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;
import org.myjaphoo.model.filterparser.processing.FilterAndGroupingProcessor;
import org.myjaphoo.project.ProjectConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * @author mla
 */
public class ParserLastTest extends TestCase {

    private static ArrayList<MovieEntry> entryList = new ArrayList<MovieEntry>();
    private static int count = 300000;

    MyjaphooCorePrefs prefs = new MyjaphooCorePrefs(new ProjectConfiguration("target/.", "target/."));


    public static ImmutableModel model;

    static {

        MovieEntry testEntry = new MovieEntry();
        testEntry.setId(1L);
        testEntry.setName("bla");
        testEntry.setCanonicalDir(
            "rqwrrzrzrqwezrezrzwezrzwzrwzerzzwerzwzrzwrzzr/rewrqwerwer/EWRWERRur/r243432423434/fnfnfnfnfn/aaa/bbb/ccc");
        testEntry.setFileLength(50000);
        testEntry.setRating(Rating.GOOD);

        ArrayList<Token> toks = new ArrayList<>();

        Token token = new Token();
        token.setId(2L);
        token.setName("mytok");
        token.getMovieEntries().add(testEntry);
        toks.add(token);

        Token t = new Token();
        t.setId(3L);
        t.setName("jonny");
        t.getMovieEntries().add(testEntry);
        toks.add(t);

        t = new Token();
        t.setId(4L);
        t.setName("brian");
        t.getMovieEntries().add(testEntry);
        toks.add(t);

        t = new Token();
        t.setId(5L);
        t.setName("jones");
        t.getMovieEntries().add(testEntry);
        toks.add(t);

        // build list of n times the same entry:
        for (int i = 0; i < count; i++) {
            entryList.add(testEntry);
        }

        model = new ImmutableModel(entryList, toks, Collections.emptyList());
    }

    public void testSimpleLikeExpr() throws ParserException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        AbstractBoolExpression expr = parser.parse("path like 'bb'");

        FilterAndGroupingProcessor.filterEntries(
            prefs,
            model,
            model.getEntryList(),
            new DatabaseComparison(),
            expr,
            AbstractParserTest.nullGrouper
        );

    }

    public void testSimpleOrExpression() throws ParserException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        AbstractBoolExpression expr = parser.parse("size > 60000 or path like 'bla'");

        FilterAndGroupingProcessor.filterEntries(
            prefs,
            model,
            model.getEntryList(),
            new DatabaseComparison(),
            expr,
            AbstractParserTest.nullGrouper
        );

    }

    public void testSimpleTokenLikeExpr() throws ParserException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        AbstractBoolExpression expr = parser.parse("tok like yto");

        FilterAndGroupingProcessor.filterEntries(
            prefs,
            model,
            model.getEntryList(),
            new DatabaseComparison(),
            expr,
            AbstractParserTest.nullGrouper
        );

    }

    public void testliterallistExpr() throws ParserException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        AbstractBoolExpression expr = parser.parse(
            "tok like tttjrjrj|wqerqwerqlr|werqwerqwe|nnadfadf|afadsf|afzzf|asdfz|dsfasdf|jonny");

        FilterAndGroupingProcessor.filterEntries(
            prefs,
            model,
            model.getEntryList(),
            null,
            expr,
            AbstractParserTest.nullGrouper
        );

    }
}
