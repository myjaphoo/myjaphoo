/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser;

/**
 * @author lang
 */
public class ParserWithGroovyTest extends AbstractParserTest {

    public void testGroovy01() throws ParserException {
        assertFalse(tst("groovy entry.name=='bla' && !(entry.path.contains('cc'))"));
    }

    public void testGroovy02() throws ParserException {
        assertTrue(tst("groovy def f(x) { x == 'bla.mpg'}; f(entry.name)"));
    }

    public void testGroovy03() throws ParserException {
        assertTrue(tst("groovy entry.name ==~ /.*bla.*/"));
    }

}
