/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.filterparser;

import junit.framework.TestCase;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.db.TokenType;
import org.myjaphoo.model.dbcompare.DatabaseComparison;
import org.myjaphoo.model.filterparser.expr.AbstractBoolExpression;
import org.myjaphoo.model.filterparser.expr.Expression;
import org.myjaphoo.model.filterparser.expr.JoinedDataRowZipper;
import org.myjaphoo.model.filterparser.processing.FilterAndGroupingProcessor;
import org.myjaphoo.model.filterparser.values.Value;
import org.myjaphoo.model.filterparser.visitors.DefaultExpressionVisitor;
import org.myjaphoo.model.grouping.GroupingExecutionContext;
import org.myjaphoo.model.groupingprocessor.Branch;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;
import org.myjaphoo.project.ProjectConfiguration;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author mla
 */
public abstract class AbstractParserTest extends TestCase {

    MyjaphooCorePrefs prefs = new MyjaphooCorePrefs(new ProjectConfiguration("target/.", "target/."));

    public static MovieEntry testEntry2 = new MovieEntry();
    public static MovieEntry testEntry = new MovieEntry();
    public static Token token = new Token();
    public static Token parentTok = new Token();

    public static ImmutableModel model;

    public static ImmutableModel model2;

    static {
        testEntry2.setId(2L);

        testEntry.setId(1L);
        testEntry.setName("bla.mpg");
        testEntry.setCanonicalDir("aaa/bbb/ccc");
        testEntry.setFileLength(50000);
        testEntry.setRating(Rating.GOOD);

        testEntry.getAttributes().put("attr1", "val1");
        testEntry.getAttributes().put("attr2", "val2");

        token.setId(1L);
        token.setName("mytok");
        token.setTokentype(TokenType.MOVIENAME);
        token.setDescription("hallihallo");
        token.getMovieEntries().add(testEntry);

        token.getAttributes().put("attr1", "val1");
        token.getAttributes().put("attr2", "val2");

        token.setParent(parentTok);

        parentTok.setId(2L);
        parentTok.setName("ptokname");
        parentTok.setDescription("ptokdescrname");

        model = new ImmutableModel(Arrays.asList(testEntry), Arrays.asList(parentTok, token), Collections.emptyList());
        model2 = new ImmutableModel(Arrays.asList(testEntry2), Collections.emptyList(), Collections.emptyList());

    }

    public static final GroupAlgorithm nullGrouper = new GroupAlgorithm() {


        /**
         * Groups a node into the tree.
         *
         * @param root the root of the tree
         * @param row
         */
        @Override
        public List<Branch> findParents(
            Branch root, JoinedDataRowZipper row
        ) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Branch getOrCreateRoot() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Branch findAccordingNode(Branch root, String path) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void postProcess(Branch root) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void preProcess(GroupingExecutionContext context) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getText() {
            return null;
        }

        @Override
        public void pruneByHavingClause(ImmutableModel model, Branch root) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void aggregate(ImmutableModel model, Branch root) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean hasAggregations() {
            throw new UnsupportedOperationException("Not supported yet.");
        }


        @Override
        public boolean needsTagRelation() {
            return false;
        }

        @Override
        public boolean needsMetaTagRelation() {
            return false;
        }
    };

    protected boolean tst(String strexpr) throws ParserException {
        return tst(strexpr, new HashMap<String, Substitution>());
    }

    protected boolean tst(String strexpr, Map<String, Substitution> subs) throws ParserException {
        FilterParser parser = new FilterParser(subs);
        AbstractBoolExpression expr = parser.parse(strexpr);

        // check visitor:
        DefaultExpressionVisitor v = new DefaultExpressionVisitor();
        v.visit(expr);

        return FilterAndGroupingProcessor.filterEntries(prefs,
            model,
            model.getEntryList(),
            new DatabaseComparison(),
            expr,
            nullGrouper
        ).size() > 0;

        //return expr.evaluate(new ExecutionContext(), testEntry).asBool();
    }

    protected Value evalConstToValue(String strexpr) throws ParserException, IOException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        Expression expr = parser.parseExpression(strexpr);
        Value value = expr.evaluate(new ExecutionContext(model, prefs.createFileTypes()), null);
        return value;
    }

    protected boolean tst2(String strexpr) throws ParserException {
        FilterParser parser = new FilterParser(new HashMap<String, Substitution>());
        AbstractBoolExpression expr = parser.parse(strexpr);

        return FilterAndGroupingProcessor.filterEntries(prefs,
            model2,
            model2.getEntryList(),
            new DatabaseComparison(),
            expr,
            nullGrouper
        ).size() > 0;
        //return expr.evaluate(new ExecutionContext(), testEntry2).asBool();
    }

    protected void tstTrue(String strexpr) throws ParserException {
        assertTrue(tst(strexpr));
        // meta tests:
        // a and !a = false
        // a and a = true
        // !(a or !a) = false
        String a1 = "(" + strexpr + ") and not (" + strexpr + ")";
        assertFalse(tst(a1));
        String a2 = "(" + strexpr + ") and (" + strexpr + ")";
        assertTrue(tst(a2));
        String a3 = "not((" + strexpr + ") or not(" + strexpr + "))";
        assertFalse(tst(a3));
    }
}
