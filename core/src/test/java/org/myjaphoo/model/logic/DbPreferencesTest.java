package org.myjaphoo.model.logic;

import junit.framework.TestCase;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.model.db.PrefsStringVal;
import org.myjaphoo.project.Project;

/**
 * @author mla
 * @version $Id$
 *          To change this template use File | Settings | File Templates.
 */
public class DbPreferencesTest extends TestCase{

    public void testDao() {
        Project project = TestSet.createTestProject("dbprefstest");
        PreferencesDao dao = new PreferencesDao(project.connection);
        
        PrefsStringVal pf = new PrefsStringVal();

        String myId = "test" + System.currentTimeMillis();
        pf.setId(myId);
        pf.setStringValue("hello");
        dao.create(pf);

        PrefsStringVal prefVal = dao.find(myId, PrefsStringVal.class);
    }

}
