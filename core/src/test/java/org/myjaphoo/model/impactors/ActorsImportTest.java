/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.impactors;

import junit.framework.TestCase;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseDriver;
import org.myjaphoo.model.logic.imp.TextDelegator;
import org.myjaphoo.model.logic.impactors.ImportMsg;
import org.myjaphoo.model.logic.impactors.ImportQueue;
import org.myjaphoo.project.Project;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author mla
 */
public class ActorsImportTest extends TestCase {

    public static final int NUMFILES = 10;

    private Project createTestProject() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:actorsImportTest");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        return new Project(".", dbconnection);
    }

    public void testImport() throws InterruptedException, URISyntaxException, MalformedURLException {
        Project project = createTestProject();

        ImportQueue impQueue = ImportQueue.createImportQueue(project, new TextDelegator(project));
        Collection<File> testFiles = new ArrayList<File>();

        File file = new File(ActorsImportTest.class.getResource("/testtextfiles/textfile.txt").toURI().toURL().getFile());
        for (int i = 0; i < NUMFILES; i++) {
            testFiles.add(file);
        }
        impQueue.send(new ImportMsg.ImportFilesMsg(testFiles));

        impQueue.waitTillAllFinished();

        assertEquals(NUMFILES, impQueue.getResults().size());
    }
}
