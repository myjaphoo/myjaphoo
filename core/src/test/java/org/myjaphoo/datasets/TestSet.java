/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.datasets;

import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseDriver;
import org.myjaphoo.model.logic.MetaTokenJpaController;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.TokenJpaController;
import org.myjaphoo.project.Project;

import java.util.ArrayList;

/**
 * Container für ein Test set an Daten. Inclusive Helper methoden zum erstellen
 * der Daten.
 *
 * @author mla
 */
public class TestSet {

    public ArrayList<MovieEntry> entries = new ArrayList<MovieEntry>();
    public ArrayList<Token> tokens = new ArrayList<Token>();
    public ArrayList<MetaToken> metaTokens = new ArrayList<MetaToken>();

    public final Project project;

    public MovieEntryJpaController movieJpa;
    public TokenJpaController tokenJpa;

    public MetaTokenJpaController mtJpa;

    public static Project createTestProject(String dbname) {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:" + dbname);

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        return new Project(".", dbconnection);
    }

    public TestSet(String dbName) {
        project = createTestProject(dbName);
        tokenJpa = new TokenJpaController(project.connection);
        mtJpa = new MetaTokenJpaController(project.connection);
        movieJpa = new MovieEntryJpaController(project.connection);
    }

    public Token createToken(int i) {
        return createToken("token" + i);
    }

    public Token createToken(String name) {
        Token token = new Token();
        token.setName(name);
        tokens.add(token);
        tokenJpa.create(token, tokenJpa.findRootToken().getId());
        return token;
    }

    public MetaToken createMetaToken(int i) {
        return createMetaToken("metatoken" + i);
    }

    public MetaToken createMetaToken(String name) {
        MetaToken token = new MetaToken();
        token.setName(name);
        metaTokens.add(token);
        mtJpa.create(token, mtJpa.findRootToken().getId());
        return token;
    }

    public void ass(MovieEntry e1, Token t1) {
        project.connection.commit(em -> {
            t1.getMovieEntries().add(e1);
        });
    }

    public MovieEntry createEntry(int i) {
        return createEntry(createName(i), createCanonDir(i), i, i, createRating(i));
    }

    public MovieEntry createEntry(String name, String dir, long filelen, long chksum, Rating rating) {
        MovieEntry entry = new MovieEntry();
        entry.setName(name);
        entry.setCanonicalDir(dir);
        entry.setFileLength(filelen);
        entry.setChecksumCRC32(chksum);
        entry.setRating(rating);
        entries.add(entry);
        project.connection.commit(em -> em.persist(entry));
        return entry;
    }

    private String createName(int i) {
        return "name";
    }

    private String createCanonDir(int i) {

        return "aaa/bbb/ccc";
    }

    private Rating createRating(int i) {
        return Rating.GOOD;
    }

    public void ass(MetaToken mt1, Token t1) {
        project.connection.commit(em -> {
            mt1.getAssignedTokens().add(t1);
        });
    }
}
