package org.myjaphoo.datasets;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;

import static org.junit.Assert.*;

/**
 * a integration test, which tests the creation of the immutable model based on a entity model.
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class ImmutableModelTest {

    @Test
    public void verifyConstructedImmutableModel() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        ImmutableModel model = set.project.cacheActor.getImmutableModel();

        // the miniset has 5 entries, and 3 tokens (+parent) and 3 metatokens (+parent)
        assertEquals(5, model.getEntryList().size());
        assertEquals(4, model.getTokenTree().getValues().size());
        assertEquals(4, model.getMetaTokenTree().getValues().size());

        // token 2 has 2 assigned metatokens, t2 has only one. check:
        ImmutableToken it1 = model.getTokenByName(set.t1.getName()).get();
        assertEquals(2, it1.getAssignedMetaTokens(model).size());
        ImmutableToken it2 = model.getTokenByName(set.t2.getName()).get();
        assertEquals(1, it2.getAssignedMetaTokens(model).size());
        // check that t1 is related to mt1 & mt2. note: it checks equality, the objects are different
        Assertions.assertThat(it1.getAssignedMetaTokens(model)).contains(
            set.mt1.toImmutable(),
            set.mt2.toImmutable()
        );
    }

}