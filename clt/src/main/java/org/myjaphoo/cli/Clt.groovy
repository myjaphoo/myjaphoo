package org.myjaphoo.cli

import org.myjaphoo.model.logic.imp.ImportTypes
import org.myjaphoo.model.util.UserDirectory

/**
 * Command line Tool.
 *
 * @author mla
 * @version $Id$
 */
public class Clt {


    public static void main(String[] args) {
        def cli = new CliBuilder(
                usage: 'mj -i <folder> | -w <folder> | d <folder>',
                header: '\nAvailable options (use -h for help):\n',
                footer: '\nInformation provided via above options is used to generate printed string.\n')

        cli.with
                {
                    h(longOpt: 'help', 'Usage Information', required: false)
                    i(longOpt: 'import', 'Import Files', args: 1, required: false)
                    t(longOpt: 'types', 'Set file types to process', args: 1, required: false)
                    p(longOpt: 'project', 'Set project directory to use for the command; if not set, the default project will be taken', args: 1, required: false)
                    w(longOpt: 'wminfo', 'Searches for Files and writes wminfo files', args: 1, required: false)
                    d(longOpt: 'deduplicate', 'Deduplicates files identifed by wminfo files and deletes duplicates', args: 1, required: false)
                    o(longOpt: 'options', 'Print Options', required: false)
                    f(longOpt: 'force', 'Force to delete duplicates', required: false)
                }
        def opt = cli.parse(args)

        if (!opt) return
        if (opt.h) cli.usage()

        def importFolder = opt.i
        def typeDescr = deriveTypeDescr(opt.t)
        def wiminfoFolder = opt.w
        def deduplicateFolder = opt.d
        def printOptions = opt.o
        def projectDir = opt.p ? opt.p : UserDirectory.getDirectory();

        def boolean force = false || opt.f;

        if (printOptions) println cli.options
        else if (importFolder) doImport(projectDir, importFolder, typeDescr)
        else if (wiminfoFolder) doWmInfo(wiminfoFolder, typeDescr)
        else if (deduplicateFolder) doDeduplication(deduplicateFolder, force, typeDescr)
        else {
            cli.usage()
        }
    }

    static String deriveTypeDescr(Object optionalTypeDescr) {
        if (optionalTypeDescr) {
            return optionalTypeDescr;
        } else {
            return "Movies";
        }
    }

    static void doImport(String projectDir, String dir, String typeDescr) {
        println("project $projectDir")
        println("starting import from $dir with $typeDescr")
        new Importer().importFiles(projectDir, dir, typeDescr);
    }

    static void doWmInfo(String dir, String typeDescr) {
        println("starting wminfo update from $dir with $typeDescr")
        new WmInfoCreator().createUpdateWmFiles(dir, typeDescr);
    }

    static void doDeduplication(String dir, boolean force, String typeDescr) {
        new Deduplicator().deduplicate(dir, force, typeDescr);
    }

}
