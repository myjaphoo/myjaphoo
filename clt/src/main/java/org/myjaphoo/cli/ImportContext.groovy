package org.myjaphoo.cli

import groovy.transform.TypeChecked
import org.myjaphoo.model.logic.imp.ImportDelegator
import org.myjaphoo.model.logic.imp.ImportTypes
import org.myjaphoo.model.logic.impbkrnd.BackgroundImport
import org.myjaphoo.model.logic.impbkrnd.BackgroundImportListener
import org.myjaphoo.project.Project

/**
 * Helper to bundle all objects required for imports.
 *
 */
@TypeChecked
class ImportContext {
    Project p
    BackgroundImportListener bil = new ClTBackgroundImportListener();
    BackgroundImport backgroundImport

    ImportContext(Project p, String typeDescr, String dir) {
        ImportDelegator delegator = createDelegator(p, typeDescr);

        backgroundImport = new BackgroundImport(p, bil, delegator, new File(dir));
    }

    ImportDelegator createDelegator(Project project, String typeDescr) {
        ImportTypes.valueOf(typeDescr).createImportDelegator(project);
    }
}
