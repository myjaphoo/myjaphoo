package org.myjaphoo.cli

import groovy.transform.TypeChecked
import org.myjaphoo.project.Project
import org.myjaphoo.project.ProjectManagement

/**
 * Importer
 *
 * @author mla
 * @version $Id$
 */
@TypeChecked
public class Importer {

    void importFiles(String projectDir, String dir, String typeDescr) {
        Project project = ProjectManagement.openProject(projectDir);

        ImportContext impCtx = new ImportContext(project, typeDescr, dir)

        impCtx.backgroundImport.doImport();

    }

}
