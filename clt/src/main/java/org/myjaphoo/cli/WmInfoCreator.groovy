package org.myjaphoo.cli

import groovy.transform.TypeChecked
import org.myjaphoo.datasets.TestSet
import org.myjaphoo.model.logic.imp.ImportDelegator
import org.myjaphoo.model.logic.impactors.ImportTokenActor
import org.myjaphoo.model.logic.impactors.ImportWorkerActorFactory
import org.myjaphoo.model.logic.impactors.ImportingWorkerActor
import org.myjaphoo.model.logic.impbkrnd.FilterOutFilesWithWmInfoFiles
import org.myjaphoo.model.logic.impbkrnd.ImportInWmInfoFilesWorkerActor
import org.myjaphoo.project.Project

/**
 * WmInfoCreator 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class WmInfoCreator {
    void createUpdateWmFiles(String dir, String typeDescr) {

        // create pseudo project with in memory db:
        Project p = TestSet.createTestProject("wminfoupdate");

        ImportContext impCtx = new ImportContext(p, typeDescr, dir)

        impCtx.backgroundImport.setImportPreFilter(new FilterOutFilesWithWmInfoFiles());
        impCtx.backgroundImport.setImportWorkerActorFactory(new ImportWorkerActorFactory() {
            @Override
            ImportingWorkerActor createWorker(Project project, ImportTokenActor importTokenActor, ImportDelegator importDelegator) {
                return new ImportInWmInfoFilesWorkerActor(project, importTokenActor, importDelegator)
            }
        })

        impCtx.backgroundImport.doImport();
    }

}
