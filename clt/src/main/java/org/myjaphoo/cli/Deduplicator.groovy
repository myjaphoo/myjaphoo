package org.myjaphoo.cli

import groovy.transform.TypeChecked
import org.myjaphoo.datasets.TestSet
import org.myjaphoo.model.cache.ImmutableModel
import org.myjaphoo.model.cache.ImmutableMovieEntry
import org.myjaphoo.project.Project

/**
 * Deduplicator.
 *
 * @author mla
 * @version $Id$
 */
@TypeChecked
public class Deduplicator {

    boolean force

    void deduplicate(String dir, boolean force, String typeDescr) {
        this.force = force;
        // create pseudo project with in memory db:
        Project p = TestSet.createTestProject("wminfoupdate");
        ImportContext impCtx = new ImportContext(p, typeDescr, dir)

        impCtx.backgroundImport.doImport();

        processDuplicates(p.cacheActor.immutableModel)
    }

    def processDuplicates(ImmutableModel model) {
        def dMap = model.dupHashMap
        dMap.getChsumMap().each { k, v ->
            if (v.size() > 1) {
                processDuplicateList(v)
            }
        }

    }

    def processDuplicateList(List<ImmutableMovieEntry> dupList) {
        dupList = dupList.sort { a, b -> a.canonicalPath <=> b.canonicalPath }
        println("# duplicates:")
        int count = 0;
        dupList.each { m ->
            if (count > 0) {
                println("# -> duplicate $m.canonicalPath")
                println("rm $m.canonicalPath")
                if (force) {
                    println("deleting $m.canonicalPath")
                    new File(m.canonicalPath).delete()
                }
            } else {
                println("# $m.canonicalPath")
            }
            count++;
        }
    }

}
