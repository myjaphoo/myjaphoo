package org.myjaphoo.cli

import groovy.transform.TypeChecked
import org.myjaphoo.model.logic.impbkrnd.BackgroundImportListener

/**
 * ClTBackgroundImportListener 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class ClTBackgroundImportListener implements BackgroundImportListener {

    @Override
    void message(String message) {
        println(message)
    }

    @Override
    void stopped() {
        println("stopped")
    }

    @Override
    void progressUpdate(int perc) {
        println("$perc%")
    }

    @Override
    void updatefileAndRemainingTime(String filePath, String formatDurationWords) {
        println("$filePath $formatDurationWords")
    }
}
