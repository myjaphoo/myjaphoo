package org.myjaphoo.cli;

import io.vavr.collection.List;
import org.junit.Test;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.project.Project;
import org.myjaphoo.project.ProjectDatabaseType;
import org.myjaphoo.project.ProjectManagement;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.stream.Collectors;

import static org.apache.commons.io.FileUtils.cleanDirectory;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * CltTest
 *
 * @author mla
 * @version $Id$
 */
public class ImpTest {

    @Test
    public void testImport() throws URISyntaxException, IOException {
        File dir = new File(ImpTest.class.getResource("/testcaserunressources/pics").toURI());

        File projectDir = new File("target/testImport");
        projectDir.mkdirs();
        cleanDirectory(projectDir);
        ProjectManagement.createProjectFiles(
            projectDir.getAbsolutePath(),
            ProjectDatabaseType.H2,
            "test",
            new File(projectDir.getAbsolutePath(), "testdb").getAbsolutePath()
        );

        String cmd = "-i " + dir.getAbsolutePath() + " -t Pictures" + " -p " + "target/testImport" + "";

        Clt.main(cmd.split(" "));

        Project compareProject = ProjectManagement.openProject(projectDir.getAbsolutePath());
        List<ImmutableMovieEntry> entries = compareProject.cacheActor.getImmutableModel().getEntryList();
        java.util.List<String> names = entries.toJavaList().stream().map(e -> e.getName()).collect(Collectors.toList());
        assertThat(names).hasSize(21);
        assertThat(names).contains("imagesm001.jpg", "imagesm002.jpg");

    }

}