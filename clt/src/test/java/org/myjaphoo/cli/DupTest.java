package org.myjaphoo.cli;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.apache.commons.io.FileUtils.cleanDirectory;
import static org.apache.commons.io.FileUtils.copyDirectory;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * CltTest
 *
 * @author mla
 * @version $Id$
 */
public class DupTest {

    @Test
    public void testImport() throws URISyntaxException, IOException {
        File dir = new File(DupTest.class.getResource("/duptest/").toURI());
        File tstDir = new File("target/testDup");
        tstDir.mkdirs();
        cleanDirectory(tstDir);
        copyDirectory(dir, tstDir);

        String cmd = "-d " + tstDir.getAbsolutePath() + " -t Pictures -f";

        Clt.main(cmd.split(" "));

        List<String> files = asList(tstDir.list());
        assertThat(files).hasSize(3);
        assertThat(files).contains("imagesm001.jpg", "imagesm002.jpg", "imagesm003.jpg");

    }

}