package org.myjaphoo.cli;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.io.FileUtils.cleanDirectory;
import static org.apache.commons.io.FileUtils.copyDirectory;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * CltTest
 *
 * @author mla
 * @version $Id$
 */
public class WmUpdateTest {

    @Test
    public void testImport() throws URISyntaxException, IOException {
        File dir = new File(WmUpdateTest.class.getResource("/testcaserunressources/pics").toURI());
        File tstDir = new File("target/testWmUpdate");
        tstDir.mkdirs();
        cleanDirectory(tstDir);
        copyDirectory(dir, tstDir);

        String cmd = "-w " + tstDir.getAbsolutePath() + " -t Pictures";

        Clt.main(cmd.split(" "));

        List<String> files = asList(tstDir.list());
        Set<String> pics = files.stream().filter(f -> f.endsWith(".jpg")).collect(toSet());
        Set<String> wms = files.stream().filter(f -> f.endsWith(".wminfo")).collect(toSet());

        Set<String> picsNames = pics.stream().map(f -> f.replace(".jpg", "")).collect(toSet());
        Set<String> wmsNames = wms.stream().map(f -> f.replace(".jpg.wminfo", "")).collect(toSet());

        assertThat(pics).hasSameSizeAs(wms);

        assertThat(picsNames).isEqualTo(wmsNames);
    }

}