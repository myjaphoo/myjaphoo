package plugin1;

import groovy.transform.BaseScript
import org.myjaphoo.gui.scripting.MJScriptBaseClass

@BaseScript MJScriptBaseClass baseClass

defCommonAction("CommandFromPlugin1") {
    name = "CommandFromPlugin1";
    descr = "CommandFromPlugin1"
    action = { controller ->

        def c = new ClassOfPlugin1();

        println c.printIt()
    }
}