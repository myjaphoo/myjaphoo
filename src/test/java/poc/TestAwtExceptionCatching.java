package poc;

import org.mlsoft.common.ExceptionHandler;

import javax.swing.*;

public class TestAwtExceptionCatching {


    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler",
                ExceptionHandler.class.getName());

        // cause an exception on the EDT
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ((Object) null).toString();
            }
        });

        // cause an exception off the EDT
        ((Object) null).toString();
    }
}