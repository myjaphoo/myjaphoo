/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mlsoft.eventbus;

import junit.framework.TestCase;

/**
 * @author lang
 */
public class EventBusSubscriberFilterTest extends TestCase {


    public void testFilter() throws InterruptedException {
        BasicEventBus bus = new BasicEventBus();

        EventBusChangeRecorder recorder = new EventBusChangeRecorder();
        bus.register(recorder, new SubscriberFilter() {
            @Override
            public boolean accept(Object event) {
                return event instanceof MyEvent && ((MyEvent) event).num < 10;
            }
        });

        bus.post(new MyEvent(5));
        Thread.sleep(1000);
        assertTrue(recorder.received);

        recorder.received = false;

        bus.post(new MyEvent(15));
        Thread.sleep(1000);
        assertFalse(recorder.received);

        bus.post(new MyEvent(7));
        Thread.sleep(1000);
        assertTrue(recorder.received);
    }


    static class MyEvent {
        int num;

        public MyEvent(int num) {
            this.num = num;
        }
    }


    static class EventBusChangeRecorder {

        public boolean received = false;


        @Subscribe
        public void recordCustomerChange(MyEvent e) {
            System.out.println("change received");
            received = true;
        }
    }

}
