package org.mlsoft.swing.listtreetableview;

import org.mlsoft.swing.annotation.ContextMenuAction;
import org.mlsoft.swing.annotation.ToolbarAction;
import org.mlsoft.swing.jxtree.MappedTreeTableModel;

import javax.swing.*;
import java.util.ArrayList;

import static org.mlsoft.swing.jtable.ColDescr.col;

/**
 */
public class ListTreeTableViewExample {


    public static class Node {
        public Node parent;

        public ArrayList<Node> children = new ArrayList<Node>();

        public String name;

        public boolean flag;

        public int val;

        public Node(String name, int val, boolean flag, Node parent) {
            this.name = name;
            this.val = val;
            this.flag = flag;
            this.parent = parent;
            if (parent != null) {
                parent.children.add(this);
            }
        }

        public Node getParent() {
            return parent;
        }

        public ArrayList<Node> getChildren() {
            return children;
        }

        public String getName() {
            return name;
        }

        public boolean isFlag() {
            return flag;
        }

        public int getVal() {
            return val;
        }

        @Override
        public String toString() {
            return "element " + name;
        }
    }


    public static void main(String[] args) {
        new ListTreeTableViewExample().start();
    }

    private void start() {
        JDialog d = new JDialog();
        JXListTreeTableComponent tt = new JXListTreeTableComponent();
        tt.setConfiguration(this);
        d.add(tt);

        Node root = buildTree();

        MappedTreeTableModel<Node> model = new MappedTreeTableModel<>(
            root,
            Node.class,
            "parent",
            "children",
            col("name", "Name"),
            col("val"),
            col("flag", "flag", true)
        );
        tt.setTreeTableModel(model);

        d.pack();
        d.setVisible(true);
    }


    @ContextMenuAction(name = "Context Action")
    public void openNewView(Node attr) {
        System.out.println(attr);
    }

    @ToolbarAction(name = "Save", order = 5, contextRelevant = false)
    public void saveAction() {
        System.out.println("save action");
    }

    public void onElementSelected(Node n) {
        System.out.println("selected " + n);
    }

    @ToolbarAction(name = "New")
    @ContextMenuAction(name = "new Attribute")
    public void newButtonAction(Node attr) {
        System.out.println("new for " + attr);
    }

    private Node buildTree() {
        Node root = new Node("A", 10, false, null);
        fill(root, 1);
        return root;
    }

    private void fill(Node root, int level) {
        for (int i = 0; i < 5; i++) {
            String name = root.getName() + level + Integer.toString(i);
            if (level < 5) {
                Node child = new Node(name, level, true, root);
                fill(child, level + 1);
            } else {
                Node child = new Node("Leaf A" + name, level, true, root);
            }
        }
    }

}
