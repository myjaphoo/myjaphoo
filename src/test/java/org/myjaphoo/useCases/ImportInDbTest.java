/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.useCases;

import junit.framework.TestCase;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseDriver;
import org.myjaphoo.model.logic.MovieImport;
import org.myjaphoo.model.logic.imp.PicDelegator;
import org.myjaphoo.model.logic.impactors.ImportMsg;
import org.myjaphoo.model.logic.impactors.ImportQueue;
import org.myjaphoo.project.Project;

import java.io.File;
import java.util.List;

/**
 * @author mla
 */
public class ImportInDbTest extends TestCase {

    private Project createTestProject() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:importInDbTest");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        return new Project(".", dbconnection);
    }

    public void testImport() throws InterruptedException {

        String picDir = System.getProperty("test.pics");
        if (picDir == null) {
            // run within the IDE:
            picDir = "testcaserunressources/pics";
        }

        Project project = createTestProject();

        final PicDelegator picDelegator = new PicDelegator(project);
        MovieImport i = new MovieImport(project);
        List<File> files = i.scanFiles(picDir, picDelegator);

        ImportQueue queue = ImportQueue.createImportQueue(project, picDelegator);
        queue.send(new ImportMsg.ImportFilesMsg(files));

        queue.waitTillAllFinished();
    }

}
