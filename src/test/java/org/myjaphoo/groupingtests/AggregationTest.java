package org.myjaphoo.groupingtests;

import junit.framework.TestCase;
import org.myjaphoo.CreatedTreeModelResult;
import org.myjaphoo.MovieFilterController;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;

import java.util.List;

/**
 * AggregationTest
 */
public class AggregationTest extends TestCase {

    public void testBasicAggregation() {
        TestSet set = DataTestSets.createTestSet1("basicAggregation");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();
        chronic.getView().setUserDefinedStruct("(name), (count(len)), (sum(checksum)), (sum(len))");

        MovieFilterController movieFilterController = new MovieFilterController(mainController);
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);

        CreatedTreeModelResult tmr = MovieFilterController.group(mainController, grouper, chronic.getView(), false);

        MovieStructureNode root = tmr.root;

        StructureChecker ch = new StructureChecker(root);
        ch.assertSorting();
        ch.assertPathExists(root, "name");
        MovieStructureNode nameNode = (MovieStructureNode) ch.getNode(root, "name");
        double expSum = 0;
        for (int i = 0; i < 1000; i++) {
            expSum += i;
        }
        assertEquals(1000.0, nameNode.getAggregatedValue("count(len)"));
        assertEquals(expSum, nameNode.getAggregatedValue("sum(checksum)"));
    }


    public void testBasicHavingClause() {
        TestSet set = DataTestSets.createTestSet1("basicHavingClause");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();
        chronic.getView().setUserDefinedStruct(
            "(name), (count(len)), (sum(checksum)), (sum(len)) having count(len) < 900");

        MovieFilterController movieFilterController = new MovieFilterController(mainController);
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);

        CreatedTreeModelResult tmr = MovieFilterController.group(mainController, grouper, chronic.getView(), false);

        MovieStructureNode root = tmr.root;

        StructureChecker ch = new StructureChecker(root);
        assertEquals(0, root.getChildCount());
    }
}
