/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.groupingtests;


import junit.framework.AssertionFailedError;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Checker für AbstractMovieTreenodes.
 *
 * @author mla
 */
public class StructureChecker {

    private MovieStructureNode root;

    public StructureChecker(MovieStructureNode root) {
        this.root = root;
    }

    public AbstractMovieTreeNode getNode(AbstractMovieTreeNode root, String... path) {
        List<String> pathlist = new ArrayList<String>(Arrays.asList(path));
        return getNode(root, pathlist);
    }

    private AbstractMovieTreeNode getNode(AbstractMovieTreeNode root, List<String> pathlist) {
        AbstractMovieTreeNode child = searchChild(root, pathlist.get(0));
        if (child == null) {
            return null;
        }
        pathlist.remove(0);
        if (pathlist.size() > 0) {
            return getNode(child, pathlist);
        } else {
            return child;
        }
    }

    private AbstractMovieTreeNode searchChild(AbstractMovieTreeNode node, String name) {
        for (Object child : node.getChildren()) {
            AbstractMovieTreeNode chn = (AbstractMovieTreeNode) child;
            if (chn.getName().equals(name)) {
                return chn;
            }
        }
        return null;
    }

    void assertPathExists(AbstractMovieTreeNode root, String... path) {
        AbstractMovieTreeNode child = getNode(root, path);
        if (child == null) {
            throw new AssertionFailedError("expected path not found!" + path.toString());
        }
    }

    int getNumOfLeafs() {
        int count = countLeafs(root);
        return count;
    }

    private int countLeafs(AbstractMovieTreeNode node) {
        if (node.getChildCount() == 0) {
            return 1; // this is a leaf;
        } else {
            int count = 0;
            for (Object child : node.getChildren()) {
                count += countLeafs((AbstractMovieTreeNode) child);
            }
            return count;
        }
    }

    public void assertSorting() {
        assertSorting(root);
    }

    private void assertSorting(AbstractMovieTreeNode node) {
        Collection<AbstractMovieTreeNode> children = node.getChildren();
        String prevName = null;
        for (AbstractMovieTreeNode child : children) {
            String currName = child.getName();
            if (prevName != null) {
                if (prevName.compareTo(currName) > 0) {
                    throw new AssertionFailedError("tree is not sorted by name! " + prevName + " > " + currName);
                }
            }
            prevName = currName;
            assertSorting(child);
        }
    }
}
