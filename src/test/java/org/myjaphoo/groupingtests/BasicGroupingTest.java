/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.groupingtests;

import junit.framework.TestCase;
import org.myjaphoo.MovieFilterController;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.StructureType;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;

import java.util.List;

/**
 * @author mla
 */
public class BasicGroupingTest extends TestCase {

    public void testBasicGroupingByDirectory() {
        TestSet set = DataTestSets.createTestSet1("basicGroupingByDir");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();
        chronic.getView().setUserDefinedStruct(StructureType.DIRECTORY.buildUserDefinedEquivalentExpr());

        MovieFilterController movieFilterController = new MovieFilterController(mainController);
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);
        MovieStructureNode root = MovieFilterController.group(mainController, grouper, chronic.getView(), false).root;

        // es gibt nur ein dir in den testdaten:
        assertEquals("muss ein dir sein!", 1, root.getChildCount());

        StructureChecker ch = new StructureChecker(root);
        ch.assertSorting();
        ch.assertPathExists(root, "aaa", "bbb", "ccc");
        ch.assertPathExists(root, "aaa", "bbb", "ccc", "name");

        AbstractMovieTreeNode child = ch.getNode(root, "aaa", "bbb", "ccc");
        assertEquals("im dir müssen 1000 hängen!", 1000, child.getChildCount());

        // unterhalb dieses dirs müssen dann alle 1000 movies hängen:
    }

    public void testBasicGroupingByKeyword() {
        TestSet set = DataTestSets.createTestSet1("basicGroupingByKeyword");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();
        chronic.getView().setUserDefinedStruct(StructureType.AUTO_KEYWORD_VERY_STRONG_GROUPER.buildUserDefinedEquivalentExpr());

        MovieFilterController movieFilterController = new MovieFilterController(mainController);
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);
        MovieStructureNode root = MovieFilterController.group(mainController, grouper, chronic.getView(), false).root;

        StructureChecker ch = new StructureChecker(root);
        ch.assertSorting();
        ch.assertPathExists(root, "aaa");
        ch.assertPathExists(root, "bbb");
        ch.assertPathExists(root, "ccc");
        ch.assertPathExists(root, "aaa", "name");

    }
}
