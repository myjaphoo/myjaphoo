/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.groupingtests;

import junit.framework.TestCase;
import org.myjaphoo.MovieFilterController;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.StructureType;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;

import java.util.List;

/**
 * Testfall fürs Profilen (Zeitmessen) von Gruppierungen.
 *
 * @author mla
 */
public class ProfileDirGroupingTest extends TestCase {

    public void testNoGroupingFiltersMovies() {
        TestSet set = DataTestSets.createTestSet1("noGroupingFiltersMovies", 10000, 100, 200);

        MainApplicationController mainController = new MainApplicationController(set.project);
        MovieFilterController movieFilterController = new MovieFilterController(mainController);

        ChronicEntry chronic = new ChronicEntry();
        chronic.getView().setUserDefinedStruct(StructureType.DIRECTORY.buildUserDefinedEquivalentExpr());
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);
        MovieFilterController.group(mainController, grouper, chronic.getView(), false);

    }

}
