/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.groupingtests;

import junit.framework.TestCase;
import org.myjaphoo.MovieFilterController;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.StructureType;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.grouping.GroupingDim;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;

import java.util.Arrays;
import java.util.List;

/**
 * @author mla
 */
public class BasicGroupingTest1 extends TestCase {


    public void testNoGroupingFiltersMovies() {
        TestSet set = DataTestSets.createTestSet1("noGroupingFilterMovies");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();
        for (StructureType type : StructureType.values()) {
            if (type != StructureType.DUPLETTES && type != StructureType.DUPLETTES_IN_DIRS) {

                chronic.getView().setUserDefinedStruct(type.buildUserDefinedEquivalentExpr());
                checkGroupingVallidity(mainController, chronic, type.toString());
            }
        }
    }

    public void testNoGroupingFiltersMoviesForUserDefinedStruct() {
        TestSet set = DataTestSets.createTestSet1("noGroupingFilterMoviesForUserDefinedStruct");

        MainApplicationController mainController = new MainApplicationController(set.project);

        for (GroupingDim dim : GroupingDim.values()) {
            for (GroupingDim dim2 : GroupingDim.values()) {
                if (dim != GroupingDim.Duplicates && dim != GroupingDim.DuplicatesWithDirs
                    && dim != GroupingDim.Bookmark
                    && dim2 != GroupingDim.Duplicates && dim2 != GroupingDim.DuplicatesWithDirs
                    && dim2 != GroupingDim.Bookmark) {
                    //chronic.setUserDefinedStructureActivated(true);
                    ChronicEntry chronic = new ChronicEntry();
                    chronic.getView().setUserDefinedStructure(Arrays.asList(dim, dim2));
                    String descr = dim.toString() + "," + dim2.toString();
                    checkGroupingVallidity(mainController, chronic, descr);
                }
            }
        }
    }

    public void testTokenMetaTokenGrouping() {
        TestSet set = DataTestSets.createTestSet1("tokenMetaTokenGrouping");

        MainApplicationController mainController = new MainApplicationController(set.project);

        ChronicEntry chronic = new ChronicEntry();

        GroupingDim dim = GroupingDim.Token;
        GroupingDim dim2 = GroupingDim.Metatoken;
        chronic.getView().setUserDefinedStructureActivated(true);
        chronic.getView().setUserDefinedStructure(Arrays.asList(dim, dim2));
        String descr = dim.toString() + "," + dim2.toString();
        checkGroupingVallidity(mainController, chronic, descr);


    }

    private void checkGroupingVallidity(
        MainApplicationController mainController, ChronicEntry chronic, String descr
    ) {
        MovieFilterController movieFilterController = new MovieFilterController(mainController);
        List<? extends GroupAlgorithm> grouper = movieFilterController.createGroupingAlgorithm(chronic);
        MovieStructureNode root = MovieFilterController.group(mainController, grouper, chronic.getView(), false).root;

        StructureChecker ch = new StructureChecker(root);
        ch.assertSorting();
        // bei jeder gruppierung müssen mind. die anzahl der movies rauskommen
        // == es darf kein movie allein durch die gruppierung rausgefiltert werden!
        // der minitest hat 5 movies:
        int leafs = ch.getNumOfLeafs();
        assertTrue("Struktur " + descr + " filtert Movies raus!!, anzahl leafs=" + leafs, 5 <= leafs);
    }
}
