package org.myjaphoo.gui.propertiespanel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.movieprops.AttributeHeaderNode;
import org.myjaphoo.gui.movieprops.AttributeNode;
import org.myjaphoo.gui.movieprops.PropertyNode;
import org.myjaphoo.gui.movieprops.PropertyStructureBuilder;
import org.myjaphoo.gui.movieprops.UpdatePropertyPanelInfoEvent;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.events.MoviesChangedEvent;
import org.myjaphoo.model.cache.zipper.Ref;

import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 */

@RunWith(MockitoJUnitRunner.class)
public class PropertiesSaveTest {

    @Mock
    MyjaphooView view;

    @Test
    public void create() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        PropertyStructureBuilder builder = new PropertyStructureBuilder(controller);
        ImmutableModel model = set.project.cacheActor.getImmutableModel();
        Ref ref = model.entryRef(model.getEntryList().last());
        UpdatePropertyPanelInfoEvent event = new UpdatePropertyPanelInfoEvent(ref);
        PropertyNode props = builder.buildNodes(event);

        PropertyNode aNode = props.getChildren().get(0);

        AttributeHeaderNode ahn = (AttributeHeaderNode) aNode;
        ahn.getChildren().add(new AttributeNode(ahn, "a", "b"));

        MoviesChangedEvent evt = catchEvent(MoviesChangedEvent.class, () ->
            ahn.save()
        );
    }
}