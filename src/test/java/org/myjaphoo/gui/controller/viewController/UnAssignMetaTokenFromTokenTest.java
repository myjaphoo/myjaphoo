package org.myjaphoo.gui.controller.viewController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.MetaTagsUnassignedEvent;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class UnAssignMetaTokenFromTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void unassign() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        ImmutableModel modelBefore = set.project.cacheActor.getImmutableModel();
        ImmutableToken t1Before = modelBefore.getTokenByName(set.t1.getName()).get();
        assertEquals(2, t1Before.getAssignedMetaTokens(modelBefore).size());

        MetaTagsUnassignedEvent event = catchEvent(MetaTagsUnassignedEvent.class, () ->
            controller.unAssignMetaTokenFromToken(set.mt1.toImmutable(), Arrays.asList(set.t1.toImmutable()))
        );
        ImmutableModel model = event.getModel();

        assertEquals(1, event.getTokenSet().size());
        assertEquals(set.t1.getId(), event.getTokenSet().get(0).getId());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }

}