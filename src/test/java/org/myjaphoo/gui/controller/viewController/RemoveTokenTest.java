package org.myjaphoo.gui.controller.viewController;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.TagsDeletedEvent;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class RemoveTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void removeToken() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        ImmutableModel modelBefore = set.project.cacheActor.getImmutableModel();
        ImmutableToken t2Before = modelBefore.getTokenByName(set.t2.getName()).get();
        assertEquals(1, t2Before.getAssignedMetaTokens(modelBefore).size());

        TagsDeletedEvent event = catchEvent(TagsDeletedEvent.class, () ->
            controller.removeToken(set.t1.toImmutable())
        );

        assertEquals(1, event.getTokenSet().size());
        assertEquals(set.t1.getId(), event.getTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();

        // compare them (note that the objects are not identical)
        Assertions.assertThat(model.getTokenTree().getValues().toList()).contains(
            set.t2.toImmutable(),
            set.t3.toImmutable()
        );
        Assertions.assertThat(model.getMetaTokenTree().getValues().toList()).contains(
            set.mt1.toImmutable(),
            set.mt2.toImmutable(),
            set.mt3.toImmutable()
        );

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }


}