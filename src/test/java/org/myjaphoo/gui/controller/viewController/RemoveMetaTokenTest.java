package org.myjaphoo.gui.controller.viewController;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.events.MetaTagsDeletedEvent;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class RemoveMetaTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void removeToken() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        MetaTagsDeletedEvent event = catchEvent(MetaTagsDeletedEvent.class, () ->
            controller.removeMetaToken(set.mt1.toImmutable())
        );

        assertEquals(1, event.getMetaTokenSet().size());
        assertEquals(set.mt1.getId(), event.getMetaTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();

        // compare them (note that the objects are not identical)
        Assertions.assertThat(model.getMetaTokenTree().getValues().toList()).contains(
            set.mt2.toImmutable(),
            set.mt3.toImmutable()
        );
        Assertions.assertThat(model.getTokenTree().getValues().toList()).contains(
            set.t1.toImmutable(),
            set.t2.toImmutable(),
            set.t3.toImmutable()
        );

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }


}