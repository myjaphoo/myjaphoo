package org.myjaphoo.gui.controller.viewController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.TagsAddedEvent;
import org.myjaphoo.model.cache.zipper.TokenRef;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class CreateTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void create() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        TagsAddedEvent event = catchEvent(TagsAddedEvent.class, () ->
            // move mt1 under mt2:
            controller.createNewToken("t4", "descr", set.t2.toImmutable())
        );

        assertEquals(1, event.getTokenSet().size());
        ImmutableToken newMt = event.getTokenSet().get(0);
        ImmutableModel model = event.getModel();

        TokenRef mtr = new TokenRef(model, newMt);

        assertEquals(set.t2.toImmutable(), mtr.getParentTag());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }
}