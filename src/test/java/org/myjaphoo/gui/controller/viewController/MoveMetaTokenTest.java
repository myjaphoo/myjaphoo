package org.myjaphoo.gui.controller.viewController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.events.MetaTagsChangedEvent;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class MoveMetaTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void move() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        MetaTagsChangedEvent event = catchEvent(MetaTagsChangedEvent.class, () ->
            // move mt1 under mt2:
            controller.moveMetaTokens(set.mt2.toImmutable(), set.mt1.toImmutable())
        );

        assertEquals(1, event.getMetaTokenSet().size());
        assertEquals(set.mt1.getId(), event.getMetaTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();
        ImmutableMetaToken im1 = model.getMetaTokenByName("meta1").get();
        MetaTokenRef imr1 = new MetaTokenRef(model, im1);
        assertEquals(set.mt2.getId(), imr1.getParentMetaTag().getId());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }
}