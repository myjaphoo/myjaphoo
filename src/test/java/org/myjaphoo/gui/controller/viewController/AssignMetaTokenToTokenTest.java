package org.myjaphoo.gui.controller.viewController;

import io.vavr.collection.Set;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.MetaTagsAssignedEvent;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class AssignMetaTokenToTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void assign() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        ImmutableModel modelBefore = set.project.cacheActor.getImmutableModel();
        ImmutableToken t2Before = modelBefore.getTokenByName(set.t2.getName()).get();
        assertEquals(1, t2Before.getAssignedMetaTokens(modelBefore).size());

        MetaTagsAssignedEvent event = catchEvent(MetaTagsAssignedEvent.class, () ->
            controller.assignMetaTokenToToken(set.mt2.toImmutable(), set.t2.toImmutable())
        );

        assertEquals(1, event.getTokenSet().size());
        assertEquals(set.t2.getId(), event.getTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();
        ImmutableToken newTok2Version = model.getTokenByName(set.t2.getName()).get();
        Set<ImmutableMetaToken> assignedMTs = newTok2Version.getAssignedMetaTokens(
            model);
        assertEquals(2, assignedMTs.size());

        // compare them (note that the objects are not identical)
        Assertions.assertThat(assignedMTs).contains(set.mt2.toImmutable(), set.mt3.toImmutable());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }


}