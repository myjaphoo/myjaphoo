package org.myjaphoo.gui.controller.viewController;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.events.MoviesRemovedEvent;
import org.myjaphoo.model.cache.zipper.EntryRef;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class RemoveEntryTest {

    @Mock
    MyjaphooView view;

    @Test
    public void removeEntry() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        ImmutableModel modelBefore = set.project.cacheActor.getImmutableModel();

        set.project.dbPrefs.PRF_FO_REMOVING_ALLOWED.setVal(true);

        MoviesRemovedEvent event = catchEvent(MoviesRemovedEvent.class, () ->
            controller.removeMovieEntriesFromDatabase(Arrays.asList(new EntryRef(modelBefore, set.e1.toImmutable())))
        );

        assertEquals(1, event.getMovieEntrySet().size());
        assertEquals(set.e1.getId(), event.getMovieEntrySet().get(0).getId());

        ImmutableModel model = event.getModel();

        // compare them (note that the objects are not identical)
        Assertions.assertThat(model.getEntryList().size()).isEqualTo(4);
        Assertions.assertThat(model.getEntryList()).contains(
            set.e2.toImmutable(),
            set.e3.toImmutable(),
            set.e4.toImmutable(),
            set.e5.toImmutable()
        );

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }


}