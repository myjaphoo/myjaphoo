package org.myjaphoo.gui.controller.viewController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.events.MetaTagsAddedEvent;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class CreateMetaTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void create() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        MetaTagsAddedEvent event = catchEvent(MetaTagsAddedEvent.class, () ->
            // move mt1 under mt2:
            controller.createNewMetaToken("mt4", "descr", set.mt2.toImmutable())
        );

        assertEquals(1, event.getMetaTokenSet().size());
        ImmutableMetaToken newMt = event.getMetaTokenSet().get(0);
        ImmutableModel model = event.getModel();

        MetaTokenRef mtr = new MetaTokenRef(model, newMt);

        assertEquals(set.mt2.toImmutable(), mtr.getParentMetaTag());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }
}