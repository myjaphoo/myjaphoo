package org.myjaphoo.gui.controller.viewController;

import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * EventCatcher
 *
 * @author mla
 * @version $Id$
 */
public class EventCatcher<T> {

    public static interface Execution {
        public void doIt();
    }

    final CountDownLatch lock = new CountDownLatch(1);
    T event;

    /** if set, then wait explicitly till an event of this class gets fired. useful, if multiple different events get
     * fired during test. */
    Class<T> explicitClassToCheck;

    public EventCatcher() {
        GlobalBus.bus.register(this);
    }

    public EventCatcher(Class<T> explicitClassToCheck) {
        GlobalBus.bus.register(this);
        this.explicitClassToCheck = explicitClassToCheck;
    }

    @Subscribe()
    public void listen(T event) {
        if (explicitClassToCheck == null || explicitClassToCheck.isInstance(event)) {
            this.event = event;
            lock.countDown();
        }
    }

    public void waitForEvent() throws InterruptedException {
        lock.await(2000, TimeUnit.MILLISECONDS);
        assertNotNull(event);
    }

    public T catchIt(Execution execution) throws InterruptedException {
        execution.doIt();
        waitForEvent();
        return event;
    }

    /**
     * executes code that is expected to yield an event via the event bus.
     * It installs an event handler, executes the code, waits a certain time till the even arrives, and then
     * returns the event to the caller.
     *
     * @param execution
     * @param <T>
     *
     * @return
     * @throws InterruptedException
     */
    public static <T> T catchEvent(Class<T> clazz, Execution execution) throws InterruptedException {
        EventCatcher<T> eventCatcher = new EventCatcher<>(clazz);
        return eventCatcher.catchIt(execution);
    }
}
