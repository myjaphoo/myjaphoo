package org.myjaphoo.gui.controller.viewController;

import io.vavr.collection.Set;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MovieNode;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.TagsUnassigendEvent;
import org.myjaphoo.model.cache.zipper.EntryRef;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class UnAssignTokenFromMovieEntryTest {

    @Mock
    MyjaphooView view;

    @Test
    public void unassign() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        EntryRef entryRef = new EntryRef(set.project.cacheActor.getImmutableModel(), set.e1.toImmutable());
        MovieNode node = new MovieNode(entryRef, false);
        TagsUnassigendEvent event = catchEvent(TagsUnassigendEvent.class, () ->

            controller.unassignTokenToMovieNodes(set.t1.toImmutable(), Arrays.asList(node))
        );

        assertEquals(1, event.getTokenSet().size());
        assertEquals(set.t1.getId(), event.getTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();
        assertEquals(1, event.getMovieEntrySet().size());
        assertEquals(set.e1.getId(), event.getMovieEntrySet().get(0).getId());

        ImmutableMovieEntry m1 = event.getMovieEntrySet().get(0);

        Set<ImmutableToken> assignedTags = model.getEntryToken().getAssignedY(m1).get();

        // compare them (note that the objects are not identical)
        Assertions.assertThat(assignedTags).isEmpty();

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }


}