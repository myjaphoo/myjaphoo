package org.myjaphoo.gui.controller.viewController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlsoft.eventbus.GlobalBus;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.MyjaphooView;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.model.cache.EntityCacheActorImpl;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.TagsChangedEvent;
import org.myjaphoo.model.cache.zipper.TokenRef;

import static org.junit.Assert.*;
import static org.myjaphoo.gui.controller.viewController.EventCatcher.catchEvent;

/**
 * MyjaphooControllerTest
 *
 * @author mla
 * @version $Id$
 */

@RunWith(MockitoJUnitRunner.class)
public class MoveTokenTest {

    @Mock
    MyjaphooView view;

    @Test
    public void move() throws InterruptedException {
        MiniTestSet set = DataTestSets.createMiniTestSet();

        MainApplicationController mainController = new MainApplicationController(set.project);
        MyjaphooController controller = new MyjaphooController(mainController, view);

        TagsChangedEvent event = catchEvent(TagsChangedEvent.class, () ->
            // move mt1 under mt2:
            controller.moveTokens(set.t2.toImmutable(), set.t1.toImmutable())
        );

        assertEquals(1, event.getTokenSet().size());
        assertEquals(set.t1.getId(), event.getTokenSet().get(0).getId());

        ImmutableModel model = event.getModel();
        ImmutableToken im1 = model.getTokenByName(set.t1.getName()).get();
        TokenRef imr1 = new TokenRef(model, im1);
        assertEquals(set.t2.getId(), imr1.getParentTag().getId());

        // the changed model should be equal to a fresh loaded model:
        new ModelComparer(model, new EntityCacheActorImpl(set.project.connection, GlobalBus.bus).getImmutableModel());
    }

}