package org.myjaphoo.gui.controller.viewController;

import io.vavr.collection.Traversable;
import junit.framework.Assert;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.ImmutableUUIDEntity;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;
import org.myjaphoo.model.cache.zipper.TokenRef;
import org.myjaphoo.util.ComparatorByProperties;
import org.myjaphoo.util.PropsToMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Comparator.comparing;

/**
 * Utility class to compare immutable models. Used for tests and part of the main classes
 * so that derived projects (plugins) could reuse it for tests.
 */
public class ModelComparer {

    private ImmutableModel m1;
    private ImmutableModel m2;

    private PropsToMap propsToMap = new PropsToMap();

    public ModelComparer(ImmutableModel m1, ImmutableModel m2) {
        this.m1 = m1;
        this.m2 = m2;
        compareModels();
    }

    public ModelComparer(ImmutableModel m1, ImmutableModel m2, PropsToMap propsToMap) {
        this.m1 = m1;
        this.m2 = m2;
        this.propsToMap = propsToMap;
        compareModels();
    }

    private void compareModels() {
        compareEntries();
        compareTags();
        compareMTags();
    }

    private void compareMTags() {
        Assert.assertEquals(m1.getMetaTokenTree().getValues().size(), m2.getMetaTokenTree().getValues().size());
        Iterator<ImmutableMetaToken> i1 = sortedMetaTags(m1.getMetaTokenTree().getValues()).iterator();
        Iterator<ImmutableMetaToken> i2 = sortedMetaTags(m2.getMetaTokenTree().getValues()).iterator();
        while (i1.hasNext()) {
            MetaTokenRef t1 = new MetaTokenRef(m1, i1.next());
            MetaTokenRef t2 = new MetaTokenRef(m2, i2.next());
            if (t1.getParentMetaTag() != null) {
                compare(t1, t2);
            }
        }
    }

    private void compareTags() {
        Assert.assertEquals(m1.getTokenTree().getValues().size(), m2.getTokenTree().getValues().size());
        Iterator<ImmutableToken> i1 = sortedTags(m1.getTokenTree().getValues()).iterator();
        Iterator<ImmutableToken> i2 = sortedTags(m2.getTokenTree().getValues()).iterator();
        while (i1.hasNext()) {

            TokenRef t1 = new TokenRef(m1, i1.next());
            TokenRef t2 = new TokenRef(m2, i2.next());
            // do not compare root token. these are always artificially created
            if (t1.getParentTag() != null) {
                compare(t1, t2);
            }
        }
    }

    private void compare(TokenRef t1, TokenRef t2) {
        compareProps(t1.getRef(), t2.getRef());

        Assert.assertEquals(t1.getParentTag().getName(), t2.getParentTag().getName());
        compareLists(sortedEntries(t1.getAssignedMovieEntries()), sortedEntries(t1.getAssignedMovieEntries()));
        compareLists(sortedMetaTags(t1.getMetaTokens()), sortedMetaTags(t2.getMetaTokens()));

    }

    private void compare(MetaTokenRef t1, MetaTokenRef t2) {
        compareProps(t1.getRef(), t2.getRef());
        Assert.assertEquals(t1.getParentMetaTag().getName(), t2.getParentMetaTag().getName());
        compareLists(sortedTags(t1.getAssignedTokens()), sortedTags(t1.getAssignedTokens()));
    }

    private void compareEntries() {
        Assert.assertEquals(m1.getEntryList().size(), m2.getEntryList().size());
        Iterator<ImmutableMovieEntry> i1 = sortedEntries(m1.getEntryList()).iterator();
        Iterator<ImmutableMovieEntry> i2 = sortedEntries(m2.getEntryList()).iterator();
        while (i1.hasNext()) {
            EntryRef e1 = new EntryRef(m1, i1.next());
            EntryRef e2 = new EntryRef(m2, i2.next());
            compare(e1, e2);
        }
    }

    private List<ImmutableMovieEntry> sortedEntries(
        Traversable<ImmutableMovieEntry> entryList
    ) {
        List<ImmutableMovieEntry> l = entryList.toJavaList();
        l.sort(comparing(ImmutableUUIDEntity::getUuid));
        return l;
    }

    private void compare(EntryRef e1, EntryRef e2) {
        compareProps(e1.getRef(), e2.getRef());

        compareLists(sortedTags(e1.getTokens()), sortedTags(e2.getTokens()));
    }

    private void compareLists(List l1, List l2) {
        Assert.assertEquals(l1.size(), l2.size());
        Iterator i1 = l1.iterator();
        Iterator i2 = l2.iterator();
        while (i1.hasNext()) {
            compareProps(i1.next(), i2.next());
        }
    }

    private List<ImmutableToken> sortedTags(Traversable<ImmutableToken> tokens) {
        List<ImmutableToken> l = tokens.toJavaList();
        l.sort(comparing(ImmutableToken::getName));
        return l;
    }

    private List<ImmutableMetaToken> sortedMetaTags(Traversable<ImmutableMetaToken> tokens) {
        List<ImmutableMetaToken> l = tokens.toJavaList();
        l.sort(comparing(ImmutableMetaToken::getName));
        return l;
    }

    private void compareProps(Object o1, Object o2) {
        Map<String, Object> map1 = propsToMap.toMap(o1);
        Map<String, Object> map2 = propsToMap.toMap(o2);
        if (!Objects.equals(map1, map2)) {
            ComparatorByProperties c = new ComparatorByProperties();
            Map<String, ComparatorByProperties.Diff> diffs = c.getDiffProps(o1, o2);
            Assert.fail("diff: " + diffs);
        }
        Assert.assertEquals(map1, map2);
    }


}
