package org.myjaphoo.gui.movietree.grouping;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.groupingprocessor.Branch;
import org.myjaphoo.model.groupingprocessor.Leaf;
import org.myjaphoo.model.groupingprocessor.Pruning;

import static org.junit.Assert.*;

/**
 * PruningTest
 *
 * @author mla
 * @version $Id$
 */
@RunWith(MockitoJUnitRunner.class)
public class PruningTest {

    @Mock
    EntryRef entryRef;

    @Test
    public void testPruneEmptyDirs01() throws Exception {

        Branch root = new Branch("root");
        Branch child1 = new Branch("child1");

        root.addChild(child1);
        assertEquals(1, root.getChildBranches().size());

        Pruning.pruneEmptyDirs(root);

        assertEquals(0, root.getChildBranches().size());
    }

    @Test
    public void testPruneEmptyDirs02() throws Exception {

        Branch root = new Branch("root");
        Branch child1 = new Branch("child1");

        root.addChild(child1);
        assertEquals(1, root.getChildBranches().size());

        Branch child2 = new Branch("child2");
        child1.addChild(child2);
        Branch child3 = new Branch("child3");
        child1.addChild(child3);

        Pruning.pruneEmptyDirs(root);

        assertEquals(2, root.getChildBranches().size());
        assertEquals("root/child1", root.getName());

    }

    @Test
    public void testPruneEmptyDirs03() throws Exception {

        Branch root = new Branch("root");
        Branch child1 = new Branch("child1");

        root.addChild(child1);
        assertEquals(1, root.getChildBranches().size());

        Leaf n = new Leaf(entryRef, null, true);
        child1.addLeaf(n);
        Leaf n2 = new Leaf(entryRef, null, true);
        child1.addLeaf(n2);

        Pruning.pruneEmptyDirs(root);

        assertEquals(2, root.getLeafs().size());

    }
}