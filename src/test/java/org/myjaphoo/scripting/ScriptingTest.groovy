package org.myjaphoo.scripting

import org.myjaphoo.gui.action.scriptactions.CommonAction
import org.myjaphoo.gui.scripting.Scripting
import org.myjaphoo.model.registry.ComponentRegistry

/**
 * CommandModeTest 
 * @author mla
 * @version $Id$
 *
 */
class ScriptingTest extends GroovyTestCase {

    def void testScriptScan() {
        // scan the test "plugins" under the plugins directory:
        Scripting.startScriptPlugins(new File("./plugins"));
        // we should now have some additional actions defined:
        def allCommonActions = ComponentRegistry.registry.getEntryCollection(CommonAction.class);
        def expectedDefinedAction = allCommonActions.find { it.name == "CommandFromPlugin1" }
        assertNotNull(expectedDefinedAction)


    }


}
