package org.myjaphoo.util

import org.junit.Test

/**
 * PropsToMapTest
 */
public class PropsToMapTest {

    static class TestClass {
        private String test;
        private Integer a;
        private Double b;
        private Map mymap = new HashMap();

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Integer getA() {
            return a;
        }

        public void setA(Integer a) {
            this.a = a;
        }

        public Double getB() {
            return b;
        }

        public void setB(Double b) {
            this.b = b;
        }

        public Map getMymap() {
            return mymap;
        }

        public void setMymap(Map mymap) {
            this.mymap = mymap;
        }
    }

    @Test
    public void testToMap() {
        TestClass t = new TestClass();
        t.setA(7);
        t.setTest("bla");
        t.getMymap().put("key", "val");
        PropsToMap toMap = new PropsToMap();
        Map<String, Object> map = toMap.toMap(t);
        assert map == [A: 7, B: null, Test: "bla", "Mymap.key": "val"]
    }

}