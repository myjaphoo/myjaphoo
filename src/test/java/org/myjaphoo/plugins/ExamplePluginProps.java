package org.myjaphoo.plugins;

import org.mlsoft.common.prefs.model.editors.EditorGroup;
import org.mlsoft.common.prefs.model.editors.StringVal;
import org.myjaphoo.project.Project;

/**
 * ExamplePluginProps
 *
 * @author mla
 * @version $Id$
 */
public class ExamplePluginProps {

    public final EditorGroup GROUP_FOR_THIS_PLUGIN;

    public final StringVal PRF_STRING_VAL;

    ExamplePluginProps(Project project) {
        GROUP_FOR_THIS_PLUGIN = new EditorGroup(
            project.prefs.ROOT,
            "testplugin",
            "testplugin",
            "testplugin"
        );

        PRF_STRING_VAL =
            new StringVal(GROUP_FOR_THIS_PLUGIN, "testplugin.aValue",  //NOI18N
                "testvalue",
                "testvalue",
                "defaultValue"
            );
    }

}
