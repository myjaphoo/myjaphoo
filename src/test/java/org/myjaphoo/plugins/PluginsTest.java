package org.myjaphoo.plugins;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.plugin.MyjaphooPlugin;

import java.util.Iterator;
import java.util.ServiceLoader;

import static org.junit.Assert.*;

/**
 * PluginsTest
 */
@RunWith(MockitoJUnitRunner.class)
public class PluginsTest {

    @Test
    public void testDiscoveryOfPlugins() throws InterruptedException {
        ServiceLoader<MyjaphooPlugin> loader = ServiceLoader.load(MyjaphooPlugin.class);
        Iterator<MyjaphooPlugin> iterator = loader.iterator();
        assertTrue(iterator.hasNext());
        MyjaphooPlugin plugin = iterator.next();
        assertTrue(plugin instanceof TestPlugin);
    }

    @Test
    public void testProjectEvent() throws InterruptedException {

        MiniTestSet set = DataTestSets.createMiniTestSet();
        ExamplePluginProps pluginProps = TestPlugin.getPluginProps(set.project);
        assertNotNull(pluginProps);
    }

}
