package org.myjaphoo.plugins;

import org.myjaphoo.model.registry.ComponentRegistry;
import org.myjaphoo.plugin.MyjaphooPlugin;
import org.myjaphoo.project.Project;

/**
 * example of a plugin.
 */
public class TestPlugin implements MyjaphooPlugin {
    /**
     * Plugin name.
     *
     * @return
     */
    @Override
    public String getName() {
        return "Test plugin";
    }

    /**
     * String representing the plugin version.
     */
    @Override
    public String getVersion() {
        return "1.0";
    }

    /**
     * initialize the plugin. Usually registers additional gui actions via the ComponentRegistry.
     *
     * @see ComponentRegistry
     */
    @Override
    public void init() {

    }

    /**
     * Do initialisations for a new project.
     *
     * @param project
     */
    @Override
    public void initProject(Project project) {
        ExamplePluginProps pluginProps = new ExamplePluginProps(project);
        project.pluginProps.put("testPlugin.properties", pluginProps);
    }

    public static ExamplePluginProps getPluginProps(Project project) {
        return (ExamplePluginProps) project.pluginProps.get("testPlugin.properties");
    }
}
