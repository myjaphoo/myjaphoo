package org.myjaphoo;

import junit.framework.TestCase;
import org.jbake.app.Oven;

import java.io.File;

/**
 * JBakeRun
 * @author mla
 * @version $Id$
 */
public class TestJBakeRun extends TestCase {

    public void testBakeIt() throws Exception {
        File source = new File("src/main/jbake");
        File destination = new File("target/docs");
        Oven oven = new Oven(source, destination, true);
        oven.setupPaths();
        oven.bake();

    }
}
