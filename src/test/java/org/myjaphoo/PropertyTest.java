package org.myjaphoo;

import junit.framework.TestCase;
import org.myjaphoo.project.ProjectConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

/**
 * PropertyTest
 *
 * @author mla
 * @version $Id$
 */
public class PropertyTest extends TestCase {

    public void testProperties() {
        File testDir = new File("./target/propertyTest/testProperties" + new Random().nextInt());
        testDir.mkdirs();
        String appDir = testDir.getAbsolutePath();
        ProjectConfiguration projectConfiguration = new ProjectConfiguration(appDir, appDir);
        MyjaphooCorePrefs appPrefs = new MyjaphooCorePrefs(projectConfiguration);

        // read:
        assertEquals("noname", appPrefs.PRF_DATABASENAME.getVal());

        // write:
        appPrefs.PRF_DATABASENAME.setVal("newName");
        assertEquals("newName", appPrefs.PRF_DATABASENAME.getVal());
        appPrefs.getPrefStructure().commit();

        // now it should be saved.
        MyjaphooCorePrefs freshLoadedPrefs = new MyjaphooCorePrefs(new ProjectConfiguration(appDir, appDir));
        assertEquals("newName", freshLoadedPrefs.PRF_DATABASENAME.getVal());

    }

    public void testFindByName() {
        File testDir = new File("./target/propertyTest/testProperties");
        testDir.mkdirs();
        String appDir = testDir.getAbsolutePath();
        ProjectConfiguration projectConfiguration = new ProjectConfiguration(appDir, appDir);
        MyjaphooCorePrefs appPrefs = new MyjaphooCorePrefs(projectConfiguration);

        assertSame(appPrefs.PRF_DATABASENAME, appPrefs.searchByName(appPrefs.PRF_DATABASENAME.getName()));
    }

    public void testPropPrecedence() throws IOException {
        // prepare two test property files:
        File testDir = new File("./target/propertyTest/propPrecedence");
        testDir.mkdirs();
        File testDirDefaults = new File("./target/propertyTest/propPrecedence/defaults");
        testDirDefaults.mkdirs();

        Properties defaults = new Properties();
        defaults.setProperty("c", "3");
        defaults.setProperty("e", "5");
        defaults.setProperty("x", "1");
        try(FileWriter writer = new FileWriter(new File(testDirDefaults, "preferences.properties"))) {
            defaults.store(writer, "");
        }

        Properties p = new Properties();
        p.setProperty("a", "7");
        p.setProperty("b", "9");
        p.setProperty("x", "2");
        try(FileWriter writer = new FileWriter(new File(testDir, "preferences.properties"))) {
            p.store(writer, "");
        }

        ProjectConfiguration props = new ProjectConfiguration(testDirDefaults.getAbsolutePath(), testDir.getAbsolutePath());

        assertEquals("7", props.getProjectProperties().getProperty("a"));
        assertEquals("9", props.getProjectProperties().getProperty("b"));
        assertEquals("2", props.getProjectProperties().getProperty("x"));
        assertEquals("5", props.getProjectProperties().getProperty("e"));
        assertEquals("Oracle Corporation", props.getProjectProperties().getProperty("java.vm.vendor"));
        
    }
}
