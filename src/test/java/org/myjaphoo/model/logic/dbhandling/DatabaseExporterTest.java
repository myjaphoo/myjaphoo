package org.myjaphoo.model.logic.dbhandling;

import junit.framework.TestCase;
import org.myjaphoo.datasets.MiniTestSet;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.controller.viewController.ModelComparer;
import org.myjaphoo.project.Project;
import org.myjaphoo.util.PropsToMap;

/**
 * DatabaseExporterTest
 */
public class DatabaseExporterTest extends TestCase {

    public void testExport() {
        MiniTestSet set = DataTestSets.createMiniTestSet();
        Project targetProject = TestSet.createTestProject("databaseexporttarget");
        DatabaseExporter databaseExporter = new DatabaseExporter();
        databaseExporter.exportToDatabase(set.project, targetProject.connection);

        // the changed model should be equal to a fresh loaded model:
        PropsToMap propsToMap = new PropsToMap();
        propsToMap.excludeMethod("getId");
        set.project.cacheActor.resetImmutableCopy();
        targetProject.cacheActor.resetImmutableCopy();
        new ModelComparer(set.project.cacheActor.getImmutableModel(), targetProject.cacheActor.getImmutableModel(), propsToMap);
    }
}