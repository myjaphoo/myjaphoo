package org.myjaphoo.model.logic.dbhandling;

import org.junit.Test;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbconfig.DBConnection;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.dbconfig.DatabaseDriver;
import org.myjaphoo.project.Project;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ThreadLocalTransactionHandlerTest
 *
 * @author mla
 * @version $Id$
 */
public class ThreadLocalTransactionHandlerTest {

    @Test
    public void basicTransaction() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:transactiontest");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        ThreadLocalTransactionHandler h = new ThreadLocalTransactionHandler(dbconnection);

        MovieEntry[] loadedEntity = new MovieEntry[1];

        h.doInNewTransaction(em -> {
                MovieEntry e = new MovieEntry();
                e.setName("bla");
                e.setChecksumCRC32(31L);
                e.setCanonicalDir("abc");
                em.persist(e);
                loadedEntity[0] = e;
                assertThat(em.contains(e)).isTrue();
            }
        );

        h.doInNewTransaction(em -> {
                assertThat(em.contains(loadedEntity[0])).isFalse();
            }
        );

        // load it again:
        MovieEntry result = h.doLoading(em -> (MovieEntry) em.createQuery("from MovieEntry m where m.checksumCRC32 = 31").getSingleResult());
        assertThat(result).isNotNull();
        assertThat(result.getCanonicalDir()).isEqualTo("abc");
    }

    @Test
    public void nestedTransaction() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:transactiontest2");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        ThreadLocalTransactionHandler h = new ThreadLocalTransactionHandler(dbconnection);

        h.doInNewTransaction(em -> {
                MovieEntry e = new MovieEntry();
                e.setName("bla");
                e.setChecksumCRC32(31L);
                e.setCanonicalDir("abc");
                em.persist(e);

                h.doInNewTransaction(em2 -> {
                    assertThat(em.contains(e)).isTrue();

                });
            }
        );

        // load it again:
        MovieEntry result = h.doLoading(em -> (MovieEntry) em.createQuery("from MovieEntry m where m.checksumCRC32 = 31").getSingleResult());
        assertThat(result).isNotNull();
        assertThat(result.getCanonicalDir()).isEqualTo("abc");
    }

    @Test
    public void rollbackTransaction() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:transactiontest3");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        ThreadLocalTransactionHandler h = new ThreadLocalTransactionHandler(dbconnection);

        MovieEntry[] loadedEntity = new MovieEntry[1];
        try {
            h.doInNewTransaction(em -> {
                    MovieEntry e = new MovieEntry();
                    e.setName("bla");
                    e.setChecksumCRC32(31L);
                    e.setCanonicalDir("abc");
                    em.persist(e);
                    loadedEntity[0] = e;
                    assertThat(em.contains(e)).isTrue();
                    // throw exception:
                    throw new RuntimeException();
                }
            );
        } catch (RuntimeException e) {

        }
        h.doInNewTransaction(em -> {
                assertThat(em.contains(loadedEntity[0])).isFalse();
            }
        );

        // load it again:
        List<MovieEntry> result = h.doLoading(em -> (List<MovieEntry>) em.createQuery(
            "from MovieEntry m where m.checksumCRC32 = 31").getResultList());
        assertThat(result).isEmpty();
    }

    @Test
    public void rollbackInNestedTransaction() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:transactiontest4");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        ThreadLocalTransactionHandler h = new ThreadLocalTransactionHandler(dbconnection);
        try {
        h.doInNewTransaction(em -> {
                MovieEntry e = new MovieEntry();
                e.setName("bla");
                e.setChecksumCRC32(31L);
                e.setCanonicalDir("abc");
                em.persist(e);

                h.doInNewTransaction(em2 -> {
                    assertThat(em.contains(e)).isTrue();
                    // throw exception:
                    throw new RuntimeException();
                });
            }
        );
        } catch (RuntimeException e) {

        }
        // load it again:
        List<MovieEntry> result = h.doLoading(em -> (List<MovieEntry>) em.createQuery(
            "from MovieEntry m where m.checksumCRC32 = 31").getResultList());
        assertThat(result).isEmpty();
    }

    /**
     * Tests independency of different database configurations
     */
    @Test
    public void independencyOfDatabases() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config.setFilename("mem:theFirstDb");

        DBConnection dbconnection = new DBConnection(config);
        dbconnection.open();
        ThreadLocalTransactionHandler h = new ThreadLocalTransactionHandler(dbconnection);


        h.doInNewTransaction(em -> {
                Token t = new Token();
                t.setName("identicalName");
                em.persist(t);
            }
        );

        DatabaseConfiguration config2 = new DatabaseConfiguration();
        config2.setDatabaseDriver(DatabaseDriver.H2_EMBEDDED);
        config2.setFilename("mem:theSecondDb");

        DBConnection dbconnection2 = new DBConnection(config2);
        dbconnection2.open();
        ThreadLocalTransactionHandler h2 = new ThreadLocalTransactionHandler(dbconnection2);


        h2.doInNewTransaction(em -> {
                Token t = new Token();
                t.setName("identicalName");
                em.persist(t);
            }
        );
    }

    /**
     * Tests independency of different (test) projects
     */
    @Test
    public void independencyOfDatabasesOnProjectLevel() {
        Project p1 = TestSet.createTestProject("independency1");
 
        p1.connection.commit(em -> {
                Token t = new Token();
                t.setName("identicalName");
                em.persist(t);
            }
        );

        Project p2 = TestSet.createTestProject("independency2");

        p2.connection.commit(em -> {
                Token t = new Token();
                t.setName("identicalName");
                em.persist(t);
            }
        );
    }

    /**
     * Metatest to check independency of the test set methods
     */
    @Test
    public void independencyOfTestSets() {
        TestSet set1 = DataTestSets.createTestSet1("independencyTestSet1");

        TestSet set2 = DataTestSets.createTestSet1("independencyTestSet2");
    }
}