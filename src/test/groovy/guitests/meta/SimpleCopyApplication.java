package guitests.meta;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import static javax.swing.SwingUtilities.invokeAndWait;


public class SimpleCopyApplication extends JFrame {
    private static final long serialVersionUID = 1L;

    public SimpleCopyApplication() {
        this.setLayout(new BorderLayout());

        final JTextField textField = new JTextField("textToCopy");
        textField.setName("textToCopy");
        JButton button = new JButton("Copy text to label");
        final JLabel label = new JLabel("copiedText");
        label.setName("label1");

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                label.setText(textField.getText());
            }
        });

        add(textField, BorderLayout.NORTH);
        add(button, BorderLayout.SOUTH);
        add(label, BorderLayout.CENTER);

        pack();
    }

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        invokeAndWait(new Runnable() {

            public void run() {
                JFrame frame = new SimpleCopyApplication();
                frame.setVisible(true);
            }
        });
    }
}
