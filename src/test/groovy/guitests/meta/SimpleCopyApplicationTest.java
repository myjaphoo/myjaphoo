package guitests.meta;

import guitests.GuiTests;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.assertj.swing.core.matcher.JButtonMatcher.withText;

@Category(GuiTests.class)
public class SimpleCopyApplicationTest {
    private FrameFixture window;

    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    @Before
    public void setUp() {
        SimpleCopyApplication frame = GuiActionRunner.execute(() -> new SimpleCopyApplication());
        window = new FrameFixture(frame);
        window.show(); // shows the frame to test
    }

    @Test
    @Category(GuiTests.class)
    public void shouldCopyTextInLabelWhenClickingButton() {
        window.textBox("textToCopy").enterText("Some random text");
        window.button(withText("Copy text to label")).click();
        window.label("label1").requireText("textToCopySome random text");
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }
}
