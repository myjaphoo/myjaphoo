package org.myjaphoo.gui.util;

import org.junit.Test;
import org.myjaphoo.MovieNode;
import org.myjaphoo.datasets.TestSet;
import org.myjaphoo.groupingtests.DataTestSets;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.project.Project;

/**
 * TipTextTest
 *
 * @author mla
 * @version $Id$
 */
public class TipTextTest {

    @Test
    public void testCompileAndRuns() {
        // simply check, that there are no dynamic groovy errors:
        TestSet set = DataTestSets.createTestSet1("testCompileAndRuns");
        Project project = set.project;
        TipText t = new TipText(project);

        ImmutableModel model = project.cacheActor.getImmutableModel();
        ImmutableMovieEntry entry = model.getEntryList().get(0);

        EntryRef entryRef = new EntryRef(model, entry);
        MovieNode node = new MovieNode(entryRef, true);
        t.createThumbTipText(node);
        t.createThumbTipTextCompact(node);
        
        TextRepresentations tr = new TextRepresentations(project);
        tr.createMovieTreeCellRendererIconLabelTextForMovieNode(node);
        MovieStructureNode movieStructureNode = new MovieStructureNode("test");
        movieStructureNode.addChild(node);
        tr.createMovieTreeCellRendererIconLabelTextForStructureNode(movieStructureNode);
    }
}