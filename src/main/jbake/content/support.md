title=Support
date=2011-11-23
type=page
tags=releases
status=published
~~~~~~

<h2>Forum</h2>
For general questions about usage or problems or ideas regarding this software please use the <a title="Discussion Forum" href="https://sourceforge.net/p/myjaphoo/discussion/ " target="_blank">https://sourceforge.net/p/myjaphoo/discussion/ </a>.  You could also blog directly in this website.
<h2>Bugs</h2>
Please report bugs at <a title="Bitbucket" href="https://bitbucket.org/myjaphoo/myjaphoo/issues" target="_blank">https://bitbucket.org/myjaphoo/myjaphoo/issues</a>

&nbsp;
<h2>Contact</h2>
Or just write me an email:

admin@myjaphoo.de

