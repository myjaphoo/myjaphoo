title=Version 3.9 released
date=2014-03-17
type=post
tags=releases
status=published
~~~~~~

You can find the new version at <a title="Version 3.9.157" href="https://sourceforge.net/projects/myjaphoo/files/3.9.157/" target="_blank">https://sourceforge.net/projects/myjaphoo/files/3.9.157/</a>
<h2>New Features</h2>
<ul>
	<li>User defined properties for Media entries, tags and metatags: you are now able to define your own properties/attributes for these entities and define values for them in the properties panel</li>
</ul>
<ul>
	<li>bookmarks and scripts could have a path to group them in the menus. You can use "/" within the name of a bookmark or script to structure it in submenus in the UI</li>
	<li>better groovy integration for filtering and grouping. You could put initialisation groovy code into the application to define your own menu points,  filter functions, grouping functions, etc. This customization will be improved over the next versions</li>
	<li>several small bugfixes and performance improvments</li>
	<li>Please note, that the application asumes a 64 bit java runtime, now as the start script increases the max memory to 3GB</li>
</ul>
<h2></h2>
<h2>Changelog</h2>
<ol>
	<li>Merge with default: preparation for release of 3.9 — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail0">detail</a></li>
	<li>increased mem preferences — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail1">detail</a></li>
	<li>MJ-47 filesubstitutions; faster version for gui usage — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail2">detail</a></li>
	<li>added context variable map — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail3">detail</a></li>
	<li>optimization: fetch queries to get tags + assigned entries and metatags + assigned tags; speeds up loading of large dbs a lot — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail4">detail</a></li>
	<li>made relative path substitutions dialog nicer — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail5">detail</a></li>
	<li>added simple test cases — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail6">detail</a></li>
	<li>fixed import strategy: save a entry first, when checksum got calculated (otherwise a race condition etc. could lead to entries which have no checksum, due to parallel imports) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail7">detail</a></li>
	<li>removed unused code — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail8">detail</a></li>
	<li>removed unused code — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail9">detail</a></li>
	<li>small optimiziation: thumb loading does not load the movie entry — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail10">detail</a></li>
	<li>removed unused code — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail11">detail</a></li>
	<li>fixed type — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail12">detail</a></li>
	<li>fix tag proposal: full word match has not used "_" as a non-word char. — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail13">detail</a></li>
	<li>fix: changed script logging logger name — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail14">detail</a></li>
	<li>use defined scripted actions for tag context menu — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail15">detail</a></li>
	<li>added methods to define actions — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail16">detail</a></li>
	<li>actionentries to add actions via script; — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail17">detail</a></li>
	<li>Added tag v3-9-RC01 for changeset 8cf29af0f0ea — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail18">detail</a></li>
	<li>showing user defined attributes in the tree structures for tags, metatags, entries and movie lists — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail19">detail</a></li>
	<li>showing user defined attributes in the tree structures for tags, metatags, entries — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail20">detail</a></li>
	<li>upgrade to groovy 2.2.1; upgrade also the groovy maven compiler — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail21">detail</a></li>
	<li>provide completions about user defined groovy methods: only the defined ones, not standard methods — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail22">detail</a></li>
	<li>provide completions about user defined groovy methods — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail23">detail</a></li>
	<li>some methods in the script base class to save/load/create script objects — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail24">detail</a></li>
	<li>fix: do not log stacktrace on missing config on config by convention — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail25">detail</a></li>
	<li>update script menu on script changes in the script table — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail26">detail</a></li>
	<li>update events on change of bookmarks or scripts — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail27">detail</a></li>
	<li>derived own class from observable list with helper function for object updates — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail28">detail</a></li>
	<li>saved scripts have now a menu path to structure them in menus (same way like bookmarks) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail29">detail</a></li>
	<li>new icons for bookmarks/folders; using them in the menu structurizer classes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail30">detail</a></li>
	<li>new icons for bookmarks and bookmark folders — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail31">detail</a></li>
	<li>bookmarks have now an attribute "menu path".
Setting a path could be used to show the menus structured by that path in the UI (menus, toolbar); "/" could be used as separator to make multiple groupings in the path — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail32">detail</a></li>
	<li>bookmarks have now an attribute "menu path".
Setting a path could be used to show the menus structured by that path in the UI (menus, toolbar); "/" could be used as separator to make multiple groupings in the path — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail33">detail</a></li>
	<li>using jdesktop bsaf instead of appframework: not really more up to date, but at least we get the source code via maven — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail34">detail</a></li>
	<li>introduced object type to wrap groovy return types — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail35">detail</a></li>
	<li>fix: increased max length of script lob — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail36">detail</a></li>
	<li>draft: groovy functions could be called within filter language with "$" prefix.
at the moment the return type is simply TEXT, nothing other allowed at the moment.
user defined groovy functions could used this way in the filter/grouping langauge — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail37">detail</a></li>
	<li>getter for script — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail38">detail</a></li>
	<li>execution context holds a groovy filter base class for usage of groovy methods within the filter language — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail39">detail</a></li>
	<li>respective java type for a expression type — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail40">detail</a></li>
	<li>executing init scripts on startup of application
with ExpandoMetaClass definitions on GroovyFilterBaseClass in scripts, a user could define user defined functions for filtering and grouping (usage only in groovy mode at the moment) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail41">detail</a></li>
	<li>fix: preselecting the value in the combo cell editor for properties (otherwise it would always select the first hint in the combobox model) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail42">detail</a></li>
	<li>fix: save reminder on closing groovy console for db scripts — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail43">detail</a></li>
	<li>show scripts saved in database in the script menu — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail44">detail</a></li>
	<li>db saved scripts are displayed in a table in the UI. editing via groovy console possible
groovy console has a additional menu to save back the script in the database — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail45">detail</a></li>
	<li>groovy script entity to save groovy scripts in the database — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail46">detail</a></li>
	<li>fix: scripts which are in userdir + /macros/scripts/ are shown in the menu — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail47">detail</a></li>
	<li>fix: swing appframework getActionMap() mess up the groovy consoles backspace action mapping;
the only solution so far: do not use the appframeworks action map method.... — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail48">detail</a></li>
	<li>moving MJConsole to groovy resources; direct usage as groovy class — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail49">detail</a></li>
	<li>better error handling when using groovy for filtering/grouping — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail50">detail</a></li>
	<li>removed unused code — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail51">detail</a></li>
	<li>set version to 3.9 — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail52">detail</a></li>
	<li>Merge — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail53">detail</a></li>
	<li>some helper fumctions defined for easier property access when filtering or grouping with groovy — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail54">detail</a></li>
	<li>removed senseless groovy demo macros — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail55">detail</a></li>
	<li>Merge with groovyFilter — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail56">detail</a></li>
	<li>first simple solution to group via groovy in the grouping editor;
grouping works by beginning with the marker word "groovy" followed by a call to the method group() which gets a list of closures for each group level.
each closure gets called per each joined data row and needs to return a string (the grouping value) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail57">detail</a></li>
	<li>groovy filtering works now by typing a first marker word "groovy" in the editor and then followed by groovy script to filter a joined data row.
(a script which gets called for each joined data row, and which needs to return a boolean) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail58">detail</a></li>
	<li>some groovy shorthand props to access entry, tag, metatag — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail59">detail</a></li>
	<li>shift return in filter editor to create new line; return to update filtering — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail60">detail</a></li>
	<li>shift return in filter editor to create new line; return to update filtering — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail61">detail</a></li>
	<li>Merge with default — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail62">detail</a></li>
	<li>fix: alternative byte size formatting which does not round the size — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail63">detail</a></li>
	<li>updated some lib versions — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail64">detail</a></li>
	<li>fixed concurrent modification problem — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail65">detail</a></li>
	<li>some null attribute value checks — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail66">detail</a></li>
	<li>Merge with default — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail67">detail</a></li>
	<li>new filter function split — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail68">detail</a></li>
	<li>fixed problem with wrong parsing of special symbols within quotes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail69">detail</a></li>
	<li>Merge with attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail70">detail</a></li>
	<li>tag parent ident: prevent NPE — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail71">detail</a></li>
	<li>filter bricks also for tag attrs and metatag attrs — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail72">detail</a></li>
	<li>adding attributes to the filter brick toolbar — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail73">detail</a></li>
	<li>removed weird test case — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail74">detail</a></li>
	<li>fixed testcase — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail75">detail</a></li>
	<li>Merge with default — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail76">detail</a></li>
	<li>Merge with attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail77">detail</a></li>
	<li>limit short syntax for function call to one argument
this makes more sense in most use cases, when e.g. nesting in other function calls, etc. — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail78">detail</a></li>
	<li>making dataview currentseldir at max 4040 chars — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail79">detail</a></li>
	<li>sort attribute list in tooltip — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail80">detail</a></li>
	<li>sort attribute list in attribute editor — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail81">detail</a></li>
	<li>removed unnecessary attach strategy — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail82">detail</a></li>
	<li>compacting the tooltips for the thumb renderers if attribute values are too long — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail83">detail</a></li>
	<li>removed stupid macro — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail84">detail</a></li>
	<li>width of tooltip limited — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail85">detail</a></li>
	<li>fixed npe on attributes editing — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail86">detail</a></li>
	<li>added not working out-commented test cases to the test case — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail87">detail</a></li>
	<li>short form for attribute filtering via functions: e.g. entryattr#attrname — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail88">detail</a></li>
	<li>short form for attribute filtering via functions: e.g. entryattr#attrname — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail89">detail</a></li>
	<li>simple test case for filtering attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail90">detail</a></li>
	<li>show attributes as html table in tooltips; added some simple table styles — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail91">detail</a></li>
	<li>show attributes as html table in tooltips — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail92">detail</a></li>
	<li>common lexer parsing of two character symbols — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail93">detail</a></li>
	<li>fixed testcase — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail94">detail</a></li>
	<li>updated vlcj version to 2.4.1 — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail95">detail</a></li>
	<li>updated vlcj version to 2.4.1 — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail96">detail</a></li>
	<li>updated vlcj version to 2.4.1 — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail97">detail</a></li>
	<li>properties: add popup menu with menus for all existing attr names from the same entity class — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail98">detail</a></li>
	<li>introduced function to embedd groovy scripts into filter expressions.
not sure if this is useful for the end user or more a internal developer feature — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail99">detail</a></li>
	<li>property editor has autocompletion with hints from existing attribute values (per attribute) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail100">detail</a></li>
	<li>fixed layout of property panel a bit — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail101">detail</a></li>
	<li>first draft version of property panel showing all nested elements of property entity — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail102">detail</a></li>
	<li>properties panel shows infos about the last selecte object, either a movie/pic entry, a tag or a meta tag — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail103">detail</a></li>
	<li>functions to access tag and metatag attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail104">detail</a></li>
	<li>cleaned up code — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail105">detail</a></li>
	<li>attributes also for tags and metatags — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail106">detail</a></li>
	<li>first simple working connection to the properties panel — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail107">detail</a></li>
	<li>fix: scanning also inherited methods for annotations — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail108">detail</a></li>
	<li>basic prop editor panel (not yet connected) — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail109">detail</a></li>
	<li>interface for entities which have attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail110">detail</a></li>
	<li>returning JXPanels instead of JPanels — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail111">detail</a></li>
	<li>fix: better error logging on configuration error — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail112">detail</a></li>
	<li>simple function to access the entry attributes — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail113">detail</a></li>
	<li>started attributes branch
movie entry has map with attributes.
attributes get joined fetch loaded when creating the internal cache — <a href="https://jenkins.lang-soft.de/user/lang/">lang</a> / <a href="https://jenkins.lang-soft.de/job/myjaphoo%20release/157/changes#detail114">detail</a></li>
</ol>