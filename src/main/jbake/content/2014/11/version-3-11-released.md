title=Version 3.11 released
date=2014-11-19
type=post
tags=releases
status=published
~~~~~~

<p>A new version has been released!</p>
<p>The new version is available at <a title="https://sourceforge.net/projects/myjaphoo/files/3.11.198/" href="https://sourceforge.net/projects/myjaphoo/files/3.11.198/.">https://sourceforge.net/projects/myjaphoo/files/3.11.198/.</a></p>
<ul>
<li>improved Filterlanguage: Simple searches for words are now possible without any syntax knowledge</li>
<li>Filter: matched text occurrences are now highlighted in the Tree-Table and in the Properties Panel</li>
<li>Filte shortcuts for regex searches, e.g. tag?myregexsearch, see documentation</li>
<li>integrated Exif Attribute View  into the Properties Panel</li>
<li>a view bug fixes and optimizations</li>
</ul>
<p></p>
