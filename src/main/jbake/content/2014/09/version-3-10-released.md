title=Version 3.10 released
date=2014-09-02
type=post
tags=releases
status=published
~~~~~~

A new maintenance version has been released and is available under  <a title="https://sourceforge.net/projects/myjaphoo/files/3.10.197/" href="https://sourceforge.net/projects/myjaphoo/files/3.10.197/">https://sourceforge.net/projects/myjaphoo/files/3.10.197</a>/. This version fixes small bugs and has slightly reworked user interface for dealing with properties:
<ul>
	<li>The Attribute Property Editor has changed and uses now a more convenient user interface using a tree table</li>
	<li>the Look and Feel has changed. Myjaphoo uses now Substantial. All other Plafs are removed</li>
	<li>internal code has been cleaned up, e.g. the Gpars Actor library gets now used instead of own code. This will be also the base to make more improvements for more parallel optimisations in the future</li>
	<li>several small bug fixes</li>
	<li>Import of files works now also to import all file types or files with a special file type (e.g. could be used to import PDF files)</li>
	<li>shell mode to work without gui: has command to import files in shell or useage in scripts</li>
	<li>groovysh mode to work with commands (import of files interactively in command mode)</li>
</ul>