title=New Version 3.2-33 released with Exif support
date=2011-08-09
type=post
tags=releases
status=published
~~~~~~

# New Version 3.2-33 released with Exif support


Exif Extraction:
<ul>
    <li>Exif data gets extracted and saved in the database.</li>
    <li>you can filter by all exif tags</li>
    <li>you can group by all exif tags</li>
</ul>
Grouping by all media attributes possible
<ul>
    <li> the grouping language has been extended to allow groupings by all existing attributes of media files. These attributes could be used to build simple groupings just by the attribute values or by more complex "If ... else " expressions</li>
</ul>
Fixed several issues with the importing of data

Fixed several gui translations

Fixed thumb nail GUI problems

