title=Version 3.2-69 released
date=2011-12-19
type=post
tags=releases
status=published
~~~~~~

This release contains a bug fix for
http://sourceforge.net/apps/mantisbt/myjaphoo/view.php?id=3 regarding the preferences handling, as well as some smaller bugfixes and small changes.

Wish you all a Merry Chrismas out there!
