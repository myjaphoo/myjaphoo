title=Version 3.2-62 released
date=2011-11-05
type=post
tags=releases
status=published
~~~~~~

<strong>French Localisation</strong>

Thanks to Gilles Saint-Denis for his gread work in introducing GUI Internationalisation to myjaphoo!

This version supports now french localisation of the user interface!

This version also has some minor fixes and small features:

- new filter&amp;grouping functions: retaindigits, removedigits, retainletters, removeletters
- new identifier: namewithoutsuffix
- new: filter &amp; grouping language now supports functions in all expression parts
- copy files function, which copies which uses the group structure to build the destination path
