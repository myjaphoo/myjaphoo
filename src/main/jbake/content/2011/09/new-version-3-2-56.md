title=New Version 3-2-56
date=2011-09-16
type=post
tags=releases
status=published
~~~~~~

This version hot-fixes a problem of the 3-2-55 &amp; 3-2-54 version.

There was a bug on some platforms (at least on Linux platforms with older KDE versions) where the application crashed during start up.

If you experience any problems on linux or mac update to 3-2-56.

Greetings
Matt
