title=3-2-45-and-3-2-55-will-not-work-on-linux
date=2011-09-15
type=post
tags=releases
status=published
~~~~~~

There exist a bug in the latest 3-2-54 and 3-2-55 versions:

they do not startup in on linux platforms (and probably all other non-windows platforms).
I´l have to investigate it, but its probably because of a Swingx component that does not support the linux platform.
I will fix this as soon as possible.

For the meanwhile:

- windows users: just take the newest 3-2-55 version
- other platform users: take the 3-2-33 version. it should be stable on all platforms.

Greetings,
Matt
