title=New Maintenance Release Version 3.2-54
date=2011-09-11
type=post
tags=releases
status=published
~~~~~~

A new version with a view bugfixes and small improvements has been released:
<ul>
	<li> separete Exif-Panel</li>
	<li> Easy Search textbox for simple search terms</li>
	<li> small improvements for creating Tags and MetaTags</li>
	<li> changed layout of preferences dialog</li>
	<li> filter-language: bugfix: identifier "x" has not worked</li>
	<li> filter-language: bugfix: exif-expression comparison with nothing has not worked</li>
	<li> reworked Info-Panel</li>
	<li> new function: open folder with explorer for a media file</li>
	<li> new operators: endswith and startswith</li>
</ul>
Have Fun!

Matt
