title=Version 3.13 released
date=2015-11-09
type=post
tags=releases
status=published
~~~~~~

<p>The new version can be downloaded at https://sourceforge.net/projects/myjaphoo/files/3.13.9/</p>
<p>Changes &amp; fixes:</p>
<ul>
<li>several enhancements for groovy scripting to add e.g. own thumb loader, own path substitution logic, etc</li>
<li>updated to latest library versions</li>
<li>fixed bug with event bus and messages</li>
<li>alt-left &amp; alt-right: go history back and forward</li>
<li>aggregations possible, e.g. sum of values</li>
<li>having clause possible to filter by aggregations</li>
</ul>
<p></p>
