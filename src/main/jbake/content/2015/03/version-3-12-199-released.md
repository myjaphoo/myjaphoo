title=Version 3.12.199 released
date=2015-03-21
type=post
tags=releases
status=published
~~~~~~

A new version has been released with some bug fixes:

&nbsp;
<ul>
	<li>works now properly on Java 8</li>
	<li>fixed some groovy scripting issues</li>
	<li>fixed vlc thumbnail provider issues</li>
	<li>new feature to use lists of groupings</li>
</ul>
&nbsp;