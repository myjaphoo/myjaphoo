title=Development
date=2016-05-19
type=page
tags=releases
status=published
~~~~~~

MyJaPhoo is developed in Java 8. It uses JPA for persistence and the Apache Derby Database for saving all data. 
## Prerequesites

 * JDK 1.8 or higher
 * A Java IDE e.g. Netbeans or Intellij IDEA. However, all java IDE´s should work.


## Getting the source code

The Project uses Git SCM. The source is available on bitbucket. You can acces it under <a title="https://bitbucket.org/myjaphoo/myjaphoo/" href="https://bitbucket.org/myjaphoo/myjaphoo/" target="_blank">https://bitbucket.org/myjaphoo/myjaphoo/</a>.

## Running and building the application

The project is configured to work with Netbeans and Intellij IDEA out of the box. After a checkout you should be able to direct start working with these IDE´s. All dependencies (libraries) are checked in. So you should be able to just start the application in Netbeans or Intellij with the "run" menu command. There exists also a ant build script to generate a version with all artefacts e.g. installation routines, etc. which get used by jenkins for CI.<strong> </strong> <strong></strong>

## Branches
There are several branches used in the repository. The main ones are

 * release: this branch contains the code of the release branch, means the versions that are already released. This is usually the place, where bugfixes are contributed.
 * default: the main development branch for the next version.

## Submitting fixes and changes
You are very welcome to work on myjaphoo. If you have bugfixes or changes you want to contribute, the easiest way is to fork the code in bitbucket and check in your changes there. 
 If you need help or have questions contact me at admin@myjaphoo.de.

## Supporting development

There are many ways how you could support MyJaPhoO.

* test the software, report all found probems in the forum or the bugtracker
* if you have ideas for new features or improvements, report them or write me directly
* tell others from this software or write articles, howtos, etc
* write documentation for this software
* translate UI or documentation to other languages
* spend the developer a beer :)
	
	


