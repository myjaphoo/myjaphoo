title=Features
date=2011-11-23
type=page
tags=releases
status=published
~~~~~~


## General Features

 * the application can handle very large sets of media files
 * data gets saved in a relational database (Derby database by default). Therefore its possible to access and use SQL
 * generation of thumbnails
 * simple integrated picture viewer. Movies are viewed via external player (e.g. VLC)
 * Multi Project Support

## Structuring and Categorisation

 * files can be tagged
 * Tags can be structured in trees
 * Tags could be "tagged" by so called metatags. Means its possible to put on a "orthogonal" tag structure.
 * Comments, Titles and Ratings could be added to each File
 * own defined attributes could be added to media files, to tags and metatags
 * extraction of exif data. exif data can be used in filter expressions and in grouping expressions
 * Filterfunction (special Filtersyntax to filter by attributes)
 * Group By functions to produce grouped views of  the material (by predefined attributes or by user defined groupings defined via a Grouping syntax). Multiple groupings are possible.
 * aggregate functions are possible in groupings; "having" clause to filter by aggregated values

## Search of Duplicates

 * Duplicates are identified by a checksum  (CRC32).
 * Group By View to display the duplicates
 * Easy deletion of duplicates
 * View filter for duplicates (to remove the duplicates just in the view)
 * View filter for duplicates which are created by groupings : (due to multiple assignments to different groups)

## Database Comparison

 * Comparison function between different myjaphoo databases (mainly to find out duplicates)

## GUI

 * GUI Layout can be adjusted and different layouts can be saved in so called Perspectives
 * creation of Bookmarks (consist of Filter and group by)
 * Undo-Function for all essential functions
 * Changelog for all essential functions
 * History Function to get back to previous views (and filters and groupings)
 * available in the following languages: english, german, french

## Misc

 * function to copy media files (flat copy, by directory structure or by grouping structure)
 * export of meta information which could be imported into another myjaphoo database
 * Scripting in Groovy is possible. Macros can be added as UI commands to menus

## Thumbnail Views

 * in general the UI is combined by a tree view which contains the media files for navigating  and a  Detail View which shows Thumbs
 * Thumb-List Mode: shows the thumbs with all atributes assigned to a media file
 * regular Thumb View: shows thumbs in a grid
 * Stripe Thumb Mode: shows a stripe for each subcategory of the current selected node in the media tree; Stripes can be opened to show all thumbs of that stripe; all thumbs of a stripe can be selected via double click on the first column
 * you can open multiple UI Main Views which all have different Filters, Group By definitions and views on the data.
