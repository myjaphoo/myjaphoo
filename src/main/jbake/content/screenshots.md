title=Screenshots
date=2011-11-23
type=page
tags=releases
status=published
~~~~~~



<h2>Screenshots from Version 3.8</h2>
This screenshots show the new filter brick features and the new docking look and feel.

<div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/02.png" class="thumbnail">
      <img src="version-3-8/02.png" >
    </a>
  </div>
  
    <div class="col-xs-6 col-md-3">
      <a href="version-3-8/03.png" class="thumbnail">
        <img src="version-3-8/03.png" >
      </a>
    </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/04.png" class="thumbnail">
      <img src="version-3-8/04.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/05.png" class="thumbnail">
      <img src="version-3-8/05.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/06.png" class="thumbnail">
      <img src="version-3-8/06.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/07.png" class="thumbnail">
      <img src="version-3-8/07.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/08.png" class="thumbnail">
      <img src="version-3-8/08.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/09.png" class="thumbnail">
      <img src="version-3-8/09.png" >
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="version-3-8/10.png" class="thumbnail">
      <img src="version-3-8/10.png" >
    </a>
  </div>

</div>


<h2>Older Screenshots</h2>

   <div class="row">
     <div class="col-xs-6 col-md-3">
       <a href="screenshots/sh01.png" class="thumbnail">
         <img src="screenshots/sh01.png" >
       </a>
     </div>
    
    
  <div class="col-xs-6 col-md-3">
    <a href="screenshots/sh02.png" class="thumbnail">
      <img src="screenshots/sh02.png" >
    </a>
  </div>
   

<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh03.png" class="thumbnail">
    <img src="screenshots/sh03.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh04.png" class="thumbnail">
    <img src="screenshots/sh04.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh05.png" class="thumbnail">
    <img src="screenshots/sh05.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh06.png" class="thumbnail">
    <img src="screenshots/sh06.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh07.png" class="thumbnail">
    <img src="screenshots/sh07.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh08.png" class="thumbnail">
    <img src="screenshots/sh08.png" >
  </a>
</div>
   
    
<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh09.png" class="thumbnail">
    <img src="screenshots/sh09.png" >
  </a>
</div>


<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh10.png" class="thumbnail">
    <img src="screenshots/sh10.png" >
  </a>
</div>



<div class="col-xs-6 col-md-3">
  <a href="screenshots/sh11.png" class="thumbnail">
    <img src="screenshots/sh11.png" >
  </a>
</div>


 <div class="col-xs-6 col-md-3">
   <a href="screenshots/sh12.png" class="thumbnail">
     <img src="screenshots/sh12.png" >
   </a>
 </div>
 

  <div class="col-xs-6 col-md-3">
    <a href="screenshots/sh13.png" class="thumbnail">
      <img src="screenshots/sh13.png" >
    </a>
  </div>
      
 <div class="col-xs-6 col-md-3">
        <a href="screenshots/sh14.png" class="thumbnail">
          <img src="screenshots/sh14.png" >
        </a>
      </div>   
                     
 <div class="col-xs-6 col-md-3">
        <a href="screenshots/sh15.png" class="thumbnail">
          <img src="screenshots/sh15.png" >
        </a>
      </div>    
                                   
 <div class="col-xs-6 col-md-3">
        <a href="screenshots/sh16.png" class="thumbnail">
          <img src="screenshots/sh16.png" >
        </a>
      </div>                                               
   </div>