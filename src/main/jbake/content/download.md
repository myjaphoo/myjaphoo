title=Download
date=2011-11-23
type=page
tags=releases
status=published
~~~~~~

You can download the latest version under

## Source Forge

Download latest version from Sourceforge. This is will contain the latest versions in different packages for window, linux, etc:

<p><a  class="btn btn-default" role="button" title="Download at SourceForge" href="http://sourceforge.net/projects/myjaphoo/files/" target="_blank"><img src="sflogo.png" alt="Download at Sourceforge" width="150" height="40" /></a></p>
 
## Computer Bild

or use Download mirror at Computer Bild (this contains only the windows version)

<p><a  class="btn btn-default" role="button" href="http://www.computerbild.de/download/MyJaPhoO-7541036.html"><img title="Computer Bild Downloads" src="cb_logo_downloadbereich.jpg" alt="" width="124" height="86" /></a></p>
 

## GIGA Software

or download from our german partner Giga Software

<p><a  class="btn btn-default" role="button" href="http://www.giga.de/downloads/myjaphoo/"><img title="GIGA Software" src="giga-award.png" alt="" width="124" height="86" /></a></p>



## Updates and Upgrade


All versions are (until now) backward comatible. This means they are able to read a database of an older version, and if necessary they change the database schema to add newly needed things (e.g. new tables or columns). You can simply use your existing myjaphoo database with a newer installation.


## Installation

Please see the <a title="Dokumentation" href="http://myjaphoo.de/docs/installation.html">documentation</a> for detailed installation instructions. In general there is a special installer for windows and an independend installer for all other systems. After installation, there should be a shortcut to start the application.