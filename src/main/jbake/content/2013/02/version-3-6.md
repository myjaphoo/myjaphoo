title=Version 3.6
date=2013-02-21
type=post
tags=releases
status=published
~~~~~~

Version 3.6.103 has been released under <a href="http://sourceforge.net/projects/myjaphoo/files/3.6.103/" target="_blank">http://sourceforge.net/projects/myjaphoo/files/3.6.103/</a>.
<h1></h1>
<h1>New Features</h1>
<h2>Better Auto Completion in Filter Editor and Grouping Editor</h2>
<ul>
	<li>The auto completion in filter and grouping editor reacts now more on the context of the inputted text. The completions propose now also tag names, metatag names, path fragments, comments, title or any exif tag content. This makes it much more powerful in helping to complete an expression.</li>
</ul>
<ul>
	<li>The proposed completions for tags and meta tags do also show a preview thumb in the completion window.</li>
</ul>
<ul>
	<li>The proposed completions take now all completions into account which partially match the already typed in word.</li>
</ul>
<h2>Advanced Compare Function to compare different databases</h2>
The database compare function delivers now detailed information about the differences of attributes. A simple function to merge the changes into the database is also added.
<h2>File View</h2>
A File view is added which shows the file system. This has been introduced to remove the old weird function to combine media tree and files in the same widget. The newly introduced file view has currently two functions:
<ul>
	<li>select a directory directly to import media files</li>
	<li>show all media files in the file view marked with colors</li>
</ul>
<h2>Other changes</h2>
<ul>
	<li>Sorting in the Movie Tree is faster</li>
	<li>Easier variable substitution (using $variablename)</li>
	<li>Validation of database edit dialog (name field)</li>
	<li>documentation uses now <a title="Version 3.5.95" href="http://docs.myjaphoo.de">wiki.</a> no more pdf bundled with the application.</li>
	<li>color mode is now available in all three thumb tabs</li>
	<li>structure nodes do now show the grouping expression rather then the grouping dim; this way all sorts of expressions are shown in the entry tree view where in the past only the word "vgroupyielding" was displayed</li>
	<li>heap indicator text: showing also the max mem</li>
	<li>additional thumb pictures can be added manually</li>
	<li>calculation of checksums can be suppressed in the preferences</li>
	<li></li>
</ul>
<h2>Bug Fixes</h2>
list of fixes: <a href="http://issues.myjaphoo.de/thebuggenie/myjaphoo/issues/find/saved_search/1/search/1" target="_blank">http://issues.myjaphoo.de/thebuggenie/myjaphoo/issues/find/saved_search/1/search/1</a>
<h2>Technical changes</h2>
<ul>
	<li>using maven for the project</li>
</ul><!--:--><!--:en-->Version 3.6.103 has been released under <a href="http://sourceforge.net/projects/myjaphoo/files/3.6.103/" target="_blank">http://sourceforge.net/projects/myjaphoo/files/3.6.103/</a>.
<h1></h1>
<h1>New Features</h1>
<h2>Better Auto Completion in Filter Editor and Grouping Editor</h2>
<ul>
	<li>The auto completion in filter and grouping editor reacts now more on the context of the inputted text. The completions propose now also tag names, metatag names, path fragments, comments, title or any exif tag content. This makes it much more powerful in helping to complete an expression.</li>
</ul>
<ul>
	<li>The proposed completions for tags and meta tags do also show a preview thumb in the completion window.</li>
</ul>
<ul>
	<li>The proposed completions take now all completions into account which partially match the already typed in word.</li>
</ul>
<h2>Advanced Compare Function to compare different databases</h2>
The database compare function delivers now detailed information about the differences of attributes. A simple function to merge the changes into the database is also added.
<h2>File View</h2>
A File view is added which shows the file system. This has been introduced to remove the old weird function to combine media tree and files in the same widget. The newly introduced file view has currently two functions:
<ul>
	<li>select a directory directly to import media files</li>
	<li>show all media files in the file view marked with colors</li>
</ul>
<h2>Other changes</h2>
<ul>
	<li>Sorting in the Movie Tree is faster</li>
	<li>Easier variable substitution (using $variablename)</li>
	<li>Validation of database edit dialog (name field)</li>
	<li>documentation uses now <a title="Version 3.5.95" href="http://docs.myjaphoo.de">wiki.</a> no more pdf bundled with the application.</li>
	<li>color mode is now available in all three thumb tabs</li>
	<li>structure nodes do now show the grouping expression rather then the grouping dim; this way all sorts of expressions are shown in the entry tree view where in the past only the word "vgroupyielding" was displayed</li>
	<li>heap indicator text: showing also the max mem</li>
	<li>additional thumb pictures can be added manually</li>
	<li>calculation of checksums can be suppressed in the preferences</li>
	<li></li>
</ul>
<h2>Bug Fixes</h2>
list of fixes: <a href="http://issues.myjaphoo.de/thebuggenie/myjaphoo/issues/find/saved_search/1/search/1" target="_blank">http://issues.myjaphoo.de/thebuggenie/myjaphoo/issues/find/saved_search/1/search/1</a>
<h2>Technical changes</h2>
<ul>
	<li>using maven for the project</li>
</ul>