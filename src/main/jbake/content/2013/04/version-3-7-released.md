title=Version 3.7 released
date=2013-04-22
type=post
tags=releases
status=published
~~~~~~

Download the new version under <a title="http://sourceforge.net/projects/myjaphoo/files/3.7.105/" href="http://sourceforge.net/projects/myjaphoo/files/3.7.105/">http://sourceforge.net/projects/myjaphoo/files/3.7.105/</a>
<h2>Java 7</h2>
Starting with the version 3.7 the application needs now a java 7 runtime. Please be sure that you have java 7 installed before you try to update to 3.7.
<h2>New Features</h2>
<ul>
	<li>Direct switch to another database from the user menu</li>
	<li>Divided preferences in some which are related to a specific database and therefore saved in the database and others which are system wide.</li>
</ul>
<h2></h2>
<h2>Technical changes</h2>
<ul>
	<li>Library updates, e.g. update to hibernate 4.1</li>
	<li>changed code to use slf4j instead of directly calling log4j</li>
</ul>
<h2></h2>
<h2>Bug Fixes</h2>
<ul>
	<li>Thumb nail creation under Windows fixed. Tools and methods to create thumbnails consolidated. Different providers could be chosen for thumbnail and movie info generation.</li>
	<li>Fixed tag proposal grouper</li>
	<li>fix: missing shortcut for uninstallation on windows</li>
</ul><!--:--><!--:en-->Download the new version under <a title="http://sourceforge.net/projects/myjaphoo/files/3.7.105/" href="http://sourceforge.net/projects/myjaphoo/files/3.7.105/">http://sourceforge.net/projects/myjaphoo/files/3.7.105/</a>
<h2>Java 7</h2>
Starting with the version 3.7 the application needs now a java 7 runtime. Please be sure that you have java 7 installed before you try to update to 3.7.
<h2>New Features</h2>
<ul>
	<li><span class="Apple-style-span" style="line-height: 13px;">Direct switch to another database from the user menu</span></li>
	<li>Divided preferences in some which are related to a specific database and therefore saved in the database and others which are system wide.</li>
</ul>
<h2></h2>
<h2>Technical changes</h2>
<ul>
	<li><span class="Apple-style-span" style="line-height: 13px;">Library updates, e.g. update to hibernate 4.1
</span></li>
	<li>changed code to use slf4j instead of directly calling log4j</li>
</ul>
<h2></h2>
<h2>Bug Fixes</h2>
<ul>
	<li>Thumb nail creation under Windows fixed. Tools and methods to create thumbnails consolidated. Different providers could be chosen for thumbnail and movie info generation.</li>
	<li><span class="Apple-style-span" style="line-height: 13px;">Fixed tag proposal grouper</span></li>
	<li>fix: missing shortcut for uninstallation on windows</li>
</ul>