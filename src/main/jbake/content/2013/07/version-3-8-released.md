title=Version 3.8 released
date=2013-07-29
type=post
tags=releases
status=published
~~~~~~

<h2>Version 3.8.121 released</h2>
You can download the version under <a href="http://sourceforge.net/projects/myjaphoo/files/3.8.121/">http://sourceforge.net/projects/myjaphoo/files/3.8.121/</a>.

&nbsp;
<h2>Filter Bricks</h2>
<a href="http://myjaphoo.de/wp-content/uploads/2013/07/3804.png"><img alt="3804" src="http://myjaphoo.de/wp-content/uploads/2013/07/3804-300x42.png" width="426" height="59" /></a>

The new version 3.8 contains a nice new feature for quickly filtering data without knowledge of the underlying filter language. This makes it much easier to work with filters.  You can now simply add a "brick" for a identifier e.g. for dir, tag, exif-tags, etc, and then simply choose the value to filter from a combobox.

<a href="http://myjaphoo.de/wp-content/uploads/2013/07/3801.png"><img alt="3801" src="http://myjaphoo.de/wp-content/uploads/2013/07/3801-300x22.png" width="484" height="35" /></a>

Of course it has not the full power of the filter language, but in most cases this is enough to quickly filter and browse the data, and this all without deeper knowledge of the language itself in detail.  The filter bricks get combined with the regular filter expression using a logical "and". So you can combine simple filter tasks and sophisticated filtering within the filter editor.

A bookmark will now also save the status of the filter bricks.
<h2>Show ability of external tools</h2>
<a href="http://myjaphoo.de/wp-content/uploads/2013/07/3802.png"><img alt="3802" src="http://myjaphoo.de/wp-content/uploads/2013/07/3802-227x300.png" width="227" height="300" /></a>

There is now also a visual to direct show all configured external tools. This way you see, if all necessary tools like VLC, mplayer, etc. is properly configured for myjaphoo.
<h2>Minor changes to user interface</h2>
<a href="http://myjaphoo.de/wp-content/uploads/2013/07/3803.png"><img alt="3803" src="http://myjaphoo.de/wp-content/uploads/2013/07/3803-236x300.png" width="236" height="300" /></a>

There are slightly changes and fixes to the user interface. E.g. the properties panel is now on the right side. Information that is displayed here may also contain hyperlinks to open file explorer or a new view filtered by the hyperlink value.
<h2></h2>
<h2>More Groovy</h2>
The groovy command language is integrated in a more consolidated way. It contains now commands to manipulate the database entities. The main purpose for this is the ability to apply batch update scripts if there is a need to re-structure database content.
<h2>Screenshots</h2>
[nggallery id=2]
<h2>Bug fixes and smaller changes</h2>
<ul>
	<li>fix: escape the filtered object value from identifier filter bricks</li>
	<li>simple hotfix to prevent race conditions that happen in the message tree model</li>
	<li>fixed problem with thumb height, not respecting the additional size for the label text</li>
	<li>reworked look and feel selection; now possible via menu; - fix bread crumbs: showing root handles, otherwise the root handles will not be shown by some plafs</li>
	<li>using horizontal layout to fix layout problems in other plafs</li>
	<li>using the shape gradient docking theme for infonode docking windows</li>
	<li>fixed regex NPE problems</li>
	<li>tray icon: updates bookmark menus when bookmarks change</li>
	<li>chronic table updates now, if a new chronic gets added</li>
	<li>show execution errors of filtering in the errors panel</li>
	<li>throw a parser exception if regular expression parsing fails</li>
	<li>fix: re-throw error on update</li>
	<li>added css for UL lists to make it a bit nicer</li>
	<li>show context menus to play movies only when movies are selected in the context</li>
	<li>moved properties panel to the right side; seems to make more sense</li>
	<li>showing hyperlinks in properties panel, too;</li>
	<li>fix: really fail during refresh process with exception to prevent subsequent errors that occur when the ui tries to update an invalid model</li>
	<li>fixed problems with desctruction of movie node</li>
	<li>synonym operator "~" for regex</li>
	<li>html links to open directory of a node</li>
	<li>links in html for tags and metatags that open a new view when clicked on</li>
	<li>simple groovy shell panel added (not as functional as groovy console, but maybe usable to set up simple commands</li>
	<li>fix: make regex case insensitiv in filter language</li>
	<li>showing system properties in separate view instead as subtab in info view; using a table instead of tree, this makes searches/filtering easier</li>
</ul>