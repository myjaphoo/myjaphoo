= Chronic
Matt Lang
2015-12-19
:jbake-type: docpage
:jbake-status: published
:jbake-tags:
:idprefix:
:icons: font
:toc: left
:jbake-category: panel
:jbake-panel: Basic Functions
:jbake-menutitle: Chronik


The application saves all previously used filter groupings. You can see them in the Chronic Menu or the Chronic Panel. You can switch back to a previously chronic entry by clicking on the respective chronic entry.
