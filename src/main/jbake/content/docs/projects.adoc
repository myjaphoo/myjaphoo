= Projects
Matt Lang
2015-12-19
:jbake-type: docpage
:jbake-status: published
:jbake-tags:
:idprefix:
:icons: font
:toc: left
:jbake-category: panel
:jbake-panel: Basic Functions
:jbake-menutitle: Projects

If you start myjaphoo it opens a "default" project, which is usually saved in your local user folder in a subfolder ".myjaphoo".
This is pretty enough as long as you work only with one collection of media files.

If you have different collections of media files which you want to manage separately with myjaphoo you can create different projects for them.

You can create as many projects as you like and open many projects concurrently within the application. Each project will be
opened in a separate window, and each project will contain its own data, configuration and properties.


The menu "Project" contains the functions to deal with projects.

== create a new project

When you click the menu "Project->New Project", a dialog opens where you could define a folder, a name and a database engine.
When you click "ok", the project gets created, and a new window opens, showing the project. You could then continue to import files, etc.

== open an existing project
When you click the menu "Project->Open Project", a dialog opens where you can select the folder of an existing project.
When you clock "ok", the project gets loaded and a new window opens which displays the projects content.


== Project Contents

A project contains the following data

- all data about media entries, tags, metatags and attributes, exif data, etc
- the connection configuration to the database and usually the database files for local databases
- the configuration options
