
= Scripting
Matt Lang
2015-12-19
:jbake-type: docpage
:jbake-status: published
:jbake-tags:
:idprefix:
:icons: font
:toc: left
:jbake-category: panel
:jbake-panel: Scripting and Extending


Since 3.9. groovy is heavily integrated into the application. You can define groovy scripts to manually be executed, or define scripts to run during application start. With this you are able to extend the functionality and user interface of the application.

What you can do is

  * define additional menu points (actions)
  * define additional filter and grouping functions
  * batch scripts to manipulate the database content
  * 


You can open scripts in the Scripting Menu. A groovy console opens where you can directly edit the script or execute the script. You can save the script with File->Save in Database. 

=== Groovy Scripts

Scripts are organized in the scripts panel. Here you see all available scripts of the myjaphoo database. You can create new ones, edit or delete existing ones.
With the type field you can set a script to be executed on the startup of the application.

You can group scripts in sub menus. There is a field menuPath where you can put in a menu path where the script should appear. You can use ”/” to build sub-menus. E.g. if you set a menupath “mygroup/mysubgroup” the script will be placed in submenu mysubgroup under mygroup. 


=== Groovy Filter and Grouping Functions

You could use groovy expressions instead of the filter and grouping language in the respective filter and grouping input text areas.
This is a feature that is not designed for the normal end user as it needs knowlegde about the internal object structure.
It is mainly intended to have more flexibility or to prototype during development.
Nevertheless, if you need the full power and flexibility of a script language, you are able to use it directly within the filter and
grouping functionality. You could e.g. define own methods and then use them in filter.

==== Filter with Groovy

You could put a groovy expression into the filter input field. The text must start with the word "groovy" so that the application
knows that now follows groovy statements.
You could then type in a groovy expression (anything that is allowed in a groovy script, e.g. methods, expressions, control structures).
The result of this script must yield a boolean expression. The script is executed for each "row" of the data.
Each "row" is a combination of a entry, tag, meta tag. You can access the row variables by using "entry", "tag", or "metatag".

example filter expression which filters entries with names which contain the word "holiday":
[source,java]
----
groovy entry.name ==~ /.*holiday.*/
----

a example of using a own defined method:
[source,java]
----
groovy def isMov(x) { x.endsWith('.mpg') }; isMov(entry.name)
----

==== Grouping with Groovy

TODO

==== Using defined Groovy Functions in the Filter and Grouping Language

Another possibility is to define groovy methods which could then be used directly in the filter and grouping language.
This makes sense, when you want to enhance the application by plugins or scripts. You could then define those methods during
the initialisation of the plugin (or script). After that the methods could be used with the substitution syntax for bookmarks
in the filter language.

TODO example

== Contexts to save state relevant information for a filter or grouping action ==

TODO

=== Definig Actions with Groovy

There are simple commands in the scripting context to create Actions which are added as menus to contexts, like
tag context menu, entry, context menu and meta tag context menu. You can simply define such actions by defining a action, e.g
as follows:


[source,java]
----
        defTagContextAction("myaction") {
                name = "Hello World!";
                descr = "a description about this action"
                // a action closure which gets the context: the controller and a list of selected tags:
                action= { controller, tags ->  controller.message "hi, I got the following tags: $tags"}
        }
----

for a full list of all possibilities, have a look at link:MJScriptBaseClass.html[Base class for scripting]









