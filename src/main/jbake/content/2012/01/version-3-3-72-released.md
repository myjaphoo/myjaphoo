title=Version 3.3.72 released
date=2012-01-24
type=post
tags=releases
status=published
~~~~~~

This new version has the following improvements:
<ul>
	<li> Autocompletion with Ctr-Space in the Filter Editor, Group By Editor and Bookmark-Table-Editing</li>
</ul>
<ul>
	<li> Function to update a saved bookmark</li>
</ul>
<ul>
	<li> reworked filer &amp; grouping: works now as expected from sql databases when dealing with relations to tags and metatags</li>
</ul>
<ul>
	<li> redesigned the group by panel: it consists now only of the free text panel; the predefined groupings and combinations are changed to popup buttons; this gives a bit more place in the ui and it more concise</li>
</ul>
<ul>
	<li> added german localisation</li>
</ul>
<ul>
	<li> actions work now more in parallel: e.g. it is possible to copy files and meanwhile continue to browse the collection</li>
</ul>
<ul>
	<li> messages view which shows the status of actions</li>
</ul>
<ul>
	<li> reworked the thumb grid view: size changes let the cells automatically "float" with respect to the changed size; Selection is now as expected from other programs: could select multiple ranges and also individual cells with shift and ctrl key.</li>
</ul>
