title=Version 3.4.88 released!
date=2012-03-29
type=post
tags=releases
status=published
~~~~~~



<h2></h2>
<h2>Support for Oracle, MySQL, Postgres and H2</h2>
This version supports now a view other database vendors. You can configure additional database connections and use them as main database for the application or for usage with the database comparison function.

There is also a simple function integrated to copy over the data from one database to another. This is great if you want to decide to switch your database vendor and port your existing data.
<h2>Card View</h2>
The Thumbnail view and the Stripes view can now display "cards" of the thumbs where some additional information gets displayed at the right side of each thumbnail.
<h2>List of all bug fixes and improvements</h2>
<ol>
	<li>new: icon for tags &amp; metatags</li>
	<li>fix: showing roothandles for structure combination pop up tree; in some plafs this is needed</li>
	<li>fix: set backgroundcolor for taskpanecontainer to make it a bit nicer for other plafs</li>
	<li>fix: the background color for thumbnails can now be set explicit (or not set); this makes it nicer when having other plafs activated to fit into the color scheme</li>
	<li>new nimrod version + preferences to select one of the predefined nimrod themes</li>
	<li>fixed drag&amp;drop problem; refactoring of "old" code which was for tables... one part was missing...</li>
	<li>fix: key "r" changed to alt-r</li>
	<li>fix: abbreviating the names of thumbs to preventing the text to flow to a second row which gets not displayed properly. The name now also is not formatted anymore with additional information, as this gets displayed entirely with overlay icons within the thumb</li>
	<li>icon for condensed files</li>
	<li>fixed problem with keyboard shortcuts: "a", "d", "n" changed to alt-a, alt-d, alt-n, because they collided with the jlist feature to jump to the first entry with the given letter</li>
	<li>previewpanel shows now preview picture, if one single movie node gets selected in the movie tree</li>
	<li>i18n for ImportWithActorsSwingWorker</li>
	<li>fix: i18n for TokenTree</li>
	<li>fix: i18n for ChangeLogType</li>
	<li>fix: prevent concurrent modification exceptions</li>
	<li>fix: selection stripes are 6 px thick now...</li>
	<li>first simple app icon added: copied from openclipart.org</li>
	<li>new: polaroid icon for non existing thumbs displayed</li>
	<li>new: show tray icon message when long taking tasks are finished (longer than 5 s)</li>
	<li>localisation for database comparison class; for en and de</li>
	<li>new: simple thumb card view added; card view for both regular thumb view and stripe view selectable</li>
	<li>set homepage link to myjaphoo.de</li>
	<li>changed menu layout: organized more experimental functions into a menu "Experimental"</li>
	<li>new: comparison with database uses now one of the defined database connections; is therefore possible for any of the supported databases</li>
	<li>using submenu for new view in the tray icon</li>
	<li>new: showing database connection url in Info Panel</li>
	<li>new: the tray icon has now menus to open a new view defined by a bookmark</li>
	<li>emphasises message method added to channel model; to inform about important messages</li>
	<li>export function: expermimental function to export all data to another database</li>
	<li>experimental support for h2; h2 driver added to package</li>
	<li>showing icon as feedback if driver is available; button to test database connection added</li>
	<li>fix: message dialog was an options dialog instead...</li>
	<li>added preferences to use one of the defined database configurations as database for startup</li>
	<li>refactored code to use now DatabaseConfiguration objects to access databases</li>
	<li>database connection edit dialog added</li>
	<li>new view for databaseconfiguration added</li>
	<li>database configuration + driver parameter + default parameter value definition</li>
	<li>basic driver and parameter definition classes added</li>
	<li>basic abstractions for database connection parameter; used now also by the comparison database code</li>
	<li>Heapindicator shows now current usedsize of size instead of only a percent value</li>
	<li>new feature: Bookmark- Changelog- and Chronictable have now table filters to filter the content</li>
	<li>added tablefilter-swing library v.4.3.0 from coderrazzi</li>
	<li>fixed some problems with braces and operators; brace nesting was not on term parsing level</li>
	<li>fixed some problems with braces and operators</li>
	<li>fix: installation: the windows link for the starter.jar has not worked;</li>
	<li>now there are only links for starting via the start.jar rather than links to run.bat and run.sh.</li>
	<li>new: code completion now shows a summary part that shows some additional information</li>
	<li>bugfix: fixed bug in regextract function; did only work properly if pattern was found at the beginning of a string</li>
</ol>
