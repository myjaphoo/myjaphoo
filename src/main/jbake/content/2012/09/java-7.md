title=Java 7
date=2012-09-29
type=post
tags=releases
status=published
~~~~~~

<h1>Support of Java 6</h1>
Hi Everybody out there,

I am currently investigating to move forward to Java7 as requirement for myjaphoo. If it makes sense I would start beginning with version 3.6 of myjaphoo to develop and explicitley use features from Java 7, and therefore end support for Java6.. Following points make me come to the decision:
<ul>
	<li>Java 6 EOL date is terminated at November 2012, which means the support for the users ends anyway soon</li>
	<li>for desktop applications it is not really critical to install a new java runtime. So it should be fairly easy for all users to move to Java7</li>
	<li>Java7 brings some new functionality and hopefully also a bit performance improvements which could be interesting: this include the new java.nio file handling which is said to be faster; Watchservice to watch changes on files and directories could be also a interesting feature for usage in myjaphoo.</li>
</ul>
I am really  interested in hearing your comments.<!--:--><!--:en--><h1>Support of Java 6</h1>
Hi Everybody out there,

I am currently investigating to move forward to Java7 as requirement for myjaphoo. If it makes sense I would start beginning with version 3.6 of myjaphoo to develop and explicitley use features from Java 7, and therefore end support for Java6.. Following points make me come to the decision:
<ul>
	<li>Java 6 EOL date is terminated at November 2012, which means the support for the users ends anyway soon</li>
	<li>for desktop applications it is not really critical to install a new java runtime. So it should be fairly easy for all users to move to Java7</li>
	<li>Java7 brings some new functionality and hopefully also a bit performance improvements which could be interesting: this include the new java.nio file handling which is said to be faster; Watchservice to watch changes on files and directories could be also a interesting feature for usage in myjaphoo.</li>
</ul>
