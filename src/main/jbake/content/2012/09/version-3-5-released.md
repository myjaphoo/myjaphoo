title=Version 3.5 released!
date=2012-09-29
type=post
tags=releases
status=published
~~~~~~


Version 3.5 has released with some small improvements and bug fixes.
<ul>
	<li>fixed movie node selection: the selection in the movie tree got always "lost" when selecting a bookmark or chronic entry.</li>
	<li>added some icons to context menu actions (delete, remove, tag, copy, play actions)</li>
	<li>bookmarks and chronic saves now also the selected tab of the thumb panel</li>
	<li>reworked Breadcrumbbar, "Last actions" bar, and also the Bookmark bar to properly show all items if screen is too small.</li>
	<li>added some more fancy autocompletion stuff</li>
	<li>new filter language functions: strlen</li>
	<li>new commands to open and edit movie entries via the java desktop api: this will use the program which is defined by the file type in the underlying OS to open or edit the file.</li>
	<li>new rsyntax text area verions 2.0.3</li>
	<li>reworked filter panel and filter error panel: error panel now properly shows error of filter and/or group by panel as a tree list of errors.</li>
	<li>Filter panel is now a separate view (which could be moved, resized, tabbed as all other views)</li>
	<li>new swingx 1.6.4</li>
	<li>new operator &lt;&gt;</li>
</ul>
You can download the latest version under https://sourceforge.net/projects/myjaphoo/files/3.5.94/<!--:--><!--:en-->Version 3.5 has released with some small improvements and bug fixes.
<ul>
	<li>fixed movie node selection: the selection in the movie tree got always "lost" when selecting a bookmark or chronic entry.</li>
	<li>added some icons to context menu actions (delete, remove, tag, copy, play actions)</li>
	<li>bookmarks and chronic saves now also the selected tab of the thumb panel</li>
	<li>reworked Breadcrumbbar, "Last actions" bar, and also the Bookmark bar to properly show all items if screen is too small.</li>
	<li>added some more fancy autocompletion stuff</li>
	<li>new filter language functions: strlen</li>
	<li>new commands to open and edit movie entries via the java desktop api: this will use the program which is defined by the file type in the underlying OS to open or edit the file.</li>
	<li>new rsyntax text area verions 2.0.3</li>
	<li>reworked filter panel and filter error panel: error panel now properly shows error of filter and/or group by panel as a tree list of errors.</li>
	<li>Filter panel is now a separate view (which could be moved, resized, tabbed as all other views)</li>
	<li>new swingx 1.6.4</li>
	<li>new operator &lt;&gt;</li>
</ul>
You can download the latest version under https://sourceforge.net/projects/myjaphoo/files/3.5.94/