title=Version 3.5.95
date=2012-10-28
type=post
tags=releases
status=published
~~~~~~

A Hot fix version has been released!

Please update existing 3.5 releases with this one, as in 3.5.94 there are some bugs when using a new created database: you are not able to get a context menu and you can not create tags.

Download the version under <a href="https://sourceforge.net/projects/myjaphoo/files/3.5.95/">http://sourceforge.net/projects/myjaphoo/files/3.5.95/</a><!--:--><!--:en-->A Hot fix version has been released!

Please update existing 3.5 releases with this one, as in 3.5.94 there are some bugs when using a new created database: you are not able to get a context menu and you can not create tags.

Download the version under <a href="https://sourceforge.net/projects/myjaphoo/files/3.5.95/">http://sourceforge.net/projects/myjaphoo/files/3.5.95/</a>

