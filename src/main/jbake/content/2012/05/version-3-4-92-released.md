title=Version 3.4.92 released!
date=2012-05-15
type=post
tags=releases
status=published
~~~~~~

A new maintenance version has been released!

Download under <a title="3-4-92" href="http://sourceforge.net/projects/myjaphoo/files/3.4.92/">http://sourceforge.net/projects/myjaphoo/files/3.4.92/</a>

&nbsp;

List of bug fixes:
<ol>
	<li>minimal fix to start up the application, if a user defined database connection is not available, or not properly configured.
In that case, the application shows an error dialog, and after that tries to startup with the default database configuration.</li>
	<li>fix: db comparison german translations where mixed up</li>
	<li>fix: made ui refresh differenziate between full refreshs of all views or only refresh of movie and thumb view (which is most of the time enough)</li>
	<li>fix: do not update full tag or metatag tree, if change events, such as assignements, newly added tags or metatags, deletion of tags and metatags happens</li>
	<li>fix: do not update full tag tree, if a new tag is added</li>
	<li>fix: do not update the full tag tree after assigning a tag to a movie; using finer event mechanism which updates only the changed nodes, without fully rebuilding the tag tree</li>
	<li>added some log messages</li>
	<li>added toString method for debugging</li>
	<li>removed unused Observer base class</li>
	<li>added some logging information</li>
	<li>refactored: finder level of event handling when db objects change</li>
	<li>fix: added submenu for exif identifiers in the info help button</li>
	<li>fix: use id for database configurations; fixed problem when refreshing the database configuration table</li>
	<li>fix: the compare database menu has not been updated, if database connections have been added or changed</li>
	<li>nicer: show crc checksum in hex</li>
	<li>fixed FS#117: nullpointer exception when else part is empty and check for tag/metatag is needed</li>
	<li>fix: removed unneccessary separator</li>
	<li>fix: metatags could not be edited in the metatag tree</li>
	<li>fixed tool tip descriptions</li>
	<li>fixed abbreviation of file names for thumbnails (thanks to nice26 for submitting this fix)</li>
</ol><!--:--><!--:en-->A new maintenance version has been released!

Download under <a title="3-4-92" href="http://sourceforge.net/projects/myjaphoo/files/3.4.92/">http://sourceforge.net/projects/myjaphoo/files/3.4.92/</a>

&nbsp;

List of bug fixes:
<ol>
	<li>minimal fix to start up the application, if a user defined database connection is not available, or not properly configured.
In that case, the application shows an error dialog, and after that tries to startup with the default database configuration.</li>
	<li>fix: db comparison german translations where mixed up</li>
	<li>fix: made ui refresh differenziate between full refreshs of all views or only refresh of movie and thumb view (which is most of the time enough)</li>
	<li>fix: do not update full tag or metatag tree, if change events, such as assignements, newly added tags or metatags, deletion of tags and metatags happens</li>
	<li>fix: do not update full tag tree, if a new tag is added</li>
	<li>fix: do not update the full tag tree after assigning a tag to a movie; using finer event mechanism which updates only the changed nodes, without fully rebuilding the tag tree</li>
	<li>added some log messages</li>
	<li>added toString method for debugging</li>
	<li>removed unused Observer base class</li>
	<li>added some logging information</li>
	<li>refactored: finder level of event handling when db objects change</li>
	<li>fix: added submenu for exif identifiers in the info help button</li>
	<li>fix: use id for database configurations; fixed problem when refreshing the database configuration table</li>
	<li>fix: the compare database menu has not been updated, if database connections have been added or changed</li>
	<li>nicer: show crc checksum in hex</li>
	<li>fixed FS#117: nullpointer exception when else part is empty and check for tag/metatag is needed</li>
	<li>fix: removed unneccessary separator</li>
	<li>fix: metatags could not be edited in the metatag tree</li>
	<li>fixed tool tip descriptions</li>
	<li>fixed abbreviation of file names for thumbnails (thanks to nice26 for submitting this fix)</li>
</ol>