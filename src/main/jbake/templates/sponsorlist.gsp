<div>

    <p><a href="http://www.jetbrains.com/idea/" target="_blank"><img
            src="${content.rootpath}icon_IntelliJIDEA.png"
            alt="jetbrains.com"/></a></p>

    <p><a href="http://www.netcup.de" target="_blank"><img
            src="${content.rootpath}netcup-hlogo-b110h50.png" width="110" height="50"
            alt="netcup.de"/></a></p>

</div>