<div id="sidebar" class="col-md-3">
    <aside>
        <section>
            <h4>Posts</h4>

            <div>
                <% include "blogshortlist.gsp" %>

            </div>

            <hr/>

            <h4>Sponsors</h4>

            <%include "sponsorlist.gsp"%>

        </section>
    </aside>
</div>

