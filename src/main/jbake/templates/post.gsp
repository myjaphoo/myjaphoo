<% include "header.gsp" %>

<% include "menu.gsp" %>

<div class="page-header">
    <h1>${content.title}</h1>
</div>

<p><em>${content.date.format("dd MMMM yyyy")}</em></p>

<%
    def allTagsFlat = published_posts.collectMany { [it.tags] }.flatten()

    def tagsByCounts = allTagsFlat.groupBy { it }.collectEntries { [it.key, it.value.size()] };

    def tagsByCountsOfThisPost = tagsByCounts.findAll { content.tags.contains(it.key) }
    tagsByCountsOfThisPost.each { tag, count -> %>
<a href="${content.rootpath}tags/${tag}.html" class="btn btn-xs btn-info" role="button">$tag <span class="badge">$count</span> </a>

<% } %>

<p>${content.body}</p>

<hr/>


<% def disqusId = content.title.hashCode() * 31 + content.date.hashCode()     %>

<div id="disqus_thread"></div>
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */

     var disqus_config = function () {
     //this.page.url = "myjaphoo.de";  // Replace PAGE_URL with your page's canonical URL variable
     this.page.identifier = "${disqusId}" //PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
     };

    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//myjaphoo.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>



<!-- right place? -->
<script id="dsq-count-scr" src="//myjaphoo.disqus.com/count.js" async></script>




<% include "footer.gsp" %>