

	<%
	    def postList = published_posts;
		if (postList.size() > 10) {
			postList = postList.subList(0,9);
		}
		postList.each {post ->%>
		<a href="<% if (content.rootpath) { %>${content.rootpath}<%
                } %>${post.uri}">${post.title}</a>
		<p>${post.date.format("dd MMMM yyyy")}</p>

  	<%}%>
	

	<p>Older posts are available in the <a href="${content.rootpath}${config.archive_file}">archive</a>.</p>
