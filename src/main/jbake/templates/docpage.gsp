<%include "header.gsp"%>

	<%include "docmenu.gsp"%>
	
	<div class="page-header">
		<h1>${content.title}</h1>
	</div>

<div class="row">

  <div class="col-md-9">


<p><em>${content.date.format("dd MMMM yyyy")}</em></p>

	<p>${content.body}</p>

	<hr />

</div> <!-- /.col-md9/12 -->
	<%include "docsidebar.gsp"%>
</div> <!-- /.row -->

<%include "footer.gsp"%>