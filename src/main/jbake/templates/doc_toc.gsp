<%

    def panelOrderList = config.site_panel_order;

    def filteredPages = docpages.findAll {
        it.category != null && it.category.contains("panel") && it.status == "published"
    }
    .sort { it.panel != null ? panelOrderList.indexOf(it.panel) : 999999 }
    def pagesByPanel = filteredPages.groupBy { it.panel != null ? it.panel.trim() : "" }
%>
<% pagesByPanel.each { panel, pages -> %>
<div class="paragraph">
    <p><b>$panel</b></p>
</div>


<ul>
    <% pages.sort { it.menusortkey != null ? it.menusortkey: 999999 }.each { page -> %>

    <li><a href="<% if (content.rootpath) { %>${content.rootpath}<%
            } %>${page.uri}">${page.menutitle ?: page.title}</a></li>

    <% } %>
</ul>
<% } %>