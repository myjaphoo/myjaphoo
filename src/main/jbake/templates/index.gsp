<% include "header.gsp" %>

<% include "menu.gsp" %>


<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>MyJaPhoO</h1>

        <p>A photo manager application to organize large photo or video collections with some unique features not found in any other photo manager software</p>

        <p><a class="btn btn-primary btn-lg" href="http://sourceforge.net/projects/myjaphoo/files/"
              role="button">Download Now</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="row">
            <div class="col-md-4">
                <h2>Platform Independent</h2>

                <p>MyJaPhoO is programmed in Java and can therefore run on any computer platform that supports Java (e.g. Windows, Linux, MacOs, Unix...)</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Unique Filter</h2>

                <p>You are able to filter by any attribute of a media file. It can group the result by any expression derived by attributes. The flexibility goes near the usage of a sql database tool - but without introducing the complexity of such a tool</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Group Data</h2>

                <p>You can group and aggregate the result by any expression derived by attributes. The flexibility goes near the usage of a sql database tool - but without introducing the complexity of such a tool</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <h2>SQL Database</h2>

                <p>MyJaPhoO uses internally a sql database to save all additional information for media files. Its also possible to use multiple databases. MyJaPhoO supports Apache Derby, Oracle, MySQL, Postgres and H2</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Open Source</h2>

                <p>The software is open source and free for usage and redistribution. You are also invited to work on the development of MyJaPhoO</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Extensible</h2>

                <p>it is extensible via the Groovy Language. You could plug in additional functionality with groovy scripts</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <h2>Multiple Projects</h2>

                <p>The application supports dealing with multiple projects. You could create and open as many independent projects as you want</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Semantic Data</h2>

                <p>Assign tags to your photos and movies. Group them hierarchical. Assign attributes to photos, tags and metatags. Build semantic information on your data. Filter, group and browse then your semantic information.</p>

                <p><a class="btn btn-default" href="${content.rootpath}features.html"
                      role="button">View details &raquo;</a></p>
            </div>

            <div class="col-md-4">
                <h2>Latest News</h2>
                <% include "blogveryshortlist.gsp" %>
            </div>

        </div>
    </div>
    <hr>

</div> <!-- /container -->


<% include "footer.gsp" %>