</div>
<div id="push"></div>
</div>

<div id="footer">
    <div class="container">
        <p class="muted credit">&copy; Matt Lang 2015 <a
                href="mailto:admin@myjaphoo.de">admin@myjaphoo.de</a> | Mixed with <a
                href="http://getbootstrap.com/">Bootstrap v3.1.1</a> | Baked with <a
                href="http://jbake.org">JBake ${version}</a><% println(" at ${new Date().toString()}") %>
        </p>

        <div>
            <div class="row">
                <div class="col-md-4">
                    <h2>Sponsors</h2>

                    <% include "sponsorlist.gsp" %>
                </div>

                <div class="col-md-4">

                    <h2>Resources</h2>

                    <div class="paragraph">
                        <p>Main page | <a href="http://myjaphoo.de">myjaphoo.de</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Documentation | <a href="http://myjaphoo.de/docs/index.html">myjaphoo.de/docs</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Issue Tracker | <a href="https://bitbucket.org/myjaphoo/myjaphoo/issues"
                                              class="bare">issues</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Forum  | <a href="http://sourceforge.net/p/myjaphoo/discussion/"
                                       class="bare">discussion</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Download | <a href="http://sourceforge.net/projects/myjaphoo/files/"
                                         class="bare">sourceforge</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Source code | <a href="https://bitbucket.org/myjaphoo/myjaphoo"
                                            class="bare">bitbucket</a></p>
                    </div>

                    <div class="paragraph">
                        <p>Contact | <a href="mailto:admin@myjaphoo.de">admin@myjaphoo.de</a></p>
                    </div>

                </div>

                <div class="col-md-4">
                    <h2>Blog Tags</h2>

                    <%
                        def allTagsFlat = published_posts.collectMany { [it.tags] }.flatten()

                        def tagsByCounts = allTagsFlat.groupBy { it }.collectEntries { [it.key, it.value.size()] };

                        tagsByCounts.each { tag, count -> %>
                    <p><a href="${content.rootpath}tags/${tag}.html">$tag <span class="badge">$count</span></a></p>

                    <% } %>
                </div>

            </div>
        </div>

    </div>
</div>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<% if (content.rootpath) { %>${content.rootpath}<% } else { %><% } %>js/jquery-1.11.1.min.js"></script>
<script src="<% if (content.rootpath) { %>${content.rootpath}<% } else { %><% } %>js/bootstrap.min.js"></script>
<script src="<% if (content.rootpath) { %>${content.rootpath}<% } else { %><% } %>js/prettify.js"></script>

</body>
</html>