import org.myjaphoo.model.logic.MovieEntryJpaController

import javax.persistence.EntityManager

/**
 * Upgrade script to fill empty uuids for entries, tags, metatags for databases which where created
 * before the uuid was introduced. Close the application after execution and restart to reload the complete state.
 */

def MovieEntryJpaController jpa = new MovieEntryJpaController();


accumulateEvents()
movies().each {
      if (it.uuid == null) {
          it.initUUID();
          jpa.withTransaction {
              EntityManager em ->
                  def q = em.createQuery("update MovieEntry m set m.uuid = :uuid where m.id = :id");
                  q.setParameter("uuid", it.uuid);
                  q.setParameter("id", it.getId());
                  q.executeUpdate();
          }
          //editMovie(it);
      }
}
fireAllAccumulatedEvents()

accumulateEvents()
tags().each {
    if (it.uuid == null) {
        it.initUUID();
        editToken(it);
    }
}
fireAllAccumulatedEvents()

accumulateEvents()
metaTags().each {
    if (it.uuid == null) {
        it.initUUID();
        editMetaToken(it);
    }
}
fireAllAccumulatedEvents()