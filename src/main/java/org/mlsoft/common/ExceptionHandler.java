package org.mlsoft.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An uncaught exception handler, to log any uncaught exceptions of the application.
 */
public class ExceptionHandler
        implements Thread.UncaughtExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    public void handle(Throwable thrown) {
        // for EDT exceptions
        handleException(Thread.currentThread().getName(), thrown);
    }

    public void uncaughtException(Thread thread, Throwable thrown) {
        // for other uncaught exceptions
        handleException(thread.getName(), thrown);
    }

    protected void handleException(String tname, Throwable thrown) {
        logger.error("Exception on " + tname, thrown);
    }
}