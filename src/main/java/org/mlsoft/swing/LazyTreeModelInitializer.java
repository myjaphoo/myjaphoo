package org.mlsoft.swing;

import javax.swing.tree.TreeModel;

/**
 * Delivers a tree model for cases where a tree model is only rarely needed. E.g. a tree popup.
 * This way the costs to produce and populate a tree model are saved, if the popup gets not clicked.
 * @author mla
 * @version $Id$
 */
public interface LazyTreeModelInitializer {

    TreeModel lazyInitializeModel();
}
