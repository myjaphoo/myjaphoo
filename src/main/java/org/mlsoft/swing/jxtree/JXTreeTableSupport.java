package org.mlsoft.swing.jxtree;

import org.jdesktop.swingx.JXTreeTable;
import org.mlsoft.swing.ComponentSupporter;
import org.mlsoft.swing.ConfigHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Helper functions for jxtreetables.
 * Goals:
 * - support easy popup menu actions
 * - support selection
 * - support actions on element T (which get placed on a jtoolbar)
 * - drag and drop within the tree.
 * <p/>
 * Uses configuration by convention. You can set a configuration object which might have some methods that
 * get used by their name for action callback handlers, etc.
 * see detailed documentation on setConfiguration.
 */
public class JXTreeTableSupport<T> implements ComponentSupporter<T> {

    private static final Logger logger = LoggerFactory.getLogger(JXTreeTableSupport.class);
    private JXTreeTable tree;

    private ConfigHandler configHandler = new ConfigHandler(this);

    private JPanel treeWithToolbar = new JPanel();
    private JToolBar toolBar = new JToolBar();


    public JXTreeTableSupport(JXTreeTable tree) {
        this.tree = tree;

        registerPopupHandler();
        registerTreeSelectionListener();
        registerMouseListener();
    }

    public JPanel createTreeWithToolbar() {
        BorderLayout bl = new BorderLayout();
        treeWithToolbar.setLayout(bl);
        JScrollPane jScrollPane1 = new JScrollPane();
        jScrollPane1.setViewportView(tree);

        treeWithToolbar.add(jScrollPane1, java.awt.BorderLayout.CENTER);
        treeWithToolbar.add(toolBar, java.awt.BorderLayout.NORTH);
        return treeWithToolbar;
    }

    /**
     * sets the configuration object. Methods defined on that object with special reserved names and signature get
     * used for callback handlers for this tree. This is the "configuration by convention" part of this implementation.
     * A implementor needs not to define all of them if he does not
     * want to react on all events. The following methods get used:
     * <p/>
     * - JPopupMenu getPopupFor(T selectedEleemnt) : creates a popup if a popup trigger has been recognized on a given item.
     * - void onDoubleClickAction(T selElement): action handler when a double click happens on a given item.
     * - void onElementSelected(T selElement): action handler when a element got selected in the tree.
     *
     * @param configByConventionHandler
     */
    public void setConfiguration(Object configByConventionHandler) {
        setConfiguration(configByConventionHandler, null);
    }

    public void setConfiguration(Object configByConventionHandler, ResourceBundle localeBundle) {
        configHandler.setConfiguration(configByConventionHandler, localeBundle);
        configHandler.updateToolbarActions(toolBar);
    }


    private void registerMouseListener() {
        tree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {// double click
                    T t = (T) tree.getTreeSelectionModel().getLeadSelectionPath().getLastPathComponent();
                    configHandler.callOnDoubleClickAction(t);
                }
            }
        });
    }

    public void registerPopupHandler() {
        tree.addMouseListener(new TreeTablePopupMouseAdapter(configHandler));
    }

    public void registerTreeSelectionListener() {
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent evt) {
                T t = (T) evt.getPath().getLastPathComponent();
                configHandler.callOnElementSelected(t);
                configHandler.updateActionsEnabledState(t);
            }
        });
    }

    /**
     * returns the first selected element within this component. Or null, if nothing is currently selected.
     *
     * @return
     */
    @Override
    public T getFirstSelectedElement() {
        T t = (T) tree.getTreeSelectionModel().getLeadSelectionPath().getLastPathComponent();
        return t;
    }

    @Override
    public List<T> getSelectedElements() {
        List<T> result = new ArrayList<>();
        TreePath[] allPaths = tree.getTreeSelectionModel().getSelectionPaths();
        if (allPaths != null) {
            for (TreePath path: allPaths) {
                result.add((T) path.getLastPathComponent());
            }
        }
        return result;
    }
}
