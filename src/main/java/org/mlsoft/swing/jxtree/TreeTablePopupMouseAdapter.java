package org.mlsoft.swing.jxtree;

import org.jdesktop.swingx.JXTreeTable;
import org.mlsoft.swing.ConfigHandler;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * TreeTablePopupMouseAdapter
 */
public class TreeTablePopupMouseAdapter extends MouseAdapter {

    private ConfigHandler configHandler;

    public TreeTablePopupMouseAdapter(ConfigHandler configHandler) {
        this.configHandler = configHandler;
    }

    // we are checking for both mouseRelease and mousePressed, since
    // on different platforms, the popup triggers are different.
    @Override
    public void mouseReleased(MouseEvent e) {
        checkPopupTrigger(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        checkPopupTrigger(e);
    }

    public void checkPopupTrigger(MouseEvent e) {
        if (e.isPopupTrigger()) {
            JXTreeTable source = (JXTreeTable) e.getSource();
            int row = source.rowAtPoint(e.getPoint());
            int column = source.columnAtPoint(e.getPoint());

            if (!source.isRowSelected(row)) {
                source.changeSelection(row, column, false, false);
            }
            if (source.getTreeSelectionModel().getLeadSelectionPath() != null) {
//                T t = (T) source.getTreeSelectionModel().getLeadSelectionPath().getLastPathComponent();
                configHandler.openPopupMenu(e);
            }
        }
    }
}
