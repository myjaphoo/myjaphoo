package org.mlsoft.swing;

import java.util.EventListener;

/**
 * TreePopupSelectionListener
 * @author mla
 * @version $Id$
 */
public interface TreePopupSelectionListener extends EventListener {
    void selectionChanged(TreePopupSelectionEvent tse);
}
