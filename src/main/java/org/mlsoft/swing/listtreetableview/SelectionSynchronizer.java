package org.mlsoft.swing.listtreetableview;

import org.mlsoft.swing.ConfigHandler;
import org.mlsoft.swing.jxtree.MappedTreeTableModel;

import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * SelectionSynchronizer
 */
public class SelectionSynchronizer<T> {

    private ConfigHandler configHandler;
    private TreeComponent<T> tree;
    private ListComponent<T> list;

    private boolean inSync = false;

    public SelectionSynchronizer(ConfigHandler configHandler, TreeComponent<T> tree, ListComponent<T> list) {
        this.configHandler = configHandler;
        this.tree = tree;
        this.list = list;
        tree.getTreeSelectionModel().addTreeSelectionListener(e -> doInSync(() -> {
            T t = (T) e.getPath().getLastPathComponent();
            callOnElementSelected(configHandler, t);
            selectInList(tree.getSelectedElements());
        }));

        list.getSelectionModel().addListSelectionListener(e -> doInSync(() -> {
                T t = (T) list.getModel().getElementAt(e.getFirstIndex());
                callOnElementSelected(configHandler, t);
                selectInTree(list.getSelectedElements());
            }
        ));
    }

    private void callOnElementSelected(ConfigHandler configHandler, T t) {
        configHandler.callOnElementSelected(t);
        configHandler.updateActionsEnabledState(t);
    }

    private void doInSync(Runnable consumer) {
        if (!inSync) {
            try {
                inSync = true;
                consumer.run();
            } finally {
                inSync = false;
            }
        }
    }

    private void selectInTree(List<T> selectedElements) {
        MappedTreeTableModel<T> model = (MappedTreeTableModel<T>) tree.getTreeTableModel();
        tree.getTreeSelectionModel().clearSelection();
        for (T t : selectedElements) {
            TreePath path = new TreePath(model.getPathToRoot(t));
            tree.getTreeSelectionModel().addSelectionPath(path);
        }
    }

    private <T> void selectInList(List<T> selectedElements) {
        list.clearSelection();
        ArrayList<T> listModelArr = null;
        if (list.getModel() instanceof LeafListModel) {
            listModelArr = ((LeafListModel) list.getModel()).getFlatList();
            boolean first = true;
            for (T t : selectedElements) {
                if (first) {
                    list.setSelectedValue(t, true);
                    first = false;
                } else {
                    int idx = listModelArr.indexOf(t);
                    if (idx > 0) {
                        list.addSelectionInterval(idx, idx);
                    }
                }
            }

        }
    }
}
