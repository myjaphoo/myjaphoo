package org.mlsoft.swing.listtreetableview;

import org.mlsoft.swing.jtable.ColDescr;
import org.mlsoft.swing.jxtree.MappedTreeTableModel;

import javax.swing.*;
import java.awt.*;

/**
 * PropertyBasedCellRenderer
 */
public class ColBasedListCellRenderer extends DefaultListCellRenderer {
    private JXListTreeTableComponent component;

    private int colIndex = 0;

    public ColBasedListCellRenderer(JXListTreeTableComponent component) {
        this.component = component;
    }


    @Override
    public Component getListCellRendererComponent(
        JList list, Object value, int index, boolean isSelected, boolean cellHasFocus
    ) {
        // use the col descr property to get a text value if model is set:
        MappedTreeTableModel model = component.getTreeTableModel();
        if (model != null) {
            if (model.getColDescrs() != null && model.getColDescrs().length > colIndex) {
                ColDescr colDescr = model.getColDescrs()[colIndex];
                value = colDescr.getPropertyAccessor().getVal(value);
            }
        }

        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}
