package org.mlsoft.swing.listtreetableview;

import org.jdesktop.swingx.JXList;
import org.mlsoft.swing.ConfigHandler;
import org.myjaphoo.gui.Commons;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * ListComponent
 */
public class ListComponent<T> extends JXList {

    private static final Logger logger = LoggerFactory.getLogger(ListComponent.class);

    private ConfigHandler configHandler;

    /**
     * Constructs a <code>JXList</code> with an empty model and filters disabled.
     */
    public ListComponent(ConfigHandler configHandler) {
        this.configHandler = configHandler;

        setLayoutOrientation(JList.HORIZONTAL_WRAP);
        setVisibleRowCount(0);
        getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setRolloverEnabled(true);
        addHighlighter(Commons.ROLLOVER_CELL_HIGHLIGHTER);

        registerPopupHandler();
        registerMouseListener();

    }

    private void registerMouseListener() {
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {// double click
                    Object val = getSelectedValue();
                    configHandler.callOnDoubleClickAction(val);
                }
            }
        });
    }

    public void registerPopupHandler() {
        addMouseListener(new MouseAdapter() {
            // we are checking for both mouseRelease and mousePressed, since
            // on different platforms, the popup triggers are different.
            @Override
            public void mouseReleased(MouseEvent e) {
                checkPopupTrigger(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                checkPopupTrigger(e);
            }

            public void checkPopupTrigger(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    ensureClickPointIsSelected(e);

                    ListComponent source = (ListComponent) e.getSource();
                    if (source.getSelectedValues() != null) {
                        configHandler.openPopupMenu(e);
                    }
                }
            }

            /**
             * Selects the element under the click point, if it is not yet selected.
             *
             * @param e
             */
            private void ensureClickPointIsSelected(MouseEvent e) {
                if (e.getSource() instanceof JList) {
                    JList list = (JList) e.getSource();
                    int index = list.locationToIndex(e.getPoint());
                    if (!list.isSelectedIndex(index)) {
                        list.addSelectionInterval(index, index);
                    }
                }
            }
        });
    }

    public T getFirstSelectedElement() {
        T t = (T) getModel().getElementAt(getSelectedIndex());
        return t;
    }

    public List<T> getSelectedElements() {
        ArrayList<T> nodes = new ArrayList<>(getModel().getSize());
        for (int i = 0; i < getModel().getSize(); i++) {
            if (getSelectionModel().isSelectedIndex(i)) {
                nodes.add((T) getModel().getElementAt(i));
            }
        }
        return nodes;
    }

}
