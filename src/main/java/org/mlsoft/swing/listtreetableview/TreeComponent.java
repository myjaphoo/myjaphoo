package org.mlsoft.swing.listtreetableview;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.mlsoft.swing.ConfigHandler;
import org.mlsoft.swing.jxtree.TreeTablePopupMouseAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * TreeComponent
 */
public class TreeComponent<T> extends JXTreeTable {

    private static final Logger logger = LoggerFactory.getLogger(TreeComponent.class);

    private ConfigHandler configHandler;

    /**
     * Constructs a JXTreeTable using a
     * {@link DefaultTreeTableModel}.
     */
    public TreeComponent(ConfigHandler configHandler) {
        this.configHandler = configHandler;
        registerPopupHandler();
        registerMouseListener();
    }

    private void registerMouseListener() {
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {// double click
                    T t = (T) getTreeSelectionModel().getLeadSelectionPath().getLastPathComponent();
                    configHandler.callOnDoubleClickAction(t);
                }
            }
        });
    }

    public void registerPopupHandler() {
        addMouseListener(new TreeTablePopupMouseAdapter(configHandler));
    }

    /**
     * returns the first selected element within this component. Or null, if nothing is currently selected.
     *
     * @return
     */
    public T getFirstSelectedElement() {
        T t = (T) getTreeSelectionModel().getLeadSelectionPath().getLastPathComponent();
        return t;
    }

    public List<T> getSelectedElements() {
        List<T> result = new ArrayList<>();
        TreePath[] allPaths = getTreeSelectionModel().getSelectionPaths();
        if (allPaths != null) {
            for (TreePath path : allPaths) {
                result.add((T) path.getLastPathComponent());
            }
        }
        return result;
    }


}
