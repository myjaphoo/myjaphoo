package org.mlsoft.swing.listtreetableview;

import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableModel;
import org.mlsoft.swing.ComponentSupporter;
import org.mlsoft.swing.ConfigHandler;
import org.mlsoft.swing.jxtree.MappedTreeTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

/**
 * Helper functions for jxtreetables.
 * Goals:
 * - support easy popup menu actions
 * - support selection
 * - support actions on element T (which get placed on a jtoolbar)
 * - drag and drop within the tree.
 * <p/>
 * Uses configuration by convention. You can set a configuration object which might have some methods that
 * get used by their name for action callback handlers, etc.
 * see detailed documentation on setConfiguration.
 */
public class JXListTreeTableComponent<T> extends JPanel implements ComponentSupporter<T> {

    private static final Logger logger = LoggerFactory.getLogger(JXListTreeTableComponent.class);

    private TreeComponent<T> tree;

    private ListComponent<T> list;

    private ConfigHandler configHandler = new ConfigHandler(this);

    private JToolBar toolBar = new JToolBar();

    private JTabbedPane tabbedPane = new JTabbedPane();

    private SelectionSynchronizer selecionSynchronizer;

    private Function<TreeTableModel, ListModel> listModelCreator = model -> new LeafListModel(model);

    private MappedTreeTableModel treeTableModel;

    private ColBasedListCellRenderer listCellRenderer = new ColBasedListCellRenderer(this);

    private ImageIcon imgIcons = new ImageIcon(JXListTreeTableComponent.class.getResource("view-list-icons.png"));

    private ImageIcon imgTree = new ImageIcon(JXListTreeTableComponent.class.getResource("view-list-tree-4.png"));

    public JXListTreeTableComponent() {
        tree = new TreeComponent<>(configHandler);
        list = new ListComponent(configHandler);
        selecionSynchronizer = new SelectionSynchronizer(configHandler, tree, list);
        init();
    }

    public void init() {
        BorderLayout bl = new BorderLayout();
        setLayout(bl);
        JScrollPane jScrollPane1 = new JScrollPane();
        jScrollPane1.setViewportView(tree);

        tabbedPane.addTab("", imgTree, jScrollPane1);

        JScrollPane jScrollPane2 = new JScrollPane();
        jScrollPane2.setViewportView(list);

        tabbedPane.addTab("", imgIcons, jScrollPane2);

        add(tabbedPane, BorderLayout.CENTER);
        add(toolBar, BorderLayout.NORTH);

        list.setCellRenderer(listCellRenderer);
    }

    /**
     * sets the configuration object. Methods defined on that object with special reserved names and signature get
     * used for callback handlers for this tree. This is the "configuration by convention" part of this implementation.
     * A implementor needs not to define all of them if he does not
     * want to react on all events. The following methods get used:
     * <p/>
     * - JPopupMenu getPopupFor(T selectedEleemnt) : creates a popup if a popup trigger has been recognized on a given item.
     * - void onDoubleClickAction(T selElement): action handler when a double click happens on a given item.
     * - void onElementSelected(T selElement): action handler when a element got selected in the tree.
     *
     * @param configByConventionHandler
     */
    public void setConfiguration(Object configByConventionHandler) {
        setConfiguration(configByConventionHandler, null);
    }

    public void setConfiguration(Object configByConventionHandler, ResourceBundle localeBundle) {
        configHandler.setConfiguration(configByConventionHandler, localeBundle);
        configHandler.updateToolbarActions(toolBar);
    }

    /**
     * returns the first selected element within this component. Or null, if nothing is currently selected.
     *
     * @return
     */
    @Override
    public T getFirstSelectedElement() {
        if (tabbedPane.getSelectedIndex() == 0) {
            return tree.getFirstSelectedElement();
        } else {
            return list.getFirstSelectedElement();
        }
    }

    @Override
    public List<T> getSelectedElements() {
        if (tabbedPane.getSelectedIndex() == 0) {
            return tree.getSelectedElements();
        } else {
            return list.getSelectedElements();
        }
    }

    public JXTreeTable getTree() {
        return tree;
    }

    public JXList getList() {
        return list;
    }

    public void setTreeTableModel(MappedTreeTableModel treeTableModel) {
        this.treeTableModel = treeTableModel;
        tree.setTreeTableModel(treeTableModel);
        list.setModel(listModelCreator.apply(treeTableModel));
    }

    public Function<TreeTableModel, ListModel> getListModelCreator() {
        return listModelCreator;
    }

    public void setListModelCreator(Function<TreeTableModel, ListModel> listModelCreator) {
        this.listModelCreator = listModelCreator;
    }

    public MappedTreeTableModel getTreeTableModel() {
        return treeTableModel;
    }
}
