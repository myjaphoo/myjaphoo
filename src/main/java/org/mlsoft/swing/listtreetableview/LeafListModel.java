package org.mlsoft.swing.listtreetableview;

import org.jdesktop.swingx.treetable.TreeTableModel;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Lists only the leafs of a tree model in a list.
 */
public class LeafListModel extends AbstractListModel {

    private TreeTableModel treeTableModel;

    private ArrayList flatList;

    public LeafListModel(TreeTableModel treeTableModel) {
        this.treeTableModel = treeTableModel;
        flatList = listLeafs(treeTableModel);
    }

    @Override
    public int getSize() {
        return flatList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return flatList.get(index);
    }


    private ArrayList listLeafs(TreeTableModel treeTableModel) {
        ArrayList list = new ArrayList();
        listLeafs(treeTableModel, treeTableModel.getRoot(), list);
        return list;
    }

    private void listLeafs(TreeTableModel model, Object node, ArrayList list) {
        int childCount = model.getChildCount(node);
        if (childCount == 0) {
            list.add(node);
        } else {
            for (int i = 0; i < childCount; i++) {
                listLeafs(model, model.getChild(node, i), list);
            }
        }
    }

    public ArrayList getFlatList() {
        return flatList;
    }
}
