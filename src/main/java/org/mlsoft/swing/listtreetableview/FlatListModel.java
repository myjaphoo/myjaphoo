package org.mlsoft.swing.listtreetableview;

import org.jdesktop.swingx.treetable.TreeTableModel;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Lists all nodes of a tree model "flat" in a list.
 */
public class FlatListModel extends AbstractListModel {

    private TreeTableModel treeTableModel;

    private ArrayList flatList;

    public FlatListModel(TreeTableModel treeTableModel) {
        this.treeTableModel = treeTableModel;
        flatList = listLeafs(treeTableModel);
    }

    @Override
    public int getSize() {
        return flatList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return flatList.get(index);
    }


    private ArrayList listLeafs(TreeTableModel treeTableModel) {
        ArrayList list = new ArrayList();
        listLeafs(treeTableModel, treeTableModel.getRoot(), list);
        return list;
    }

    private void listLeafs(TreeTableModel model, Object node, ArrayList list) {
        list.add(node);
        for (int i = 0; i < model.getChildCount(node); i++) {
            listLeafs(model, model.getChild(node, i), list);
        }
    }

    public ArrayList getFlatList() {
        return flatList;
    }
}
