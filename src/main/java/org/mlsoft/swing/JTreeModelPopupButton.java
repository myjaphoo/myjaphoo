package org.mlsoft.swing;

import org.mlsoft.swing.popup.PopupWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import java.awt.*;

/**
 * JTreeModelPopupButton
 * @author mla
 * @version $Id$
 */
public class JTreeModelPopupButton extends JButton {

    /** the tree to pop up. */

    private TreeCellRenderer renderer;
    private LazyTreeModelInitializer lazyTreeModelInitializer;
    PopUps.LazyPopupMenuCreator lpmc;

    public JTreeModelPopupButton(final TreeNode root, final TreeCellRenderer renderer) {
        this(new DefaultTreeModel(root), renderer);
    }

    public JTreeModelPopupButton(final LazyTreeModelInitializer lazyTreeModelInitializer, final TreeCellRenderer renderer) {
        this.renderer = renderer;
        this.lazyTreeModelInitializer = lazyTreeModelInitializer;
        lpmc = new PopUps.LazyPopupMenuCreator() {

            @Override
            public JPopupMenu createPopup() {
                throw new IllegalArgumentException();
            }

            public JComponent createComponent(PopupWindow pw) {
                return createTree(JTreeModelPopupButton.this, lazyTreeModelInitializer, renderer, pw);
            }
        };
        PopUps.makePopupForButton2(this, lpmc);
    }

    public JTreeModelPopupButton(final TreeModel treeModel, final TreeCellRenderer renderer) {
        this(new LazyTreeModelInitializer() {
            @Override
            public TreeModel lazyInitializeModel() {
                return treeModel;
            }
        }, renderer);
    }

    public static final Logger LOGGER = LoggerFactory.getLogger(JTreeModelPopupButton.class.getName());

    /**
     * Create a new tree component each time the popup gets displayed.
     * Maybe this should be optimized, however it makes it much easer as we do not to take care
     * about listener removal, selection resetting, etc...
     * @param button
     * @param root
     * @param renderer
     * @param pw
     * @return
     */
    private static JTree createTree(final JTreeModelPopupButton button, LazyTreeModelInitializer lazyTreeModelInitializer, TreeCellRenderer renderer, final PopupWindow pw) {
        final JTree tree = new JTree();
        tree.setModel(lazyTreeModelInitializer.lazyInitializeModel());
        if (renderer != null) {
            tree.setCellRenderer(renderer);
        }
        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
//        tree.setPreferredSize(new Dimension(250, 200));

        tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {

            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {

                TreePopupSelectionEvent tse = new TreePopupSelectionEvent(tree, evt);
                TreePopupSelectionListener[] listeners = button.listenerList.getListeners(TreePopupSelectionListener.class);
                for (TreePopupSelectionListener listener : listeners) {
                    listener.selectionChanged(tse);
                }

                pw.setVisible(false);
            }
        });
        return tree;
    }

    public void addListener(TreePopupSelectionListener listener) {
        listenerList.add(TreePopupSelectionListener.class, listener);
    }

    public void removeListener(TreePopupSelectionListener listener) {
        listenerList.remove(TreePopupSelectionListener.class, listener);
    }
}
