package org.myjaphoo;

import groovy.lang.Closure;
import org.myjaphoo.gui.errors.ErrorUpdateEvent;
import org.myjaphoo.model.filterparser.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * background job to refresh the movie and thumb view (and all immediately
 * related views, e.g. filter and grouping view).
 */
public class BackgroundClosureJob extends MyjaphooBackgroundJob<Object, Object> {

    public static final Logger LOGGER = LoggerFactory.getLogger(BackgroundClosureJob.class);

    private Closure closure;

    public BackgroundClosureJob(MyjaphooView view, MyjaphooController controller, Closure closure) {
        super(view, controller);
        this.closure = closure;
    }

    @Override
    protected Object doInBackground() throws Exception {
        setTitle("working...");
        try {
            this.firePropertyChange("started", this, this);
            closure.call();
            this.firePropertyChange("done", this, this);
        } catch (Exception e) {
            LOGGER.error("error happened during background job!", e);
            throw e;

        }
        return new Object();
    }

    @Override
    protected void failed(Throwable throwable) {
        super.failed(throwable);
        if (throwable instanceof ParserException) {
            controller.getEventBus().post(new ErrorUpdateEvent("Background Job", (ParserException) throwable));
        }
    }
}
