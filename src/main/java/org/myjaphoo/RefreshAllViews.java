package org.myjaphoo;

import org.myjaphoo.gui.perspective.ViewParts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RefreshViews
 */
public class RefreshAllViews extends AbstractRefreshTreeAndThumbsJob {

    public static final Logger LOGGER = LoggerFactory.getLogger(RefreshAllViews.class);

    public RefreshAllViews(MyjaphooView view, MyjaphooController controller) {
        super(view, controller);
    }

    @Override
    protected void succeeded(MyjaphooView.Results result) {
        super.succeeded(result);
        ViewParts viewParts = view.getViewParts();
        viewParts.getMovieTreePanel().updateMovieTree(result.rootNode);
        viewParts.getThumbPanel().refillThumbView(result.thumbModelResult);
        view.refreshTokenRelevantThings();
        viewParts.getFilterEditorPanel().setFilterInfoText(controller.getFilter().getFilterInfoText());
        view.updateChronic();
        view.updateBookmark();

        viewParts.getInfoPanel().structureUpdated(controller);
        viewParts.getFilterEditorPanel().setFilter(controller.getFilter().getFilterPattern());
        viewParts.getFilterEditorPanel().setPreFilter(controller.getFilter().getPreFilterPattern());
        viewParts.getGroupByPanel().setGroupByExpression(controller.getFilter().getUserDefinedStruct());

        controller.getProject().connection.getStatistics().logSummary();
    }

}
