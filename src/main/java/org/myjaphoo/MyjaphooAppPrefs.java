package org.myjaphoo;

import org.mlsoft.common.prefs.model.editors.EnumSelectionVal;
import org.myjaphoo.project.ProjectConfiguration;

import java.util.ResourceBundle;

/**
 * application preferences which are specific to the fat client version.
 * These preferences extend the AppConfig preferences.
 *
 * @author unbekannt
 * @version 1.0
 */
public class MyjaphooAppPrefs extends MyjaphooCorePrefs {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MyjaphooAppPrefs");


    public final EnumSelectionVal PRF_PLAF_CLASS =
        new EnumSelectionVal(COMMON, "app.plaf.plafnameOrStyle",  //NOI18N
            localeBundle.getString("PRF_PLAF GUI"),
            localeBundle.getString("PRF_PLAF DESC"),
            AvailableLookAndFeels.CREMECOFFEESKIN, AvailableLookAndFeels.class
        );

    public MyjaphooAppPrefs(ProjectConfiguration projectConfiguration) {
        super(projectConfiguration);
    }
}
