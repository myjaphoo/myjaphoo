/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import groovy.lang.Closure;
import groovyx.gpars.dataflow.DataflowVariable;
import io.vavr.collection.Seq;
import org.apache.commons.lang.StringUtils;
import org.mlsoft.eventbus.BasicEventBus;
import org.mlsoft.eventbus.EventBus;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.PlayerHandler;
import org.myjaphoo.gui.action.CompleteRefresh;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.DiffNode;
import org.myjaphoo.gui.movimp.ImportDialog;
import org.myjaphoo.gui.scripting.Scripting;
import org.myjaphoo.gui.util.MyjaphooDialogs;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.TagsAddedEvent;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.cache.zipper.TokenRef;
import org.myjaphoo.model.db.BookMark;
import org.myjaphoo.model.db.ChangeLog;
import org.myjaphoo.model.db.ChangeLogType;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.MovieEntry;
import org.myjaphoo.model.db.Rating;
import org.myjaphoo.model.db.SavedGroovyScript;
import org.myjaphoo.model.db.Thumbnail;
import org.myjaphoo.model.db.Token;
import org.myjaphoo.model.dbcompare.ComparisonContext;
import org.myjaphoo.model.dbcompare.integration.Integration;
import org.myjaphoo.model.logic.CheckSumCalculator;
import org.myjaphoo.model.logic.FileSubstitutionImpl;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.ScriptJpaController;
import org.myjaphoo.model.logic.ThumbnailJpaController;
import org.myjaphoo.model.logic.imp.ImportDelegator;
import org.myjaphoo.model.logic.imp.MovieDelegator;
import org.myjaphoo.model.logic.imp.PicDelegator;
import org.myjaphoo.model.logic.imp.WmInfoImExport;
import org.myjaphoo.model.player.KMPlayerPlayer;
import org.myjaphoo.model.player.MPlayerPlayer;
import org.myjaphoo.model.player.Player;
import org.myjaphoo.model.player.VLCPlayer;
import org.myjaphoo.processing.AbstractEntryRefListProcessor;
import org.myjaphoo.processing.AbstractImmEntryListProcessor;
import org.myjaphoo.processing.DelayedCacheActorEventsWrapper;
import org.myjaphoo.processing.ListProcessor;
import org.myjaphoo.processing.Processing;
import org.myjaphoo.project.Project;
import org.myjaphoo.util.Filtering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * The controller for one MyjaphooView. Since multiple views can be opened, there will be exactly one controller
 * exist for each view.
 */
public class MyjaphooController {

    public static final Logger LOGGER = LoggerFactory.getLogger(MyjaphooController.class.getName());
    private MovieFilterController filter;
    private MovieEntryJpaController jpa;
    private ScriptJpaController scriptJpa;
    private ThumbnailJpaController thumbController;
    private FileSubstitutionImpl fileSubstitution;
    private VLCPlayer vlcPlayer;
    private MPlayerPlayer mPlayerPlayer;
    private KMPlayerPlayer kmPlayerPlayer;

    /**
     * the project this controller belongs to.
     */
    private final Project project;

    private final MainApplicationController mainController;
    /**
     * the main view, this controller belongs to.
     */
    private final MyjaphooView view;
    /**
     * die zuletzt benutzen tokens.
     */
    private ArrayList<TokenRef> lastUsedTokens = new ArrayList<>();
    private final static int MAXLASTTOKENSIZE = 10;
    private boolean previewThumbsInMovieTree = false;
    private MainThumbController mainThumbController;
    private MainMoviePanelController mainMoviePanelController;
    private MainTokenPanelController mainTokenPanelController;
    private MainMetaTokenPanelController mainMetaTokenPanelController;
    private Scripting scripting = new Scripting(this);
    private EventBus eventBus = new BasicEventBus();

    public MyjaphooController(MainApplicationController mainController, MyjaphooView view) {
        this.mainController = requireNonNull(mainController);
        this.project = requireNonNull(mainController.project);
        this.view = view;
        filter = new MovieFilterController(mainController);
        this.mainThumbController = new MainThumbController(this);
        mainMoviePanelController = new MainMoviePanelController(this);
        mainTokenPanelController = new MainTokenPanelController(this);
        mainMetaTokenPanelController = new MainMetaTokenPanelController(this);
        fileSubstitution = new FileSubstitutionImpl(project);

        jpa = new MovieEntryJpaController(project.connection);
        scriptJpa = new ScriptJpaController(project.connection);
        thumbController = new ThumbnailJpaController(project.connection);

        vlcPlayer = new VLCPlayer(project);
        mPlayerPlayer = new MPlayerPlayer(project);
        kmPlayerPlayer = new KMPlayerPlayer(project);
    }

    public void assignTokenToMovieNodes(ImmutableToken token, List<MovieNode> selNodes) {
        updateLastUsedTokens(token);
        List<ImmutableMovieEntry> movies = Filtering.nodes2Immutables(selNodes);
        project.cacheActor.assignToken2MovieEntry(token, movies);
    }

    public void assignMetaTokenToToken(ImmutableMetaToken metaToken, ImmutableToken token) {
        project.cacheActor.assignMetaTokenToToken(metaToken, token);
    }

    public void deleteMovies(ArrayList<EntryRef> nodes) {
        if (!project.dbPrefs.PRF_FO_DELETION_ALLOWED.getVal()) {
            return;
        }

        AbstractEntryRefListProcessor copyProcessor = new AbstractEntryRefListProcessor() {

            @Override
            public void process(EntryRef entry) throws Exception {
                deleteMovie(entry.ref);
            }
        };
        Processing.processMovieList(nodes, copyProcessor, "delete entries");
    }

    public ArrayList<TokenRef> getLastUsedTokens() {
        return lastUsedTokens;
    }

    public ArrayList<AbstractLeafNode> getAllSelectedNodes() {
        return getView().getAllSelectedMovieNodes();
    }

    public Token createNewToken(String newTokenName, String descr, ImmutableToken parent) {
        Token token = new Token();
        token.setName(newTokenName);
        token.setDescription(descr);
        DataflowVariable<TagsAddedEvent> eventPromise = project.cacheActor.createToken(
            token,
            parent
        );
        TagsAddedEvent event = null;
        try {
            event = eventPromise.get(3000, TimeUnit.MILLISECONDS);
        } catch (Throwable throwable) {
            throw new RuntimeException("error adding tag!", throwable);
        }
        updateLastUsedTokens(event.getTokenSet().get(0));
        return token;
    }

    public MetaToken createNewMetaToken(String newTokenName, String descr, ImmutableMetaToken parent) {
        MetaToken token = new MetaToken();
        token.setName(newTokenName);
        token.setDescription(descr);
        project.cacheActor.createMetaToken(token, parent);
        return token;
    }

    /**
     * @return the currentSelectedDir
     */
    public AbstractMovieTreeNode getCurrentSelectedDir() {
        return filter.getCurrentSelectedDir();
    }

    public void moveTokens(ImmutableToken tokenParent, ImmutableToken token2Move) {
        project.cacheActor.moveToken(tokenParent, token2Move);
    }

    public void moveMetaTokens(ImmutableMetaToken tokenParent, ImmutableMetaToken token2Move) {
        project.cacheActor.moveMetaTokens(tokenParent, token2Move);
    }

    public Seq<ImmutableToken> getTokens() {
        return project.cacheActor.getImmutableModel().getTokenTree().getValues();
    }

    public void playMovie(AbstractLeafNode node) {
        List<AbstractLeafNode> nodeList = new ArrayList<AbstractLeafNode>();
        nodeList.add(node);
        playMovies(nodeList);

    }

    public void removeToken(ImmutableToken currentSelectedToken) {
        project.cacheActor.removeToken(currentSelectedToken);
        filter.setCurrentToken(null);
    }

    public void removeMetaToken(ImmutableMetaToken currentSelectedToken) {
        project.cacheActor.removeMetaToken(currentSelectedToken);
        filter.setCurrentMetaToken(null);
    }

    public void unassignTokenToMovieNodes(ImmutableToken token, List<MovieNode> nodes) {
        project.cacheActor.unassignTokenFromMovies(token, Filtering.nodes2Immutables(nodes));
    }

    public void copyMovies(
        final List<MovieNode> nodes, final String toDir, final boolean createWmInfoFile,
        final FileCopying.PathOptionForCopying pathOption
    ) throws IOException, JAXBException {

        List<FileCopyInstruction> fcis = FileCopyInstruction.createInstructions(
            nodes,
            toDir,
            createWmInfoFile,
            pathOption
        );

        ListProcessor<FileCopyInstruction> copyProcessor = new ListProcessor<FileCopyInstruction>() {

            FileCopying fileCopying = new FileCopying(MyjaphooController.this);

            @Override
            public void process(FileCopyInstruction fci) throws Exception {
                fileCopying.copyMovie(fci);
            }

            @Override
            public void startProcess() {
            }

            @Override
            public void stopProcess() {
            }

            @Override
            public String shortName(FileCopyInstruction fci) {
                return fci.getMovieEntry().getName();
            }

            @Override
            public String longName(FileCopyInstruction fci) {
                return fci.getMovieEntry().getCanonicalPath();
            }
        };
        String info = "copy to " + toDir; //NOI18N
        if (createWmInfoFile) {
            info += ", with wminfo files"; //NOI18N
        }
        info += ", " + pathOption.getDescription();
        Processing.processBigList(fcis, copyProcessor, info);
    }

    private void extractMovieInfos(ImmutableMovieEntry immutableMovieEntry)
        throws Exception {
        MovieEntry entry = project.loadMovieEntry(immutableMovieEntry.getId());
        if (project.Pictures.is(immutableMovieEntry)) {
            PicDelegator pd = new PicDelegator(project);
            pd.getMediaInfos(entry);

        } else if (project.Movies.is(immutableMovieEntry)) {
            MovieDelegator md = new MovieDelegator(project);
            md.getMediaInfos(entry);
        }
        project.cacheActor.editMovie(entry);
    }

    private void recalcCheckSums(ImmutableMovieEntry immutableMovieEntry) throws Exception {
        final File file = new File(fileSubstitution.substitude(immutableMovieEntry.getCanonicalPath()));
        CheckSumCalculator.Checksums checkSums = CheckSumCalculator.build(file);
        MovieEntry entry = project.loadMovieEntry(immutableMovieEntry.getId());
        if (checkSums.applyChecksums(entry)) {
            project.cacheActor.editMovie(entry);
        }
    }

    public void extractMovieInfos(final List<ImmutableMovieEntry> nodes) throws IOException, JAXBException {

        AbstractImmEntryListProcessor extractProcessor = new AbstractImmEntryListProcessor() {

            @Override
            public void process(ImmutableMovieEntry entry) throws Exception {
                extractMovieInfos(entry);
            }
        };
        Processing.processBigList(
            nodes,
            new DelayedCacheActorEventsWrapper<ImmutableMovieEntry>(project.cacheActor, extractProcessor),
            "extract infos "
        );
    }

    public void recalcCheckSums(final List<ImmutableMovieEntry> nodes) throws IOException, JAXBException {

        AbstractImmEntryListProcessor extractProcessor = new AbstractImmEntryListProcessor() {

            @Override
            public void process(ImmutableMovieEntry entry) throws Exception {
                recalcCheckSums(entry);
            }
        };
        Processing.processBigList(
            nodes,
            new DelayedCacheActorEventsWrapper<ImmutableMovieEntry>(project.cacheActor, extractProcessor),
            "extract infos "
        );
    }

    public void integrateComparisonDifferences(final List<DiffNode> nodes) throws IOException, JAXBException {
        ComparisonContext comparisonContext = nodes.get(0).getDbdiff().getContext();
        
        ListProcessor<DiffNode> extractProcessor = new ListProcessor<DiffNode>() {
            Integration integration = new Integration(project, comparisonContext);

            @Override
            public void startProcess() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void process(DiffNode entry) throws Exception {
                integration.integrate(entry.getDbdiff());
            }

            @Override
            public void stopProcess() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String shortName(DiffNode diffNode) {
                return diffNode.getName();
            }

            @Override
            public String longName(DiffNode diffNode) {
                return diffNode.getCanonicalPath();
            }
        };
        Processing.processBigList(
            nodes,
            new DelayedCacheActorEventsWrapper<DiffNode>(project.cacheActor, extractProcessor),
            "integrate differences "
        );
    }


    public void playMovies(final List<AbstractLeafNode> nodes) {
        internPlayMovies(nodes, vlcPlayer);
    }

    private void internPlayMovies(final List<AbstractLeafNode> nodes, Player player) {
        PlayerHandler.playMovies(getProject(), nodes, player, filter.getCurrentDisplayedMovieNodes(), getView().getFrame());
    }

    private void deleteMovie(ImmutableMovieEntry movieEntry) {
        if (!project.dbPrefs.PRF_FO_DELETION_ALLOWED.getVal()) {
            return;
        }
        String sourcepath = fileSubstitution.locateFileOnDrive(movieEntry.getCanonicalPath());
        if (sourcepath != null) {
            File toDel = new File(sourcepath);
            if (!toDel.delete()) {
                throw new ApplicationException("Can not delete File " + sourcepath);
            }
        }

        project.cacheActor.removeMovieEntry(movieEntry);

    }

    private void updateLastUsedTokens(ImmutableToken token) {
        if (!lastUsedTokens.contains(token)) {
            lastUsedTokens.add(0, new TokenRef(project.cacheActor.getImmutableModel(), token));
            if (lastUsedTokens.size() > MAXLASTTOKENSIZE) {
                lastUsedTokens.remove(MAXLASTTOKENSIZE - 1);
            }
        }
    }

    /**
     * @return the view
     */
    public MyjaphooView getView() {
        return view;
    }

    public void recreateThumbs(List<ImmutableMovieEntry> entries) {
        AbstractImmEntryListProcessor recreateThumbProcessor = new AbstractImmEntryListProcessor() {

            @Override
            public void process(ImmutableMovieEntry entry) {
                recreateThumb(entry);
            }
        };
        Processing.processMovieList(entries, recreateThumbProcessor, "recreate Thumbs");
    }

    private void recreateThumb(ImmutableMovieEntry entry) {
        ImportDelegator id = null;
        if (project.Pictures.is(entry)) {
            id = new PicDelegator(project);
        } else {
            id = new MovieDelegator(project);
        }
        recreateThumbnails(entry, id);
    }

    private void recreateThumbnails(ImmutableMovieEntry immutableMovieEntry, ImportDelegator id) {
        String foundPath = fileSubstitution.locateFileOnDrive(immutableMovieEntry.getCanonicalPath());
        if (foundPath != null) {
            // delete all old thumbs:

            MovieEntry entry = project.loadMovieEntry(immutableMovieEntry.getId());

            ArrayList<Thumbnail> copyOfList = new ArrayList<>(entry.getThumbnails());
            for (Thumbnail tn : copyOfList) {
                thumbController.removeThumb(tn);
            }
            List<Thumbnail> newThumbs = id.createAllThumbNails(entry, new File(foundPath));
            for (Thumbnail tn : newThumbs) {
                tn.setMovieEntry(entry);
                thumbController.create(tn);
            }
        }
    }

    public void setPreviewThumbsInMovieTree(boolean b) {
        previewThumbsInMovieTree = b;
    }

    /**
     * @return the previewThumbsInMovieTree
     */
    public boolean isPreviewThumbsInMovieTree() {
        return previewThumbsInMovieTree;
    }

    public void createWmInfoFiles(List<EntryRef> nodes2Entries) {
        AbstractEntryRefListProcessor copyProcessor = new AbstractEntryRefListProcessor() {

            @Override
            public void process(EntryRef entry) throws Exception {
                createWmInfoFileForMovie(entry);
            }
        };
        Processing.processMovieList(nodes2Entries, copyProcessor, "create wmfiles ");
    }

    private void createWmInfoFileForMovie(EntryRef entryRef)
        throws JAXBException, IOException {
        WmInfoImExport exp = new WmInfoImExport(project);
        String sourcepath = fileSubstitution.locateFileOnDrive(entryRef.getCanonicalPath());
        String infoFile = sourcepath + WmInfoImExport.WMINFO_POSTFIX;
        exp.export(new File(infoFile), entryRef);
    }

    public void setRating(final Rating rating, List<MovieNode> nodes) {
        ArrayList<EntryRef> entries = Filtering.nodes2Entries(nodes);
        AbstractEntryRefListProcessor changeRatingProcessor = new AbstractEntryRefListProcessor() {

            @Override
            public void process(EntryRef entry) throws Exception {
                MovieEntry movieEntry = project.loadMovieEntry(entry.ref.getId());
                movieEntry.setRating(rating);
                project.cacheActor.editMovie(movieEntry);
            }
        };
        Processing.processMovieList(entries, changeRatingProcessor, "change ratings to " + rating.getName());
    }

    public void showAndLogErroDlg(String title, String errMsg, Throwable e) {
        MyjaphooDialogs.showAndLogErroDlg(getView().getFrame(), title, errMsg, e);
    }

    public boolean confirm(Object msgToConfirmWithYes) {
        return MyjaphooDialogs.confirm(getView().getFrame(), msgToConfirmWithYes);
    }

    public void message(Object msg) {
        MyjaphooDialogs.message(getView().getFrame(), msg);
    }

    public String getInputValue(String msg) {
        return MyjaphooDialogs.getInputValue(getView().getFrame(), msg);
    }

    public void playMoviesMPlayer(List<AbstractLeafNode> nodes) {
        internPlayMovies(nodes, mPlayerPlayer);
    }

    public void playMoviesKMPlayer(List<AbstractLeafNode> nodes) {
        internPlayMovies(nodes, kmPlayerPlayer);
    }

    public void removeMovieEntriesFromDatabase(List<EntryRef> movies) {
        if (!project.dbPrefs.PRF_FO_REMOVING_ALLOWED.getVal()) {
            return;
        }

        AbstractEntryRefListProcessor copyProcessor = new AbstractEntryRefListProcessor() {
            @Override
            public void process(EntryRef entry) throws Exception {
                removeEntryFromDatabase(entry.getRef());
            }
        };
        Processing.processMovieList(movies, copyProcessor, "remove entries from database");

    }

    private void removeEntryFromDatabase(ImmutableMovieEntry movieEntry) {
        if (!project.dbPrefs.PRF_FO_REMOVING_ALLOWED.getVal()) {
            return;
        }
        project.cacheActor.removeMovieEntry(movieEntry);
    }

    public void removeTokenRelations(List<MovieNode> nodes, final List<ImmutableToken> tokens2RemoveRelations) {
        ArrayList<EntryRef> entries = Filtering.nodes2Entries(nodes);
        AbstractEntryRefListProcessor changeRatingProcessor = new AbstractEntryRefListProcessor() {

            @Override
            public void process(EntryRef entry) throws Exception {
                for (ImmutableToken token : tokens2RemoveRelations) {
                    project.cacheActor.unassignTokenFromMovies(token, Arrays.asList(entry.getRef()));
                }
            }
        };
        String tokenstr = StringUtils.join(tokens2RemoveRelations, ", ");
        Processing.processMovieList(entries, changeRatingProcessor, "remove Token relations " + tokenstr);
    }

    public MovieFilterController getFilter() {
        return filter;
    }

    public List<ChangeLog> getAllChangeLogs() {
        return jpa.findChangeLogEntities();
    }

    public void createChangeLog(ChangeLogType changeLogType, String txt, List<MovieNode> targets) {
        createChangeLog2(changeLogType, txt, Filtering.nodes2Entries(targets));
    }

    public void createChangeLog2(ChangeLogType changeLogType, String txt, List<EntryRef> targets) {
        ChangeLog log = new ChangeLog();
        log.setCltype(changeLogType);
        log.setMsg(txt);
        log.setObjDescription(cutMax(createMovList(targets), 4000));
        jpa.create(log);
    }

    private String createMovList(List<EntryRef> targetMovies) {
        if (targetMovies == null) {
            return null;
        }
        StringBuilder b = new StringBuilder();
        for (EntryRef entry : targetMovies) {
            b.append(entry.getCanonicalPath());
            b.append(";");
        }
        return b.toString();
    }

    private String cutMax(String str, int max) {
        if (str == null) {
            return null;
        }
        if (str.length() > max) {
            return str.substring(0, max);
        } else {
            return str;
        }
    }

    public void addNewBookMark(String bookmarkname) {
        BookMark bm = getFilter().createBookMarkFromCurrentChronic();
        bm.setName(bookmarkname);
        jpa.create(bm);
        mainController.getBookmarkList().add(bm);
    }

    public void deleteBookMark(BookMark bm) {
        jpa.removeBookMark(bm);
        mainController.getBookmarkList().remove(bm);
    }


    public void deleteScript(SavedGroovyScript bm) {
        scriptJpa.removeScript(bm);
        mainController.getScriptList().remove(bm);
    }

    public void updateScript(SavedGroovyScript bm) {
        scriptJpa.edit(bm);
        mainController.getScriptList().fireListElementChanged(bm);
    }

    public void addScript(SavedGroovyScript script) {
        scriptJpa.create(script);
        mainController.getScriptList().add(script);
    }

    public void updateBookMark(BookMark bm) {
        jpa.edit(bm);
        mainController.getBookmarkList().fireListElementChanged(bm);
    }

    /**
     * @return the mainThumbController
     */
    public MainThumbController getMainThumbController() {
        return mainThumbController;
    }

    /**
     * @return the mainMoviePanelController
     */
    public MainMoviePanelController getMainMoviePanelController() {
        return mainMoviePanelController;
    }

    /**
     * @return the mainTokenPanelController
     */
    public MainTokenPanelController getMainTokenPanelController() {
        return mainTokenPanelController;
    }

    /**
     * @return the mainMetaTokenPanelController
     */
    public MainMetaTokenPanelController getMainMetaTokenPanelController() {
        return mainMetaTokenPanelController;
    }

    public void unAssignMetaTokenFromToken(ImmutableMetaToken currMetaToken, List<ImmutableToken> toks2Remove) {
        project.cacheActor.unAssignMetaTokenFromToken(currMetaToken, toks2Remove);
    }

    /**
     * @return the scripting
     */
    public Scripting getScripting() {
        return scripting;
    }

    /**
     * Shows the bookmark in the view.
     *
     * @param bm
     */
    public void showBookMark(BookMark bm) {
        // do not put the same bookmark entry back to the filter (would lead to save bookmarks as chronic...)
        // instead create new chronic entry based on the bookmark:
        ChronicEntry entry = bm.toChronic();
        getFilter().popChronic(entry);
        getView().updateMovieAndThumbViews();
    }

    /**
     * @return the eventBus
     */
    public EventBus getEventBus() {
        return eventBus;
    }

    public void addCoverFrontPicture(File file, ImmutableMovieEntry entry) throws Exception {
        MovieDelegator md = new MovieDelegator(project);
        md.addFrontCoverThumb(entry, file);
    }

    public void addCoverBackPicture(File file, ImmutableMovieEntry entry) throws Exception {
        MovieDelegator md = new MovieDelegator(project);
        md.addBackCoverThumb(entry, file);
    }


    public void showImportDialog() {
        ImportDialog dlg = new ImportDialog(view.getFrame(), this);
        doImport(dlg);
    }

    public void showImportDialog(String dir) {
        ImportDialog dlg = new ImportDialog(view.getFrame(), this, dir);
        doImport(dlg);
    }

    private void doImport(ImportDialog dlg) {
        dlg.setLocationRelativeTo(view.getFrame());
        MyjaphooApp.getApplication().show(dlg);
        // trigger a complete refresh:
        new CompleteRefresh(this).actionPerformed(new ActionEvent(this, 0, "refresh"));
    }

    public void executeBackground(Closure closure) {
        view.executeBackground(closure);
    }


    public void historyBack() {
        ChronicEntry chronicEntry = getFilter().getBackHistory();
        if (chronicEntry != null) {
            getFilter().popChronic(chronicEntry);
            getView().updateChronic(chronicEntry);
        }
    }

    public void historyForward() {
        ChronicEntry chronicEntry = getFilter().getForwardHistory();
        if (chronicEntry != null) {
            getFilter().popChronic(chronicEntry);
            getView().updateChronic(chronicEntry);
        }
    }

    public Project getProject() {
        return project;
    }

    public MainApplicationController getMainController() {
        return mainController;
    }
}
