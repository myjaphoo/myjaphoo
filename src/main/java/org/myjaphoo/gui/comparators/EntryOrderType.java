package org.myjaphoo.gui.comparators;

import org.myjaphoo.gui.movietree.AbstractLeafNode;

import java.util.Comparator;

/**
 * A defined order for entries.
 * There are some predefined ones. Scripts and plugins could define additional ones.
 * @author mla
 * @version $Id$
 */
public interface EntryOrderType {


    public String getGuiName();

    /**
     * Return localized gui text name.
     * @return
     */
    public String toString();

    Comparator<AbstractLeafNode> getComparator();
}
