package org.myjaphoo.gui.comparators;

import com.eekboom.utils.Strings;
import org.apache.commons.lang.StringUtils;
import org.myjaphoo.MovieNode;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.model.util.ComparatorUtils;
import org.myjaphoo.plugin.Components;

import java.util.Comparator;
import java.util.Date;

/**
 * InitPredefinedOrderTypes
 * @author mla
 * @version $Id$
 */
public class InitPredefinedOrderTypes {
    private static final Comparator<AbstractLeafNode> DIR_FILENAME_COMPARATOR = new Comparator<AbstractLeafNode>() {

        @Override
        public int compare(AbstractLeafNode o1, AbstractLeafNode o2) {
            int val = o1.getCanonicalDir().compareTo(o2.getCanonicalDir());
            if (val != 0) {
                return val;
            }
            return Strings.compareNaturalAscii(o1.getName(), o2.getName());
        }
    };
    public static final AbstractEntryOrderType BY_DIR_AND_NAME = new AbstractEntryOrderType("order by dir and name") {
        @Override
        public Comparator<AbstractLeafNode> getComparator() {
            return DIR_FILENAME_COMPARATOR;
        }
    };
    private static final Comparator<AbstractLeafNode> EXIF_CREATE_DATE_COMPARATOR = new Comparator<AbstractLeafNode>() {

        @Override
        public int compare(AbstractLeafNode o1, AbstractLeafNode o2) {
            if (o1 instanceof MovieNode && o2 instanceof MovieNode) {
                Date d1 = ((MovieNode) o1).getMovieEntry().getExifData().getExifCreateDate();
                Date d2 = ((MovieNode) o2).getMovieEntry().getExifData().getExifCreateDate();
                return ComparatorUtils.compareTo(d1, d2);
            } else {
                return 0;
            }
        }
    };
    private static final Comparator<AbstractLeafNode> FILENAME_COMPARATOR = new Comparator<AbstractLeafNode>() {

        @Override
        public int compare(AbstractLeafNode o1, AbstractLeafNode o2) {
            return Strings.compareNaturalIgnoreCaseAscii(o1.getName(), o2.getName());
        }
    };
    private static final Comparator<AbstractLeafNode> DIR_FILENAME_REVERT_COMPARATOR = new Comparator<AbstractLeafNode>() {

        @Override
        public int compare(AbstractLeafNode o1, AbstractLeafNode o2) {
            int val = o1.getCanonicalDir().compareTo(o2.getCanonicalDir());
            if (val != 0) {
                return val;
            }
            return Strings.compareNaturalIgnoreCaseAscii(StringUtils.reverse(o1.getName()), StringUtils.reverse(o2.getName()));
        }
    };


    public static void init() {

        Components.addEntryOrderType("BY_DIR_AND_NAME", BY_DIR_AND_NAME);

        Components.addEntryOrderType("BY_NAME", new AbstractEntryOrderType("order by name") {
            @Override
            public Comparator<AbstractLeafNode> getComparator() {
                return FILENAME_COMPARATOR;
            }
        });


        Components.addEntryOrderType("BY_DIR_AND_REVERSE_NAME", new AbstractEntryOrderType("order by dir and name desc") {
            @Override
            public Comparator<AbstractLeafNode> getComparator() {
                return DIR_FILENAME_REVERT_COMPARATOR;
            }
        });

        Components.addEntryOrderType("BY_EXIF_CREATE_DATE", new AbstractEntryOrderType("order by exif create date") {
            @Override
            public Comparator<AbstractLeafNode> getComparator() {
                return EXIF_CREATE_DATE_COMPARATOR;
            }
        });

    }
}
