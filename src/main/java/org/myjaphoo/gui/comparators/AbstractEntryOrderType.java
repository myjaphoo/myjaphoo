package org.myjaphoo.gui.comparators;

import java.util.ResourceBundle;

/**
 * AbstractEntryOrderType
 * @author mla
 * @version $Id$
 */
public abstract class AbstractEntryOrderType implements EntryOrderType {

    private String guiName;

    public AbstractEntryOrderType(String guiName) {
        this.guiName = guiName;
    }

    @Override
    public String getGuiName() {
        return guiName;
    }

    @Override
    public String toString() {
        final ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/resources/OrderType");
        return localeBundle.getString(guiName);
    }

}
