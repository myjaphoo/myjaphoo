/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MyjaphooController;

import javax.swing.*;
import javax.swing.undo.UndoableEdit;
import java.awt.event.ActionEvent;

/**
 *
 * @author mla
 */
abstract public class AbstractUndoAction extends AbstractMJAction {

    public AbstractUndoAction(MyjaphooController controller, String name, Icon icon) {
        super(controller, name, icon);
    }

    public AbstractUndoAction(MyjaphooController controller, String name, Icon icon, ViewContext context) {
        super(controller, name, icon, context);
    }

    @Override
    public final void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        UndoableEdit edit = runUndoAction(controller, e, context);
        if (edit != null) {
            getController().getView().addUndoableEdit(edit);
        }
    }

    abstract public UndoableEdit runUndoAction(MyjaphooController controller, ActionEvent e, ViewContext context);
}
