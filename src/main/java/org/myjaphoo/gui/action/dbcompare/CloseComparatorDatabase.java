/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action.dbcompare;

import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.AbstractMJAction;
import org.myjaphoo.gui.action.ViewContext;

import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

/**
 * @author
 */
public class CloseComparatorDatabase extends AbstractMJAction {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/gui/action/dbcompare/resources/CloseComparatorDatabase");

    public CloseComparatorDatabase(MyjaphooController controller) {
        super(controller, localeBundle.getString("CLOSE COMPARISON DB"), null);
    }

    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        controller.getMainController().getDatabaseComparison().closeComparisonDatabase();
        controller.getView().getInfoPanel().updateDBComparisonValues();
    }
}
