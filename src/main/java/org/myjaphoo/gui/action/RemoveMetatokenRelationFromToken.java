/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MyjaphooApp;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.icons.Icons;
import org.myjaphoo.gui.removeElementsDialog.RemoveElementsDialog;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.TokenRef;
import org.myjaphoo.model.db.ChangeLogType;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.Token;

import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.util.*;

/**
 *
 * @author mla
 */
public class RemoveMetatokenRelationFromToken extends AbstractMJAction {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/action/resources/RemoveMetatokenRelationFromToken");

    public RemoveMetatokenRelationFromToken(MyjaphooController controller) {
        super(controller, localeBundle.getString("REMOVE METATAG RELATION FROM TAG"), Icons.IR_TAG_DEL.icon);
    }

    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {

        TokenRef currToken = controller.getFilter().getCurrentToken();

        ArrayList<ImmutableMetaToken> orderedList = new ArrayList<>(currToken.getMetaTokens().toJavaList());
        Collections.sort(orderedList);
        RemoveElementsDialog<ImmutableMetaToken> dlg = new RemoveElementsDialog<>(controller.getView().getFrame(),
                MessageFormat.format(localeBundle.getString("SELECT WHICH METATAG"), currToken.getName()),
                localeBundle.getString("CHOOSE METATAG RELATIONS TO REMOVE"), orderedList);
        MyjaphooApp.getApplication().show(dlg);
        if (dlg.isOk()) {
            List<ImmutableMetaToken> toks2Remove = dlg.getCheckedElements();
            List<ImmutableToken> tokenList = Arrays.asList(currToken.getRef());
            for (ImmutableMetaToken mt : toks2Remove) {
                controller.unAssignMetaTokenFromToken(mt, tokenList);
            }
            controller.createChangeLog(ChangeLogType.REMOVEMETATOKEN, "remove metatag relations from tag " + currToken.getName() + ": " + tokList(toks2Remove), null); //NOI18N
        }
    }

    public static String tokList(List<ImmutableMetaToken> tokens2RemoveRelations) {
        StringBuilder b = new StringBuilder();
        for (ImmutableMetaToken token : tokens2RemoveRelations) {
            b.append(token.getName());
            b.append(";"); //NOI18N
        }
        return b.toString();
    }
}
