/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action.metatoken;

import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.AbstractUndoAction;
import org.myjaphoo.gui.action.ViewContext;
import org.myjaphoo.gui.newtagdlg.NewTagDialog;
import org.myjaphoo.gui.newtagdlg.NewTagDialog.NewTagDlgResult;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;
import org.myjaphoo.model.db.ChangeLogType;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.logic.MetaTokenJpaController;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;

/**
 *
 * @author lang
 */
public class AddNewMetaToken extends AbstractUndoAction {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/action/metatoken/resources/AddNewMetaToken");

    private MetaTokenJpaController tokenjpa;

    public AddNewMetaToken(MyjaphooController controller) {
        super(controller, localeBundle.getString("ADD NEW META-TAG"), null);
        tokenjpa = new MetaTokenJpaController(controller.getProject().connection);
    }

    @Override
    public UndoableEdit runUndoAction(final MyjaphooController controller, ActionEvent e, ViewContext context) {

        final MetaTokenRef parentPreselected = controller.getFilter().getCurrentMetaToken();
        List<ImmutableMetaToken> parensList = controller.getProject().cacheActor.getImmutableModel().getMetaTokenTree().getValues().toJavaList();
        final NewTagDlgResult result = NewTagDialog.newMetaTag(localeBundle.getString("ADD NAME FOR THE NEW META TAG"), parensList, parentPreselected.getRef());

        if (result != null) {
            MetaToken token = controller.createNewMetaToken(result.name, result.descr, result.parentMetaToken);
            controller.createChangeLog(ChangeLogType.NEWMETATOK, "new metatag " + token.getName(), null); //NOI18N
            return new AbstractUndoableEdit() {

                @Override
                public void undo() throws CannotUndoException {
                    super.undo();
                    MetaToken token = tokenjpa.findTokenByName(result.name);
                    controller.removeMetaToken(token.toImmutable());
                }

                @Override
                public void redo() throws CannotRedoException {
                    super.redo();
                    MetaToken token = controller.createNewMetaToken(result.name, result.descr, result.parentMetaToken);
                }

                @Override
                public String getPresentationName() {
                    return MessageFormat.format(localeBundle.getString("CREATE META TAG"), result.name);
                }
            };

        }
        return null;
    }
}
