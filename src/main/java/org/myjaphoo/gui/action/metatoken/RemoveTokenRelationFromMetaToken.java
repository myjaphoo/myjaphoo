/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action.metatoken;

import org.myjaphoo.MyjaphooApp;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.AbstractMJAction;
import org.myjaphoo.gui.action.RemoveTokenRelations;
import org.myjaphoo.gui.action.ViewContext;
import org.myjaphoo.gui.removeElementsDialog.RemoveElementsDialog;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.Immutables;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;
import org.myjaphoo.model.db.ChangeLogType;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.model.db.Token;

import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

/**
 *
 * @author mla
 */
public class RemoveTokenRelationFromMetaToken extends AbstractMJAction {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/action/metatoken/resources/RemoveTokenRelationFromMetaToken");

    public RemoveTokenRelationFromMetaToken(MyjaphooController controller) {
        super(controller, localeBundle.getString("REMOVE TAG RELATION FROM META TAG"), null);
    }

    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {

        MetaTokenRef currMetaToken = controller.getFilter().getCurrentMetaToken();

        List<ImmutableToken> orderedList = currMetaToken.getAssignedTokens().toJavaList();
        Collections.sort(orderedList);
        RemoveElementsDialog<ImmutableToken> dlg = new RemoveElementsDialog<>(controller.getView().getFrame(),
                MessageFormat.format(localeBundle.getString("SELECT WHICH TAG RELATIONS SHOULD BE REMOVED FROM METATAG "), currMetaToken.getName()),
                localeBundle.getString("CHOOSE TAG RELATIONS TO REMOVE"), orderedList);
        MyjaphooApp.getApplication().show(dlg);
        if (dlg.isOk()) {
            List<ImmutableToken> toks2Remove = dlg.getCheckedElements();

            controller.unAssignMetaTokenFromToken(currMetaToken.getRef(), toks2Remove);
            controller.createChangeLog(ChangeLogType.REMOVEMETATOKEN, "remove tag relations from metatag " + currMetaToken.getName() + ": " + RemoveTokenRelations.tokList(toks2Remove), null); //NOI18N
        }
    }
}
