/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action.db;

import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.AbstractMJAction;
import org.myjaphoo.gui.action.ViewContext;
import org.myjaphoo.model.dbconfig.DatabaseConfiguration;
import org.myjaphoo.model.logic.dbhandling.DatabaseExporter;

import java.awt.event.ActionEvent;

/**
 *
 * @author mla
 */
public class ExportToOtherDatabase extends AbstractMJAction {

    private DatabaseConfiguration targetDatabase;

    public ExportToOtherDatabase(MyjaphooController controller, DatabaseConfiguration targetDatabase) {
        super(controller, "Export Database", null);
        this.targetDatabase = targetDatabase;
    }

    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        DatabaseExporter exporter = new DatabaseExporter();
        exporter.exportToDatabase(controller.getProject(), targetDatabase);

    }
}
