package org.myjaphoo.gui.action.scriptactions

import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.util.MenuPathStructurizer
import org.myjaphoo.model.registry.ComponentRegistry

import javax.swing.*

/**
 * ScriptActionsPreparer 
 * @author mla
 * @version $Id$
 *
 */
//@TypeChecked
class ScriptActionsPreparer {


    MenuPathStructurizer structurizer = new MenuPathStructurizer(true);

    def <T> void prepare(MyjaphooController controller, Class<? extends ActionEntry<T>> actionEntryClass, T contextVar, group = null) {
        Collection<ActionEntry> entries = ComponentRegistry.registry.getEntryCollection(actionEntryClass);
        prepare(controller, entries, contextVar, group)
    }

    def <T> void prepare(MyjaphooController controller, Collection<ActionEntry> entries, T contextVar, group = null) {
        for (ActionEntry entry : entries) {
            if (entry.isAvailable(controller, contextVar)) {
                def theGroup = group == null ? entry.group : group;
                structurizer.add(entry.createAction(controller, contextVar), entry.order, entry.subCategory, theGroup);
            }
        }
    }

    def void addActionsToPopuMenu(JPopupMenu m) {
        structurizer.structurize(m, 10);
    }

    def void addActionsToMenu(JMenu m) {
        structurizer.structurize(m, 10);
    }
}
