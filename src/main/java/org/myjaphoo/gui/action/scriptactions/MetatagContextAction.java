package org.myjaphoo.gui.action.scriptactions;

import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.db.MetaToken;

import java.util.List;

/**
 * MetatagContextAction
 * @author mla
 * @version $Id$
 */
public class MetatagContextAction extends ActionEntry<List<ImmutableMetaToken>> {
}
