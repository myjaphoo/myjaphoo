package org.myjaphoo.gui.action.scriptactions

import org.myjaphoo.MyjaphooController
import org.myjaphoo.model.registry.ComponentRegistry

import javax.swing.*

/**
 * Helper class to add actions which are defined via scripts or plugins to context menus or menus.
 * @author mla
 * @version $Id$
 *
 */
class ScriptActions {

    static
    def <T> void addActionsToPopuMenu(MyjaphooController controller, JPopupMenu m, Class<? extends ActionEntry<T>> actionEntryClass, T contextVar) {
        m.addSeparator();
        ScriptActionsPreparer preparer = new ScriptActionsPreparer();
        preparer.prepare(controller, actionEntryClass, contextVar, null);
        preparer.addActionsToPopuMenu(m);
    }

    static
    def <T> void addActionsToMenu(MyjaphooController controller, JMenu m, Class<? extends ActionEntry<T>> actionEntryClass, T contextVar) {
        m.addSeparator();
        ScriptActionsPreparer preparer = new ScriptActionsPreparer();
        preparer.prepare(controller, actionEntryClass, contextVar, null);
        preparer.addActionsToMenu(m);
    }

    static void addCommonActions(MyjaphooController controller, JMenuBar menuBar, JMenu fileMenu) {

        Collection<CommonAction> entries = ComponentRegistry.registry.getEntryCollection(CommonAction.class);
        def byMenuName = entries.groupBy { it.menuName == null ? "File" : it.menuName }
        byMenuName.each { menuName, entryList ->
            def menu = fileMenu;
            if (menuName != "File") {
                menu = new JMenu(menuName);
                menuBar.add(menu, 6);
            }
            ScriptActionsPreparer preparer = new ScriptActionsPreparer();
            preparer.prepare(controller, entryList, null, null);
            preparer.addActionsToMenu(menu)
        }
    }
}


