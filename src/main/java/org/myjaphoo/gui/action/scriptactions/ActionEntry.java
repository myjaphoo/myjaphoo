package org.myjaphoo.gui.action.scriptactions;

import groovy.lang.Closure;
import org.myjaphoo.MyjaphooController;

import javax.swing.*;

/**
 * ActionEntry.
 * This is the context of the execution of a closure for script action configuration.
 * Therefore the properties could directly set within the config closure.
 * Example:
 * defTagContextAction("myaction") {
 * name = "Hello World!";
 * descr = "blabla"
 * action= { controller, tags ->  controller.message "hi, I got the following tags: $tags"}
 * }
 * @author lang
 * @version $Id$
 */
public class ActionEntry<T> {

    /** the name of the action entry. */
    private String name;

    private String descr;

    private Icon icon;

    private Closure action;

    private Closure activeCheck;

    private String group;

    private int order = 999999999;

    private String subCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Closure getAction() {
        return action;
    }

    public void setAction(Closure action) {
        this.action = action;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }


    public Action createAction(MyjaphooController controller, T contextVar) {
        ActionEntryContextAction action = new ActionEntryContextAction(controller, this, contextVar);
        return action;
    }

    public boolean isAvailable(MyjaphooController controller, T contextVar) {
        if (activeCheck != null) {
            Object result = activeCheck.call(controller, contextVar);
            if (result == null) {
                return false;
            }
            if (result instanceof Boolean) {
                return ((Boolean) result).booleanValue();
            }
            return false;

        } else {
            return true;
        }
    }
}
