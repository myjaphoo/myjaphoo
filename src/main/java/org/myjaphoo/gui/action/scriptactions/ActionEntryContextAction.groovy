package org.myjaphoo.gui.action.scriptactions

import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.action.AbstractMJAction
import org.myjaphoo.gui.action.ViewContext

import javax.swing.*
import java.awt.event.ActionEvent

/**
 * ActionEntryContextAction 
 * @author mla
 * @version $Id$
 *
 */
class ActionEntryContextAction<T> extends AbstractMJAction {

    private ActionEntry actionEntry;

    private T contextVar;

    public ActionEntryContextAction(MyjaphooController controller, ActionEntry actionEntry, T contextVar) {
        super(controller, contextVar == null ? actionEntry.name : (String) "$actionEntry.name: ${prepareArgList(contextVar)}", (Icon) actionEntry.getIcon());
        this.actionEntry = actionEntry;
        this.contextVar = contextVar;
    }

    static def prepareArgList(T contextVar) {
        if (contextVar instanceof List) {
            List cList = (List) contextVar;
            if (cList.size() > 3) {
                List sub = cList.subList(0, 2);
                return "${sub.size()} items: ${sub.join(', ')}...";
            }
        }
        if (contextVar == null) {
            return "";
        }
        return contextVar.toString();
    }

    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        if (contextVar != null) {
            actionEntry.getAction().call(controller, contextVar);
        } else {
            actionEntry.getAction().call(controller);
        }
    }
}
