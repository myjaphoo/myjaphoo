package org.myjaphoo.gui.action.scriptactions;

/**
 * CommonAction
 * @author mla
 * @version $Id$
 */
public class CommonAction extends ActionEntry<Object> {

    /** if set, it defines the main menu entry where this action should show up in the
     * menu bar.
     * If it is empty, the action is shown under the "Files" menu.
     * */
    private String menuName;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
