/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MovieNode;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.util.Filtering;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Recreates the tumbs for  one or more files.
 * @author lang
 */
public class RecalcCheckSums extends AbstractMJAction implements DisplayAsLastUsedActions {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/action/resources/RecalcCheckSums");

    public RecalcCheckSums(MyjaphooController controller, ViewContext context) {
        super(controller, localeBundle.getString("RECALC_CHECKSUM"), null, context);
    }

    @Override
    public void run(final MyjaphooController controller, ActionEvent e, ViewContext context)
            throws  Exception {

        final List<MovieNode> selMovies = context.getSelMovies();
        controller.recalcCheckSums(Filtering.nodes2Immutables(selMovies));
    }
}
