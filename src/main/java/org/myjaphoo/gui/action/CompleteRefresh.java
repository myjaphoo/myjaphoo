/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MyjaphooController;

import java.awt.event.ActionEvent;

/**
 * Clears  the caches and refreshes the view.
 *
 * @author lang
 */
public class CompleteRefresh extends AbstractMJAction {

    public CompleteRefresh(MyjaphooController controller) {
        super(controller, "Clear all Caches & refresh View", null);
    }

    @Override
    public void run(final MyjaphooController controller, ActionEvent e, ViewContext context) {
        invalidateAllCaches();
        controller.getView().updateAllViews();
    }

    public void invalidateAllCaches() {
        getController().getMainController().thumbCache.clearCache();
        getController().getProject().cacheActor.resetInternalCache();
        getController().getProject().cacheActor.resetImmutableCopy();
    }


}
