package org.myjaphoo.gui.action.projects

import groovy.swing.SwingBuilder
import groovy.transform.TypeChecked
import org.myjaphoo.MyjaphooApp
import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.action.AbstractMJAction
import org.myjaphoo.gui.action.ViewContext
import org.myjaphoo.gui.projects.NewProjectDialog
import org.myjaphoo.gui.projects.NewProjectInfo
import org.myjaphoo.gui.projects.ProjectsGuiActions

import java.awt.event.ActionEvent
import java.text.MessageFormat

/**
 * NewProject 
 * @author mla
 * @version $Id$
 *
 */

@TypeChecked
class NewProjectAction extends AbstractMJAction {

    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/resources/uitext");


    public NewProjectAction(MyjaphooController controller) {
        super(controller, MessageFormat.format(localeBundle.getString("newProjectAction.newProject")), null);
    }


    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        def SwingBuilder b = new SwingBuilder();
        b.edt {
            NewProjectDialog dlg = new NewProjectDialog(controller.getView().getFrame());
            dlg.pack();
            dlg.setLocationRelativeTo(controller.getView().getFrame());
            MyjaphooApp.getApplication().show(dlg);
            if (dlg.isOk()) {
                NewProjectInfo info = dlg.result;
                info.createProjectFiles();
                ProjectsGuiActions.openProject(info.dir)
            }
        }

    }
}
