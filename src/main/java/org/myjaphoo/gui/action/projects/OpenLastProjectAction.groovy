package org.myjaphoo.gui.action.projects

import groovy.transform.TypeChecked
import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.action.AbstractMJAction
import org.myjaphoo.gui.action.ViewContext
import org.myjaphoo.gui.projects.ProjectsGuiActions

import java.awt.event.ActionEvent
import java.text.MessageFormat

/**
 * NewProject 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class OpenLastProjectAction extends AbstractMJAction {

    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/resources/uitext");

    def String pathOfLastProjectFile;

    def OpenLastProjectAction(MyjaphooController controller, String pathOfLastProjectFile) {
        super(controller, MessageFormat.format(localeBundle.getString("openLastProjectAction.openLastProject"), pathOfLastProjectFile), null);
        this.pathOfLastProjectFile = pathOfLastProjectFile;
    }


    @Override
    def void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        ProjectsGuiActions.openProject(pathOfLastProjectFile)
    }
}
