package org.myjaphoo.gui.action.projects

import groovy.transform.TypeChecked
import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.action.AbstractMJAction
import org.myjaphoo.gui.action.ViewContext
import org.myjaphoo.gui.projects.ProjectsGuiActions

import java.awt.event.ActionEvent
import java.text.MessageFormat

/**
 * NewProject 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class OpenProjectAction extends AbstractMJAction {

    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/resources/uitext");

    public OpenProjectAction(MyjaphooController controller) {
        super(controller, MessageFormat.format(localeBundle.getString("openProjectAction.openProjectByFileDialog")), null);
    }


    @Override
    public void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        File file = chooseDir("Please select a Project dir");
        if (file != null) {
            ProjectsGuiActions.openProject(file.absolutePath)
        }
    }
}
