package org.myjaphoo.gui.action.projects

import groovy.transform.TypeChecked
import org.myjaphoo.MyjaphooController
import org.myjaphoo.gui.action.AbstractMJAction
import org.myjaphoo.gui.action.ViewContext
import org.myjaphoo.gui.projects.ProjectsGuiActions

import java.awt.event.ActionEvent

/**
 * NewProject
 *
 */
@TypeChecked
class CompareWithProjectAction extends AbstractMJAction {

    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/resources/uitext");

    def String projectPath;

    def CompareWithProjectAction(MyjaphooController controller, String projectPath) {
        super(controller,  projectPath, null);
        this.projectPath = projectPath;
    }


    @Override
    def void run(MyjaphooController controller, ActionEvent e, ViewContext context) throws Exception {
        ProjectsGuiActions.compareWithProject(controller, projectPath)
    }
}
