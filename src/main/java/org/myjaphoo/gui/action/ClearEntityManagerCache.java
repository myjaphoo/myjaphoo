/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MyjaphooController;

import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

/**
 * Clears the entity manager cache.
 *
 * @author lang
 * @todo remove! this does not make sense anymore with the new transaction handling
 */
@Deprecated
public class ClearEntityManagerCache extends AbstractMJAction {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/gui/action/resources/ClearEntityManagerCache");

    public ClearEntityManagerCache(MyjaphooController controller) {
        super(controller, localeBundle.getString("CLEAR ENTITY MANAGER CACHE"), null);
    }

    @Override
    public void run(final MyjaphooController controller, ActionEvent e, ViewContext context) {
        controller.getProject().connection.clear();

    }
}
