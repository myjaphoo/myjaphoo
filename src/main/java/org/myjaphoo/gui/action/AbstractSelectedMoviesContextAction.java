/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.action;

import org.myjaphoo.MovieNode;
import org.myjaphoo.MyjaphooApp;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.sellists.MovieSelectionConfirmationDialog;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.util.Filtering;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Copies one or more files to a destination dir.
 *
 * @author lang
 */
public abstract class AbstractSelectedMoviesContextAction extends AbstractMJAction {

    public AbstractSelectedMoviesContextAction(MyjaphooController controller, String title, ViewContext context) {
        super(controller, title, null, context);
    }

    public AbstractSelectedMoviesContextAction(
        MyjaphooController controller, String title, ViewContext context, Icon icon
    ) {
        super(controller, title, icon, context);
    }

    protected ArrayList<EntryRef> confirmSelection(String title, ViewContext context) {
        final List<MovieNode> selNodes = context.getSelMovies();
        ArrayList<EntryRef> selMovies = Filtering.nodes2Entries(selNodes);
        return confirmSelection(title, selMovies);
    }

    protected ArrayList<EntryRef> confirmSelection(
        String title, ArrayList<EntryRef> moviesToConfirm
    ) {
        return invokeAndWait(() -> {
            MovieSelectionConfirmationDialog dlg =
                new MovieSelectionConfirmationDialog(
                    getController(),
                    title,
                    getController().getView().getFrame(),
                    moviesToConfirm
                );
            MyjaphooApp.getApplication().show(dlg);
            if (dlg.isOk()) {
                return moviesToConfirm;
            } else {
                return null;
            }
        });
    }

    protected List<MovieNode> confirmSelectionOfNodes(String title, ViewContext context) {
        final List<MovieNode> selNodes = context.getSelMovies();
        return invokeAndWait(() -> {
            MovieSelectionConfirmationDialog dlg =
                new MovieSelectionConfirmationDialog(
                    getController(),
                    title,
                    getController().getView().getFrame(),
                    selNodes
                );
            MyjaphooApp.getApplication().show(dlg);
            if (!dlg.isOk()) {
                return null;
            }
            return selNodes;
        });
    }

    private <T> T invokeAndWait(Supplier<T> s) {
        if (SwingUtilities.isEventDispatchThread()) {
            return s.get();
        } else {
            final Object[] returnVal = new Object[1];
            try {
                SwingUtilities.invokeAndWait(() -> {
                    returnVal[0] = s.get();
                });
            } catch (InterruptedException | InvocationTargetException e) {
                throw new RuntimeException("error during execution on edt thread!", e);
            }
            return (T) returnVal[0];
        }
    }
}
