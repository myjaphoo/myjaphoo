package org.myjaphoo.gui.editor.rsta;

import org.apache.commons.lang.StringUtils;
import org.myjaphoo.model.cache.EntityCacheActor;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.Immutables;
import org.myjaphoo.model.cache.zipper.EntryZipper;
import org.myjaphoo.model.cache.zipper.MetaTokenZipper;
import org.myjaphoo.model.cache.zipper.TokenZipper;
import org.myjaphoo.model.filterparser.functions.Function;
import org.myjaphoo.model.filterparser.functions.Functions;
import org.myjaphoo.model.filterparser.idents.FixIdentifier;
import org.myjaphoo.model.filterparser.idents.Identifiers;
import org.myjaphoo.model.filterparser.idents.Qualifier;
import org.myjaphoo.model.filterparser.values.Value;
import org.myjaphoo.model.grouping.AutoKeyWordVeryStrongPartialGrouper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Caches completion hints, which are expensive to build (those from movie entries).
 */
public class CachedHints {

    private static Logger logger = LoggerFactory.getLogger(CachedHints.class);

    public static class AttributeHints {
        public final List<String> keys;
        public final HashMap<String, List<String>> words;

        public AttributeHints(HashMapSet hashMapSet) {
            words = hashMapSet.toTargetStructure();
            keys = hashMapSet.allKeysAsList();
        }
    }

    private HashMap<String, List<String>> identWords = new HashMap<>();

    private AttributeHints entryAttributeHints = null;

    private AttributeHints tagAttributeHints = null;

    private AttributeHints metaTagAttributeHints = null;

    private EntityCacheActor cacheActor;

    public CachedHints(EntityCacheActor cacheActor) {
        this.cacheActor = cacheActor;
    }

    private Set<String> createValuesForIdent(FixIdentifier ident) {
        long start = System.currentTimeMillis();

        Set<String> resultSet = new HashSet<>();

        boolean allTextIdent = ident == Identifiers.TEXT || ident == Identifiers.X || ident == Identifiers.ANYTHING
            || ident == Identifiers.SOMETHING;

        ImmutableModel model = cacheActor.getImmutableModel();
        if (ident.getOutputContextClass() == EntryZipper.class || allTextIdent) {
            boolean ismovie = ident.getOutputContextClass() == EntryZipper.class;
            Iterable<EntryZipper> entryIterable = Immutables.newEntrryZipperIterable(model, model.getEntryList());
            for (EntryZipper entry : entryIterable) {
                if (allTextIdent) {
                    String[] hints = org.apache.commons.lang.StringUtils.split(
                        entry.getCanonicalPath().toLowerCase(),
                        AutoKeyWordVeryStrongPartialGrouper.SEPARATORS
                    );
                    for (String hint : hints) {
                        if (hint.length() > 3) {
                            resultSet.add(hint);
                        }
                    }
                    if (!StringUtils.isEmpty(entry.getTitle())) {
                        resultSet.add(entry.getTitle());
                    }
                    if (!StringUtils.isEmpty(entry.getComment())) {
                        resultSet.add(entry.getComment());
                    }
                }

                if (ismovie) {
                    Value val = ident.extractIdentValue(entry);
                    HashMapSet.addValue(resultSet, val);
                }
            }
        } else if (ident.getOutputContextClass() == TokenZipper.class) {
            Iterable<TokenZipper> zipperIterable = Immutables.newTokenZipperIterable(
                model,
                model.getTokenTree().getValues()
            );
            for (TokenZipper token : zipperIterable) {
                Value val = ident.extractIdentValue(token);
                HashMapSet.addValue(resultSet, val);
            }
        } else if (ident.getOutputContextClass() == MetaTokenZipper.class) {
            Iterable<MetaTokenZipper> zipperIterable = Immutables.newMetaTokenZipperIterable(
                model,
                model.getMetaTokenTree().getValues()
            );
            for (MetaTokenZipper token : zipperIterable) {
                Value val = ident.extractIdentValue(token);
                HashMapSet.addValue(resultSet, val);
            }

        }
        long stop = System.currentTimeMillis();
        long duration = (stop - start) / 1000;
        logger.info("cached hints created for " + ident.getName() + " in " + duration + "s ");
        return resultSet;
    }

    public static AttributeHints calcEntryAttrHints(ImmutableModel model) {
        HashMapSet entryAttrMap = new HashMapSet();

        for (ImmutableMovieEntry entry : model.getEntryList()) {
            entryAttrMap.put(entry.getAttributes().toJavaMap());
        }
        return new AttributeHints(entryAttrMap);

    }

    public static AttributeHints calcTokenAttrHints(ImmutableModel model) {
        HashMapSet tagAttrMap = new HashMapSet();

        for (ImmutableToken token : model.getTokenTree().getValues()) {
            tagAttrMap.put(token.getAttributes().toJavaMap());
        }
        return new AttributeHints(tagAttrMap);
    }

    public static AttributeHints calcMetaTokenAttrHints(ImmutableModel model) {
        HashMapSet metaTagAttrMap = new HashMapSet();

        for (ImmutableMetaToken token : model.getMetaTokenTree().getValues()) {
            metaTagAttrMap.put(token.getAttributes().toJavaMap());
        }
        return new AttributeHints(metaTagAttrMap);
    }

    private void checkInitAttrs() {
        if (entryAttributeHints == null) {
            long start = System.currentTimeMillis();

            entryAttributeHints = calcEntryAttrHints(cacheActor.getImmutableModel());
            tagAttributeHints = calcTokenAttrHints(cacheActor.getImmutableModel());
            metaTagAttributeHints = calcMetaTokenAttrHints(cacheActor.getImmutableModel());

            long stop = System.currentTimeMillis();
            long duration = (stop - start) / 1000;
            logger.info("cached attr hints created in " + duration + "s");
        }
    }


    public List<String> getHintsForIdentifier(Qualifier ident) {
        List<String> result = identWords.get(ident.getName());
        if (result == null) {
            result = new ArrayList<>(createValuesForIdent((FixIdentifier) ident));
            Collections.sort(result);
            identWords.put(ident.getName(), result);
        }
        return result;
    }


    public List<String> getHintsForAttributeKeys(ImmutableAttributedEntity ae) {
        checkInitAttrs();
        if (ae instanceof ImmutableMovieEntry) {
            return entryAttributeHints.keys;
        } else if (ae instanceof ImmutableToken) {
            return tagAttributeHints.keys;
        } else if (ae instanceof ImmutableMetaToken) {
            return metaTagAttributeHints.keys;
        } else {
            throw new RuntimeException("unknown attributed entity!");
        }
    }

    public List<String> getEntryAttrKeys() {
        checkInitAttrs();
        return entryAttributeHints.keys;
    }

    public List<String> getTagAttrKeys() {
        checkInitAttrs();
        return tagAttributeHints.keys;
    }

    public List<String> getMetaTagAttrKeys() {
        checkInitAttrs();
        return metaTagAttributeHints.keys;
    }

    public List<String> getHintsForAttributes(Function func, String attributeName) {
        checkInitAttrs();
        if (func == Functions.ENTRYATTR) {
            return entryAttributeHints.words.get(attributeName);
        } else if (func == Functions.TAGATTR) {
            return tagAttributeHints.words.get(attributeName);
        } else if (func == Functions.METATAGATTR) {
            return metaTagAttributeHints.words.get(attributeName);
        } else {
            throw new RuntimeException("unknown attributed entity!");
        }
    }

    public List<String> getHintsForAttributes(ImmutableAttributedEntity ae, String attributeName) {
        checkInitAttrs();
        if (ae instanceof ImmutableMovieEntry) {
            return entryAttributeHints.words.get(attributeName);
        } else if (ae instanceof ImmutableToken) {
            return tagAttributeHints.words.get(attributeName);
        } else if (ae instanceof ImmutableMetaToken) {
            return metaTagAttributeHints.words.get(attributeName);
        } else {
            throw new RuntimeException("unknown attributed entity!");
        }
    }

}
