package org.myjaphoo.gui.editor.rsta;

import org.myjaphoo.model.filterparser.values.Value;
import org.myjaphoo.model.filterparser.values.ValueSet;

import java.util.*;

/**
 * HashMapSet
 * @author mla
 * @version $Id$
 */
public class HashMapSet {
    HashMap<String, Set<String>> mapSet = new HashMap<>();


    public void put(Map<String, String> map) {
        for (Map.Entry<String, String> attrEntry : map.entrySet()) {
            put(attrEntry.getKey(), attrEntry.getValue());
        }
    }

    public void put(String key, String val) {
        Set<String> entry = get(key);
        entry.add(val);
    }

    public Set<String> get(String key) {
        Set<String> entry = mapSet.get(key);
        if (entry == null) {
            entry = new HashSet<>();
            mapSet.put(key, entry);
        }
        return entry;
    }

    public HashMap<String, List<String>> toTargetStructure() {
        HashMap<String, List<String>> targetMap = new HashMap<>();
        for (Map.Entry<String, Set<String>> entry : mapSet.entrySet()) {
            List<String> list = new ArrayList<>(entry.getValue());
            // order the list:
            Collections.sort(list);
            targetMap.put(entry.getKey(), list);
        }
        return targetMap;
    }

    public List<String> allKeysAsList() {
        List<String> list = new ArrayList<>(mapSet.keySet());
        // order the list:
        Collections.sort(list);
        return list;
    }

    public static void addValue(Set<String> entry, Value val) {
        if (val instanceof ValueSet) {
            for (Value value : ((ValueSet) val).getValues()) {
                String realVal = value.convertToString();
                if (realVal != null) {
                    entry.add(realVal);
                }
            }
        } else {
            String realVal = val.convertToString();
            if (realVal != null) {
                entry.add(realVal);
            }
        }
    }
}
