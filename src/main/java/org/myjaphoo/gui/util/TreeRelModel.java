/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.util;

import io.vavr.collection.List;
import io.vavr.collection.Tree.Node;
import org.mlsoft.structures.TreeStructure;
import org.mlsoft.structures.Trees;
import org.myjaphoo.gui.WmTreeTableModel;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.TreeRel2;
import org.myjaphoo.model.db.EntityObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreePath;
import java.util.Collections;
import java.util.Comparator;

/**
 * Tree table model for wrapped nodes.
 * One special about this model is, that it could be used
 * to also build a flat list of the tree structure and it could be used to filter
 * the nodes.
 *
 * @author lang
 */
public abstract class TreeRelModel<T extends EntityObject> extends WmTreeTableModel {

    private static Logger logger = LoggerFactory.getLogger(TreeRelModel.class);
    protected boolean isEditable;


    public static class WrappedNodeWithModel<T> extends WrappedNode<T> {

        public final ImmutableModel model;
        public WrappedNodeWithModel(ImmutableModel model, T ref) {
            super(ref);
            this.model = model;
        }
    }

    public TreeRelModel(
        ImmutableModel model,
        TreeRel2<T, ?> treeRel, boolean filter, String typedText, boolean flatList, String[] columns
    ) {
        super(new WrappedNodeWithModel(model, treeRel.getRoot() == null ? null: treeRel.getRoot().getValue()), columns);
        setRoot(prepareModel(model, treeRel, filter, typedText, flatList));
    }

    protected WrappedNode prepareModel(ImmutableModel model, TreeRel2<T, ?> treeRel, boolean filter, String typedText, boolean flatList) {
        // todo could this be null?
        if (treeRel.getRoot() == null) {
            return null;
        }
        WrappedNodeWithModel root = new WrappedNodeWithModel(model, treeRel.getRoot().getValue());
        if (flatList) {
            prepareFlatList(model, root, treeRel.getRoot(), filter, typedText, 0);
        } else {
            prepareChildren(model, root, treeRel.getRoot(), filter, typedText);
        }

        return root;
    }

    private static Comparator<WrappedNode> C = new Comparator<WrappedNode>() {

        @Override
        public int compare(WrappedNode o1, WrappedNode o2) {
            return ((Comparable) o1.getRef()).compareTo(o2.getRef());
        }
    };

    private void prepareFlatList(ImmutableModel model,WrappedNode root, Node<T> newroot, boolean filter, String typedText, int level) {
        for (Node<T> child : newroot.getChildren()) {
            if (!filter || match(child.getValue(), typedText)) {
                if (shouldDisplayNodeInFlatMode(child.getValue(), level)) {
                    WrappedNodeWithModel childNode = new WrappedNodeWithModel(model, child.getValue());
                    root.addChild(childNode);
                }
            }
            prepareFlatList(model, root, child, filter, typedText, level + 1);
        }
        Collections.sort(root.getChildren(), C);

    }

    private void prepareChildren(ImmutableModel model, WrappedNode root, Node<T> newroot, boolean filter, String typedText) {
        for (Node<T> child : newroot.getChildren()) {
            if (!filter || match(child.getValue(), typedText)) {
                WrappedNodeWithModel childNode = new WrappedNodeWithModel(model, child.getValue());
                root.addChild(childNode);
                prepareChildren(model, childNode, child, filter, typedText);
            } else {
                prepareChildren(model, root, child, filter, typedText);
            }

        }
    }

    abstract protected boolean match(T tok, String typedText);

    public TreeStructure[] findPathForWrappedObject(T token) {
        WrappedNode tokNode = findWrappedNodeforWrappedObject(token);
        return getPathToRoot(tokNode);
    }

    public TreePath findTreePathForWrappedObject(T t) {
        return new TreePath(findPathForWrappedObject(t));
    }

    public WrappedNodeWithModel<T> findWrappedNodeforWrappedObject(T token) {
        WrappedNodeWithModel root = (WrappedNodeWithModel) getRoot();
        return searchRecursively(root, token);
    }

    private WrappedNodeWithModel<T> searchRecursively(WrappedNode<T> node, final T obj) {
        return (WrappedNodeWithModel<T>) Trees.searchDepthFirstSearch(node, new Trees.SearchFunction<WrappedNode<T>>() {

            @Override
            public boolean found(WrappedNode<T> node) {
                return obj.equals(node.getRef());
            }
        });
    }

    /**
     * Updates a list of wrapped nodes with new references values.
     * It then fires a node change event for each changed node.
     * Note, that the node value objects are identified by their equals
     * method.
     */
    public void updateNodes(List<T> objectsToUpdate) {

        logger.debug("updating wrapped nodes:{}", objectsToUpdate);
        for (T obj : objectsToUpdate) {
            if (obj != null) {
                logger.debug("searching node value {} in tree...", obj);
                WrappedNode<T> node = findWrappedNodeforWrappedObject(obj);
                if (node != null) {
                    logger.debug("found node value {}, now exchanging data, and firing node change event", obj);
                    // update the node
                    node.setRef(obj);
                    // and fire update:
                    nodeChanged(node);
                }
            }
        }
    }

    /**
     * adds a new object into the tree.
     * Builds a new wrapped node for the object, and adds it under
     * its parent node element.
     *
     * @param parent the paren object
     * @param newObj the new object to wrap and to insert into the tree
     */
    public void addedNewNodeObject(ImmutableModel model, T parent, T newObj) {
        WrappedNode<T> node = findWrappedNodeforWrappedObject(parent);
        WrappedNodeWithModel<T> newChild = new WrappedNodeWithModel<T>(model, newObj);
        node.addChild(newChild);
        modelSupport.fireChildAdded(
            findTreePathForWrappedObject(parent),
            node.getChildren().indexOf(newChild),
            newChild
        );
    }

    /**
     * Removes a object from the tree.
     */
    public void removeNodeObject(T parent, T objToDelete) {
        WrappedNode<T> node = findWrappedNodeforWrappedObject(parent);
        WrappedNode<T> childToRemove = findWrappedNodeforWrappedObject(objToDelete);
        modelSupport.fireChildRemoved(
            findTreePathForWrappedObject(parent),
            node.getChildren().indexOf(childToRemove),
            childToRemove
        );
    }

    /**
     * determines if the given children should be displayed in flat mode or not.
     * This method always returns true, which means, all nodes are displayed
     * in flat mode.
     * Implementations can override this to show only certain nodes, e.g.
     * only leafs of a certain level, etc.
     *
     * @param node  the node to check
     * @param level the level of this node (path depth)
     *
     * @return true, if this node should be displayed in flat mode
     */
    public boolean shouldDisplayNodeInFlatMode(T node, int level) {
        return true;
    }
}
