package org.myjaphoo.gui.util;

import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.myjaphoo.gui.comparators.EntryOrderType;
import org.myjaphoo.model.registry.ComponentRegistry;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Helper for gui components using registered classes of the component registry.
 * @author mla
 * @version $Id$
 */
public class RegistryUIUtils {

    public static ListComboBoxModel createListComboBoxModel(Class clazz) {
        Collection<EntryOrderType> orderEntries = ComponentRegistry.registry.getEntryCollection(clazz);
        return new ListComboBoxModel(new ArrayList(orderEntries));
    }
}
