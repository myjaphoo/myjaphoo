package org.myjaphoo.gui.util

import groovy.swing.SwingBuilder
import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j
import org.jdesktop.swingx.JXErrorPane
import org.jdesktop.swingx.error.ErrorInfo

import javax.swing.*
import java.util.logging.Level

/**
 * MyjaphooDialogs 
 * @author mla
 * @version $Id$
 *
 */
@Slf4j
@TypeChecked
class MyjaphooDialogs {

    static def showAndLogErroDlg(JFrame frame, String title, String errMsg, Throwable e) {
        log.error(errMsg, e);
        ErrorInfo info = new ErrorInfo(title, errMsg, null, null, e, Level.SEVERE, null);
        new SwingBuilder().doLater { JXErrorPane.showDialog(frame, info); }
    }

    static def boolean confirm(JFrame frame, Object msgToConfirmWithYes) {
        SwingBuilder s = new SwingBuilder();
        def boolean returnValue;
        s.edt {
            returnValue = JOptionPane.showConfirmDialog(frame, msgToConfirmWithYes, "Please Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
        }
        return returnValue;

    }

    static def message(JFrame frame, Object msg) {
        new SwingBuilder().doLater { JOptionPane.showMessageDialog(frame, msg) }
    }

    static def String getInputValue(JFrame frame, Object msg) {
        SwingBuilder s = new SwingBuilder();
        def String returnValue;
        s.edt {
            returnValue = JOptionPane.showInputDialog(frame,
                    msg, "Please input value", JOptionPane.QUESTION_MESSAGE);
        }
        return returnValue;
    }
}
