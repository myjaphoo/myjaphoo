/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.util;

import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author lang
 */
public abstract class MenuStructurizer {

    private ArrayList<Entry> entryList = new ArrayList<Entry>();

    private static final Comparator<Entry> COMPARATOR = new Comparator<Entry>() {

        @Override
        public int compare(Entry o1, Entry o2) {
            return o1.orderCriteria.compareTo(o2.orderCriteria);
        }
    };
    private Icon folderIcon;

    public void setFolderIcon(Icon folderIcon) {
        this.folderIcon = folderIcon;
    }

    protected JMenu createMenu(String txt) {
        JMenu m = new JMenu(txt);
        if (folderIcon != null) {
            m.setIcon(folderIcon);
        }
        return m;
    }

    protected static class Entry {

        JMenuItem item;
        Comparable orderCriteria;
        String path;

        String group;
    }

    public void add(JMenuItem item, Comparable orderCriteria, String path) {
        add(item, orderCriteria, path, null);
    }

    public void add(JMenuItem item, Comparable orderCriteria, String path, String group) {
        Entry entry = new Entry();
        entry.item = item;
        entry.orderCriteria = orderCriteria;
        entry.path = StringUtils.strip(path, "/");
        entry.group = group;
        entryList.add(entry);
    }

    public void add(Action action, Comparable orderCriteria, String path) {
        add(action, orderCriteria, path, null);
    }

    public void add(Action action, Comparable orderCriteria, String path, String group) {
        add(new JMenuItem(action), orderCriteria, path, group);
    }

    public int entrySize() {
        return entryList.size();
    }

    protected ArrayList<Entry> getEntryList() {
        Collections.sort(entryList, COMPARATOR);
        return entryList;
    }

    abstract public void structurize(JComponent parent, int groupSize);
}
