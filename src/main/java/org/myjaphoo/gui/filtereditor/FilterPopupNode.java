package org.myjaphoo.gui.filtereditor;

import org.mlsoft.structures.AbstractTreeNode;
import org.mlsoft.swing.TreePopupSelectionEvent;
import org.mlsoft.swing.TreePopupSelectionListener;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * DefNode
 * @author mla
 * @version $Id$
 */
public class FilterPopupNode extends AbstractTreeNode {
    private String name;
    private Action action;

    public FilterPopupNode(FilterPopupNode parent, String name) {
        super(parent);
        this.name = name;
        if (parent != null) {
            parent.addChild(this);
        }
    }

    public FilterPopupNode(FilterPopupNode parent, Action action) {
        super(parent);
        if (parent != null) {
            parent.addChild(this);
        }
        this.action = action;
        this.name = (String) action.getValue(Action.NAME);
    }

    public FilterPopupNode add(Action action) {
        FilterPopupNode child = new FilterPopupNode(this, action);
        return child;
    }


    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void executeActionIfDefined() {
        if (action != null) {
            action.actionPerformed(new ActionEvent(this, 0, ""));
        }
    }

    /**
     * Default selection listener for filter popup nodes. Simply call the action method defined on the node.
     */
    public final static TreePopupSelectionListener FILTERPOPUPNODESELECTIONLISTENER = new TreePopupSelectionListener() {
        @Override
        public void selectionChanged(TreePopupSelectionEvent tse) {
            FilterPopupNode node = (FilterPopupNode) tse.evt.getPath().getLastPathComponent();
            node.executeActionIfDefined();
        }
    };
}