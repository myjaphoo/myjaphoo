package org.myjaphoo.gui.projects

import groovy.transform.TypeChecked
import org.myjaphoo.project.ProjectDatabaseType
import org.myjaphoo.project.ProjectManagement

/**
 * Result type from the new project dialog.
 *
 */
@TypeChecked
class NewProjectInfo {

    def String dir;

    def String name;

    def String description;

    def ProjectDatabaseType databaseType = ProjectDatabaseType.H2;

    def createProjectFiles() {
        ProjectManagement.createProjectFiles(dir, databaseType, name, "projectDatabase");
    }
}
