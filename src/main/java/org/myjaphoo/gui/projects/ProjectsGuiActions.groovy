package org.myjaphoo.gui.projects

import groovy.swing.SwingBuilder
import groovy.transform.TypeChecked
import org.myjaphoo.MyjaphooApp
import org.myjaphoo.MyjaphooController
import org.myjaphoo.MyjaphooCorePrefs
import org.myjaphoo.MyjaphooView
import org.myjaphoo.gui.MainApplicationController
import org.myjaphoo.gui.action.projects.CompareWithProjectAction
import org.myjaphoo.gui.action.projects.NewProjectAction
import org.myjaphoo.gui.action.projects.OpenLastProjectAction
import org.myjaphoo.gui.action.projects.OpenProjectAction
import org.myjaphoo.model.util.UserDirectory
import org.myjaphoo.project.Project
import org.myjaphoo.project.ProjectConfiguration
import org.myjaphoo.project.ProjectManagement

import javax.swing.*

/**
 * ProjectsGuiActions 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class ProjectsGuiActions {


    def initGuiMenus(JMenu projectsMenu, MyjaphooController controller) {
        projectsMenu.add(new NewProjectAction(controller))
        projectsMenu.add(new OpenProjectAction(controller))
        ProjectConfiguration appConfig = new ProjectConfiguration(UserDirectory.getDirectory(), UserDirectory.getDirectory());
        List<String> lruList = appConfig.projectProperties.getList("lastUsedProject");

        if (lruList.size() > 0) {
            projectsMenu.addSeparator()
            lruList.each {
                projectsMenu.add(new OpenLastProjectAction(controller, it))
            }
            projectsMenu.addSeparator();
            JMenu compareMenu = new JMenu("compare with")
            projectsMenu.add(compareMenu)
            lruList.each {
                compareMenu.add(new CompareWithProjectAction(controller, it))
            }
        }

    }

    static def openProject(String pathOfProjectDir) {
        Project newProject = ProjectManagement.openProject(pathOfProjectDir);

        updateLRU(pathOfProjectDir)
        new SwingBuilder().edt {
            def mainController = new MainApplicationController(newProject);
            MyjaphooView firstView = new MyjaphooView(mainController, MyjaphooApp.application);
            MyjaphooApp.application.show(firstView);
        }


    }

    static void compareWithProject(MyjaphooController controller, String otherProjectPath) {
        ProjectManagement.checkProject(otherProjectPath)
        def compareController = MainApplicationController.getOrOpenProjectAndMainController(otherProjectPath);
        controller.mainController.databaseComparison.comparisonController = compareController
        controller.getView().getInfoPanel().updateDBComparisonValues();
    }

    static def updateLRU(String projectDir) {
        ProjectConfiguration appConfig = new ProjectConfiguration(UserDirectory.getDirectory(), UserDirectory.getDirectory());
        MyjaphooCorePrefs prefs = new MyjaphooCorePrefs(appConfig);
        List<String> lruList = appConfig.projectProperties.getList("lastUsedProject");
        lruList.add(0, projectDir);
        lruList = lruList.toUnique()
        while (lruList.size() > 20) {
            lruList = lruList.dropRight(1);
        }
        appConfig.projectProperties.setList("lastUsedProject", lruList);
        prefs.ROOT.save();
    }
}
