package org.myjaphoo.gui.projects;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.Validator;
import com.jgoodies.validation.util.PropertyValidationSupport;
import org.apache.commons.lang.StringUtils;

import java.io.File;

/**
 * Validator for Database Configurations.
 */
public class NewProjectValidator implements Validator<NewProjectInfo> {

    public static final String NAME_MANDATORY = "Name is mandatory";

    @Override
    public ValidationResult validate(NewProjectInfo newProjectInfo) {
        PropertyValidationSupport support = new PropertyValidationSupport(
                newProjectInfo, "New Project");
        if (StringUtils.isEmpty(newProjectInfo.getName())) {
            support.addError("Name", NAME_MANDATORY);
        }
        if (StringUtils.isEmpty(newProjectInfo.getDir())) {
            support.addError("Dir", "Directory is mandatory");
        } else {
            File dirFile = new File(newProjectInfo.getDir());
            if (!dirFile.exists()) {
                support.addError("Dir", "Directory does not exists");
            }
        }
        return support.getResult();
    }
}
