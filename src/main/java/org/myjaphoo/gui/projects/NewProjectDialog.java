/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EditDatabaseConnectionDialog.java
 *
 * Created on 27.02.2012, 14:56:19
 */
package org.myjaphoo.gui.projects;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.combobox.EnumComboBoxModel;
import org.mlsoft.common.prefs.ui.GenericPrefPage;
import org.myjaphoo.gui.db.EditDatabaseConnectionDialog;
import org.myjaphoo.project.ProjectDatabaseType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * @author lang
 */
public class NewProjectDialog extends JDialog {

    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;
    private FormLayout layout = new FormLayout(
        "right:max(40dlu;p), 4dlu, 110dlu:grow, 2dlu, " // 1st major column
            + " right:max(20dlu;p)", // 2nd major column
        ""
    );                                      // add rows dynamically
    private DefaultFormBuilder builder = new DefaultFormBuilder(layout);
    private NewProjectInfo result = new NewProjectInfo();
    private JTextField jtextFieldName = new JTextField();
    private EnumComboBoxModel<ProjectDatabaseType> databaseTypeModel = new EnumComboBoxModel(ProjectDatabaseType.class);
    private JComboBox jcomboBoxDbType = new JComboBox(databaseTypeModel);
    private JTextField jtextFieldDir = new JTextField();
    private JTextField jtextFieldDescription = new JTextField();
    private static final JFileChooser downloadLocationChooser = new JFileChooser();
    private JLabel labelDatabase;

    /**
     * Creates new form EditDatabaseConnectionDialog
     */
    public NewProjectDialog(JFrame frame) {
        super(frame, true);
        initComponents();
        prepareForm();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {

            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
        setSize(400, 600);
    }

    private void prepareForm() {
        ValidationComponentUtils.setMandatory(jtextFieldName, true);
        ValidationComponentUtils.setMessageKey(jtextFieldName, "Project.Name");
        ValidationComponentUtils.setSeverity(jtextFieldName, Severity.ERROR);

        JXHeader title = new JXHeader(
            "New Project Parameter",
            "Please choose a directory for the new project as well as a name for the project file. You could also choose to use H2 or Derby as database."
        );
        builder.append(title, builder.getColumnCount());

//        builder.appendSeparator("Connection Info");
        builder.append("Name", jtextFieldName);
        builder.nextLine();

        builder.append(new JLabel("Directory"), jtextFieldDir);
        JButton downloadLocationButton = GenericPrefPage.createFileChooserButton("choose directory");
        downloadLocationButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                downloadLocationChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnVal = downloadLocationChooser.showOpenDialog(NewProjectDialog.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = downloadLocationChooser.getSelectedFile();
                    result.setDir(file.getAbsolutePath());
                    jtextFieldDir.setText(file.getAbsolutePath());
                
//                    if (result.getName() == null) {
//                        result.setName(file.getName() + Project.PROJECT_FILE_SUFFIX);
//                    }
                   // setValues();
                }
            }
        });
        builder.append(downloadLocationButton);
        builder.nextLine();

        builder.append("Database Type", jcomboBoxDbType);
        builder.nextLine();

        labelDatabase = new JLabel("Description");
        builder.append(labelDatabase, jtextFieldDescription);
        builder.nextLine();

        jMainPanel.setLayout(new BorderLayout());

        ValidationComponentUtils.updateComponentTreeMandatoryAndBlankBackground(builder.getPanel());

        ((JXTitledPanel) jMainPanel).setContentContainer(new JScrollPane(builder.getPanel()));
        ((JXTitledPanel) jMainPanel).setTitle("Edit Database Connection Parameter");
        enableDisable();
        setValues();
    }

    private void enableDisable() {
        pack();
    }

    private void copyBackValues() {
        result.setName(jtextFieldName.getText());
        result.setDir(jtextFieldDir.getText());

        result.setDescription(jtextFieldDescription.getText());
        result.setDatabaseType(databaseTypeModel.getSelectedItem());

    }

    private void setValues() {
        jtextFieldName.setText(result.getName());
        jtextFieldDir.setText(result.getDir());

        jtextFieldDescription.setText(result.getDescription());
        databaseTypeModel.setSelectedItem(result.getDatabaseType());
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMainPanel = new JXTitledPanel();
        jActionPanel = new JPanel();
        cancelButton = new JButton();
        okButton = new JButton();

        setName("Form"); // NOI18N
        setTitle("Create New Project");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jMainPanel.setName("jMainPanel"); // NOI18N

        GroupLayout jMainPanelLayout = new GroupLayout(jMainPanel);
        jMainPanel.setLayout(jMainPanelLayout);
        jMainPanelLayout.setHorizontalGroup(
            jMainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 400, Short.MAX_VALUE)
        );
        jMainPanelLayout.setVerticalGroup(
            jMainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 267, Short.MAX_VALUE)
        );

        getContentPane().add(jMainPanel, BorderLayout.CENTER);

        jActionPanel.setName("jActionPanel"); // NOI18N
        jActionPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.myjaphoo.MyjaphooApp.class).getContext().getResourceMap(
            NewProjectDialog.class);

        cancelButton.setText("Cancel"); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jActionPanel.add(cancelButton);

        okButton.setText("Ok"); // NOI18N
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        jActionPanel.add(okButton);
        getRootPane().setDefaultButton(okButton);

        getContentPane().add(jActionPanel, BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        copyBackValues();
        ValidationResult validationResult = new NewProjectValidator().validate(result);
        ValidationComponentUtils.updateComponentTreeSeverity(builder.getPanel(), validationResult);
        ValidationComponentUtils.updateComponentTreeSeverityBackground(builder.getPanel(), validationResult);

        if (validationResult.hasErrors()) {
            EditDatabaseConnectionDialog.showValidationMessage(
                evt,
                "To create the new project, fix the following errors:",
                validationResult
            );
            return;
        }
        if (validationResult.hasWarnings()) {
            EditDatabaseConnectionDialog.showValidationMessage(
                evt,
                "Note: some fields are invalid.",
                validationResult
            );
        }

        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog


    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    public NewProjectInfo getResult() {
        return result;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton cancelButton;
    private JPanel jActionPanel;
    private JPanel jMainPanel;
    private JButton okButton;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = RET_CANCEL;

    public boolean isOk() {
        return returnStatus == RET_OK;
    }


}
