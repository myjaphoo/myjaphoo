package org.myjaphoo.gui.protocolhanders.mtagpic;

import io.vavr.control.Option;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.picmode.Picture;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Protocol connection for internal protocol to deliver tag images.
 * see
 * http://stackoverflow.com/questions/9388264/jeditorpane-with-inline-image
 * for explanation.
 */
public class MTagPicConnection extends URLConnection {

    private static final Logger logger = LoggerFactory.getLogger(MTagPicConnection.class);

    public MTagPicConnection(URL u) {
        super(u);
    }

    @Override
    public void connect() throws IOException {
        connected = true;
    }

    public static class ParsedTagPic {
        public String tagName;
        public MainApplicationController mainController;
    }

    public static ParsedTagPic parseUrl(String prefix, URL url) {
        String data = url.toString();
        data = data.replaceFirst(prefix, "");
        String[] parts = data.split("_");
        int controllerHash = Integer.parseInt(parts[0]);
        ParsedTagPic result = new ParsedTagPic();

        result.tagName = parts[1];
        result.mainController = MainApplicationController.getController(controllerHash);
        return result;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        ParsedTagPic info = parseUrl("mtagpic:", url);

        if (info.mainController != null) {
            logger.info("mtag pic protocol for " + info.tagName);
            // this should be now a metatag name.
            // get the tag and return the picture:
            Option<ImmutableMetaToken> token = findMetaTag(info.mainController, info.tagName);
            if (token.isDefined()) {
                logger.info("return input stream for meta tag " + token.get().getName());

                int thumbSize = info.mainController.project.prefs.PRF_THUMBSIZE.getVal();
                ImageIcon icon = (ImageIcon) info.mainController.metaTokenThumbCache.loadImageForToken(
                    token.get(),
                    thumbSize,
                    null
                );
                return Picture.toInputStream(icon);
            }
        }
        return new ByteArrayInputStream(new byte[0]);
    }


    private Option<ImmutableMetaToken> findMetaTag(MainApplicationController mainController, String name) {
        return mainController.project.cacheActor.getImmutableModel().getMetaTokenTree().getValues().find(t -> t.getName().equalsIgnoreCase(
            name));
    }
}
