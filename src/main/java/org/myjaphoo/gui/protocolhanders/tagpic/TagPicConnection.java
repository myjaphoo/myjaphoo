package org.myjaphoo.gui.protocolhanders.tagpic;

import io.vavr.control.Option;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.picmode.Picture;
import org.myjaphoo.gui.protocolhanders.mtagpic.MTagPicConnection;
import org.myjaphoo.model.cache.ImmutableToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Protocol connection for internal protocol to deliver tag images.
 * see
 * http://stackoverflow.com/questions/9388264/jeditorpane-with-inline-image
 * for explanation.
 */
public class TagPicConnection extends URLConnection {

    private static final Logger logger = LoggerFactory.getLogger(TagPicConnection.class);

    public TagPicConnection(URL u) {
        super(u);
    }

    @Override
    public void connect() throws IOException {
        connected = true;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        MTagPicConnection.ParsedTagPic info = MTagPicConnection.parseUrl("tagpic:", url);
        logger.info("tag pic protocol for " + info.tagName);
        // this should be now a tag name.
        // get the tag and return the picture:
        if (info.mainController != null) {
            Option<ImmutableToken> token = findTag(info.mainController, info.tagName);
            if (token.isDefined()) {
                logger.info("return input stream for tag " + token.get().getName());
                int thumbSize = info.mainController.project.prefs.PRF_THUMBSIZE.getVal();
                ImageIcon icon = info.mainController.tokenThumbCache.loadImageForToken(
                    token.get(),
                    thumbSize,
                    null
                );
                if (icon != null) {
                    return Picture.toInputStream(icon);
                }
            }
        }
        return new ByteArrayInputStream(new byte[0]);
    }


    private Option<ImmutableToken> findTag(MainApplicationController mainController, String name) {
        return mainController.project.cacheActor.getImmutableModel().getTokenTree().getValues().find(t -> t.getName().equalsIgnoreCase(
            name));
    }
}
