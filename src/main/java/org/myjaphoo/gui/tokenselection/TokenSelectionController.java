/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.tokenselection;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.action.AddNewTokenActionInTokenTree;
import org.myjaphoo.gui.thumbtable.thumbcache.TokenThumbCache;
import org.myjaphoo.gui.token.AbstractTokenPanelController;
import org.myjaphoo.gui.token.TokenTree;
import org.myjaphoo.model.cache.ChangeSet;
import org.myjaphoo.model.cache.zipper.TokenRef;

import javax.swing.*;

/**
 * @author mla
 */
public class TokenSelectionController extends AbstractTokenPanelController {

    private String typedText;
    private TokenRef token;
    private TokenSelectionDialog dialog;
    private MyjaphooController mainController;

    public TokenSelectionController(MyjaphooController mainController, TokenSelectionDialog dialog) {
        this.dialog = dialog;
        this.mainController = mainController;
        GlobalBus.bus.register(this);
    }

    @Subscribe(onETD = true)
    public void databaseCacheChanged(ChangeSet e) {
        if (e.getTokenSet().size() > 0) {
            TokenSelectionController.this.dialog.getTokenPanel().refreshView();
        }
    }

    @Override
    public void setCurrentToken(TokenRef token) {
        this.token = token;
    }

    @Override
    public TransferHandler createTransferHandler(TokenTree tokenTree) {
        return null;
    }

    @Override
    public JPopupMenu createTokenTreePopupMenu() {
        JPopupMenu m = new JPopupMenu();
        m.add(new AddNewTokenActionInTokenTree(mainController));
        return m;
    }

    void setTypedText(String typedText) {
        this.typedText = typedText;
    }

    @Override
    public AbstractTreeTableModel createTokenTreeModel() {
        final AbstractTreeTableModel tokenTreeModel = new FilteredTokenTreeModel(
            mainController.getMainController(),
            mainController.getProject().cacheActor.getImmutableModel(),
            isFlatView(),
            typedText
        );
        return tokenTreeModel;
    }

    /**
     * @return the token
     */
    public TokenRef getToken() {
        return token;
    }

    @Override
    public void doubleClicked() {
        dialog.okAndClose();
    }

    @Override
    public void onTokenSelected(TokenRef token) {
        // do nothing
    }

    @Override
    public TokenThumbCache getTokenThumbCache() {
        return mainController.getMainController().tokenThumbCache;
    }

    @Override
    public MainApplicationController getMainController() {
        return mainController.getMainController();
    }
}
