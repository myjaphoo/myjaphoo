/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movietree;

import org.mlsoft.structures.TreeStructure;

import javax.swing.tree.TreeNode;
import java.util.Comparator;

/**
 * @author mla
 */
public abstract class AbstractMovieTreeNode<C extends AbstractMovieTreeNode<C>> implements TreeNode, TreeStructure<C> {

    private C parent;

    public AbstractMovieTreeNode() {
    }

    public abstract String getName();

    public abstract String getTitle();

    public abstract int getNumOfContainingMovies();

    public abstract long getSizeOfContainingMovies();

    public abstract void sortList(Comparator comparator);

    public void sortNaturalByName(Comparator<AbstractMovieTreeNode> comparator) {
        sortList(comparator);
        for (AbstractMovieTreeNode node : getChildren()) {
            node.sortNaturalByName(comparator);
        }
    }

    public String getPathName() {
        StringBuilder b = new StringBuilder();
        AbstractMovieTreeNode node = this;
        while (node != null) {
            if (node.getParent() != null) {
                if (b.length() > 0) {
                    b.insert(0, "/"); //NOI18N
                }
                b.insert(0, node.getName());
            } else {
                // für root setzen wir nicht den namen:
                //b.insert(0, "Structure/");
            }
            node = node.getParent();
        }
        return b.toString();
    }

    public C getParent() {
        return parent;
    }

    public void setParent(C node) {
        this.parent = node;
    }
}
