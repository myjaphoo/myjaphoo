/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movietree;

import org.myjaphoo.MovieNode;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Node, zum strukturieren von movies im movietree.
 *
 * @author lang
 */
public class MovieStructureNode extends AbstractMovieTreeNode {

    private String name;
    private Integer numOfContainingMovies = null;

    private List<AbstractMovieTreeNode> children = new ArrayList<>();

    private Long size = null;
    /**
     * the dimension or grouping expression for this node.
     */
    private String groupingExpr;
    /**
     * kanonischer directory pfad, der durch diese struktur-node repräsentiert wird. Kann auch null sein,
     * falls diese node kein directory repräsentiert.
     */
    private String canonicalDir;

    /**
     * lightweight reference to a marker for that particular node,
     * e.g. special context relevant menu entries, etc.
     */
    private Object marker = null;

    private HashMap<String, Double> aggregatedValues = null;

    public MovieStructureNode(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<AbstractMovieTreeNode> getChildren() {
        return children;
    }

    public List<AbstractMovieTreeNode> getChildrenAsList() {
        return children;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getNumOfContainingMovies() {
        if (numOfContainingMovies != null) {
            return numOfContainingMovies.intValue();
        } else {
            int count = 0;
            for (AbstractMovieTreeNode node : getChildren()) {
                if (node instanceof MovieStructureNode) {
                    count += ((MovieStructureNode) node).getNumOfContainingMovies();
                } else if (node instanceof MovieNode) {
                    count++;
                }
            }

            numOfContainingMovies = count;
            return numOfContainingMovies.intValue();
        }
    }

    @Override
    public long getSizeOfContainingMovies() {
        if (size != null) {
            return size;
        } else {
            long count = 0;
            for (AbstractMovieTreeNode node : getChildren()) {
                if (node instanceof MovieStructureNode) {
                    count += ((MovieStructureNode) node).getSizeOfContainingMovies();
                } else if (node instanceof MovieNode) {
                    count += ((MovieNode) node).getMovieEntry().getFileLength();
                }
            }

            size = count;
            return count;
        }
    }


    /**
     * @return the canonicalDir
     */
    public String getCanonicalDir() {
        return canonicalDir;
    }

    /**
     * @param canonicalDir the canonicalDir to set
     */
    public void setCanonicalDir(String canonicalDir) {
        this.canonicalDir = canonicalDir;
    }

    @Override
    public String getTitle() {
        return null;
    }

    public String getGroupingExpr() {
        return groupingExpr;
    }

    public void setGroupingExpr(String groupingExpr) {
        this.groupingExpr = groupingExpr;
    }

    public Object getMarker() {
        return marker;
    }

    public void setMarker(Object marker) {
        this.marker = marker;
    }

    public Set<String> getAllAggregatedKeys() {
        if (aggregatedValues == null) {
            return Collections.emptySet();
        } else {
            return aggregatedValues.keySet();
        }
    }

    public Double getAggregatedValue(String key) {
        if (aggregatedValues == null) {
            return null;
        } else {
            return aggregatedValues.get(key);
        }
    }

    public void putAggregatedValue(String key, Double value) {
        if (aggregatedValues == null) {
            aggregatedValues = new HashMap<>();
        }
        aggregatedValues.put(key, value);
    }

    public void setAggregatedValues(HashMap<String, Double> aggregatedValues) {
        this.aggregatedValues = aggregatedValues;
    }

    @Override
    public TreeNode getChildAt(int childIndex) {
        return children.get(childIndex);
    }

    @Override
    public int getChildCount() {
        return children.size();
    }

    @Override
    public int getIndex(TreeNode node) {
        return children.indexOf(node);
    }

    @Override
    public boolean getAllowsChildren() {
        return false;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public Enumeration children() {
        return Collections.enumeration(children);
    }

    public void sortList(Comparator comparator) {
        Collections.sort(children, comparator);
    }

    public void addChild(AbstractMovieTreeNode movieStructureNode) {
        children.add(movieStructureNode);
        movieStructureNode.setParent(this);
    }

    public void removeChild(AbstractMovieTreeNode child) {
        children.remove(child);
    }
}
