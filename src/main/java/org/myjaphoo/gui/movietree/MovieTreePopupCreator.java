/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movietree;

import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.ViewContext;
import org.myjaphoo.gui.thumbtable.ThumbPopupMenus;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author mla
 */
public class MovieTreePopupCreator {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/gui/movietree/resources/MovieTreePopupCreator");

    public static JPopupMenu createPopup(MyjaphooController controller, AbstractMovieTreeNode currentSelectedDir) {
        ViewContext context = new ViewContext(controller.getProject(), flatAllNodesInTree(currentSelectedDir));
        return ThumbPopupMenus.createThumbPopupMenu(controller, context);
    }

    private static List<AbstractLeafNode> flatAllNodesInTree(AbstractMovieTreeNode currentSelectedDir) {
        ArrayList<AbstractLeafNode> result = new ArrayList<AbstractLeafNode>();
        if (currentSelectedDir != null) {
            flatLeafs(currentSelectedDir, result);
        }
        return result;
    }

    private static void flatLeafs(
        AbstractMovieTreeNode<? extends AbstractMovieTreeNode> root, ArrayList<AbstractLeafNode> result
    ) {
        if (root.isLeaf()) {
            result.add((AbstractLeafNode) root);
        } else {
            for (AbstractMovieTreeNode child : root.getChildren()) {
                flatLeafs(child, result);
            }
        }
    }
}
