/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movietree;

import java.awt.Color;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import org.jdesktop.swingx.renderer.DefaultTreeRenderer;
import org.jdesktop.swingx.renderer.IconValue;
import org.jdesktop.swingx.renderer.StringValue;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.util.Helper;
import org.myjaphoo.model.logic.FileSubstitutionImpl;

/**
 *
 * @author mla
 */
public class StructureDefiningTreeCellRenderer extends DefaultTreeRenderer {

    public StructureDefiningTreeCellRenderer(MyjaphooController controller) {
        super(createIconValue(), createStringValue(controller));
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        Component comp = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        return comp;
    }

    public static StringValue createStringValue(MyjaphooController controller) {
        StringValue sv = new StringValue() {

            private FileSubstitutionImpl fs = new FileSubstitutionImpl(controller.getProject());

            @Override
            public String getString(Object value) {
                if (value instanceof DefNode) {
                    return createLabelTextForDefNode((DefNode) value);
                } else {
                    return value.toString();
                }
            }

            private String createLabelTextForDefNode(DefNode defNode) {
                Color color = Helper.getColorForDim(defNode.getDim());
                if (color != null) {
                    return "<html>" + Helper.wrapColored(color, defNode.toString()) + "</html>"; //NOI18N
                } else {
                    return defNode.toString();
                }
            }
        };
        return sv;
    }

    public static IconValue createIconValue() {
        IconValue iv = new IconValue() {

            @Override
            public Icon getIcon(Object value) {
                return null;
            }
        };
        return iv;

    }
}
