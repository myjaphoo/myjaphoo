/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movietree;

import org.myjaphoo.model.FileTypeInterface;

import javax.swing.tree.TreeNode;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

/**
 * @author mla
 */
public abstract class AbstractLeafNode extends AbstractMovieTreeNode {

    public AbstractLeafNode() {

    }

    @Override
    public final int getNumOfContainingMovies() {
        return 1;
    }

    public abstract String getCanonicalDir();

    public abstract long getFileLength();

    public abstract String getCanonicalPath();

    public abstract String getComment();


    public static boolean any(FileTypeInterface ft, List<AbstractLeafNode> nodes) {
        for (AbstractLeafNode movieNode : nodes) {
            if (movieNode.is(ft)) {
                return true;
            }
        }
        return false;
    }

    public boolean is(FileTypeInterface ft) {
        return ft.is(getName());
    }

    @Override
    public TreeNode getChildAt(int childIndex) {
        return null;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public int getIndex(TreeNode node) {
        return 0;
    }

    @Override
    public boolean getAllowsChildren() {
        return false;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public Enumeration children() {
        return null;
    }

    @Override
    public List<AbstractMovieTreeNode> getChildren() {
        return Collections.emptyList();
    }

    public void sortList(Comparator comparator) {

    }
}
