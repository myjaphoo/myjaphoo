package org.myjaphoo.gui.movietree;

import org.mlsoft.structures.AbstractTreeNode;
import org.myjaphoo.model.grouping.GroupingDim;

import java.util.ArrayList;
import java.util.List;

/**
 * DefNode
 * @author mla
 * @version $Id$
 */
public class DefNode extends AbstractTreeNode {
    private GroupingDim dim;

    public DefNode(DefNode parent, GroupingDim dim) {
        super(parent);
        this.dim = dim;
    }

    public String getName() {
        return dim != null ? getDim().getGuiName() : ""; //NOI18N
    }

    @Override
    public String toString() {

        return getName();
    }

    /**
     * @return the dim
     */
    public GroupingDim getDim() {
        return dim;
    }

    /**
     * @param dim the dim to set
     */
    public void setDim(GroupingDim dim) {
        this.dim = dim;
    }

    public static void createDefNodes(DefNode root, int i) {
        if (i > 3) {
            return;
        }
        for (GroupingDim dim : GroupingDim.values()) {
            if (!isInPath(dim, root)) {
                DefNode child = new DefNode(root, dim);
                root.addChild(child);
                createDefNodes(child, i + 1);
            }
        }
    }

    private static boolean isInPath(GroupingDim dim, DefNode node) {
        if (node.getDim() == dim) {
            return true;
        }
        while (node.getParent() != null) {
            node = (DefNode) node.getParent();
            if (node.getDim() == dim) {
                return true;
            }
        }
        return false;
    }

    public static DefNode createStructureDefNodes() {

        DefNode root = new DefNode(null, null);

        createDefNodes(root, 0);

        return root;
    }

    public static List<GroupingDim> findSelectedHierarchy(DefNode lastPathComponent) {
        ArrayList<GroupingDim> structure = new ArrayList<GroupingDim>();
        structure.add(0, lastPathComponent.getDim());
        while (lastPathComponent.getParent() != null) {
            lastPathComponent = (DefNode) lastPathComponent.getParent();
            if (lastPathComponent.getDim() != null) {
                structure.add(0, lastPathComponent.getDim());
            }
        }
        return structure;
    }
}