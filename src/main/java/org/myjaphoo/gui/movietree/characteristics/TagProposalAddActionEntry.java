package org.myjaphoo.gui.movietree.characteristics;

import org.apache.commons.lang.StringUtils;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.action.ViewContext;
import org.myjaphoo.gui.action.scriptactions.EntryContextAction;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.gui.thumbtable.ThumbPopupMenus;
import org.myjaphoo.gui.util.TokenMenuCreation;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.TokenRef;
import org.myjaphoo.model.grouping.TokenAssignemntProposolerPartialGrouper;

import javax.swing.*;
import java.util.List;

/**
 * TagProposalAddActionEntry.
 *
 * @author mla
 * @version $Id$
 */
public class TagProposalAddActionEntry extends EntryContextAction {

    @Override
    public Action createAction(MyjaphooController controller, List<AbstractLeafNode> selNodes) {
        MovieStructureNode structureNode = ThumbPopupMenus.findAndCheckSameStructureNodeOfNodes(selNodes);
        TokenRef token = findToken(controller, structureNode.getName());
        if (token != null) {
            return TokenMenuCreation.createAddTokenAction(controller, token, new ViewContext(controller.getProject(), selNodes));
        }
        throw new RuntimeException("internal error!");
    }

    @Override
    public boolean isAvailable(MyjaphooController controller, List<AbstractLeafNode> selNodes) {
        MovieStructureNode structureNode = ThumbPopupMenus.findAndCheckSameStructureNodeOfNodes(selNodes);
        return structureNode != null
            && structureNode.getMarker() == TokenAssignemntProposolerPartialGrouper.PATHMARKER_TAGPROPOSALGROUP
            && findToken(controller, structureNode.getName()) != null;
    }

    private TokenRef findToken(MyjaphooController controller, String name) {
        ImmutableModel model = controller.getProject().cacheActor.getImmutableModel();

        for (ImmutableToken token : model.getTokenTree().getValues()) {
            if (StringUtils.equals(token.getName(), name)) {
                return new TokenRef(model, token);
            }
        }
        return null;
    }
}
