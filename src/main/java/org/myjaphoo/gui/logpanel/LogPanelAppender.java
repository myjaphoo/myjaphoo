/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.gui.logpanel;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * LogPanel Appender. Konfigurierbar über log4j.xml.
 * Intern delegiert dieser nur an LogPanel, welches die Ausgabe übernimmt.
 * @author mla
 */
public class LogPanelAppender extends AppenderBase<ILoggingEvent> {

    @Override
    protected void append(ILoggingEvent le) {
        LogPanel.appendLogEvent(le);
    }

}
