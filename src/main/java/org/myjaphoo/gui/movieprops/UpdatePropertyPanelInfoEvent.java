package org.myjaphoo.gui.movieprops;

import io.vavr.collection.HashMap;
import org.myjaphoo.MovieNode;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.zipper.Ref;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Event that the property panel should be updated with new information.
 */
public class UpdatePropertyPanelInfoEvent {

    public final Ref entryRef;

    public final MovieNode movieNode;

    public final SelectionInfo selectionInfo;

    public final ImmutableAttributedEntity optionalEntity;

    public static class SelectionInfo {
        public final long fileLengthOfAll;
        public final int numOfFiles;

        public SelectionInfo(long fileLengthOfAll, int numOfFiles) {
            this.fileLengthOfAll = fileLengthOfAll;
            this.numOfFiles = numOfFiles;
        }
    }


    public UpdatePropertyPanelInfoEvent(
        Ref entryRef,
        MovieNode movieNode,
        SelectionInfo selectionInfo,
        ImmutableAttributedEntity optionalEntity
    ) {
        this.entryRef = entryRef;
        this.movieNode = movieNode;
        this.selectionInfo = selectionInfo;
        this.optionalEntity = optionalEntity;
    }

    public UpdatePropertyPanelInfoEvent(Ref entryRef) {
        this(entryRef, null, null, null);
    }

    public UpdatePropertyPanelInfoEvent(Ref entryRef, MovieNode movieNode) {
        this(entryRef, movieNode, null, null);
    }

    public UpdatePropertyPanelInfoEvent(Ref entryRef, MovieNode movieNode, List<MovieNode> currSelectedMovies) {
        this(entryRef, movieNode, createSelectionInfo(currSelectedMovies), null);
    }

    private static SelectionInfo createSelectionInfo(List<MovieNode> currSelectedMovies) {
        if (currSelectedMovies != null) {
            long fileLengthOfAll = 0;
            for (MovieNode node : currSelectedMovies) {
                if (node != null && node.getMovieEntry() != null) {
                    fileLengthOfAll += node.getMovieEntry().getFileLength();
                }
            }
            int numOfFiles = currSelectedMovies.size();
            return new SelectionInfo(fileLengthOfAll, numOfFiles);
        } else {
            return null;
        }
    }

    public UpdatePropertyPanelInfoEvent(String nameOfArbitraryInfo, Map<String, String> arbitraryAttributes) {
        this(null, null, null, createOptionalEntry(nameOfArbitraryInfo, arbitraryAttributes));
    }

    private static ImmutableAttributedEntity createOptionalEntry(
        String nameOfArbitraryInfo, Map<String, String> attrs
    ) {
        HashMap<String, String> immAttrs = HashMap.ofAll(attrs);
        return new ImmutableAttributedEntity() {
            @Override
            public String getName() {
                return nameOfArbitraryInfo;
            }

            @Override
            public String getComment() {
                return null;
            }

            @Override
            public io.vavr.collection.Map<String, String> getAttributes() {
                return immAttrs;
            }

            @Override
            public Long getCreatedMs() {
                return null;
            }

            @Override
            public Date getCreated() {
                return null;
            }

            @Override
            public Date getEdited() {
                return null;
            }

            @Override
            public String getUuid() {
                return null;
            }
        };
    }
}
