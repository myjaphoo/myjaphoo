package org.myjaphoo.gui.movieprops

import org.myjaphoo.model.cache.ImmutableAttributedEntity

/**
 * CommentNode 
 * @author mla
 * @version $Id$
 *
 */
class CommentNode extends EditableNode {

    public CommentNode(PropertyNode parent, ImmutableAttributedEntity attributedEntity) {
        super(parent, "Comment", attributedEntity.comment);
    }
}
