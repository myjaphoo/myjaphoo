package org.myjaphoo.gui.movieprops

import groovy.transform.TypeChecked
import groovy.util.logging.Slf4j
import org.myjaphoo.gui.icons.Icons
import org.myjaphoo.gui.util.Helper
import org.myjaphoo.gui.util.Utils
import org.myjaphoo.model.FileSubstitution
import org.myjaphoo.model.cache.*
import org.myjaphoo.model.db.MetaToken
import org.myjaphoo.model.db.MovieEntry
import org.myjaphoo.model.db.Token
import org.myjaphoo.model.filterparser.functions.Functions
import org.myjaphoo.model.filterparser.idents.Identifiers
import org.myjaphoo.model.logic.FasterFileSubstitution
import org.myjaphoo.project.Project

/**
 * HeaderNode 
 * @author mla
 * @version $Id$
 *
 */
@Slf4j
@TypeChecked
public class AttributeHeaderNode extends HeaderNode {

    private static FileSubstitution substitution;

    def ImmutableAttributedEntity attributedEntity;

    def boolean attributesHaveBeenDeleted = false;

    private Project project;

    public AttributeHeaderNode(Project project, PropertyNode parent, ImmutableAttributedEntity attributedEntity) {
        super(parent, attributedEntity.getName());
        this.project = project;
        this.attributedEntity = attributedEntity;
        substitution = new FasterFileSubstitution(project);

        getChildren().add(new InfoNode(this, "UUID", (String) attributedEntity.getUuid()));
        getChildren().add(new InfoNode(this, "Created", (Date) attributedEntity.getCreated()));
        getChildren().add(new InfoNode(this, "Edited", (Date) attributedEntity.getEdited()));

        def String attrFilterFuncName = null;
        if (attributedEntity instanceof ImmutableMovieEntry) {
            buildMovieEntryInfos((ImmutableMovieEntry) attributedEntity);
            attrFilterFuncName = Functions.ENTRYATTR.name;
        } else if (attributedEntity instanceof ImmutableToken) {
            buildTokenInfos((ImmutableToken) attributedEntity);
            attrFilterFuncName = Functions.TAGATTR.name;
        } else if (attributedEntity instanceof ImmutableMetaToken) {
            buildMetaTokenInfos((ImmutableMetaToken) attributedEntity);
            attrFilterFuncName = Functions.METATAGATTR.name
        }

        children.add(new CommentNode(this, attributedEntity));
        // build up child nodes:
        def attrNodes = attributedEntity.attributes.toJavaMap().collect { String key, String value -> new AttributeNode(this, key, value) }
        attrNodes = attrNodes.sort { AttributeNode a1, AttributeNode a2 -> a1.name <=> a2.name }
        // set filter expressions for the attributes:
        attrNodes.each { AttributeNode a -> a.filterExpression = "${attrFilterFuncName}(\"${a.name}\") like \"${a.value}\"" }
        children.addAll(attrNodes);
    }

    def buildMetaTokenInfos(ImmutableMetaToken metaToken) {
        this.setIcon(Icons.IR_METATAG.icon);
        filterExpression = "${Identifiers.METATAG.getName()}  is '${metaToken.getName()}'";
    }

    def buildTokenInfos(ImmutableToken token) {
        this.setIcon(Icons.IR_TAG.icon);
        filterExpression = "${Identifiers.TAG.getName()}  is '${token.getName()}'";
    }

    def buildMovieEntryInfos(ImmutableMovieEntry entry) {
        this.setIcon(Icons.IR_MOVIE.icon);

        HeaderNode info = new HeaderNode(this, "Info");
        this.getChildren().add(info);
        info.getChildren().add(new InfoNode(info, "File", entry.name));
        info.getChildren().add(new InfoNode(info, "Directory", entry.canonicalDir));
        info.getChildren().add(new InfoNode(info, "File Length", Utils.humanReadableByteCount(entry.getFileLength())));
        if (entry.getChecksumCRC32() != null) {
            info.getChildren().add(new InfoNode(info, "CRC32", Long.toHexString(entry.getChecksumCRC32())));
        }
        if (entry.getSha1() != null) {
            info.getChildren().add(new InfoNode(info, "SHA1", entry.getSha1()));
        }
        if (entry.getRating() != null) {
            info.getChildren().add(new InfoNode(info, "Rating", entry.rating.name));
        }
        if (project.prefs.PRF_SHOW_FILLOCALISATION_HINTS.getVal()) {
            String located = substitution.locateFileOnDrive(entry.getCanonicalPath());
            if (located != null) {
                info.getChildren().add(new InfoNode(info, "Located", substitution.substitude(located)));
            } else {
                info.getChildren().add(new InfoNode(info, "Located", "---"));
            }
        }

        String fmt = ""; //NOI18N
        if (entry.getMovieAttrs().getFormat() != null) {
            fmt = Helper.prepareFormat(entry.getMovieAttrs().getFormat());
        }
        info.getChildren().add(new InfoNode(info, "Format", "$fmt ${entry.movieAttrs.width}x${entry.movieAttrs.height}"));

        // add only movieattributes for movies, not for pictures
        if (project.Movies.is(entry)) {
            String fmtBitRate = Utils.humanReadableByteCount(entry.getMovieAttrs().getBitrate());
            info.getChildren().add(new InfoNode(info, "Bitrate", fmtBitRate));
            info.getChildren().add(new InfoNode(info, "FPS", Integer.toString(entry.getMovieAttrs().getFps())));
            String len = org.apache.commons.lang.time.DurationFormatUtils.formatDurationHMS(entry.getMovieAttrs().getLength() * 1000);
            info.getChildren().add(new InfoNode(info, "Length", len));
        }
    }

    void save() {
        def attributedMutable = project.loadEntity(attributedEntity);
        attributedMutable.comment = children.find { it instanceof CommentNode }.value;
        attributedMutable.attributes = children.findAll {
            it instanceof AttributeNode
        }.collect { PropertyNode a -> [a.name, a.value] }.collectEntries()

        try {
            if (attributedMutable instanceof MovieEntry) {
                project.cacheActor.editMovie((MovieEntry) attributedMutable);
            } else if (attributedMutable instanceof Token) {
                project.cacheActor.editToken((Token) attributedMutable);
            } else if (attributedMutable instanceof MetaToken) {
                project.cacheActor.editMetaToken((MetaToken) attributedMutable);
            }
            // reset edited flag afterwards:
            children.each {
                if (it instanceof EditableNode) {
                    it.edited = false
                }
            }
        } catch (Exception ex) {
            log.error("failed to save entry!", ex); //NOI18N
        }
    }

    public def boolean needsSaving() {
        return attributesHaveBeenDeleted || children.find { it.hasBeenChanged() }
    }
}
