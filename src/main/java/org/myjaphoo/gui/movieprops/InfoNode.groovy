package org.myjaphoo.gui.movieprops

import java.text.DateFormat

/**
 * InfoNode 
 * @author mla
 * @version $Id$
 *
 */
class InfoNode extends PropertyNode {

    InfoNode(PropertyNode parent, String name, Date value) {
        super(parent)
        this.name = name;
        if (value != null) {
            this.value = DateFormat.getDateTimeInstance().format(value);
        }
        this.editable = false;
    }

    InfoNode(PropertyNode parent, String name, String value) {
        super(parent)
        this.name = name;
        this.value = value;
        this.editable = false;
    }
}
