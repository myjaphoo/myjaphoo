package org.myjaphoo.gui.movieprops

import groovy.transform.TypeChecked
import org.myjaphoo.MovieNode
import org.myjaphoo.MyjaphooCorePrefs
import org.myjaphoo.gui.icons.Icons
import org.myjaphoo.gui.movietree.DiffNode
import org.myjaphoo.model.cache.ImmutableMovieEntry
import org.myjaphoo.model.cache.zipper.EntryRef
import org.myjaphoo.util.ComparatorByProperties

/**
 * MovieNodeInfoNode 
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class MovieNodeInfoNode extends HeaderNode {

    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/util/resources/Helper");

    MovieNodeInfoNode(MyjaphooCorePrefs prefs, PropertyNode parent, MovieNode movieNode) {
        super(parent, movieNode.getName())
        this.setIcon(Icons.IR_MOVIE.icon);

        if (!movieNode.isUnique()) {
            def uniqueNode = new InfoNode(this, "Uniqueness", localeBundle.getString("(NOT UNIQUE GROUPED!)"))
            uniqueNode.icon = Icons.IR_NOTUNIQUE.icon;
            children.add(uniqueNode);
        }
        if (movieNode.getCondensedDuplicatesSize() > 0) {
            HeaderNode condensed = new HeaderNode(this, "Condensed");
            condensed.icon = Icons.IR_CONDENSED.icon;
            this.children.add(condensed);
            for (EntryRef dupEntry : movieNode.getCondensedDuplicates()) {
                HeaderNode cEntry = new HeaderNode(condensed, dupEntry.name);
                condensed.children.add(cEntry);
                cEntry.getChildren().add(new InfoNode(cEntry, "Directory", dupEntry.canonicalDir));
            }

        }
        if (movieNode.hasDups) {
            HeaderNode duplicates = new HeaderNode(this, "Duplicates");
            duplicates.icon = Icons.IR_DUPLICATES.icon;
            this.children.add(duplicates);
            for (ImmutableMovieEntry dupEntry : movieNode.getDupsInDatabase()) {
                HeaderNode cEntry = new HeaderNode(duplicates, dupEntry.name);
                duplicates.children.add(cEntry);
                cEntry.getChildren().add(new InfoNode(cEntry, "Directory", dupEntry.canonicalDir));
            }
        }
        ExifExtraction.createExifNodes(prefs, this, movieNode);

        if (movieNode instanceof DiffNode) {
            HeaderNode duplicates = new HeaderNode(this, "Diff");
            DiffNode diffNode = (DiffNode) movieNode;
            def dbdiff = diffNode.getDbdiff()
            def ctx = dbdiff.getContext()
            appendDiffNOdes(duplicates, ctx.determineCompareDiffs(dbdiff.getEntry(), dbdiff.getCDBEntry()));
            appendDiffNOdes(duplicates, ctx.determineCompareDiffs(dbdiff.getToken(), dbdiff.getCDBToken()));
            appendDiffNOdes(duplicates, ctx.determineCompareDiffs(dbdiff.getMetaToken(), dbdiff.getCDBMetaToken()));
        }
    }

    def appendDiffNOdes(HeaderNode headerNode, Map<String, ComparatorByProperties.Diff> diffs) {
        diffs.each { k, v ->
            headerNode.getChildren().add(new InfoNode(headerNode, k, "$v.o1 <> $v.o2"))
        }
    }
}
