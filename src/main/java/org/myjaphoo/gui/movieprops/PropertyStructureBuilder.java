package org.myjaphoo.gui.movieprops;

import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.util.Utils;
import org.myjaphoo.model.cache.ImmutableAttributedEntity;
import org.myjaphoo.model.cache.zipper.ImmutableAttributedEntityZipper;
import org.myjaphoo.model.cache.zipper.Ref;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * PropertyStructureBuilder
 */
public class PropertyStructureBuilder {

    private MyjaphooController controller;

    public PropertyStructureBuilder(MyjaphooController controller) {
        this.controller = controller;
    }

    public PropertyNode buildNodes(UpdatePropertyPanelInfoEvent event) {
        PropertyNode newRoot = new PropertyNode(null);
        addSelectionInfo(newRoot, event.selectionInfo);
        if (event.movieNode != null) {
            newRoot.getChildren().add(new MovieNodeInfoNode(
                controller.getProject().prefs,
                newRoot,
                event.movieNode
            ));

        }

        if (event.entryRef != null) {
            List<ImmutableAttributedEntity> entityList = findAllConnectedAttributes(event.entryRef);
            for (ImmutableAttributedEntity entity : entityList) {
                buildAttributes(newRoot, entity);
            }
        }
        if (event.optionalEntity != null) {
            buildAttributes(newRoot, event.optionalEntity);
        }

        return newRoot;
    }

    private void addSelectionInfo(PropertyNode newRoot, UpdatePropertyPanelInfoEvent.SelectionInfo selectionInfo) {
        if (selectionInfo != null) {
            InfoNode selNr = new InfoNode(newRoot, "Files", Integer.toString(selectionInfo.numOfFiles));
            newRoot.getChildren().add(selNr);
            InfoNode selSize = new InfoNode(
                newRoot,
                "Size",
                Utils.humanReadableByteCount(selectionInfo.fileLengthOfAll)
            );
            newRoot.getChildren().add(selSize);
        }
    }


    private void buildAttributes(PropertyNode root, ImmutableAttributedEntity attributedEntity) {
        PropertyNode entityRoot = new AttributeHeaderNode(controller.getProject(), root, attributedEntity);
        root.getChildren().add(entityRoot);
    }

    private List<ImmutableAttributedEntity> findAllConnectedAttributes(Ref entryRef) {
        LinkedHashSet<ImmutableAttributedEntity> set = new LinkedHashSet<>();
        set.add((ImmutableAttributedEntity) entryRef.getRef());
        addReferencedAttributes(set, (ImmutableAttributedEntityZipper) entryRef.toZipper());
        return new ArrayList<>(set);
    }

    private void addReferencedAttributes(
        Collection<ImmutableAttributedEntity> list, ImmutableAttributedEntityZipper zipper
    ) {
        for (ImmutableAttributedEntityZipper nextZipper : zipper.getReferences()) {
            Ref<? extends ImmutableAttributedEntity> ref = nextZipper.toRef();
            list.add(ref.getRef());
            addReferencedAttributes(list, nextZipper);
        }
    }
}
