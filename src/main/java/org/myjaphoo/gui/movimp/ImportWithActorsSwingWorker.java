/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.movimp;

import org.apache.commons.lang.time.StopWatch;
import org.myjaphoo.model.logic.MovieImport;
import org.myjaphoo.model.logic.imp.ImportDelegator;
import org.myjaphoo.model.logic.imp.MovieDelegator;
import org.myjaphoo.model.logic.imp.ProviderFactory;
import org.myjaphoo.model.logic.imp.movInfoProvider.MovieAttributeProvider;
import org.myjaphoo.model.logic.imp.thumbprovider.ThumbnailProvider;
import org.myjaphoo.model.logic.impactors.ImportMsg;
import org.myjaphoo.model.logic.impactors.ImportQueue;
import org.myjaphoo.model.logic.impactors.ImportWorkerActor;
import org.myjaphoo.model.logic.impactors.ImportWorkerActorFactory;
import org.myjaphoo.model.logic.impbkrnd.FilterOutExistingEntries;
import org.myjaphoo.model.logic.impbkrnd.ImportPreFilter;
import org.myjaphoo.model.util.FRessourceBundle;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.util.List;

/**
 * @author mla
 */
public class ImportWithActorsSwingWorker extends SwingWorker<Object, String> {

    private static Logger logger = LoggerFactory.getLogger(ImportWithActorsSwingWorker.class);
    public static Logger IMPLOGGER = LoggerFactory.getLogger(ImportDialog.WMIMPORT);
    private FRessourceBundle bundle = new FRessourceBundle(
        "org/myjaphoo/gui/movimp/resources/ImportWithActorsSwingWorker");
    private ImportQueue queue;

    void abortImport() {

        dialog.setStatusBarMessage(bundle.getString("CANCELLING IMPORT..."));
        IMPLOGGER.info(bundle.getString("CANCELLING IMPORT...")); //NOI18N
        if (queue != null) {
            queue.send(new ImportMsg.AbortImport());
        }
    }


    private ImportDelegator delegator;
    private File dir2Scan;
    private ImportDialogFeedback dialog;

    private Project project;
    private MovieImport importer;
    private ImportPreFilter importPreFilter;

    private ImportWorkerActorFactory importWorkerActorFactory = (p, importTokenActor, id) -> new ImportWorkerActor(p, importTokenActor, id);

    public ImportWithActorsSwingWorker(
        Project project, ImportDialogFeedback dialog, ImportDelegator delegator, File dir2Scan
    ) {
        this.delegator = delegator;
        this.dir2Scan = dir2Scan;
        this.dialog = dialog;
        this.project = project;
        this.importer = new MovieImport(project);
        this.importPreFilter = new FilterOutExistingEntries(importer);
    }

    int allFiles;
    int currFile;
    long start;

    @Override
    protected Object doInBackground() throws Exception {

        StopWatch watch = new StopWatch();
        watch.start();
        IMPLOGGER.info(bundle.getString("SCANNING_FILES"));
        start = System.currentTimeMillis();
        List<File> movieFiles = importer.scanFiles(dir2Scan.getAbsolutePath(), delegator);
        IMPLOGGER.info(bundle.getString("FOUND_N_FILES", movieFiles.size()));
        IMPLOGGER.info(bundle.getString("NOW_IMPORTING_FILES"));

        // prefilter files:
        List<File> filteredFiles = importPreFilter.filterScannedFiles(movieFiles);
        allFiles = filteredFiles.size();

        int diff = movieFiles.size() - filteredFiles.size();
        if (diff > 0) {
            IMPLOGGER.info(bundle.getString("SKIPPED_N_FILES_WHICHALREADY_EXIST", diff));
        }
        IMPLOGGER.info(bundle.getString("FOUND_N_NEWFILESTOIMPORT", filteredFiles.size()));

        if (allFiles > 0) {
            if (delegator instanceof MovieDelegator) {
                try {
                    // check for a properly configured thumb nail provider
                    ThumbnailProvider tn = ProviderFactory.getBestThumbnailProvider(project.prefs);
                    IMPLOGGER.info("using thumb nail provider " + tn.getDescr());

                } catch (Throwable rte) {
                    // no provider is configured:
                    IMPLOGGER.error(rte.getLocalizedMessage());
                    dialog.setStatusBarActionStopped();
                    return null;
                }
                // check for movie attribute extraction. but this is not mandatory; we do anyway start the import now:
                try {
                    MovieAttributeProvider ma = ProviderFactory.getBestMovieAttributeProvider(project.prefs);
                    IMPLOGGER.info("using movie attribute provider " + ma.getDescr());
                } catch (RuntimeException rte) {
                    IMPLOGGER.error(rte.getLocalizedMessage());
                }

            }

            SwingCallBackActor callBackActor = new SwingCallBackActor(dialog, this, allFiles);

            queue = ImportQueue.createImportQueue(project, callBackActor, delegator, importWorkerActorFactory);
            queue.send(new ImportMsg.ImportFilesMsg(filteredFiles));
            queue.waitTillAllFinished();
        }
        watch.stop();
        IMPLOGGER.info(bundle.getString("FINISHED_WITH_DURATION", watch.toString()));

        project.cacheActor.resetInternalCache();
        project.cacheActor.resetImmutableCopy();
        dialog.setStatusBarActionStopped();
        return null;
    }

    @Override
    protected void process(List<String> chunks) {
        currFile += chunks.size();
        int perc = currFile * 100 / allFiles;
        if (perc > 100) {
            perc = 100;
        }
        dialog.setStatusBarProgress(perc);
    }

    public void publishIt(String filePath) {
        this.publish(filePath);
    }

    public void setImportPreFilter(ImportPreFilter importPreFilter) {
        this.importPreFilter = importPreFilter;
    }

    public void setImportWorkerActorFactory(ImportWorkerActorFactory importWorkerActorFactory) {
        this.importWorkerActorFactory = importWorkerActorFactory;
    }
}
