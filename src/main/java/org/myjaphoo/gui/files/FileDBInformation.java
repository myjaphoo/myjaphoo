package org.myjaphoo.gui.files;

import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;
import org.mlsoft.structures.Trees;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.FileSubstitution;
import org.myjaphoo.model.cache.events.ModelReloadEvent;

import org.myjaphoo.model.logic.FileSubstitutionImpl;
import org.myjaphoo.model.util.Helper;


/**
 * delivers information about db entries from th file view.
 */
public class FileDBInformation {

    FileSubstitution fileSubstitution;

    private MyjaphooController controller;
    private MovieStructureNode dbFilesRoot;
    private static final Trees.EqualPathComponent<AbstractMovieTreeNode> EQUAL_FUNCTION = new Trees.EqualPathComponent<AbstractMovieTreeNode>() {
        @Override
        public boolean isEqual(AbstractMovieTreeNode node, Object pathComponent) {
            return node.getName().equals(pathComponent);
        }
    };


    public FileDBInformation(MyjaphooController controller) {
        this.controller = controller;
        fileSubstitution = new FileSubstitutionImpl(controller.getProject());
        GlobalBus.bus.register(this);

    }

    @Subscribe(onETD = true)
    public void modelReloaded(ModelReloadEvent modelReloadEvent) {
        // when model is loaded, init the dir structure.
        // this is not perfectly done, as we would in a perfect world use the immutable model of
        // the event instead of again calling the actor inside the creation of the dir model.
        // should be rewritten to take that model
        dbFilesRoot = controller.getFilter().createDirectoryStructuredTreeModel();
    }

    public AbstractMovieTreeNode findNodeOfDB(String absPath) {
        AbstractMovieTreeNode foundNode = findPathInDirectoryStructure(absPath);
        return foundNode;
    }

    private AbstractMovieTreeNode findPathInDirectoryStructure(String absPath) {
        if (dbFilesRoot == null) {
            return null;
        }
        String[] path = Helper.splitPathName(absPath);
        String[] pathWithRoot = new String[path.length + 1];
        pathWithRoot[0] = dbFilesRoot.getName();
        for (int i = 0; i < path.length; i++) {
            pathWithRoot[i + 1] = path[i];
        }
        AbstractMovieTreeNode foundNode = Trees.pathSearchByObjectPath(
            (AbstractMovieTreeNode) dbFilesRoot,
            pathWithRoot,
            EQUAL_FUNCTION,
            0
        );
        return foundNode;
    }

}
