/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.metaToken;

import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.apache.commons.lang.StringUtils;
import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;
import org.myjaphoo.gui.editor.rsta.CachedHints;
import org.myjaphoo.gui.util.TreeRelModel;
import org.myjaphoo.gui.util.WrappedNode;
import org.myjaphoo.model.cache.EntityCacheActor;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.TreeRel2;
import org.myjaphoo.model.cache.events.AbstractMetaTagsChangedEvent;
import org.myjaphoo.model.cache.events.MetaTagsAddedEvent;
import org.myjaphoo.model.cache.events.MetaTagsAssignedEvent;
import org.myjaphoo.model.cache.events.MetaTagsDeletedEvent;
import org.myjaphoo.model.cache.events.MetaTagsUnassignedEvent;
import org.myjaphoo.model.db.MetaToken;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * @author lang
 */
public class MetaTokenTreeModel extends TreeRelModel<ImmutableMetaToken> {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/gui/metaToken/resources/MetaTokenTreeModel");
    public static final String[] COLUMNS = new String[]{
        localeBundle.getString("META TAG"),
        localeBundle.getString("DESCRIPTION"),
        localeBundle.getString("ASSIGNED TAGS")};
    public static final Logger LOGGER = LoggerFactory.getLogger(MetaTokenTreeModel.class.getName());

    private ImmutableModel immutableModel;

    private EntityCacheActor cacheActor;

    private Project project;

    public MetaTokenTreeModel(
        Project project, EntityCacheActor cacheActor, ImmutableModel immutableModel,
        boolean isFlat
    ) {
        super(
            immutableModel,
            immutableModel.getMetaTokenTree(),
            false,
            null,
            isFlat,
            prepareColumnList(immutableModel)
        );
        this.project = project;
        this.immutableModel = immutableModel;
        this.cacheActor = cacheActor;
        isEditable = true;
        GlobalBus.bus.register(this);
    }

    public static MetaTokenTreeModel createEmptyModel() {
        return new MetaTokenTreeModel(null, null, ImmutableModel.EMPTY_MODEL, true);
    }

    private static String[] prepareColumnList(ImmutableModel model) {
        ArrayList<String> colList = new ArrayList<>();
        colList.addAll(Arrays.asList(COLUMNS));

        colList.addAll(CachedHints.calcMetaTokenAttrHints(model).keys);
        return colList.toArray(new String[colList.size()]);
    }

    public void dispose() {
        GlobalBus.bus.unregister(this);
    }

    @Override
    protected boolean match(ImmutableMetaToken tok, String typedText) {
        if (typedText == null) {
            return true;
        }
        return StringUtils.containsIgnoreCase(tok.getName(), typedText);
    }

    @Override
    public Object getValueAt(Object token, int column) {
        switch (column) {
            case 0:
                return ((WrappedNode<ImmutableMetaToken>) token).getRef().getName();
            case 1:
                return ((WrappedNode<ImmutableMetaToken>) token).getRef().getDescription();
            case 2:
                return createTokenDescr(((WrappedNode<ImmutableMetaToken>) token).getRef());
            default:
                String attrName = getColumnName(column);
                return ((WrappedNode<ImmutableMetaToken>) token).getRef().getAttributes().get(attrName).getOrNull();
        }
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        if (!isEditable) {
            return false;
        } else {
            return column == 0 || column == 1;
        }
    }

    @Override
    public Class<?> getColumnClass(int column) {
        if (column == 0 || column == 1) {
            return String.class;
        }
        return Object.class;
    }

    @Override
    public void setValueAt(Object value, Object node, int column) {
        if (!(column == 0 || column == 1 || column == 5)) {
            return;
        }
        WrappedNode<ImmutableMetaToken> mnode = (WrappedNode<ImmutableMetaToken>) node;
        ImmutableMetaToken imt = mnode.getRef();
        MetaToken mt = project.loadMetaToken(imt.getId());
        try {
            if (column == 0) {
                mt.setName((String) value);
            }
            if (column == 1) {
                mt.setDescription((String) value);
            }
            cacheActor.editMetaToken(mt);
        } catch (Exception ex) {
            LOGGER.error("error", ex); //NOI18N
            throw new RuntimeException(ex);
        }
    }

    /**
     * {@inheritDoc}
     * editierung eines tokennames
     *
     * @param path     {@inheritDoc}
     * @param newValue {@inheritDoc}
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        try {
            WrappedNode<ImmutableMetaToken> token = (WrappedNode<ImmutableMetaToken>) path.getLastPathComponent();
            MetaToken mt = project.loadMetaToken(token.getRef().getId());
            mt.setName((String) newValue);
            cacheActor.editMetaToken(mt);
        } catch (Exception ex) {
            LOGGER.error("error", ex); //NOI18N
        }

    }

    private Object createTokenDescr(ImmutableMetaToken metaToken) {
        StringBuilder b = new StringBuilder();
        Option<Set<ImmutableToken>> assignedTokens = immutableModel.getTokenMetaToken().getAssignedX(
            metaToken);
        if (assignedTokens.isDefined()) {
            for (ImmutableToken t : assignedTokens.get()) {
                if (b.length() > 0) {
                    b.append(";"); //NOI18N
                }
                b.append(t.getName());
            }
        }
        return b.toString();
    }

    @Subscribe(onETD = true)
    public void tagassigned(MetaTagsAssignedEvent tae) {
        updateTagNodes(tae);
    }

    @Subscribe(onETD = true)
    public void tagunAssigned(MetaTagsUnassignedEvent tae) {
        updateTagNodes(tae);
    }

    @Subscribe(onETD = true)
    public void tagsAdded(MetaTagsAddedEvent tae) {
        // tags where added:
        immutableModel = tae.getModel();
        TreeRel2<ImmutableMetaToken, MetaToken> tree = tae.getModel().getMetaTokenTree();
        for (ImmutableMetaToken newTag : tae.getMetaTokenSet()) {
            addedNewNodeObject(tae.getModel(), tree.getParent(newTag), newTag);
        }
    }

    @Subscribe(onETD = true)
    public void tagsDeleted(MetaTagsDeletedEvent tae) {
        // tags where deleted:
        immutableModel = tae.getModel();
        TreeRel2<ImmutableMetaToken, MetaToken> tree = tae.getModel().getMetaTokenTree();
        for (ImmutableMetaToken newTag : tae.getMetaTokenSet()) {
            removeNodeObject(tree.getParent(newTag), newTag);
        }
    }

    private void updateTagNodes(AbstractMetaTagsChangedEvent tae) {
        immutableModel = tae.getModel();
        updateNodes(tae.getMetaTokenSet());
    }
}
