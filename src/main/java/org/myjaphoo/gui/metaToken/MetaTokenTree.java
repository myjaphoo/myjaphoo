/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.metaToken;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableModel;
import org.myjaphoo.gui.Commons;
import org.myjaphoo.gui.util.Helper;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;
import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

/**
 *
 * @author mla
 */
public class MetaTokenTree extends JXTreeTable {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/metaToken/resources/MetaTokenTree");
    private Icon movieIcon = new ImageIcon("org/myjaphoo/gui/icons/Movie16.gif"); //NOI18N
    private MetaTokenPanelController controller;
    private boolean showThumbs = false;

    public MetaTokenTree(final MetaTokenPanelController controller) {
        this.controller = controller;

        setDropMode(DropMode.ON);
        setDragEnabled(true);
        setEditable(true);
        MetaTokenTreeCellRenderer renderer = new MetaTokenTreeCellRenderer(this);
        setTreeCellRenderer(renderer);
        setColumnControlVisible(true);
        setHorizontalScrollEnabled(true);
        getActionMap().put("column.toggleOnlyCategory", //NOI18N
                new AbstractActionExt(localeBundle.getString("ONLY CATEGORY ON/OFF")) {

            boolean active = true;

            @Override
            public void actionPerformed(ActionEvent e) {

                active = !active;
                getColumnExt(MetaTokenTreeModel.COLUMNS[0]).setVisible(true);
                getColumnExt(MetaTokenTreeModel.COLUMNS[1]).setVisible(active);
            }
        });

        getActionMap().put("column.toggleThumbPreview", //NOI18N
                new AbstractActionExt(localeBundle.getString("SHOW THUMBS ON/OFF")) {

            @Override
            public void actionPerformed(ActionEvent e) {
                MetaTokenTree.this.showThumbs = !MetaTokenTree.this.isShowThumbs();
            }
        });
        Helper.initHeightMenusForJXTreeTable(controller.getMainController().project.prefs, this);

        TransferHandler transferHandler = controller.createTransferHandler(this);

        if (transferHandler != null) {
            setTransferHandler(transferHandler);
        }

        setRolloverEnabled(true);
        addHighlighter(Commons.ROLLOVER_ROW_HIGHLIGHTER);
        setOverwriteRendererIcons(true);
        setLeafIcon(movieIcon);

        addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                Object value = e.getPath().getLastPathComponent();
                MetaTokenRef token= MetaTokenTreeCellRenderer.getToken(value);
                controller.onMetaTokenSelected(token);
            }
        });
    }

    /**
     * @return the showThumbs
     */
    public boolean isShowThumbs() {
        return showThumbs;
    }

    public void setShowPreviewBigMode() {
        getColumnExt(MetaTokenTreeModel.COLUMNS[0]).setVisible(true);
        getColumnExt(MetaTokenTreeModel.COLUMNS[1]).setVisible(false);
        setRowHeight( controller.getMainController().project.prefs.PRF_THUMBSIZE.getVal());
        showThumbs = true;
    }

    public void refreshTree() {
        final AbstractTreeTableModel tokenTreeModel = controller.createMetaTokenTreeModel();
        TreeTableModel oldModel = getTreeTableModel();
        if (oldModel != null && oldModel instanceof MetaTokenTreeModel) {
            ((MetaTokenTreeModel) oldModel).dispose();
        }
        setTreeTableModel(tokenTreeModel);
        initColumnWidth();
    }

    private void initColumnWidth() {
        // set prototype widths for the columns:
        for (int i = 0; i < MetaTokenTreeModel.COLUMNS.length; i++) {
            int width = i == 0 ? 200 : 40;
            getColumnExt(MetaTokenTreeModel.COLUMNS[i]).setPreferredWidth(width);
        }

    }

    protected void selectToken(ImmutableMetaToken tok) {
        final TreePath treePath = ((MetaTokenTreeModel) getTreeTableModel()).findTreePathForWrappedObject(tok);
        if (treePath != null) {
            getTreeSelectionModel().setSelectionPath(treePath);

            scrollPathToVisible(treePath);
            setExpandsSelectedPaths(true);
            setScrollsOnExpand(true);
        }

    }

    public MetaTokenPanelController getController() {
        return controller;
    }
}
