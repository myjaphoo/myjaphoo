/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.token;

import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.util.WrappedNode;
import org.myjaphoo.model.cache.ChangeSet;
import org.myjaphoo.model.cache.ImmutableModel;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.events.MetaTagsAssignedEvent;
import org.myjaphoo.model.cache.events.TagsAddedEvent;
import org.myjaphoo.model.cache.events.TagsAssignedEvent;
import org.myjaphoo.model.cache.events.TagsDeletedEvent;
import org.myjaphoo.model.cache.events.TagsUnassigendEvent;
import org.myjaphoo.model.db.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreePath;

/**
 * @author lang
 */
public class TokenTreeModel extends AbstractTokenTreeModel {

    public static final Logger logger = LoggerFactory.getLogger(TokenTreeModel.class);

    /**
     * Erzeugt eine Instanz von {@link TokenTreeModel}.
     *
     * @param model
     */
    public TokenTreeModel(MainApplicationController mainController, ImmutableModel model, boolean isFlat) {
        super(mainController, model, false, null, isFlat, true);
        GlobalBus.bus.register(this);
    }

    public void dispose() {
        GlobalBus.bus.unregister(this);
    }

    /**
     * {@inheritDoc}
     * editierung eines tokennames
     *
     * @param path     {@inheritDoc}
     * @param newValue {@inheritDoc}
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        try {
            WrappedNode<Token> tokennode = (WrappedNode<Token>) path.getLastPathComponent();
            Token token = tokennode.getRef();
            token.setName((String) newValue);
            getCacheActor().editToken(token);
        } catch (Exception ex) {
            logger.error("error", ex); //NOI18N
        }

    }

    @Subscribe(onETD = true)
    public void tagassigned(TagsAssignedEvent tae) {
        updateTagNodes(tae);
    }

    @Subscribe(onETD = true)
    public void metatagassigned(MetaTagsAssignedEvent tae) {
        updateTagNodes(tae);
    }

    @Subscribe(onETD = true)
    public void tagunAssigned(TagsUnassigendEvent tae) {
        updateTagNodes(tae);
    }

    @Subscribe(onETD = true)
    public void tagsAdded(TagsAddedEvent tae) {
        // tags where added:
        ImmutableModel model = tae.getModel();
        setImmutableModel(model);
        for (ImmutableToken newTag : tae.getTokenSet()) {
            addedNewNodeObject(model, model.getParentTag(newTag), newTag);
        }
    }


    @Subscribe(onETD = true)
    public void tagsDeleted(TagsDeletedEvent tae) {
        // tags where deleted:
        ImmutableModel model = tae.getModel();
        setImmutableModel(model);
        for (ImmutableToken newTag : tae.getTokenSet()) {
            removeNodeObject(model.getParentTag(newTag), newTag);
        }
    }

    private void updateTagNodes(ChangeSet tae) {
        ImmutableModel model = tae.getModel();
        setImmutableModel(model);
        updateNodes(tae.getTokenSet());
    }
}
