/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.token;

import org.jdesktop.swingx.renderer.DefaultTreeRenderer;
import org.jdesktop.swingx.renderer.IconValue;
import org.jdesktop.swingx.renderer.StringValue;
import org.myjaphoo.gui.icons.Icons;
import org.myjaphoo.gui.thumbtable.thumbcache.TokenThumbCache;
import org.myjaphoo.gui.util.Helper;
import org.myjaphoo.gui.util.TreeRelModel;
import org.myjaphoo.gui.util.WrappedNode;
import org.myjaphoo.model.cache.ImmutableToken;
import org.myjaphoo.model.cache.zipper.TokenRef;

import javax.swing.*;
import java.awt.*;

/**
 * @author mla
 */
public class TokenTreeCellRenderer extends DefaultTreeRenderer {

    private TokenTree tokenTree;

    public TokenTreeCellRenderer(TokenTree tokenTree) {
        super(createIconValue(tokenTree), createStringValue(tokenTree));
        this.tokenTree = tokenTree;

    }

    @Override
    public Component getTreeCellRendererComponent(
        JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus
    ) {
        Component comp = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        TokenRef token = getToken(value);
        Color color = Helper.getColorForTokenType(token.getTokentype());
        comp.setForeground(color);

        return comp;
    }

    public static StringValue createStringValue(final TokenTree tokenTree) {
        StringValue sv = new StringValue() {

            @Override
            public String getString(Object value) {
                return getToken(value).toString();
            }
        };
        return sv;
    }

    public static TokenRef getToken(Object value) {
        if (value instanceof TreeRelModel.WrappedNodeWithModel) {
            TreeRelModel.WrappedNodeWithModel<ImmutableToken> node = (TreeRelModel.WrappedNodeWithModel<ImmutableToken>) value;
            // todo save ref object in node to not create it all the time...
            return new TokenRef(node.model, node.getRef());
        }
        throw new RuntimeException("interner fehler"); //NOI18N
    }

    public static IconValue createIconValue(final TokenTree tokenTree) {
        IconValue iv = new IconValue() {

            @Override
            public Icon getIcon(Object value) {

                if (tokenTree.isShowThumbs()) {
                    if (value instanceof WrappedNode) {
                        TokenRef token = getToken(value);
                        return tokenTree.getController().getTokenThumbCache().loadImageForToken(
                            token.getRef(),
                            tokenTree.getRowHeight(),
                            null
                        );
                    }
                }
                return Icons.IR_TAG.icon;
            }
        };
        return iv;

    }
}
