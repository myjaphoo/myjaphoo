/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.thumbtable;

import org.myjaphoo.MovieNode;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.model.cache.ChangeSet;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.events.MoviesRemovedEvent;

import javax.swing.*;
import java.util.List;
import java.util.Map;

/**
 * @author mla
 */
class ThumbListModel extends AbstractListModel {

    private List<AbstractLeafNode> nodes;

    public ThumbListModel(List<AbstractLeafNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public int getSize() {
        return nodes.size();
    }

    @Override
    public Object getElementAt(int index) {
        return nodes.get(index);
    }

    void fireCellUpdated(int index) {
        fireContentsChanged(this, index, index);
    }

    void updateNodes(ChangeSet e) {
        Map<Long, ImmutableMovieEntry> idMap = e.createEntryIdMap();
        for (AbstractLeafNode node1 : nodes) {
            MovieNode node = ((MovieNode) node1);
            ImmutableMovieEntry entry = node.getMovieEntry();

            ImmutableMovieEntry changedEntry = idMap.get(entry.getId());
            if (changedEntry != null) {
                node.updateNode(e.getModel().entryRef(changedEntry));
            }
        }
    }

    void updateRemovedNodes(MoviesRemovedEvent mre) {
        Map<Long, ImmutableMovieEntry> idMap = mre.createEntryIdMap();
        for (int i = nodes.size() - 1; i >= 0; i--) {
            MovieNode node = ((MovieNode) nodes.get(i));
            ImmutableMovieEntry entry = node.getMovieEntry();
            ImmutableMovieEntry changedEntry = idMap.get(entry.getId());
            if (changedEntry != null) {
                // remove from model:
                nodes.remove(i);
                fireIntervalRemoved(this, i, i);
            }
        }
    }
}
