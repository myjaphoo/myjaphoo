package org.myjaphoo.gui.thumbtable.thumbcache;

import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.myjaphoo.gui.ThumbTypeDisplayMode;
import org.myjaphoo.model.cache.EntityCacheActor;
import org.myjaphoo.model.cache.ImmutableMetaToken;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;

import javax.swing.*;
import java.util.HashMap;

import static io.vavr.collection.HashSet.empty;

/**
 * Cache to get thumbs for meta tags.
 * Delegates to the regular thumb cache, but ensures, that always the same thumb is returned for a meta tag.
 */
public class MetaTokenThumbCache {
    /**
     * mapped ein token immer auf den gleichen movie...
     */
    private HashMap<Long, Long> tokMap = new HashMap<>();
    private ThreadedThumbCache thumbCache;

    private EntityCacheActor cacheActor;

    public MetaTokenThumbCache(EntityCacheActor cacheActor, ThreadedThumbCache thumbCache) {
        this.cacheActor = cacheActor;
        this.thumbCache = thumbCache;
    }

    public Icon loadImageForToken(ImmutableMetaToken token, int actualRowHeight, ThumbIsLoadedCallback loadedCallBack) {
        Long entryId = mapEntry(token);
        if (entryId == null) {
            return null;
        }
        return thumbCache.getThumb(
            entryId,
            0,
            true,
            actualRowHeight,
            ThumbTypeDisplayMode.NORMAL,
            loadedCallBack
        );
    }

    private Long mapEntry(ImmutableMetaToken token) {
        Long entry = tokMap.get(token.getId());
        if (entry != null) {
            return entry;
        }
        entry = findNextBestEntry(token);
        if (entry != null) {
            tokMap.put(token.getId(), entry);
            return entry;
        } else {
            return null;
        }
    }

    private Long findNextBestEntry(ImmutableMetaToken metatoken) {
        Option<Set<ImmutableToken>> tokens = cacheActor.getImmutableModel().getTokenMetaToken().getAssignedX(
            metatoken);
        for (ImmutableToken token : tokens.getOrElse(empty())) {
            Option<Set<ImmutableMovieEntry>> entries = cacheActor.getImmutableModel().getEntryToken().getAssignedX(
                token);
            if (entries.isDefined() && entries.get().size() > 0) {
                return entries.get().head().getId();
            }
        }

        return null;
    }

}
