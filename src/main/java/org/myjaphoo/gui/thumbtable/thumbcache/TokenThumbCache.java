/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.thumbtable.thumbcache;

import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.myjaphoo.gui.ThumbTypeDisplayMode;
import org.myjaphoo.model.cache.EntityCacheActor;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.ImmutableToken;

import javax.swing.*;
import java.util.HashMap;


/**
 * Spezieller Cache für Token thumbs. Dieser delegiert letztendlich nur
 * and den ThreadedthumbCache.
 *
 * @author mla
 */
public class TokenThumbCache {

    /**
     * mapped a token id to a movie id.
     */
    private HashMap<Long, Long> tokMap = new HashMap<>();

    private HashMap<String, Long> tokByNameMap = new HashMap<>();

    private EntityCacheActor cacheActor;
    private ThreadedThumbCache thumbCache;

    public TokenThumbCache(EntityCacheActor cacheActor, ThreadedThumbCache thumbCache) {
        this.cacheActor = cacheActor;
        this.thumbCache = thumbCache;
    }

    public ImageIcon loadImageForToken(
        ImmutableToken token, int actualRowHeight, ThumbIsLoadedCallback loadedCallBack
    ) {
        Long entryId = mapEntry(token);
        if (entryId == null) {
            return null;
        }
        return thumbCache.getThumb(
            entryId,
            0,
            true,
            actualRowHeight,
            ThumbTypeDisplayMode.NORMAL,
            loadedCallBack
        );
    }

    public ImageIcon loadImageForToken(String tokenName, int actualRowHeight, ThumbIsLoadedCallback loadedCallBack) {
        Long entryId = mapEntry(tokenName);
        if (entryId == null) {
            return null;
        }
        return thumbCache.getThumb(
            entryId,
            0,
            true,
            actualRowHeight,
            ThumbTypeDisplayMode.NORMAL,
            loadedCallBack
        );
    }

    private Long mapEntry(ImmutableToken token) {
        Long entryId = tokMap.get(token.getId());
        if (entryId != null) {
            return entryId;
        }
        Option<Set<ImmutableMovieEntry>> entries = cacheActor.getImmutableModel().getEntryToken().getAssignedX(
            token);
        if (entries.isDefined() && entries.get().size() > 0) {
            entryId = entries.get().head().getId();
            tokMap.put(token.getId(), entryId);
            tokByNameMap.put(token.getName(), entryId);
            return entryId;
        } else {
            return null;
        }
    }

    private Long mapEntry(String tokenName) {
        Long entry = tokByNameMap.get(tokenName);
        if (entry != null) {
            return entry;
        }
        Option<ImmutableToken> token = cacheActor.getImmutableModel().getTokenByName(tokenName);
        if (token.isDefined()) {
            return mapEntry(token.get());
        }
        return null;
    }
}
