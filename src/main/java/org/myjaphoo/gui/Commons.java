/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.gui;

import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.rollover.RolloverProducer;
import org.myjaphoo.model.dbcompare.DatabaseComparison;

import javax.swing.border.Border;
import java.awt.*;

/**
 */
public class Commons {
    public static ColorHighlighter ROLLOVER_ROW_HIGHLIGHTER =
        new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, Color.ORANGE, Color.BLACK);

    private static HighlightPredicate ROLLOVER_CELL = new HighlightPredicate() {

        @Override
        public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
            if (!adapter.getComponent().isEnabled()) return false;
            Point p = (Point) adapter.getComponent().getClientProperty(
                RolloverProducer.ROLLOVER_KEY);
            return p != null && p.y == adapter.row && p.x == adapter.column;
        }
    };

    public static ColorHighlighter ROLLOVER_CELL_HIGHLIGHTER =
        new ColorHighlighter(ROLLOVER_CELL, Color.ORANGE, Color.WHITE);


    private static final HighlightPredicate createDBCompareModePredicate(DatabaseComparison databaseComparison) {
        return new HighlightPredicate() {
            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                return databaseComparison.isActive();
            }
        };
    }

    /**
     * a highlighter to show that we do a comparison.
     */
    public static final Highlighter createDBCompareHighlighter(DatabaseComparison databaseComparison) {
        return new ColorHighlighter(
            createDBCompareModePredicate(databaseComparison),
            new Color(184, 245, 239),
            Color.black
        );
    }

    public static Border createBorder() {
        return new org.jdesktop.swingx.border.DropShadowBorder();
    }
}
