/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.gui.scripting;

import groovy.lang.Binding;
import groovy.lang.GroovyRuntimeException;
import groovy.lang.GroovyShell;
import groovy.ui.Console;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.MyjaphooController;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.icons.Icons;
import org.myjaphoo.gui.util.MenuPathStructurizer;
import org.myjaphoo.model.db.SavedGroovyScript;
import org.myjaphoo.model.db.ScriptType;
import org.myjaphoo.model.util.UserDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author mla
 */
public class Scripting {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/scripting/resources/Scripting");

    private static final Logger logger = LoggerFactory.getLogger(Scripting.class);
    private MyjaphooController controller;

    public Scripting(MyjaphooController controller) {
        this.controller = controller;
    }

    private static MJConsole buildMJConsole(MyjaphooController controller) {
        // load our special console:
        MJConsole console = new MJConsole();
        console.setMyjaphooController(controller);

        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
        compilerConfiguration.setScriptBaseClass(MJScriptBaseClass.class.getName());
        console.setVariable("controller", controller); //NOI18N
        console.setConfig(compilerConfiguration);
        return console;
    }

    public void createOrUpdateScriptingMenu(JMenu jMenuScripting) {
        jMenuScripting.removeAll();
        jMenuScripting.add(new AbstractAction("Open Groovy Console", null) {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Console console = buildMJConsole(controller);
                    console.run();
                } catch (Exception ex) {
                    logger.error("error during script execution!", ex); //NOI18N
                }
            }
        });

        // add the edit menus for the existing scripts:
        jMenuScripting.addSeparator();
        addEditActions(jMenuScripting);
    }

    public static GroovyShell createMjShell(/*MyjaphooController controller*/) {
        CompilerConfiguration config = new CompilerConfiguration();
        config.setScriptBaseClass(MJScriptBaseClass.class.getName());
        LoggingOutputStream out = new LoggingOutputStream();
        PrintWriter output = new PrintWriter(out);
        config.setOutput(output);

        Binding binding = new Binding();

        GroovyShell shell = new GroovyShell(binding, config);

        return shell;
    }


    private void addEditActions(JMenu menuEditing) {
        // scale groovy console pic to 16x16:
        Image scaledImage = Icons.IR_GROOVY.icon.getImage().getScaledInstance(16, 16, Image.SCALE_DEFAULT);
        ImageIcon scaledImageIcon = new ImageIcon(scaledImage);

        // display also the ones saved in the database:
        List<SavedGroovyScript> scripts = controller.getMainController().getScriptList();
        MenuPathStructurizer structurizer = new MenuPathStructurizer(false);

        structurizer.setFolderIcon(scaledImageIcon);
        for (final SavedGroovyScript script : scripts) {
            String title = script.getName();
            structurizer.add(new AbstractAction(title, scaledImageIcon) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    // start console and open the script:
                    editScript(controller, script);
                }
            }, script.getName(), script.getMenuPath());
        }
        structurizer.structurize(menuEditing, 10);

    }


    public static void editScript(MyjaphooController controller, SavedGroovyScript node) {
        MJConsole console = buildMJConsole(controller);
        console.run();
        console.loadDbScript(node);
    }

    public static void executeUserDefinedInitScripts(MainApplicationController mainController) {
        List<SavedGroovyScript> scripts = mainController.getScriptList();
        for (final SavedGroovyScript script : scripts) {
            if (script.getScriptType() == ScriptType.INITSCRIPT) {
                executeScript(script);
            }
        }
    }

    public static void executeScript(SavedGroovyScript script) {
        try {
            createMjShell().evaluate(script.getScriptText());
        } catch (Exception e) {
            logger.error("error executing script " + script.getName() + "!", e);
        }
    }

    /**
     * executes scripts defined as resources in the user dir.
     * It expects a plugins directory. In that directory each subdir is a separate "plugin".
     * If such a subdir contains a Start class, it gets then automatically executed. This is
     * then the entry point to startup a plugin.
     * A plugins directory has the following format:
     * plugins
     */
    public static void startScriptPlugins(MyjaphooCorePrefs prefs) {
        if (prefs.PRF_LOADPLUGINS.getVal()) {
            logger.info("scan now for script plugins...");

            String userPluginDir = UserDirectory.getDirectory() + "plugins";
            File fUserPluginDir = new File(userPluginDir);
            startScriptPlugins(fUserPluginDir);
        }
    }

    public static void startScriptPlugins(File pluginDirectory) {
        if (pluginDirectory.exists()) {
            logger.info("checking plugin dir " + pluginDirectory.getAbsolutePath());
            try {
                ArrayList<URL> urls = new ArrayList<>();
                urls.add(pluginDirectory.toURI().toURL());
                GroovyScriptEngine gse = new GroovyScriptEngine(urls.toArray(new URL[urls.size()]));

                // now scan each sub dir (which needs to be named by the plugin and check for "Start" scripts:
                for (File file : pluginDirectory.listFiles()) {
                    if (file.isDirectory()) {
                        startScriptPlugin(gse, file);
                    }
                }
            } catch (MalformedURLException e) {
                logger.error("error initializing scripting engine!", e);
            }
        }
    }

    private static void startScriptPlugin(GroovyScriptEngine gse, File pluginDir) {
        String pluginName = pluginDir.getName();
        logger.info("Found Plugin " + pluginName);
        File startScript = searchStartScript(pluginDir);
        if (startScript != null) {
            logger.info("Plugin " + pluginName + ": starting start script...");
            String scriptName = pluginName + "/Start.groovy";
            Binding binding = new Binding();
            try {
                gse.run(scriptName, binding);
                logger.info("Plugin " + pluginName + ": start script successfully finished!");
            } catch (ResourceException | ScriptException | GroovyRuntimeException e) {
                logger.error("error initialising plugin " + pluginName + "!", e);
            }
        }
    }

    private static File searchStartScript(File pluginDir) {
        for (File file : pluginDir.listFiles()) {
            if ("Start.groovy".equals(file.getName())) {
                return file;
            }
        }
        return null;
    }
}
