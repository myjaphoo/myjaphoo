/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.action.AddNewTokenActionInTokenTree;
import org.myjaphoo.gui.action.FilterToFindUnassigned;
import org.myjaphoo.gui.action.RemoveMetatokenRelationFromToken;
import org.myjaphoo.gui.action.RemoveToken;
import org.myjaphoo.gui.action.scriptactions.ScriptActions;
import org.myjaphoo.gui.action.scriptactions.TagContextAction;
import org.myjaphoo.gui.movieprops.UpdatePropertyPanelInfoEvent;
import org.myjaphoo.gui.thumbtable.thumbcache.TokenThumbCache;
import org.myjaphoo.gui.token.AbstractTokenPanelController;
import org.myjaphoo.gui.token.TokenTransferHandler;
import org.myjaphoo.gui.token.TokenTree;
import org.myjaphoo.gui.token.TokenTreeModel;
import org.myjaphoo.gui.util.TokenMenuCreation;
import org.myjaphoo.model.cache.zipper.TokenRef;

import javax.swing.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author mla
 */
public class MainTokenPanelController extends AbstractTokenPanelController {

    private MyjaphooController controller;

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MainTokenPanelController");

    public MainTokenPanelController(MyjaphooController controller) {
        this.controller = controller;
    }

    @Override
    public void setCurrentToken(TokenRef token) {
        controller.getFilter().setCurrentToken(token);
    }

    @Override
    public TransferHandler createTransferHandler(TokenTree tokenTree) {
        return new TokenTransferHandler(tokenTree, controller);
    }

    @Override
    public JPopupMenu createTokenTreePopupMenu() {
        JPopupMenu m = new JPopupMenu();
        TokenRef token = controller.getFilter().getCurrentToken();
        List<TokenRef> tags = Collections.emptyList();

        if (token != null) {
            JMenu openViewMenu = new JMenu(localeBundle.getString("OPEN VIEW"));
            m.add(openViewMenu);
            TokenMenuCreation.addMenusToFilterToTokens(controller, openViewMenu, Arrays.asList(token));

            // menus to assign to meta tags:
            TokenMenuCreation.addMetaTokenAssignments(controller, m, Arrays.asList(token));
            m.addSeparator();

            tags = Arrays.asList(token);
        }

        m.add(new FilterToFindUnassigned(controller));
        m.addSeparator();
        m.add(new AddNewTokenActionInTokenTree(controller));
        m.addSeparator();
        m.add(new RemoveToken(controller));
        m.addSeparator();
        m.add(new RemoveMetatokenRelationFromToken(controller));

        // add user defined actions from scripts:
        ScriptActions.addActionsToPopuMenu(controller, m, TagContextAction.class, tags);

        return m;
    }

    @Override
    public AbstractTreeTableModel createTokenTreeModel() {
        final TokenTreeModel tokenTreeModel = new TokenTreeModel(
            controller.getMainController(),
            controller.getProject().cacheActor.getImmutableModel(),
            isFlatView()
        );
        return tokenTreeModel;
    }

    @Override
    public void doubleClicked() {
        // do not react on double click
    }

    @Override
    public void onTokenSelected(TokenRef token) {
        controller.getEventBus().post(new UpdatePropertyPanelInfoEvent(token));
    }

    @Override
    public TokenThumbCache getTokenThumbCache() {
        return controller.getMainController().tokenThumbCache;
    }

    @Override
    public MainApplicationController getMainController() {
        return controller.getMainController();
    }
}
