/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryRef;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author lang
 */
public class MovieNode extends AbstractLeafNode {

    private EntryRef entryRef;
    /**
     * duplicates, lazy initialized.
     */
    private ArrayList<EntryRef> condensedDuplicates = null;

    /**
     * true, wenn dieser movie unter der aktuellen gruppierung nur einmal im tree hängt.
     */
    private boolean unique;

    public MovieNode(EntryRef entryRef, boolean unique) {
        this.entryRef = entryRef;
        this.unique = unique;
    }

    public void addCondensedDuplicate(EntryRef duplicate) {
        getCondensedDuplicates().add(duplicate);
    }

    public int getCondensedDuplicatesSize() {
        if (condensedDuplicates == null) {
            return 0;
        } else {
            return condensedDuplicates.size();
        }
    }

    /**
     * @return the movieEntry
     */
    public ImmutableMovieEntry getMovieEntry() {
        return entryRef.ref;
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * @return the duplicates
     */
    public ArrayList<EntryRef> getCondensedDuplicates() {
        if (condensedDuplicates == null) {
            condensedDuplicates = new ArrayList<>(1);
        }
        return condensedDuplicates;
    }

    @Override
    public String getName() {
        return entryRef.ref.getName();
    }

    @Override
    public long getSizeOfContainingMovies() {
        return entryRef.ref.getFileLength();
    }

    @Override
    public String getCanonicalDir() {
        return entryRef.ref.getCanonicalDir();
    }

    @Override
    public long getFileLength() {
        return entryRef.ref.getFileLength();
    }

    @Override
    public String getCanonicalPath() {
        return entryRef.ref.getCanonicalPath();
    }

    /**
     * @return the unique
     */
    public boolean isUnique() {
        return unique;
    }

    /**
     * @return the hasDups
     */
    public boolean isHasDups() {
        return entryRef.getModel().getDupHashMap().hasDuplicates(entryRef.ref.getChecksumCRC32());
    }

    /**
     * @return the dupsInDatabase
     */
    public Collection<ImmutableMovieEntry> getDupsInDatabase() {
        return entryRef.getModel().getDupHashMap().getDuplicatesForMovie(entryRef.ref);
    }

    @Override
    public String getTitle() {
        return entryRef.ref.getTitle();
    }

    @Override
    public String getComment() {
        return entryRef.ref.getComment();
    }

    /**
     * updated den entry für diese node. Wird nach Modell-Änderungen
     * gemacht, um nicht das ganze GUI modell auszutauschen.
     */
    public void updateNode(EntryRef entryRef) {
        this.entryRef = entryRef;
    }

    public EntryRef getEntryRef() {
        return entryRef;
    }
}
