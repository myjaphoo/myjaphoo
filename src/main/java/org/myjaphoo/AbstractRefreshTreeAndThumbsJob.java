package org.myjaphoo;

import org.mlsoft.common.acitivity.Channel;
import org.mlsoft.common.acitivity.ChannelManager;
import org.myjaphoo.gui.errors.ErrorUpdateEvent;
import org.myjaphoo.model.ThumbMode;
import org.myjaphoo.model.filterparser.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RefreshViews
 */
public abstract class AbstractRefreshTreeAndThumbsJob extends MyjaphooBackgroundJob<MyjaphooView.Results, Object> {

    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractRefreshTreeAndThumbsJob.class);

    public AbstractRefreshTreeAndThumbsJob(MyjaphooView view, MyjaphooController controller) {
        super(view, controller);
    }

    @Override
    protected MyjaphooView.Results doInBackground() throws Exception {
        Channel channel = ChannelManager.createChannel(this.getClass(), "reloading tree and thumbs...");
        try {
            this.setTitle("reloading tree ...");
            channel.startActivity();
            channel.message("reloading tree ...");
            CreatedTreeModelResult createdTreeModelResult = controller.getFilter().createMovieTreeModel();
            ThumbMode mode = controller.getMainThumbController().getThumbMode();
            boolean distinctThumbs = controller.getMainThumbController().isPreventGroupingDups();
            channel.message("reloading thumbs ...");
            ThumbDisplayFilterResult thumbModelResult = controller.getFilter().getThumbsModel(
                mode.getMode(),
                distinctThumbs
            );
            this.firePropertyChange("done", this, this);
            this.done();
            return new MyjaphooView.Results(createdTreeModelResult, thumbModelResult);
        } catch (ParserException pe) {
            channel.errormessage("error happened during refreshing view!", pe);
            // do log parser errors with info level, as they usually happen because of wrong user input
            LOGGER.info("parser exception on input!", pe);
            throw pe;
        } catch (Exception e) {
            channel.errormessage("error happened during refreshing view!", e);
            LOGGER.error("error happened during refreshing view!", e);
            throw e;
        } finally {
            channel.stopActivity();
        }
    }

    @Override
    protected void failed(Throwable throwable) {
        super.failed(throwable);
        if (throwable instanceof ParserException) {
            controller.getEventBus().post(new ErrorUpdateEvent("Filter", (ParserException) throwable));
        }
    }

}
