/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.action.metatoken.AddNewMetaToken;
import org.myjaphoo.gui.action.metatoken.RemoveMetaToken;
import org.myjaphoo.gui.action.metatoken.RemoveTokenRelationFromMetaToken;
import org.myjaphoo.gui.action.scriptactions.MetatagContextAction;
import org.myjaphoo.gui.action.scriptactions.ScriptActions;
import org.myjaphoo.gui.metaToken.MetaTokenPanelController;
import org.myjaphoo.gui.metaToken.MetaTokenTransferHandler;
import org.myjaphoo.gui.metaToken.MetaTokenTree;
import org.myjaphoo.gui.metaToken.MetaTokenTreeModel;
import org.myjaphoo.gui.movieprops.UpdatePropertyPanelInfoEvent;
import org.myjaphoo.gui.thumbtable.thumbcache.MetaTokenThumbCache;
import org.myjaphoo.gui.util.TokenMenuCreation;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;

import javax.swing.*;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * @author mla
 */
public class MainMetaTokenPanelController implements MetaTokenPanelController {

    private MyjaphooController controller;
    private boolean flatView = false;

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MainMetaTokenPanelController");

    public MainMetaTokenPanelController(MyjaphooController controller) {
        this.controller = controller;
    }

    @Override
    public void setCurrentMetaToken(MetaTokenRef token) {
        controller.getFilter().setCurrentMetaToken(token);
    }

    @Override
    public TransferHandler createTransferHandler(MetaTokenTree tokenTree) {
        return new MetaTokenTransferHandler(tokenTree, controller);
    }

    @Override
    public JPopupMenu createTokenTreePopupMenu() {
        JPopupMenu m = new JPopupMenu();
        MetaTokenRef token = controller.getFilter().getCurrentMetaToken();
        if (token != null) {
            JMenu openViewMenu = new JMenu(localeBundle.getString("OPEN VIEW"));
            m.add(openViewMenu);
            openViewMenu.add(TokenMenuCreation.createMetaTagOV(controller, token.getRef()));
        }

        m.add(new AddNewMetaToken(controller));
        m.addSeparator();
        m.add(new RemoveTokenRelationFromMetaToken(controller));
        m.add(new RemoveMetaToken(controller));

        if (token != null) {
            ScriptActions.addActionsToPopuMenu(
                controller,
                m,
                MetatagContextAction.class,
                Arrays.asList(token.getRef())
            );
        }
        return m;
    }

    /**
     * @return the flatView
     */
    @Override
    public boolean isFlatView() {
        return flatView;
    }

    /**
     * @param flatView the flatView to set
     */
    @Override
    public void setFlatView(boolean flatView) {
        this.flatView = flatView;
    }

    @Override
    public AbstractTreeTableModel createMetaTokenTreeModel() {
        final MetaTokenTreeModel tokenTreeModel = new MetaTokenTreeModel(
            controller.getProject(),
            controller.getProject().cacheActor,
            controller.getProject().cacheActor.getImmutableModel(), isFlatView()
        );
        return tokenTreeModel;
    }

    @Override
    public void doubleClicked() {
        // do not react on double click
    }

    @Override
    public void onMetaTokenSelected(MetaTokenRef token) {
        controller.getEventBus().post(new UpdatePropertyPanelInfoEvent(token));
    }

    @Override
    public MetaTokenThumbCache getMetaTokenThumbCache() {
        return controller.getMainController().metaTokenThumbCache;
    }

    @Override
    public MainApplicationController getMainController() {
        return controller.getMainController();
    }
}
