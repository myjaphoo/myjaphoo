/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.comparators.EntryOrderType;
import org.myjaphoo.gui.comparators.InitPredefinedOrderTypes;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.gui.thumbtable.DistinctFilter;
import org.myjaphoo.gui.thumbtable.groupedthumbs.GroupedThumbView;
import org.myjaphoo.gui.thumbtable.groupedthumbs.ThumbStripe;
import org.myjaphoo.model.StructureType;
import org.myjaphoo.model.ThumbDisplayFilterResultMode;
import org.myjaphoo.model.ThumbMode;
import org.myjaphoo.model.cache.zipper.MetaTokenRef;
import org.myjaphoo.model.cache.zipper.TokenRef;
import org.myjaphoo.model.db.BaseEntity;
import org.myjaphoo.model.db.BookMark;
import org.myjaphoo.model.db.ChronicEntry;
import org.myjaphoo.model.db.DataView;
import org.myjaphoo.model.filterparser.ParserException;
import org.myjaphoo.model.filterparser.processing.FilterAndGroupingProcessor;
import org.myjaphoo.model.grouping.GroupingDim;
import org.myjaphoo.model.groupingprocessor.CommonGroupStructureGenerator;
import org.myjaphoo.model.groupingprocessor.GroupAlgorithm;
import org.myjaphoo.model.groupingprocessor.GroupingResult;
import org.myjaphoo.model.groupingprocessor.MovieTreeModelGenerator;
import org.myjaphoo.model.groupingprocessor.PartialGrouper;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.util.Objects.requireNonNull;

/**
 * Movie filter controller. Holds filter and filter methods
 *
 * @author mla
 */
public class MovieFilterController {
    private final static ResourceBundle localeBundle = ResourceBundle.getBundle(
        "org/myjaphoo/resources/MovieDataBaseFilter");

    public static final Logger LOGGER = LoggerFactory.getLogger(MovieFilterController.class.getName());
    private ChronicEntry currChronic = new ChronicEntry();
    /**
     * the currently displayed/filtered movie nodes.
     */
    private List<AbstractLeafNode> currentDisplayedMovieNodes;
    private CopyOnWriteArrayList<ChronicEntry> chronicList = new CopyOnWriteArrayList<>();

    private int chronicIndex = 0;

    /**
     * duplicates should be combined into one movie entry?
     */
    private boolean condenseDuplicates = false;

    private AbstractMovieTreeNode currentSelectedDir = null;

    private EntryOrderType orderType = InitPredefinedOrderTypes.BY_DIR_AND_NAME;
    MovieEntryJpaController movieEntryjpa;

    /**
     * the current displayed movie tree model.
     */
    private MovieStructureNode root;

    private List<String> usedLiterals = new ArrayList<>();

    private final MainApplicationController mainController;

    private final Project project;
    private int numOfMovies;
    private boolean treeShowsMoviesUnique = true;

    /**
     * the current selected token; may be null, or may be the root of the tokens, which means, that no token is selected.
     */
    private TokenRef currentToken;

    private MetaTokenRef currentMetaToken;

    public MovieFilterController(MainApplicationController mainController) {
        this.mainController = requireNonNull(mainController);
        this.project = requireNonNull(mainController.project);
        movieEntryjpa = new MovieEntryJpaController(project.connection);
    }

    public ThumbDisplayFilterResult getThumbsModel(
        ThumbDisplayFilterResultMode mode, boolean preventGroupingDups
    ) {
        if (mode == ThumbDisplayFilterResultMode.PlAINLIST) {
            return new ThumbDisplayFilterResult(getCurrentThumbs(preventGroupingDups));
        } else {
            return new ThumbDisplayFilterResult(getCurrentThumbsBreakDownByChildren(preventGroupingDups));
        }
    }

    private List<AbstractLeafNode> getCurrentThumbs(boolean preventGroupingDups) {
        if (getCurrentSelectedDir() == null) {
            return new ArrayList<AbstractLeafNode>();
        } else {
            this.currentDisplayedMovieNodes = createNodeList(getCurrentSelectedDir());
            if (preventGroupingDups) {
                DistinctFilter filter = new DistinctFilter();
                this.currentDisplayedMovieNodes = filter.filter(currentDisplayedMovieNodes);
            }
            return currentDisplayedMovieNodes;
        }
    }

    private List<AbstractLeafNode> createNodeList(AbstractMovieTreeNode fromNodeOn) {
        ArrayList<AbstractLeafNode> movieNodes = new ArrayList<AbstractLeafNode>(50000);
        addMovieNodes(movieNodes, fromNodeOn);
        Collections.sort(movieNodes, orderType.getComparator());
        return movieNodes;
    }

    private GroupedThumbView getCurrentThumbsBreakDownByChildren(boolean preventGroupingDups) {
        final AbstractMovieTreeNode currentDir = getCurrentSelectedDir();
        GroupedThumbView group = new GroupedThumbView(new ArrayList<ThumbStripe>());
        if (currentDir == null) {
            return group;
        } else {
            // hier werden alle leafs gesammelt, die direkt unter dem ausgewählten knoten liegen:
            // all leafs are collected here, which lie directly beneath the selected node:
            ArrayList<AbstractLeafNode> leafs = new ArrayList<AbstractLeafNode>(50000);

            for (Object child : currentDir.getChildren()) {
                if (child instanceof MovieStructureNode) {
                    MovieStructureNode structureNode = (MovieStructureNode) child;
                    List<AbstractLeafNode> movieNodes = createNodeList(structureNode);

                    ThumbStripe stripe = new ThumbStripe(structureNode, movieNodes);
                    group.getStripes().add(stripe);
                } else if (child instanceof AbstractLeafNode) {
                    leafs.add((AbstractLeafNode) child);
                }
            }
            if (leafs.size() > 0) {
                // pseude struktur node für streifen erstellen, der keiner weiteren untergruppe angehört:
                // structure's pseudo node for creating stripes, which belongs to no other subgroup:
                MovieStructureNode directLeafs = new MovieStructureNode("Leaf"); // Blätter //NOI18N
                ThumbStripe stripe = new ThumbStripe(directLeafs, leafs);
                group.getStripes().add(stripe);
            }

            if (preventGroupingDups) {
                DistinctFilter filter = new DistinctFilter();
                filter.filter(group);
            }

            return group;
        }
    }

    public TokenRef getCurrentToken() {
        return currentToken;
    }

    private void addMovieNodes(ArrayList<AbstractLeafNode> movieNodes, AbstractMovieTreeNode movieDirNode) {
        // falls ein movie selektiert ist (u. kein dir) dann nur diesen movie returnieren:
        // if a movie is selected (and not you) then just returnieren this movie:
        if (movieDirNode instanceof AbstractLeafNode) {
            movieNodes.add((AbstractLeafNode) movieDirNode);
            return;
        }
        final Collection<AbstractMovieTreeNode> children = movieDirNode.getChildren();
        for (AbstractMovieTreeNode node : children) {
            if (node instanceof AbstractLeafNode) {
                movieNodes.add((AbstractLeafNode) node);
            } else {
                // its a directory:
                if (isListChildMovies()) {
                    addMovieNodes(movieNodes, node);
                }
            }
        }
    }

    public CreatedTreeModelResult createMovieTreeModel() {
        pushChronik();
        boolean condensateDups = isCondenseDuplicates();

        List<? extends GroupAlgorithm> algorithms = createGroupingAlgorithm(
            currChronic);

        CreatedTreeModelResult result = group(mainController, algorithms, currChronic.getView(), condensateDups);

        root = result.root;
        usedLiterals = result.filterResult.usedLiterals;
        numOfMovies = result.filterResult.calcNumOfDistinctMovies();
        treeShowsMoviesUnique = result.treeShowsMoviesUnique;
        currentSelectedDir = result.retainedSelectedDir;

        return result;
    }

    public static CreatedTreeModelResult group(
        MainApplicationController mainController,
        List<? extends GroupAlgorithm> algorithms,
        DataView dataView,
        boolean condensateDups
    ) {

        GroupingResult gr = new CommonGroupStructureGenerator(mainController.createCommonFilterContext()).createStructuredTreeModel(
            algorithms,
            dataView,
            condensateDups
        );
        MovieTreeModelGenerator mtmg = new MovieTreeModelGenerator(mainController);
        CreatedTreeModelResult result = mtmg.create(gr);
        return result;
    }

    public MovieStructureNode getLastCreatedMovieTreeModel() {
        return root;
    }

    public MovieStructureNode createDirectoryStructuredTreeModel() {
        GroupAlgorithm algorithm = new PartialGrouper(
            project.prefs,
            null,
            null,
            StructureType.DIRECTORY.createPartialPathBuilder()
        );
        boolean condensateDups = isCondenseDuplicates();

        CreatedTreeModelResult tmr = group(mainController, Arrays.asList(algorithm), new DataView(),
            condensateDups
        );
        return tmr.root;
    }

    public List<? extends GroupAlgorithm> createGroupingAlgorithm(ChronicEntry entry) {
        String userDefinedStructure = entry.getView().getUserDefinedStruct();
        try {
            return FilterAndGroupingProcessor.createGroupingAlgorithm(mainController.project.prefs,
                () -> mainController.createSubstitutions(),
                userDefinedStructure);
        } catch (ParserException ex) {
            LOGGER.error("unable to parse and load user defined structure  " + userDefinedStructure, ex); //NOI18N
            return Arrays.asList(new PartialGrouper(
                project.prefs,
                null,
                null,
                StructureType.DIRECTORY.createPartialPathBuilder()
            ));
        }
    }

    public void setCurrentToken(TokenRef tokenNode) {
        this.currentToken = tokenNode;
    }

    /**
     * @return the filterPattern
     */
    public String getFilterPattern() {
        return currChronic.getView().getFilterExpression();
    }

    /**
     * @param filterPattern the filterPattern to set
     */
    public void setFilterPattern(String filterPattern) {
        currChronic.getView().setFilterExpression(filterPattern);
    }

    public String getPreFilterPattern() {
        return currChronic.getView().getPreFilterExpression();
    }

    public void setPreFilterPattern(String preFilterPattern) {
        currChronic.getView().setPreFilterExpression(preFilterPattern);
    }

    public void setCurrentDirIntern(AbstractMovieTreeNode movieDirNode) {
        LOGGER.debug("set current dir intern " + movieDirNode.toString()); //NOI18N
        this.currentSelectedDir = movieDirNode;
    }

    public void setCurrentDir(AbstractMovieTreeNode movieDirNode) {
        LOGGER.debug("set current dir " + movieDirNode.toString()); //NOI18N
        this.currentSelectedDir = movieDirNode;
        currChronic.getView().setCurrentSelectedDir(movieDirNode.getPathName());
        pushChronik();
    }

    public String getFilterInfoText() {
        if (!currChronic.getView().isFilter()) {
            return MessageFormat.format(localeBundle.getString("UNFILTERED"), numOfMovies);
        } else {
            return MessageFormat.format(localeBundle.getString("FILTERED"), numOfMovies);
        }
    }

    /**
     * @return the currentSelectedDir
     */
    public AbstractMovieTreeNode getCurrentSelectedDir() {
        return currentSelectedDir;
    }

    /**
     * @return the listChildMovies
     */
    public boolean isListChildMovies() {
        return currChronic.getView().isListChildMovies();
    }

    public void setListChildMovies(boolean enabled) {
        currChronic.getView().setListChildMovies(enabled);
    }

    public void setPruneTree(boolean selected) {
        currChronic.getView().setPruneTree(selected);
    }

    /**
     * @return the pruneTree
     */
    public boolean isPruneTree() {
        return currChronic.getView().isPruneTree();
    }

    /**
     * @return the currentDisplayedMovieNodes
     */
    public List<AbstractLeafNode> getCurrentDisplayedMovieNodes() {
        return currentDisplayedMovieNodes;
    }

    private void pushChronik() {
        try {
            final ChronicEntry copy = (ChronicEntry) currChronic.clone();
            if (!containsSameContents(chronicList, copy)) {
                copy.setId(null);
                copy.setCreatedMs(BaseEntity.createOrderedUniqueTimeMillis());
                movieEntryjpa.create(copy);

                getChronicList().add(0, copy);
                if (getChronicList().size() > project.prefs.PRF_MAXCHRONIC.getVal()) {
                    getChronicList().remove(getChronicList().size() - 1);
                }
                // add it to the cached chronic list: since its a observable list, it will update the ui:
                mainController.getChronicList().add(copy);
                chronicIndex = 0;
            }
        } catch (Exception ex) {
            LOGGER.error("error creating new chronic entry", ex); //NOI18N
        }
    }

    /**
     * @return the condenseDuplicates
     */
    public boolean isCondenseDuplicates() {
        return condenseDuplicates;
    }

    /**
     * @param condenseDuplicates the condenseDuplicates to set
     */
    public void setCondenseDuplicates(boolean condenseDuplicates) {
        this.condenseDuplicates = condenseDuplicates;
    }

    /**
     * @param structType the structType to set
     */
    public void setStructType(StructureType structType) {
        setUserDefinedStructure(Arrays.asList(structType.getDims()));
    }

    public void setOrder(EntryOrderType orderType) {
        this.orderType = orderType;
    }

    public EntryOrderType getOrder() {
        return orderType;
    }

    public void setUserDefinedStructure(List<GroupingDim> hierarchy) {
        currChronic.getView().setUserDefinedStructure(hierarchy);
    }

    public void setUserDefinedStructureActivated(boolean selected) {
        currChronic.getView().setUserDefinedStructureActivated(selected);
    }

    /**
     * @return the userDefinedStructure
     */
    public String getUserDefinedStruct() {
        return currChronic.getView().getUserDefinedStruct();
    }

    /**
     * @return the treeShowsMoviesUnique
     */
    public boolean isTreeShowsMoviesUnique() {
        return treeShowsMoviesUnique;
    }

    /**
     * @return the chronicList
     */
    public List<ChronicEntry> getChronicList() {
        if (chronicList.size() == 0) {
            loadChronicList();
        }
        return chronicList;
    }


    public void popChronic(ChronicEntry c) {
        try {
            currChronic = (ChronicEntry) c.clone();
        } catch (CloneNotSupportedException ex) {
            LOGGER.error("error cloning", ex); //NOI18N
        }
    }

    private void loadChronicList() {
        final List<ChronicEntry> loaded = movieEntryjpa.findChronicEntryEntities(
            project.prefs.PRF_MAXCHRONIC.getVal(),
            0
        );
        for (ChronicEntry loadedEntry : loaded) {
            if (!containsSameContents(chronicList, loadedEntry)) {
                chronicList.add(loadedEntry);
            }
        }
        chronicIndex = 0;
    }

    private boolean containsSameContents(List<ChronicEntry> chronicList, ChronicEntry copy) {
        for (ChronicEntry entry : chronicList) {
            if (entry.isContentequals(copy)) {
                return true;
            }
        }
        return false;
    }

    public BookMark createBookMarkFromCurrentChronic() {
        BookMark bm = new BookMark();
        bm.setView((DataView) currChronic.getView().clone());
        return bm;
    }

    void setCurrentMetaToken(MetaTokenRef metaToken) {
        this.currentMetaToken = metaToken;
    }

    public MetaTokenRef getCurrentMetaToken() {
        return currentMetaToken;
    }

    public void setGroupByPattern(String text) {
        currChronic.getView().setUserDefinedStruct(text);
        currChronic.getView().setUserDefinedStructureActivated(true);
    }

    public ThumbMode getThumbMode() {
        return currChronic.getView().getThumbmode();
    }

    void setThumbMode(ThumbMode mode) {
        currChronic.getView().setThumbmode(mode);
    }

    public List<String> getUsedLiterals() {
        return usedLiterals;
    }

    private boolean updateChronicIndex(int newVal) {
        boolean updateSuccessfull = true;
        chronicIndex = newVal;
        if (chronicIndex < 0) {
            chronicIndex = 0;
            updateSuccessfull = false;
        }
        if (chronicIndex >= getChronicList().size()) {
            chronicIndex = getChronicList().size() - 1;
            updateSuccessfull = false;
        }
        return updateSuccessfull;
    }

    public ChronicEntry getBackHistory() {
        if (!updateChronicIndex(chronicIndex + 1)) {
            return null;
        }
        ChronicEntry chronicEntry = getChronicList().get(chronicIndex);
        return chronicEntry;
    }

    public ChronicEntry getForwardHistory() {
        if (!updateChronicIndex(chronicIndex - 1)) {
            return null;
        }
        ChronicEntry chronicEntry = getChronicList().get(chronicIndex);
        return chronicEntry;
    }

}
