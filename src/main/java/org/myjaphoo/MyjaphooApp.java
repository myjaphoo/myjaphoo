/*
 * MyjaphooApp.java
 */
package org.myjaphoo;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.jdesktop.application.Application;
import org.jdesktop.application.MultiFrameApplication;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXTipOfTheDay;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.tips.TipLoader;
import org.jdesktop.swingx.tips.TipOfTheDayModel;
import org.mlsoft.common.ExceptionHandler;
import org.mlsoft.eventbus.GlobalBus;
import org.myjaphoo.cli.Clt;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.comparators.InitPredefinedOrderTypes;
import org.myjaphoo.gui.protocolhanders.tagpic.Handler;
import org.myjaphoo.gui.scripting.Scripting;
import org.myjaphoo.model.filterparser.FilterParser;
import org.myjaphoo.model.grouping.GroupingDim;
import org.myjaphoo.model.logic.MovieEntryJpaController;
import org.myjaphoo.model.logic.dbhandling.WmDatabaseOpener;
import org.myjaphoo.model.registry.ComponentRegistry;
import org.myjaphoo.model.util.UserDirectory;
import org.myjaphoo.plugin.Components;
import org.myjaphoo.plugin.Plugins;
import org.myjaphoo.project.Project;
import org.myjaphoo.project.ProjectConfiguration;
import org.myjaphoo.util.LocalFileSystemEntryResourceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.Enumeration;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * The main class of the application.
 */
public class MyjaphooApp extends MultiFrameApplication {

    /**
     * logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(MyjaphooApp.class.getName());
    MyjaphooTrayIcon trayIcon = new MyjaphooTrayIcon();

    private Project mainProject;

    private MainApplicationController mainController;

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        // set handler to log out uncaugth exceptions of the application:
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        System.setProperty(
            "sun.awt.exception.handler",
            ExceptionHandler.class.getName()
        );

        Handler.install();

        ProjectConfiguration appConfig = new ProjectConfiguration(
            UserDirectory.getDirectory(),
            UserDirectory.getDirectory()
        );
        MyjaphooAppPrefs appPrefs = new MyjaphooAppPrefs(appConfig);

        LookAndFeels.setPlaf(appPrefs);

        mainProject = openMainProject(appPrefs);
        mainController = new MainApplicationController(mainProject);

        initRSTAThings();

        InitPredefinedOrderTypes.init();
        Components.addEntryResourceProvider(ComponentRegistry.DEFAULT, new LocalFileSystemEntryResourceProvider());

        Plugins.initAllPlugins();

        Scripting.startScriptPlugins(appPrefs);

        trayIcon.initTrayIcon();

        //doLogin();
        long start = System.currentTimeMillis();

        MyjaphooView firstView = new MyjaphooView(mainController, this);
        show(firstView);
        long stop = System.currentTimeMillis();

        logger.info("Start time = " + (stop - start)); //NOI18N

        Scripting.executeUserDefinedInitScripts(mainController);

        if (appPrefs.PRF_SHOWTIPOFDAY.getVal()) {
            try {
                Properties tips = new Properties();
                ResourceBundle tipsBundle = ResourceBundle.getBundle("org/myjaphoo/resources/tips"); //NOI18N
                Enumeration<String> keys = tipsBundle.getKeys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    tips.setProperty(key, tipsBundle.getString(key));
                }
                TipOfTheDayModel model = TipLoader.load(tips);
                JXTipOfTheDay totd = new JXTipOfTheDay(model);

                totd.showDialog(firstView.getFrame());
            } catch (MissingResourceException ex) {
                logger.error("error", ex); //NOI18N
            }
        }
        GlobalBus.bus.register(this);
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     *
     * @return the instance of MyjaphooApp
     */
    public static MyjaphooApp getApplication() {
        return Application.getInstance(MyjaphooApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            // delegate to the clt client:
            Clt.main(args);
        } else {
            launch(MyjaphooApp.class, args);
        }
    }

    private Project openMainProject(MyjaphooAppPrefs prefs) {
        try {
            return new Project(UserDirectory.getDirectory());
        } catch (Throwable e) {
            logger.error("unable to open database!", e); //NOI18N
            ErrorInfo info = new ErrorInfo(
                "Wm",
                "unable to open database '" + WmDatabaseOpener.configuredDatabaseFileName(prefs) + "'!",
                //NOI18N
                null,
                null,
                e,
                Level.SEVERE,
                null
            );
            JXErrorPane.showDialog(null, info);
            return new Project(UserDirectory.getDirectory());
        }

    }

    /**
     * access the database to check if its available.
     */
    private static void touchDatabase(MyjaphooCorePrefs prefs) {
        // check it a second time: the opener will now try to open the default
        // database:
        MovieEntryJpaController jpa = new MovieEntryJpaController(WmDatabaseOpener.openDatabase(prefs));
        jpa.findBookMarkEntities();
    }

    private void initRSTAThings() {

        // ensure, that all syntax relevant objects are initialized before
        // register the syntax highlighting token maker; otherwise some weird
        // things could happen
        FilterParser.initialize();

        // define own syntax highlighting token maker and assign it to the text area:
        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("wmstyle", "org.myjaphoo.gui.editor.rsta.syntax.WmTokenMaker");
        TokenMakerFactory.setDefaultInstance(atmf);
    }

    /**
     * Starts a new view which is already filtered by a filter expression
     *
     * @param filterExpression
     */
    public void startNewView(final MainApplicationController mainController, final String filterExpression) {
        EventQueue.invokeLater(() -> MyjaphooApp.getApplication().show(new MyjaphooView(
            mainController,
            MyjaphooApp.getApplication(),
            filterExpression
        )));
    }

    /**
     * Starts a new view which is already filtered by a filter expression and grouped by a grouping list.
     *
     * @param filterExpression
     */
    public void startNewView(
        final MainApplicationController mainController, final String filterExpression, final List<GroupingDim> hierarchy
    ) {
        EventQueue.invokeLater(() -> MyjaphooApp.getApplication().show(new MyjaphooView(
            mainController,
            MyjaphooApp.getApplication(),
            filterExpression,
            hierarchy
        )));
    }

}
