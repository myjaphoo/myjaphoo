/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo;

import org.mlsoft.eventbus.GlobalBus;
import org.mlsoft.eventbus.Subscribe;
import org.mlsoft.structures.TreeStructure;
import org.mlsoft.structures.Trees;
import org.myjaphoo.gui.WmTreeTableModel;
import org.myjaphoo.gui.editor.rsta.CachedHints;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.gui.util.Utils;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.events.MoviesRemovedEvent;

import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * @author lang
 */
public class MovieTreeModel extends WmTreeTableModel {

    private final static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/resources/MovieTreeModel");
    public static final String[] COLUMNS = new String[]{
        localeBundle.getString("GROUPING CATEGORY"),
        localeBundle.getString("TITLE"),
        localeBundle.getString("NUM OF MEDIA"),
        localeBundle.getString("SIZE OF MEDIA"),
        localeBundle.getString("DIMENSION")};

    private CachedHints cachedHints;

    /**
     * Constructor.
     */
    public MovieTreeModel(CachedHints cachedHints) {
        super(null, new String[]{"Loading..."});
        this.cachedHints = cachedHints;
        GlobalBus.bus.register(this);
    }

    /**
     * Constructor.
     *
     * @param newroot Wurzel
     */
    public MovieTreeModel(CachedHints cachedHints, MovieStructureNode newroot) {
        super(newroot, prepareColumnList(cachedHints, newroot));
        this.cachedHints = cachedHints;
        GlobalBus.bus.register(this);
    }

    private static String[] prepareColumnList(CachedHints cachedHints, MovieStructureNode newroot) {
        ArrayList<String> colList = new ArrayList<>();
        colList.addAll(Arrays.asList(COLUMNS));

        colList.addAll(newroot.getAllAggregatedKeys());
        // add all the attributes:
        colList.addAll(cachedHints.getEntryAttrKeys());
        return colList.toArray(new String[colList.size()]);
    }

    @Override
    public Object getValueAt(Object node, int column) {
        switch (column) {
            case 0:
                return ((AbstractMovieTreeNode) node).getName();
            case 1:
                return ((AbstractMovieTreeNode) node).getTitle();
            case 2:
                return ((AbstractMovieTreeNode) node).getNumOfContainingMovies();
            case 3:
                return Utils.humanReadableByteCount(((AbstractMovieTreeNode) node).getSizeOfContainingMovies());
            case 4:
                return getDimInfo((AbstractMovieTreeNode) node);
            default:
                String attrName = getColumnName(column);
                if (node instanceof MovieNode) {
                    return ((MovieNode) node).getMovieEntry().getAttributes().get(attrName).getOrNull();
                } else if (node instanceof MovieStructureNode) {
                    return ((MovieStructureNode) node).getAggregatedValue(attrName);
                } else return null;
        }
    }

    private Object getDimInfo(AbstractMovieTreeNode node) {
        if (node instanceof MovieStructureNode) {
            return ((MovieStructureNode) node).getGroupingExpr();
        }
        return null;
    }


    /**
     * setzt eine neue Wurzel
     *
     * @param newRoot neue Wurzel
     */
    public void setRoot(AbstractMovieTreeNode newRoot) {
        root = newRoot;
        setColumns(prepareColumnList(cachedHints, (MovieStructureNode) newRoot));
        modelSupport.fireNewRoot();
    }


    @Subscribe(onETD = true)
    public void moviesHaveBeenRemovedEvent(MoviesRemovedEvent mre) {
        for (ImmutableMovieEntry entry : mre.getMovieEntrySet()) {
            updateRemoveEvent(entry);
        }
    }

    private void updateRemoveEvent(final ImmutableMovieEntry entry) {
        AbstractMovieTreeNode foundNode = (AbstractMovieTreeNode) Trees.searchDepthFirstSearch(
            (TreeStructure) getRoot(),
            (Trees.SearchFunction<TreeStructure>) node -> node instanceof MovieNode
                && entry.equals(((MovieNode) node).getMovieEntry())
        );
        if (foundNode != null) {
            TreePath path = new TreePath(getPathToRoot(foundNode));
            modelSupport.fireChildRemoved(path, foundNode.getParent().getChildren().indexOf(foundNode), foundNode);
        }
    }
}
