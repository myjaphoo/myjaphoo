package org.myjaphoo.plugin

import groovy.transform.TypeChecked
import org.myjaphoo.gui.action.scriptactions.*
import org.myjaphoo.gui.comparators.EntryOrderType
import org.myjaphoo.gui.util.ConfigurableThumbLoader
import org.myjaphoo.model.logic.ConfigurableFileSubstitution
import org.myjaphoo.model.registry.ComponentRegistry
import org.myjaphoo.util.EntryResourceProvider

/**
 * Simple wrapper to add components to the component registry.
 * Plugins/Scripts should use this instead to directly use the more generic registry.
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class Components {


    private static void addActionDef(String key, Class actionClass, Closure actionDef) {
        try {
            Object entry = actionClass.newInstance();

            actionDef.setDelegate(entry);
            actionDef.setResolveStrategy(Closure.DELEGATE_FIRST);
            actionDef.call();

            ComponentRegistry.registry.register(key, actionClass, entry);
        } catch (Exception e) {
            throw new RuntimeException("error defining action!", e);
        }
    }

    /**
     * user defined Action definition for a tag context action
     * @param key
     * @param actionDef
     */
    static def void defTagContextAction(String key, @DelegatesTo(TagContextAction.class) Closure actionDef) {
        addActionDef(key, TagContextAction.class, actionDef);
    }

    static def void defMetaTagContextAction(String key, @DelegatesTo(MetatagContextAction.class) Closure actionDef) {
        addActionDef(key, MetatagContextAction.class, actionDef);
    }

    static def void defMovieEntryContextAction(String key, @DelegatesTo(EntryContextAction.class) Closure actionDef) {
        addActionDef(key, EntryContextAction.class, actionDef);
    }

    static def void defBookMarkContextAction(String key, @DelegatesTo(BookmarkContextAction.class) Closure actionDef) {
        addActionDef(key, BookmarkContextAction.class, actionDef);
    }

    static def void defSavedScriptContextAction(String key,
                                                @DelegatesTo(SavedScriptContextAction.class) Closure actionDef) {
        addActionDef(key, SavedScriptContextAction.class, actionDef);
    }

    static def void defDatabaseConfigurationContextAction(String key,
                                                          @DelegatesTo(DatabaseConfigurationContextAction.class) Closure actionDef) {
        addActionDef(key, DatabaseConfigurationContextAction.class, actionDef);
    }

    static def void defMovieStructureNodeContextAction(String key,
                                                       @DelegatesTo(MovieStructureNodeContextAction.class) Closure actionDef) {
        addActionDef(key, MovieStructureNodeContextAction.class, actionDef);
    }

    static def void defCommonAction(String key, @DelegatesTo(CommonAction.class) Closure actionDef) {
        addActionDef(key, CommonAction.class, actionDef);
    }

    /**
     * adds an additional file substitution implementation.
     * @param key name for the file substitution to register
     * @param fs
     */
    static def void addFileSubstitution(String key, ConfigurableFileSubstitution fs) {
        ComponentRegistry.registry.register(key, ConfigurableFileSubstitution.class, fs);
    }

    /**
     * adds an additional thumb loader implementation.
     * @param key name for the thumb loader to register
     * @param tl
     */
    static def void addThumbLoader(String key, ConfigurableThumbLoader tl) {
        ComponentRegistry.registry.register(key, ConfigurableThumbLoader.class, tl);
    }

    /**
     * adds an additional order type
     * @param key name for the order type
     * @param entryOrderType
     */
    static def void addEntryOrderType(String key, EntryOrderType entryOrderType) {
        ComponentRegistry.registry.register(key, EntryOrderType.class, entryOrderType);
    }

    /**
     * adds an additional entry resource provider
     * @param key
     * @param provider
     */
    static def void addEntryResourceProvider(String key, EntryResourceProvider provider) {
        ComponentRegistry.registry.register(key, EntryResourceProvider.class, provider);
    }
}
