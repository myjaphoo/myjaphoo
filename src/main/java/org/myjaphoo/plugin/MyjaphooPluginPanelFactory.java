package org.myjaphoo.plugin;

import org.myjaphoo.MyjaphooController;

import javax.swing.*;

/**
 * Factory for plugins to create an additional JPanel to add to the UI.
 * If the plugin needs multiple panels, it should create multiple implementations and register multiple of of this interface.
 * @author lang
 * @version $Id$
 */
public interface MyjaphooPluginPanelFactory {

    public String getUniquePanelId();

    public String getPanelUIName();

    /**
     * Create an additional panel.
     * @param controller
     * @return
     */
    public JPanel creatPluginPanel(MyjaphooController controller);
}
