package org.myjaphoo.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Plugins
 *
 * @author lang
 * @version $Id$
 */
public class GuiPlugins {

    private static final Logger logger = LoggerFactory.getLogger(GuiPlugins.class);

    private static final Comparator<? super MyjaphooPluginPanelFactory> IDCOMPARATOR = new Comparator<MyjaphooPluginPanelFactory>() {
        @Override
        public int compare(MyjaphooPluginPanelFactory o1, MyjaphooPluginPanelFactory o2) {
            return o1.getUniquePanelId().compareTo(o2.getUniquePanelId());
        }
    };

    public static List<MyjaphooPluginPanelFactory> getOrderedPluginPanelFactories() {
        ArrayList<MyjaphooPluginPanelFactory> result = new ArrayList<>();

        ServiceLoader<MyjaphooPluginPanelFactory> loader = ServiceLoader.load(MyjaphooPluginPanelFactory.class);
        Iterator<MyjaphooPluginPanelFactory> iterator = loader.iterator();
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }
        // now order them in a canonical order, because the view should always have the same order/indices:
        Collections.sort(result, IDCOMPARATOR);
        return result;
    }

}
