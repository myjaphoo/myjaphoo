package org.myjaphoo.util;

import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.project.Project;

import java.io.InputStream;

/**
 * Accessor for data of entries. Usually they are simply coming from the local file system.
 * Addons could configure additional ones.
 *
 * @author lang
 * @version $Id$
 */
public interface EntryResourceProvider {

    /**
     * Is the entry available via this provider?
     *
     * @param project the project
     * @param entry   the entry.
     *
     * @return true, if it is available via this provider
     */
    boolean isAvailable(Project project, ImmutableMovieEntry entry);

    /**
     * Returns an input stream to load the content of this entry denoted by the path via this provider.
     *
     * @param project the project
     * @param entry   the entry.
     *
     * @return the input stream.
     */
    InputStream openStream(Project project, ImmutableMovieEntry entry) throws Exception;

}
