/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.myjaphoo.util;

import org.myjaphoo.MovieNode;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.gui.movietree.DiffNode;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.cache.zipper.EntryRef;
import org.myjaphoo.model.db.MovieEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author mla
 */
public class Filtering {


    public static ArrayList<EntryRef> entries(Collection<AbstractLeafNode> nodes) {
        return nodes2Entries(filterMovieNodes(nodes));
    }

    public static ArrayList<EntryRef> nodes2Entries(Collection<MovieNode> nodes) {
        if (nodes == null) {
            return null;
        }
        ArrayList<EntryRef> movies = new ArrayList<>();
        for (MovieNode node : nodes) {
            if (node != null) {
                movies.add(node.getEntryRef());
            }
        }
        return movies;
    }
    

    public static List<ImmutableMovieEntry> nodes2Immutables(Collection<MovieNode> nodes) {
        return nodes2Entries(nodes).stream().map(entryRef -> entryRef.getRef()).collect(toList());
    }

    public static ArrayList<MovieNode> filterMovieNodes(Collection<AbstractLeafNode> nodes) {
        ArrayList<MovieNode> allmovNodes = new ArrayList<MovieNode>();
        for (AbstractLeafNode node : nodes) {
            if (node instanceof MovieNode) {
                allmovNodes.add((MovieNode) node);
            }
        }
        return allmovNodes;
    }

    public static ArrayList<DiffNode> filterDiffNodes(Collection<AbstractLeafNode> nodes) {
        ArrayList<DiffNode> allmovNodes = new ArrayList<DiffNode>();
        for (AbstractLeafNode node : nodes) {
            if (node instanceof DiffNode) {
                allmovNodes.add((DiffNode) node);
            }
        }
        return allmovNodes;
    }
}
