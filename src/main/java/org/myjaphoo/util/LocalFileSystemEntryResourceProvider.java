package org.myjaphoo.util;

import org.myjaphoo.model.FileSubstitution;
import org.myjaphoo.model.cache.ImmutableMovieEntry;
import org.myjaphoo.model.logic.FileSubstitutionImpl;
import org.myjaphoo.project.Project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * LocalFileSystemEntryResourceProvider
 *
 * @author mla
 * @version $Id$
 */
public class LocalFileSystemEntryResourceProvider implements EntryResourceProvider {

    private FileSubstitutionImpl fileSubstitution;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable(Project project, ImmutableMovieEntry entry) {
        return getFileSubstitution(project).locateFileOnDrive(entry.getCanonicalPath()) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream openStream(Project project, ImmutableMovieEntry entry) throws FileNotFoundException {
        String localizedPath = getFileSubstitution(project).locateFileOnDrive(entry.getCanonicalPath());
        return new FileInputStream(new File(localizedPath));
    }

    private FileSubstitution getFileSubstitution(Project project) {
        if (fileSubstitution == null) {
            fileSubstitution = new FileSubstitutionImpl(project);
        }
        return fileSubstitution;
    }
}
