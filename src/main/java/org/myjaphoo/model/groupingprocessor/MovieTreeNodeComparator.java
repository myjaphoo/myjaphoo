package org.myjaphoo.model.groupingprocessor;

import com.eekboom.utils.Strings;
import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.gui.movietree.AbstractMovieTreeNode;

import java.util.Comparator;

/**
 * MovieTreeNodeComparator
 *
 * @author mla
 * @version $Id$
 */
public class MovieTreeNodeComparator implements Comparator<AbstractMovieTreeNode> {

    private static Comparator<String> NATURAL_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return Strings.compareNatural(o1, o2, true, null);
        }
    };

    private static Comparator<String> NORMAL_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    };

    private Comparator<String> comparator = NATURAL_COMPARATOR;

    public MovieTreeNodeComparator(MyjaphooCorePrefs prefs) {
        if (!prefs.PRF_USE_NATURALSORTING.getVal()) {
            comparator = NORMAL_COMPARATOR;
        }
    }

    @Override
    public int compare(AbstractMovieTreeNode o1, AbstractMovieTreeNode o2) {
        // sortierung:
        // 1. nach dir, 2. nach name:
        boolean o1haschildren = o1.getChildCount() > 0;
        boolean o2haschildren = o2.getChildCount() > 0;
        if (o1haschildren && !o2haschildren) {
            return 1;
        }
        if (!o1haschildren && o2haschildren) {
            return -1;
        }
        return comparator.compare(o1.getName(), o2.getName());
    }
}
