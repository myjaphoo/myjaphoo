package org.myjaphoo.model.groupingprocessor;

import org.myjaphoo.CreatedTreeModelResult;
import org.myjaphoo.MovieNode;
import org.myjaphoo.gui.MainApplicationController;
import org.myjaphoo.gui.movietree.DiffNode;
import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.dbcompare.DBDiffCombinationResult;

/**
 * MovieTreeModelGenerator
 */
public class MovieTreeModelGenerator {

    private boolean isDiffMode = false;

    private MovieTreeNodeComparator comparator;

    public MovieTreeModelGenerator(MainApplicationController mainApplicationController) {
        isDiffMode = mainApplicationController.getDatabaseComparison().isActive();
        comparator = new MovieTreeNodeComparator(mainApplicationController.project.prefs);
    }

    static class Context {
        public final GroupingResult groupingResult;

        public MovieStructureNode retainedDir;

        public Context(GroupingResult groupingResult) {
            this.groupingResult = groupingResult;
        }
    }

    public CreatedTreeModelResult create(GroupingResult groupingResult) {
        Context context = new Context(groupingResult);
        MovieStructureNode root = create(groupingResult.root, context);
        return new CreatedTreeModelResult(
            root,
            groupingResult.condenseDuplicates,
            groupingResult.filterResult,
            context.retainedDir,
            groupingResult.treeShowsMoviesUnique
        );
    }

    public MovieStructureNode create(Branch branch, Context context) {
        MovieStructureNode msn = createStructureNode(branch);
        // transfer retained dir, if given:
        if (context.groupingResult.retainedSelectedDir == branch) {
            context.retainedDir = msn;
        }
        for (Branch childBranch : branch.getChildBranches()) {
            msn.addChild(create(childBranch, context));
        }
        for (Leaf leaf : branch.getLeafs()) {
            msn.addChild(createNode(leaf));
        }

        msn.sortList(comparator);
        // destruct the branches to let gc free mem in all cases:
        // in general this is not really necessary, since all node refs are not used anymore and should released after
        // construction of the result tree,
        // but to prevent any side effects (anyone holding a reference accidently, and then preventing to free the whole tree)
        // we clear all refs directly:
        branch.getLeafs().clear();
        branch.getChildBranches().clear();

        return msn;
    }

    private MovieNode createNode(Leaf child) {
        MovieNode node = null;
        if (isDiffMode) {
            node = new DiffNode((DBDiffCombinationResult) child.getRow(), child.isUnique());
        } else {
            node = new MovieNode(child.getEntryRef(), child.isUnique());
        }
        if (child.getCondensedDuplicatesSize() > 0) {
            node.getCondensedDuplicates().addAll(child.getCondensedDuplicates());
        }
        return node;
    }

    private MovieStructureNode createStructureNode(Branch branch) {
        MovieStructureNode msn = new MovieStructureNode(branch.getName());
        msn.setCanonicalDir(branch.getCanonicalDir());
        msn.setGroupingExpr(branch.getGroupingExpr());
        msn.setMarker(branch.getMarker());
        if (branch.getAllAggregatedKeys().size() > 0) {
            msn.setAggregatedValues(branch.getAggregatedValues());
        }
        return msn;
    }
}
