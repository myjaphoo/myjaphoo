/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myjaphoo.model.player;

import org.myjaphoo.MyjaphooCorePrefs;
import org.myjaphoo.gui.movietree.AbstractLeafNode;
import org.myjaphoo.model.externalPrograms.ExternalPrograms;
import org.myjaphoo.project.Project;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author mla
 */
public class KMPlayerPlayer extends AbstractPlayer {

    public KMPlayerPlayer(Project project) {
        super(project);
    }

    @Override
    public void playMovies(MyjaphooCorePrefs prefs, final Collection<AbstractLeafNode> entries) {
        ArrayList<String> args = new ArrayList<String>();

        addSubstitutedMovieFileNames(args, entries);
        ExternalPrograms.KMPLAYER.startNoWait(prefs, args);
    }
}
