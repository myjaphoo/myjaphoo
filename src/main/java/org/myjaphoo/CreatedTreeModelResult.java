package org.myjaphoo;

import org.myjaphoo.gui.movietree.MovieStructureNode;
import org.myjaphoo.model.groupingprocessor.Branch;

/**
 * CreatedTreeModelResult
 */
public class CreatedTreeModelResult {
    /**
     * the root node.
     */
    public final MovieStructureNode root;

    public final boolean condenseDuplicates;
    /**
     * the filter result and immutable model, this is based on.
     */
    public final FilterResult filterResult;

    /**
     * if a dir was selected before, this attribute holds a retained dir of the new model. maybe null if it is not
     * possible
     * to retain the dir, or if no dir was selected before.
     */
    public final MovieStructureNode retainedSelectedDir;

    /**
     * abhängig von den gewählten gruppierungen (und den daten) können movies
     * mehrfach unter verschiedenen gruppierungen erscheinen. Dann ist dieses
     * flag auf false gesetzt. (Wäre z.b. der Fall, wenn zwei verschiedene Tokens
     * einem Movie zugeordnet werden, und Gruppierung nach Token gewählt ist).
     */
    public final boolean treeShowsMoviesUnique;

    public CreatedTreeModelResult(
        MovieStructureNode root, boolean condenseDuplicates, FilterResult filterResult,
        MovieStructureNode retainedSelectedDir,
        boolean treeShowsMoviesUnique
    ) {
        this.root = root;
        this.condenseDuplicates = condenseDuplicates;
        this.filterResult = filterResult;
        this.retainedSelectedDir = retainedSelectedDir;
        this.treeShowsMoviesUnique = treeShowsMoviesUnique;
    }
}
