package org.myjaphoo;

import org.myjaphoo.gui.perspective.ViewParts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * background job to refresh the movie and thumb view (and all imediatly
 * related views, e.g. filter and grouping view).
 */
public class RefreshMovieAndThumbViewsWithoutFilterBricks extends AbstractRefreshTreeAndThumbsJob {

    public static final Logger LOGGER = LoggerFactory.getLogger(RefreshMovieAndThumbViewsWithoutFilterBricks.class);

    public RefreshMovieAndThumbViewsWithoutFilterBricks(MyjaphooView view, MyjaphooController controller) {
        super(view, controller);
    }

    @Override
    protected void succeeded(MyjaphooView.Results result) {
        super.succeeded(result);
        ViewParts viewParts = view.getViewParts();
        viewParts.getMovieTreePanel().updateMovieTree(result.rootNode);
        viewParts.getThumbPanel().refillThumbView(result.thumbModelResult);
        viewParts.getFilterEditorPanel().setFilterInfoText(controller.getFilter().getFilterInfoText());
        viewParts.getInfoPanel().structureUpdated(controller);
        viewParts.getFilterEditorPanel().setFilter(controller.getFilter().getFilterPattern());
        viewParts.getGroupByPanel().setGroupByExpression(controller.getFilter().getUserDefinedStruct());

        controller.getProject().connection.getStatistics().logSummary();
    }

}