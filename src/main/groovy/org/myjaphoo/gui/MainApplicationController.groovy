package org.myjaphoo.gui

import groovy.swing.SwingBuilder
import groovy.transform.TypeChecked
import org.myjaphoo.CommonFilterContext
import org.myjaphoo.MyjaphooView
import org.myjaphoo.gui.editor.rsta.CachedHints
import org.myjaphoo.gui.thumbtable.ScaledThumbCache
import org.myjaphoo.gui.thumbtable.thumbcache.MetaTokenThumbCache
import org.myjaphoo.gui.thumbtable.thumbcache.ThreadedThumbCache
import org.myjaphoo.gui.thumbtable.thumbcache.TokenThumbCache
import org.myjaphoo.model.db.BookMark
import org.myjaphoo.model.dbcompare.DatabaseComparison
import org.myjaphoo.model.filterparser.Substitution
import org.myjaphoo.model.logic.MovieEntryJpaController
import org.myjaphoo.model.logic.ScriptJpaController
import org.myjaphoo.project.Project
import org.myjaphoo.util.MJObservableList

import java.lang.ref.WeakReference
import java.util.function.Supplier

/**
 * main controller. Contains models/data which have all views of a project in common.
 *
 * @author mla
 * @version $Id$
 *
 */
@TypeChecked
class MainApplicationController {

    /** all chronic entries. */
    MJObservableList chronicList = new MJObservableList();

    /** all bookmark entries. */
    MJObservableList bookmarkList = new MJObservableList();

    MJObservableList scriptList = new MJObservableList();

    public final Project project;

    public final ThreadedThumbCache thumbCache;

    public final TokenThumbCache tokenThumbCache;

    public final MetaTokenThumbCache metaTokenThumbCache;

    public final ScaledThumbCache scaledThumbCache;

    public final CachedHints cachedHints;

    private static HashMap<Integer, WeakReference<MainApplicationController>> openControllers = new HashMap<>();

    private int numOfOpenViews = 0;

    def DatabaseComparisonExt databaseComparison = new DatabaseComparisonExt();



    public MainApplicationController(Project project) {
        this.project = Objects.requireNonNull(project);

        this.thumbCache = new ThreadedThumbCache(project);
        this.thumbCache.start();
        metaTokenThumbCache = new MetaTokenThumbCache(project.cacheActor, this.thumbCache);
        tokenThumbCache = new TokenThumbCache(project.cacheActor, thumbCache);
        scaledThumbCache = new ScaledThumbCache(project);
        cachedHints = new CachedHints(project.cacheActor);
        reloadBookmarkList();
        reloadChronicList();
        reloadScriptList();

        addController(project.uniqueHash, this);
    }

    static def addController(Integer id, MainApplicationController mainApplicationController) {
        MainApplicationController duplicate = getController(id);
        if (duplicate != null) {
            throw new RuntimeException("created main controller for same project id!")
        }
        openControllers.put(id, new WeakReference<MainApplicationController>(mainApplicationController));
    }

    static MainApplicationController getController(Integer id) {
        def wr = openControllers.get(id);
        if (wr == null) {
            return null;
        }
        return wr.get();
    }

    static def MainApplicationController getOrOpenProjectAndMainController(String projectPath) {
        def MainApplicationController controller = openControllers.values().collect { it.get() }.find {
            it != null && it.project.projectDir == projectPath
        }
        if (controller != null) {
            return controller;
        }
        Project newProject = new Project(projectPath);
        controller = new MainApplicationController(newProject);
        return controller;
    }

    def reloadBookmarkList() {
        SwingBuilder sb = new SwingBuilder()
        sb.edt {
            MovieEntryJpaController jpa = new MovieEntryJpaController(project.connection);
            bookmarkList.clear();
            bookmarkList.addAll(jpa.findBookMarkEntities());
        }
    }

    def reloadScriptList() {
        SwingBuilder sb = new SwingBuilder()
        sb.edt {
            ScriptJpaController jpa = new ScriptJpaController(project.connection);
            scriptList.clear();
            scriptList.addAll(jpa.findScriptEntities());
        }
    }

    def reloadChronicList() {
        SwingBuilder sb = new SwingBuilder()
        sb.edt {
            MovieEntryJpaController jpa = new MovieEntryJpaController(project.connection);
            chronicList.clear();
            chronicList.addAll(jpa.findChronicEntryEntities());
        }
    }

    public Map<String, Substitution> createSubstitutions() {
        HashMap<String, Substitution> subs = new HashMap<String, Substitution>();
        for (Object bmo : bookmarkList) {
            BookMark bm = (BookMark) bmo;
            subs.put(bm.getName(), new Substitution(bm.getName(), bm.getView().getCombinedFilterExpression()));
        }
        return subs;
    }

    int getUniqueHashId() {
        return project.uniqueHash;
    }

    void registerView(MyjaphooView view) {
        numOfOpenViews++;
    }

    void unregisterView(MyjaphooView view) {
        numOfOpenViews--;
        if (numOfOpenViews == 0) {
            // the last view has been closed, close connections, dispose controller
            project.connection.close();
            openControllers.remove(project.uniqueHash);
        }
    }

    CommonFilterContext createCommonFilterContext() {
        return new CommonFilterContext(project, databaseComparison, new Supplier<Map<String, Substitution>>() {
            @Override
            Map<String, Substitution> get() {
                return createSubstitutions()
            }
        })
    }
}
