package org.myjaphoo.gui.util

import groovy.transform.Memoized
import groovy.xml.MarkupBuilder
import org.myjaphoo.MovieNode
import org.myjaphoo.MyjaphooCorePrefs
import org.myjaphoo.gui.icons.Icons
import org.myjaphoo.gui.movietree.DiffNode
import org.myjaphoo.model.FileSubstitution
import org.myjaphoo.model.cache.Immutables
import org.myjaphoo.model.cache.zipper.EntryRef
import org.myjaphoo.model.cache.zipper.EntryWrapper
import org.myjaphoo.model.cache.zipper.EntryZipper
import org.myjaphoo.model.logic.FasterFileSubstitution
import org.myjaphoo.project.Project

/**
 * TipText 
 * @author mla
 * @version $Id$
 *
 */
class TipText {
    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/util/resources/Helper");
    /**
     * subst. mit extra entity manager construieren, da diese hier in separaten
     * thread genutzt wird.
     */
    private FileSubstitution substitution;

    private MyjaphooCorePrefs prefs;
    
    public TipText(Project project) {
        substitution = new FasterFileSubstitution(project);
        prefs = project.prefs;
    }

    /**
     * creates a compact thumb tip text. The return values are cached (memoized), since
     * it could be rel. expensive to create the html string via a markup builder, and its often
     * repetetly called when the gui refreshes.
     * @param node
     * @return
     */
    @Memoized(maxCacheSize = 400)
    public String createThumbTipTextCompact(MovieNode node) {
        return createThumbTipText(node, true);
    }

    @Memoized(maxCacheSize = 200)
    public String createThumbTipText(MovieNode node) {
        return createThumbTipText(node, false);
    }

    public String createThumbTipText(MovieNode node, boolean compact) {
        def writer = new StringWriter()
        def mkp = new MarkupBuilder(writer)
        mkp.html {

            Helper.addCss(mkp)

            Helper.uniqueHTMLFragment(mkp, node);

            addTipText(mkp, node.getEntryRef(), compact);

            boolean hasDups = node.isHasDups();
            if (hasDups) {
                hr()
                br()
                Helper.pic(mkp, Icons.IR_DUPLICATES)
            }
            // duplikate anzeigen: entweder die kondensieren im kondensierungsmodus, oder aber einfach die duplikate:
            if (node.getCondensedDuplicatesSize() > 0) {
                Helper.pic(mkp, Icons.IR_CONDENSED);
                b(localeBundle.getString("CONDENSED DUPLICATES:"))
                br();
                i {
                    small {
                        for (EntryRef dupEntry : node.getCondensedDuplicates()) {
                            br()
                            addTipText(mkp, dupEntry, compact);
                        }
                    }
                }
            } else if (hasDups) {
                b(localeBundle.getString("DUPLICATES IN DATABASE:"))
                br()
                i {
                    small {
                        for (EntryZipper dupEntry : Immutables.newEntrryZipperIterable(node.getEntryRef().getModel(), node.getDupsInDatabase())) {
                            br();
                            addTipText(mkp, dupEntry, compact);
                        }
                    }
                }
            }

            if (node instanceof DiffNode) {
                DiffNode dn = (DiffNode) node;
                TextRepresentations.addComparisonTipTextInfo(mkp, dn);
                TextRepresentations.addDiffComparisonTipTextInfo(this, mkp, dn, compact);
            }
        }
        return writer.toString();
    }


    private void addTipText(MarkupBuilder mkb, EntryWrapper entry, boolean compact) {
        // todo do we need to update this here???
//        entry = CacheManager.getCacheActor().getImmutableModel().getMovieEntrySet().find(entry);
//        if (entry == null) {
//            return;
//        }
        addTipTextForMovieEntryFragment(mkb, entry, compact);
    }

    public void addTipTextForMovieEntryFragment(MarkupBuilder mkb, EntryWrapper entry, boolean compact) {
        if (entry == null) {
            return;
        }
        mkb.div() {
            b(entry.getName())
            br();
            a(href: "file=$entry.canonicalDir", entry.canonicalDir)
            br(Utils.humanReadableByteCount(entry.getFileLength()))

            if (entry.getChecksumCRC32() != null) {
                br(localeBundle.getString("CRC32: ") + Long.toHexString(entry.getChecksumCRC32()))
            }
            Helper.htmlAttributesFragment(prefs, mkb, entry);

            if (entry.getRating() != null) {
                br()
                for (int i = 0; i < entry.getRating().ordinal(); i++) {
                    Helper.pic(mkb, Icons.IR_RATING)
                }
                b(entry.getRating().getName())
            }
            Helper.internBuildTokenText(mkb, entry);

            if (prefs.PRF_SHOW_FILLOCALISATION_HINTS.getVal()) {
                String located = substitution.locateFileOnDrive(entry.getCanonicalPath());
                if (located != null) {
                    br(localeBundle.getString("LOCATED:") + substitution.substitude(located))
                } else {
                    br()
                    Helper.pic(mkb, Icons.IR_NOTLOCATED)
                    font(color: "RED", localeBundle.getString("FILE CAN NOT BE FOUND, CHECK SUBSTITUTIONS!"))
                }
            }

        }
    }
}
