package org.myjaphoo.gui.util

import groovy.xml.MarkupBuilder
import org.apache.commons.lang.StringUtils
import org.jdesktop.swingx.JXTreeTable
import org.jdesktop.swingx.action.AbstractActionExt
import org.myjaphoo.MovieNode
import org.myjaphoo.MyjaphooCorePrefs
import org.myjaphoo.gui.MainApplicationController
import org.myjaphoo.gui.icons.Icons
import org.myjaphoo.model.cache.ImmutableMetaToken
import org.myjaphoo.model.cache.ImmutableToken
import org.myjaphoo.model.cache.Immutables
import org.myjaphoo.model.cache.zipper.EntryWrapper
import org.myjaphoo.model.cache.zipper.TokenWrapper
import org.myjaphoo.model.cache.zipper.TokenZipper
import org.myjaphoo.model.db.Rating
import org.myjaphoo.model.db.Token
import org.myjaphoo.model.db.TokenType
import org.myjaphoo.model.grouping.GroupingDim
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.awt.*
import java.awt.event.ActionEvent

/**
 * Helper 
 * @author mla
 * @version $Id$
 *
 */
class Helper {
    private final
    static ResourceBundle localeBundle = ResourceBundle.getBundle("org/myjaphoo/gui/util/resources/Helper");
    private static final Logger logger = LoggerFactory.getLogger(Helper.class);
    public static final Color TIME_COLOR = Color.green.darker();
    public static final Color LOCATION_COLOR = Color.blue;
    public static final Color FILTEREXPR_COLOR = Color.blue.darker();
    private static Comparator<Token> TOKENTYPECOMPARATOR = new Comparator<Token>() {

        @Override
        public int compare(Token o1, Token o2) {
            return o1.getTokentype().compareTo(o2.getTokentype());
        }
    };


    public static pic(MarkupBuilder mkb, Icons.IconRes icon) {
        mkb.img(src: icon.url.toString())
    }

    public static  void uniqueHTMLFragment(MarkupBuilder mkb, MovieNode node) {
        if (!node.isUnique()) {
            mkb.div {
                pic(mkb, Icons.IR_NOTUNIQUE)
                font(color: "RED", localeBundle.getString("(NOT UNIQUE GROUPED!)"))
                br();
            }
        }
    }

    public static void addCss(MarkupBuilder mkb) {
        mkb.style(type: 'text/css', '''
            ul {
                list-style-type: none;
                margin-left: 10px
            }
  ''')
    }


    public static void htmlAttributesFragment(MyjaphooCorePrefs prefs, MarkupBuilder mkb, EntryWrapper entry) {
        mkb.br(/*style: "font-family:verdana;padding:5px;border-radius:5px;border:2px solid #00235A;"*/) {

            String fmt = ""; //NOI18N
            if (entry.getMovieAttrs().getFormat() != null) {
                fmt = prepareFormat(entry.getMovieAttrs().getFormat());
            }
            b(fmt + " " + entry.getMovieAttrs().getWidth() + "x" + entry.getMovieAttrs().getHeight())

            // add only movieattributes for movies, not for pictures
            if (prefs.Movies.is(entry)) {

                br();
                String fmtBitRate = Utils.humanReadableByteCount(entry.getMovieAttrs().getBitrate());
                b(entry.getMovieAttrs().getFps() + " FPS");
                br();
                b(fmtBitRate + " bitrate");
                String len = org.apache.commons.lang.time.DurationFormatUtils.formatDurationHMS(entry.getMovieAttrs().getLength() * 1000);
                br();
                b(len); //NOI18N

            }
            br()
        }

    }

    public static void internBuildTokenText(MarkupBuilder mkb, EntryWrapper nodeitem) {
        if (nodeitem.getTokenSet() != null && nodeitem.getTokenSet().size() > 0) {
            mkb.div(style: "background-color:silver;" /*font-family:verdana;padding:5px;-moz-border-radius:15px;border:2px solid #66235A;"*/) {
                internBuildTokenPlainList(mkb, nodeitem);
            }
        }
    }

    public static void internBuildTokenPlainList(MarkupBuilder mkb, EntryWrapper nodeitem) {
        if (nodeitem.tokenSet.size() > 0) {
            mkb.p {
                ul {
                    // zugeordnete tokens auflisten:
                    def tokens = nodeitem.getTokenSet().sort { a, b -> a.tokentype <=> b.tokentype };
                    for (TokenZipper token : Immutables.newTokenZipperIterable(nodeitem.getModel(), tokens)) {
                        internTokenHtmlText(mkb, token);
                    }
                }
            }
        }
    }

    private static void internTokenHtmlText(MarkupBuilder mkb, TokenWrapper token) {
        mkb.li {
            b() {
                colorToken(mkb, token);
                parentListing(mkb, token);
            }
            // now show also metatokens:
            metataginfo(mkb, token);
        }
    }

    public static void parentListing(MarkupBuilder mkb, TokenWrapper token) {
        TokenZipper parent = token.getParentZipper();
        while (parent != null && parent.getParentZipper() != null) {
            mkb.i(" <-")
            colorToken(mkb, parent);
            parent = parent.getParentZipper();
        }
    }

    public static void metataginfo(MarkupBuilder mkb, TokenWrapper token) {
        if (token.getMetaTokens().size() > 0) {
            def namesList = token.getMetaTokens().collect({ it.name });
            mkb.b {
                b("[")
                namesList.eachWithIndex { String entry, int i ->
                    mkb.a(href: "metatagref=$entry", entry)
                    if (i < namesList.size() - 1) {
                        b(";")
                    }
                }
                b("]")
            }
        }
    }

    public static void colorToken(MarkupBuilder mkb, TokenWrapper token) {
        pic(mkb, Icons.IR_TAG)
        mkb.font(color: hexColor(tokentypeColorMap.get(token.getTokentype()))) {
            mkb.a(href: "tagref=$token.name", token.getName())
        };
    }

    public static String createMjCompletionTagFragment(MainApplicationController mainController, ImmutableToken tag) {
        def writer = new StringWriter()
        def mkp = new MarkupBuilder(writer)

        mkp.div {
            b {
                img(src: "tagpic:${mainController.uniqueHashId}_$tag.name")
                colorToken(mkp, tag)
            }
            p();
            // show also the parent tokens (the indirect tokens); all but except the root:
            Helper.parentListing(mkp, tag);

            // now show also metatokens:
            if (tag.getAssignedMetaTokens().size() > 0) {
                i("assigned Meta tags");
                br();
                metataginfo(mkp, tag);
            }
            br();
        }
        return writer.toString();
    }


    public
    static String createMjCompletionMetaTagFragment(MainApplicationController mainController, ImmutableMetaToken tag) {
        def writer = new StringWriter()
        def mkp = new MarkupBuilder(writer)
        mkp.div {
            b {
                img(src: "mtagpic:${mainController.uniqueHashId}_$tag.name")
            }
            br();
            b(tag.name);
            p();
        }
        return writer.toString();
    }

    public static String createTokenText(EntryWrapper nodeitem) {
        if (nodeitem == null) {
            return null;
        }
        def writer = new StringWriter()
        def mkp = new MarkupBuilder(writer)
        mkp.html {
            internBuildTokenText(mkp, nodeitem)
        }
        return writer.toString()
    }

    private static final Map<Rating, String> ratingMap = new EnumMap<Rating, String>(Rating.class);

    static {
        ratingMap.put(Rating.NONE, ""); //NOI18N
        ratingMap.put(Rating.VERY_BAD, "*"); //NOI18N
        ratingMap.put(Rating.BAD, "**"); //NOI18N
        ratingMap.put(Rating.MIDDLE, "***"); //NOI18N
        ratingMap.put(Rating.GOOD, "****"); //NOI18N
        ratingMap.put(Rating.VERY_GOOD, "*****"); //NOI18N

    }

    public static void addRating(MarkupBuilder mkb, Rating rating) {
        mkb.font(color: "RED", ratingMap.get(rating))
    }

    private static String toHex(int val) {
        String h = Integer.toHexString(val);
        if (h.length() == 1) {
            return "0" + h; //NOI18N
        } else {
            return h;
        }
    }

    public static String wrapColored(Color color, String text) {
        return "<FONT COLOR=" + hexColor(color) + ">" + text + "</FONT>"; //NOI18N
    }

    public static String hexColor(Color color) {
        String hexcolor = "#" + toHex(color.getRed()) + toHex(color.getGreen()) + toHex(color.getBlue());
        return hexcolor;
    }

    private static final Map<TokenType, Color> tokentypeColorMap = new EnumMap(TokenType.class);

    static {
        tokentypeColorMap.put(TokenType.DARSTELLER, new Color(10, 30, 200));
        tokentypeColorMap.put(TokenType.MOVIENAME, new Color(20, 140, 150));
        tokentypeColorMap.put(TokenType.SERIE, new Color(180, 10, 200));
        tokentypeColorMap.put(TokenType.THEMA, new Color(10, 180, 50));
        tokentypeColorMap.put(TokenType.UNBESTIMMT, Color.BLACK);
    }

    public static Color getColorForTokenType(TokenType type) {
        return tokentypeColorMap.get(type);
    }

    private static final Map<GroupingDim, Color> dimColorMap = new EnumMap(GroupingDim.class);

    static {
        dimColorMap.put(GroupingDim.AutoKeyWord, new Color(10, 30, 200));
        dimColorMap.put(GroupingDim.AutoKeyWordStrong, new Color(10, 30, 180));
        dimColorMap.put(GroupingDim.AutoKeyWordVeryStrong, new Color(10, 60, 180));
        dimColorMap.put(GroupingDim.DB_Comparison, new Color(10, 180, 50));
        dimColorMap.put(GroupingDim.Directory, Color.BLACK);
        dimColorMap.put(GroupingDim.Dup_Links_ByLocating, new Color(20, 140, 150));
        dimColorMap.put(GroupingDim.Duplicates, new Color(180, 10, 200));
        dimColorMap.put(GroupingDim.File_Or_Entry_Divider, new Color(10, 180, 80));
        dimColorMap.put(GroupingDim.LocatedDir, new Color(10, 140, 80));
        dimColorMap.put(GroupingDim.Rating, new Color(10, 220, 50));
        dimColorMap.put(GroupingDim.Size, new Color(10, 220, 10));
        dimColorMap.put(GroupingDim.Token, new Color(10, 140, 20));
        dimColorMap.put(GroupingDim.TokenHierarchy, new Color(70, 180, 50));
        dimColorMap.put(GroupingDim.TokenType, new Color(30, 170, 90));
        dimColorMap.put(GroupingDim.Bookmark, new Color(60, 110, 20));
        dimColorMap.put(GroupingDim.Metatoken, new Color(220, 110, 100));
        dimColorMap.put(GroupingDim.MetatokenHierarchy, new Color(210, 140, 140));
        dimColorMap.put(GroupingDim.TokenProposal, new Color(40, 140, 140));
        dimColorMap.put(GroupingDim.VGroupYielding, new Color(160, 140, 10));
        dimColorMap.put(GroupingDim.Title, new Color(160, 90, 10));
        dimColorMap.put(GroupingDim.DuplicatesWithDirs, new Color(20, 10, 50));
        dimColorMap.put(GroupingDim.Assocation, new Color(40, 10, 50));
        dimColorMap.put(GroupingDim.ExifCreateDate, new Color(140, 10, 80));
    }

    public static Color getColorForDim(GroupingDim dim) {
        Color color = dimColorMap.get(dim);
        if (color == null) {
            if (dim != null) {
                logger.warn("no color defined for dimension " + dim); //NOI18N
            }
            return Color.black;
        }
        return color;
    }

    /**
     * FÃ¼gt die MenÃ¼s fÃ¼r die HÃ¶henauswahl fÃ¼r einen treetabel hinzu. ist identisch bei
     * dem MoiveTree als auch beim TokenTree.
     */
    public static void initHeightMenusForJXTreeTable(MyjaphooCorePrefs prefs, final JXTreeTable treetable) {
        final int smallHeight = treetable.getRowHeight();
        final int midHeight = smallHeight * 2;
        final int bigHeight = smallHeight * 3;
        final int normalThumbHeight = prefs.PRF_THUMBSIZE.getVal();

        treetable.getActionMap().put("column.height1NormalHeight", //NOI18N
                new AbstractActionExt(localeBundle.getString("SMALL ROW SIZE")) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        treetable.setRowHeight(smallHeight);
                    }
                });
        treetable.getActionMap().put("column.height2MidHeight", //NOI18N
                new AbstractActionExt(localeBundle.getString("MID ROW SIZE")) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        treetable.setRowHeight(midHeight);
                    }
                });
        treetable.getActionMap().put("column.height3MaxHeight", //NOI18N
                new AbstractActionExt(localeBundle.getString("BIG ROW SIZE")) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        treetable.setRowHeight(bigHeight);
                    }
                });
        treetable.getActionMap().put("column.height4NormalThumbHeight", //NOI18N
                new AbstractActionExt(localeBundle.getString("THUMB ROW SIZE")) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        treetable.setRowHeight(normalThumbHeight);
                    }
                });
    }

    /**
     * Some format descriptions are far too long. just convert them to a
     * shorter string form
     *
     * @param format
     * @return
     */
    public static String prepareFormat(String format) {
        format = StringUtils.replace(format, "JPEG (Joint Photographic Experts Group)", "JPEG"); //NOI18N
        return format;
    }
}
