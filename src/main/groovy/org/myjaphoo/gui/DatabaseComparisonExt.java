package org.myjaphoo.gui;

import org.myjaphoo.gui.thumbtable.thumbcache.ThreadedThumbCache;
import org.myjaphoo.model.dbcompare.DatabaseComparison;

/**
 * DatabaseComparisonExt
 *
 * @author mla
 * @version $Id$
 */
public class DatabaseComparisonExt extends DatabaseComparison {

    MainApplicationController comparisonController;

    public MainApplicationController getComparisonController() {
        return comparisonController;
    }

    public void setComparisonController(MainApplicationController comparisonController) {
        this.comparisonController = comparisonController;
        setComparisonProject(comparisonController.project);

    }

    public ThreadedThumbCache getThumbCache() {
        if (comparisonController == null) {
            return null;
        }
        return comparisonController.thumbCache;
    }
}
